//URL para la paginacion
var URL_PAGINACION = BASE_PATH + 'index.php/Adiccion/getPaginacion';

/**
* Funcion para detectar el click al boton de
* eliminar alguna toxicomania recibe el id
* envia la peticion en caso de exito regresa
* un codigo de respuesta 200 y se elimina el 
* campo de la tabla
* @param $idAdic : id Toxicomania
*/
$(function(){

    $("#table_adiccion").on('click','.delete-adic',function(){
        idAdic = $(this).attr('data-id');
        console.log("Id Profesion es "+ idAdic);
        swal({
            title: "¿Esta seguro?",
            text: "El registro se borrar definitivamente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url:BASE_PATH + 'index.php/Adiccion/deleteAdiccion',
                type:'POST',
                dataType:'json',
                data:{
                    idAdiccion : idAdic
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $("#tr-"+idAdic).remove();
                        swal("Borrado!", "El campo fue borrado de la base de datos.", "success");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });      
        });
    });
});

/**
* Funcion para detectar el click del boton
* de editar la toxicomania
* @param idAdic : identificador de la toxicomania
*/
$(function(){
     $("#table_adiccion").on('click','.edit-adic',function(){

        idAdic = $(this).attr('data-id');

        if(idAdic === null || idAdic === ""){
            swal("Error!", "El id de la profesion no puede estar vacio", "error");
        }else{
            $.ajax({
                url:BASE_PATH + 'index.php/Adiccion/getInfoAdiccion',
                type:'POST',
                dataType:'json',
                data:{
                    idAdiccion : idAdic
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('#idEditToxicomania').val(json.adiccion.ID_ADIC_PK);
                        $('#nameEditToxicomania').val(json.adiccion.NOM_ADIC);
                        $("#editToxicomania").modal('show');
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
     });
});

/**
* Funcion para guardar una nueva
* toxicomania
* @param nameAdic : nombre de la toxicomania
*/
$(function(){

    $('#btn_save_toxicomania').click(function(){

        var nameAdic = $('#nombreToxicomania').val();

        if(nameAdic === null || nameAdic === ""){
            swal("Error!", "El campo de nombre de profesion no puede estar vacio", "error");
        }else{

            var URL_SAVE_TOX = BASE_PATH +'index.php/Adiccion/saveOrUpdateAdiccion';
            var parametros = [nameAdic];
            preparaAjax(parametros,URL_SAVE_TOX,'R');
        }
    });
});

/**
* Funcion para editar la informacion
* de una toxicomania
* @param $nameAdic : Nombre de la toxicomania
* @param $idAdic : id de la toxicomania
*/
$(function(){
    $('#btn_edit_toxicomania').click(function(){
        var nameAdic = $('#nameEditToxicomania').val();
        var idAdic = $('#idEditToxicomania').val();

        if(nameAdic === null || nameAdic === ""){
            swal("Error!", "El campo de nombre de profesion no puede estar vacio", "error");
        }else if(idAdic === null || idAdic === ""){
            swal("Error!", "No hay id de profesion", "error");
        }else{

            var URL_EDIT_ADIC = BASE_PATH +'index.php/Adiccion/saveOrUpdateAdiccion';
            var parametros = [nameAdic,idAdic];
            var filtro = $("#name_filter").val();

            if(filtro !== null && filtro !== ""){
                parametros = [nameAdic,idAdic,filtro];
            }
            preparaAjax(parametros,URL_EDIT_ADIC,'E');
        }
    });
});

/**
* Funcion para limpiar el campo del nombre
* de la toxicomania.
*/
$(function(){
    $('#dont_save_toxicomania').click(function(){
        $('#nombreToxicomania').val("");
    });
});

/**
* Funcion que detecta el click al boton siguiente 
* de la paginacion valida si hay algun filtro ,
* en caso contrario se manda a pedir sin filtro 
* se manda el formulario en caso de existir, 
* la URL de destino y si es tipo siguiente 'S'
* @param $filtro : opcional
* @param $URL : url de destipo
* @param $TIPO : 'S' de siguiente
*/
$(function(){
    $(document).on('click','#btn_next_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'S');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'S');
        }
    });
});

/**
* Funcion que detecta el click del boton anterior
* de la paginacion , valida si ay algun filtro para
* para realizar la peticion en caso contrario se 
* se manda nulo, se envia tambien los parametros
* de URL y tipo 'A'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'A' de anterior
*/
$(function(){
    $(document).on('click','#btn_back_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'A');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'A');     
        }
    });
});

/**
* Funcion que detecta el click del boton de filtro
* recibe el filtro en caso de que sea nulo recarga
* la tabla desde cero, se envia URL y tipo 'R'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'R' de recargar o reload
*/
$(function(){
    $("#btn_filto").click(function(){
        var filtro = $("#name_filter").val();
        console.log(filtro);
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'R');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'R');     
        }
    });
});

/**
* Funcion para el llenado de la tabla
* de profesiones y en caso de que 
* venga nulo se llena con un mensaje 
* de que no hay campos para mostrar
* @param $respuestaJson : json que 
* contiene toda la informacion que se 
* pondra en la tabla
*/
function rellenaTabla(respuestaJson){
    
    $("#editToxicomania").modal('hide');
    $("#formToxicamania").modal('hide');

    if(respuestaJson.response_code !== '200'){
        swal("Error : "+respuestaJson.response_code, respuestaJson.response_msg, "error");
    }else{

        $("#table_adiccion tbody").html("");
        if(respuestaJson.resultado.lista !== null){

            $.each(respuestaJson.resultado.lista, function(i,p){
            var newRow = "<tr id='tr-"+p.ID_ADIC_PK+"'>"
                            +"<td>"+p.ID_ADIC_PK+"</td>"
                            +"<td>"+p.NOM_ADIC+"</td>"
                            +"<td>"
                                +"<button type='button' class='btn btn-danger waves-effect delete-adic' data-id='"+p.ID_ADIC_PK+"' data-toggle='tooltip' data-placement='top' title='Eliminar Profesion'>"
                                    +"<i class='material-icons'>delete_sweep</i>"
                                +"</button>"
                                +"<button type='button' class='btn btn-warning waves-effect edit-adic' style='margin: 5px' data-id='"+p.ID_ADIC_PK+"' data-toggle='tooltip' data-placement='top' title='Editar Profesion'>"
                                    +"<i class='material-icons'>create</i>"
                                +"</button>"
                            +"</td>"
                        +"</tr>";
                $(newRow).appendTo("#table_adiccion tbody");

                $('#nameEditToxicomania').val("");
                $('#idEditToxicomania').val("");
            });
        }else{
            var newRow = "<tr>"
                            +"<td colspan='3' class='align-center'>No hay Datos</td>"
                        +"</tr>";
            $(newRow).appendTo("#table_adiccion tbody");
        }
    }
}