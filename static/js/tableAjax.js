/**
* Variables para la libreria
* antes de esta libreria se deben de cargar 
* 2 variables
* @var PAGINA_ACTUAL la cual contendra la pagina actual
* @var PAGINAS_TOTALES la contendra el numero de paginas
*/
var form = $("<form></form>");
//numero de registros por default
var numeRegistros = 5;

/**
* Funcion para agregar el select, el nav con el numero de
* paginas 
*/
$(function(){
    //Agregamos el select que tendra el numero de 
    //registros que se quiran mostrar por pagina
    $(".tabla-paginada").before("<div class=' row col-md-2'><select id='numRegistros' class='form-control'><option value='5' >5</option><option value='10' >10</option><option value='15' >15</option><option value='20' >20</option><option value='50' >50</option><option value='100' >100</option></select></div>");

    //Se Agrega el objeto nav con id : navPaginado y tambien se le agregna los botones siguiente y anterior
    $(".tabla-paginada").after("<nav id='navPaginado' >"+
                                "<ul id='listaPaginas' class='pagination'>"+
                                    "<li id='btn_back_pag'class='disabled'>"+
                                        "<a href='javascript:void(0);' class='waves-effect'>"+
                                            "<i class='material-icons'>chevron_left</i>"+
                                        "</a>"+
                                    "</li>"+
                                    "<li id='pag-1' class='active'><a href='javascript:void(0);'>1</a></li>"+
                                    "<li id='btn_next_pag'>"+
                                        "<a class='waves-effect'>"+
                                            "<i class='material-icons'>chevron_right</i>"+
                                        "</a>"+
                                    "</li>"+
                                "</ul>"+
                            "</nav>");

    //Agregarmos los botones al nav
    listaBotones(PAGINAS_TOTALES);
});

/**
* Funcion para recargar la tabla con el numero
* de registros que se seleccione
*/
$(function(){
   $("#numRegistros").on('change',function(){
        numeRegistros = $(this).val();
        // si cambia el numero de registros se vuelve a cargar la tabla sin filtros
        if(numeRegistros > 0){
            preparaAjax(null,URL_PAGINACION,'R');
        }
   });
});

/**
* Funcio  que crea los botones para la paginacion
* primero remueve todos los lementos botones que 
* tangan la clase .btn_pag, despues toma el total 
* de paginas y agrega los botones al nav con id : navPaginado.
* @param $TOTAL_PAGINAS : el numero de paginas a pintar
*/
function listaBotones(TOTAL_PAGINAS){
    $(".btn_pag").remove();
    
    if(TOTAL_PAGINAS === '1'){
        $("#btn_next_pag").addClass('disabled');
    }

    for (var i=1; i<=TOTAL_PAGINAS; i++) {
        if(i !== 1){
            $("#btn_next_pag").before("<li id='pag-"+i+"' class='btn_pag' ><a href='javascript:void(0);' class='waves-effect'>"+i+"</a></li>");
        }
    }
}

/**
* Funcion que prepara el Ajax para hacer la peticion
* recibe lo siguiente FORMULARIO_FILTRO, URL, TIPO
* @param $FORMULARIO_FILTRO : foemulario que se enviara para filtros
* @param $URL : url donde se hara la peticion
* @param $TIPO: que tipo de peticion es 'S', 'A', 'R'
*/
function preparaAjax(FORMULARIO_FILTRO,URL,TIPO){

    if(typeof FORMULARIO_FILTRO !== 'undefined' && FORMULARIO_FILTRO !== null){
        conDatos(FORMULARIO_FILTRO,URL,TIPO);
    }else{
        sinDatos(URL,TIPO);
    }
}

/**
* Funcion que hace la peticion ajax sin datos, valida si
* se pide la pagina siguiente o la pagina anterior, 
* para la pagina siguiente se usa el tipo 'S' cuando la 
* operacion es siguientes a la pagina actual se le suma 1,
* para la pagina anterior se usa el tipo 'N' cuando la 
* operacion es anterior a la pagina actual se le resta 1.
* @param $URL : la url de destino de la peticion
* @param $TIPO : 'S' si es siguiente pagina
*                'A' si es pagina anterior
*                'R' si un reload desde pagina cero
*/
function sinDatos(URL,TIPO){
    // se convierte la pagina actual a int
    var PAGINA = parseInt(PAGINA_ACTUAL);

    if(TIPO === 'S'){
        
        if(PAGINA === 0){
            PAGINA = 1;
        }else{
            PAGINA++;
        }

    }else if(TIPO === 'A'){

        if(PAGINA !== 0){
            PAGINA--;
        }
    }else{
        PAGINA = 0;
    }
    
    $.ajax({
        url: URL,
        type:'POST',
        dataType:'json',
        data:{
            paginaConsulta : PAGINA,
            numeroRegistros : numeRegistros
        },
        success:function(json) {
            if(json.response_code === "200"){

                PAGINA_ACTUAL =  parseInt(json.resultado.paginActual);
                ULTIMA_PAGINA =  parseInt(json.resultado.paginasTotales);

                if(PAGINA_ACTUAL > 0){
                    $("#btn_back_pag").removeClass('disabled');
                }else if(PAGINA_ACTUAL === 0){
                    $("#btn_back_pag").addClass('disabled');
                }

                if(ULTIMA_PAGINA === 0 || (ULTIMA_PAGINA-1) === PAGINA_ACTUAL){
                    $("#btn_next_pag").addClass('disabled');
                }else{
                    $("#btn_next_pag").removeClass('disabled');
                }

                if(ULTIMA_PAGINA < PAGINAS_TOTALES || ULTIMA_PAGINA > PAGINAS_TOTALES){
                    PAGINAS_TOTALES = ULTIMA_PAGINA; 
                    listaBotones(ULTIMA_PAGINA);
                }

                //quitamos la clase active al elemnto que no tenga
                $("#listaPaginas > li.active").removeClass('active');
                
                //agregamos el elemnto active a la pagina actual
                var paginaSelect = PAGINA + 1;
                $("#pag-"+paginaSelect).addClass('active');

                rellenaTabla(json);
            }else{
                rellenaTabla(json);
            }
        },
        error : function(xhr, status) {
             swal("Error!", "Ocurrio un error durante la ejecución", "error");
        }
    });
}

/**
* Funcion para hacer la peticion ajax con filtros
* @param $FILTROS : filtros que se aplique para la busqueda
* @param $URL : la url a donde se hara la petición
* @param $TIPO : 'S' si es pagina siguiente 
*                'A' si es pagina anterior 
*                'R' si es recargar desde cero
*/
function conDatos(FILTROS,URL,TIPO){

    var PAGINA = parseInt(PAGINA_ACTUAL);

    if(TIPO === 'S'){
        
        if(PAGINA === 0){
            PAGINA = 1;
        }else{
            PAGINA++;
        }

    }else if(TIPO === 'A'){

        if(PAGINA !== 0){
            PAGINA--;
        }
    }else if(TIPO === 'R'){
        PAGINA = 0;
    }

    form.append("<input type='text' id='paginaConsulta' name= 'paginaConsulta' class='input_pag' value='"+PAGINA+"'>");
    form.append("<input type='text' id='numeroRegistros' name='numeroRegistros' class='input_pag' value='"+numeRegistros+"'>");
    
    //llenamos el formulario
    for (var i = 0; i < FILTROS.length; i++) {
        form.append("<input type='text' id='filtro_"+i+"' name='filtro_"+i+"' class='input_pag' value='"+FILTROS[i]+"'>");
    }

    $.ajax({
        url: URL,
        type:'POST',
        dataType:'json',
        data:form.serialize(),
        success:function(json) {
            if(json.response_code === "200"){
                //vaciamos el formulario
                form.find('.input_pag').remove();
                
                PAGINA_ACTUAL =  parseInt(json.resultado.paginActual);
                ULTIMA_PAGINA =  parseInt(json.resultado.paginasTotales);
                
                if(PAGINA_ACTUAL > 0){
                    $("#btn_back_pag").removeClass('disabled');
                }else if(PAGINA_ACTUAL === 0){
                    $("#btn_back_pag").addClass('disabled');
                }

                if(ULTIMA_PAGINA === 0 || (ULTIMA_PAGINA-1) === PAGINA_ACTUAL){
                    $("#btn_next_pag").addClass('disabled');
                }else{
                    $("#btn_next_pag").removeClass('disabled');
                }

                if(ULTIMA_PAGINA < PAGINAS_TOTALES || ULTIMA_PAGINA > PAGINAS_TOTALES){
                    PAGINAS_TOTALES = ULTIMA_PAGINA; 
                    listaBotones(ULTIMA_PAGINA);
                }

                //quitamos la clase active al elemnto que no tenga
                $("#listaPaginas > li.active").removeClass('active');
                
                //agregamos el elemento active a la pagina actual
                var paginaSelect = PAGINA + 1;
                $("#pag-"+paginaSelect).addClass('active');

                rellenaTabla(json);
            }else{
                rellenaTabla(json);
            }
        },
        error : function(xhr, status) {
            //en caso de que falle se limpiar el formulario para que cuando este activo
            // el servicio de nuevo no envie doble la peticion
            form.find('.input_pag').remove();
            swal("Error!", "Ocurrio un error durante la ejecución", "error");
        }
    });

}
