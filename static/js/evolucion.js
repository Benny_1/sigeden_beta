// variables globales
var $demoMaskedInput = $('.demo-masked-input');
var flg_primercita = true;
var paginaActual = ARR_ID_CITAS.length - 1;
var paginaAnterior = paginaActual - 1;
var paginaSiguiente = null;

//mascara para los campos
$(function(){
    $demoMaskedInput.find('.arteria').inputmask('999/999', { placeholder: '000/00 '});
    $demoMaskedInput.find('.cardiaca').inputmask('999', { placeholder: '00 '});
    $demoMaskedInput.find('.respiracion').inputmask('999', { placeholder: '00 '});
});

// metodos para hacer el paginado
$(function(){
    $("#cita_paginacion").before("<div class='row'>"+
                                    "<div class='col-md-12 text-center' >"+
                                        "<button id='id_btn_prev' class='btn btn-default waves-effect pull-left' disabled> < Prev</button>"+
                                        "<button id='id_btn_next' class='btn btn-default waves-effect pull-right' disabled> Next ></button>"+
                                    "</div>"+
                                "<div> <br>");
    if(PAGINAS_CITAS_TOTALES > 1){
        numPaginas = PAGINAS_CITAS_TOTALES;
        flg_primercita = false;
        $("#id_btn_prev").prop('disabled','');
    }
});

/**
* Funcion para el guardado de la informacion en la cita
* @param id_cita
* @param id_pac
* @param ta_pac
* @param fc_pac
* @param fr_pac
* @param opt_proce
* @param obs_cita
* @param opt_diente_aten
*/
$(function(){
    $("#save_evolucion").click(function(){
        
        var cita = $("#id_cita").val();
        var paciente = $("#id_pac").val();
        var arteria = $("#ta_pac").val();
        var cardiaca = $("#fc_pac").val();
        var respiracion = $("#fr_pac").val();
        var procedimiento = $("#opt_proce").val();
        var obsCita = $("#obs_cita").val();
        var dienteEnfermo = $("#opt_diente_aten").val();
        var validaNota = true;

        if(arteria === null || arteria === ""){
            validaNota = false;
            swal("Campo vacio!", "valor arterial no informado", "error");
        }else if(cardiaca === null || cardiaca === ""){
            validaNota = false;
            swal("Campo vacio!", "valor cardiaco no informado", "error");
        }else if(respiracion === null || respiracion === ""){
            validaNota = false;
            swal("Campo vacio!", "valor respiracion no informado", "error");
        }else if(procedimiento === null || procedimiento === ""){
            validaNota = false;
            swal("Campo vacio!", "valor procedimiento no informado", "error");
        }else if(obsCita === null || obsCita === ""){
            validaNota = false;
            swal("Campo vacio!", "valor observacion no informado", "error");
        }else if(dienteEnfermo === null || dienteEnfermo === "0"){
            validaNota = false;
            swal("Campo vacio!", "seleccione le diente al que le va hacer el tratamiento", "error");
        }
        
        if(validaNota === true){

            if(flg_primercita){
            
                swal({
                    title: "Es primer cita!",
                    text: "Antes de continuar el paciente debe de pagar $50.00 por la consulta",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya pago, continuar!",
                    closeOnConfirm: false
                    }, function () {
                        saveDetalleEvolucion(paciente,cita,arteria,cardiaca,respiracion,procedimiento,obsCita,dienteEnfermo);
                    });
            }else{
                saveDetalleEvolucion(paciente,cita,arteria,cardiaca,respiracion,procedimiento,obsCita,dienteEnfermo);
            }

        }
    });

});


function saveDetalleEvolucion(paciente,cita,arteria,cardiaca,respiracion,procedimiento,obsCita,dienteEnfermo){
        $.ajax({
            url:BASE_PATH + 'index.php/Seguimiento/saveDetalleCita',
            type:'POST',
            dataType:'json',
            data:{
                idPaciente : paciente,
                idCita : cita,
                tencionArt : arteria,
                frecuenciaCar : cardiaca,
                frecuenciaRes : respiracion,
                proce : procedimiento,
                obs : obsCita,
                dienteEnf : dienteEnfermo
            },
            success:function(json) {
                if(json.response_code === "200"){    
                    
                    $("#ta_pac").val("");
                    $("#fc_pac").val("");
                    $("#fr_pac").val("");
                    $("#opt_proce").val("0");
                    $("#obs_cita").val("");
                    $("#opt_diente_aten").val("0");

                    swal("Exito!", "Se guardo con exito proceda a agendar la siguiente cita", "success");
                    $("#modalReAgendaCita").modal("show");

                    /*
                    if(flg_primercita){
                        //mostrar modal para hacer el pago
                        $("#modalReAgendaCita").modal("show");                        
                    }*/
                }
            },
            error : function(xhr, status) {
                swal("Error!", "Ocurrio un error durante la ejecución", "error");
            }
        });
}

/*   ---------------- BTN-PREVIA-CITA ----------------------   */
$(function(){
    $("#id_btn_prev").click(function(){

        var citaAnterior = ARR_ID_CITAS[paginaAnterior];

        $.ajax({
            url:BASE_PATH + 'index.php/Busqueda/getCitaById',
            type:'POST',
            dataType:'json',
            data:{
                idCitaPaciente : citaAnterior
            },success:function(json) {
                if(json.response_code === "200"){

                    console.log(json.response_code.detalle);
                    
                    $("#ta_pac").val(json.detalle.TA);
                    $("#fc_pac").val(json.detalle.FC);
                    $("#fr_pac").val(json.detalle.FR);
                    $("#obs_cita").val(json.detalle.OBS_PROC);

                    //$("#opt_proce").val(json.detalle.ID_PROC_FK);
                    $('#opt_proce').selectpicker('val',json.detalle.ID_PROC_FK);
                    //quitamos el disable de next 
                    $("#id_btn_next").prop('disabled','');
                    // se desabilita la opcion de guardar
                    $("#save_evolucion").prop('disabled', true);

                    //recalculamos el paginado
                    paginaSiguiente = paginaActual;
                    paginaActual = paginaActual - 1;
                    paginaAnterior = paginaActual - 1;

                    //si la pagina actual es la 0 ya no se permite retroceder
                    if(paginaActual === 0){
                        $("#id_btn_prev").prop('disabled',true);
                    }

                }else{
                    swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                }
            },
            error : function(xhr, status) {
                swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
            }
        });

    });
});

/*   ---------------- BTN-SIGUIENTE-CITA ----------------------   */

$(function(){
    $("#id_btn_next").click(function(){
        
        var citaSiguiente = ARR_ID_CITAS[paginaSiguiente];

        $.ajax({
            url:BASE_PATH + 'index.php/Busqueda/getCitaById',
            type:'POST',
            dataType:'json',
            data:{
                idCitaPaciente : citaSiguiente
            },success:function(json) {
                
                if(json.response_code === "200"){

                    //recalculamos el painado
                    paginaActual = paginaActual + 1;
                    paginaSiguiente = paginaActual + 1;
                    paginaAnterior = paginaActual - 1 ;

                    if(paginaAnterior === 0){
                        $("#id_btn_prev").prop('disabled',false);
                    }

                    if(json.detalle === null){
                        
                        $("#ta_pac").val("");
                        $("#fc_pac").val("");
                        $("#fr_pac").val("");
                        $("#obs_cita").val("");
                        $("#id_btn_prev").prop('disabled',false);
                        $("#id_btn_next").prop('disabled',true);
                        $("#save_evolucion").prop('disabled', false);
                        
                    }else{
                        
                        $("#ta_pac").val(json.detalle.TA);
                        $("#fc_pac").val(json.detalle.FC);
                        $("#fr_pac").val(json.detalle.FR);
                        $("#save_evolucion").prop('disabled', true);
                        $("#obs_cita").val(json.detalle.OBS_PROC);
                        $('#opt_proce').selectpicker('val',json.detalle.ID_PROC_FK);

                    }
                }else{
                    swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                }
            },error : function(xhr, status) {
                swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
            }
        });

    });
});



/**
* Funcion para mostrar el datePiker y dateTime
*/
$(function(){

    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        minDate : new Date(),
        time: false
    });

    $('.timepicker').bootstrapMaterialDatePicker({
        format: 'HH:mm',
        clearButton: true,
        date: false,
        shortTime: true
    });
});


/**
* Funcion para agendar la cita del paciente
* recibe la hora y la fecha de la cita, si
* es exitoso regresa un response code 200
* @param fecha
* @param hora
*/
$(function(){
    $("#btn_agenda_cita_evo").click(function(){

        var fecha = $("#fechCita").val(); 
        var hora = $("#horaCita").val();
        var paciente = $("#id_pac").val();

        console.log("valor fecha : "+fecha);
        console.log("valor hora : "+hora);
        console.log("valor paciente : "+paciente);

        $.ajax({
            url:BASE_PATH + 'index.php/Busqueda/agendaCitaPac',
            type:'POST',
            dataType:'json',
            data:{
                fechaCita : fecha,
                horaCita : hora, 
                pacCita : paciente
            },
            success:function(json) {
                if(json.response_code === "200"){

                    swal({
                        title: "Exito!",
                        text: "Se agendo la cita con exito, se redireccionara al calendario ....",
                        icon: "success",
                        button: "Ok!",
                    },function(){
                        window.location.href = BASE_PATH+"index.php/Seguimiento";
                    });

                }else{
                    swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                }
            },
            error : function(xhr, status) {
                swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
            }
        });
    });
});