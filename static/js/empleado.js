/**
*
*/
$(function(){
    var $demoMaskedInput = $('.demo-masked-input');

    $demoMaskedInput.find('.mobile-phone-number').inputmask('(999) 999-99-99', { placeholder: '(___) ___-__-__' });
    $demoMaskedInput.find('.email').inputmask({ alias: "email" });
    $demoMaskedInput.find('.money-dollar').inputmask('decimal', {'rightAlign': false,'alias': 'numeric','groupSeparator': ',','autoGroup': true,'digits': 2,'radixPoint': ".",'digitsOptional': false,'allowMinus': false,'placeholder': '0.00'});

});

/**
* Funcion para guardar la informacion del empleado
* al dar click en el boton
* @param nombre 
* @param app
* @param apm
* @param tel
* @param email
* @param salary
*/
$(function(){
    $('#save_empleado').click(function(){
        
        var generoEmp = "";
        var nombre = $("#nombre_p").val();
        var app = $("#paterno_p").val();
        var apm = $("#materno_p").val();
        var tel = $("#num_tel").val();
        var corr = $("#correo").val();
        var salario = $("#sueldo").val();
        var validForm = true;
        
        generoEmp = $('input:radio[name=grupo_genero]:checked').attr('id');
        
        if(nombre === "" || nombre === null){
            swal("Campo vacio!", "El nombre no puede estar vacio!", "error");
            validForm = false;
        }else if(app === "" || app === null){
            swal("Campo vacio!", "El Apellido Paterno no puede estar vacio!", "error");
            validForm = false;
        }else if(apm === "" || apm === null){
            swal("Campo vacio!", "El apellido Materno no puede estar vacio!", "error");
            validForm = false;
        }else if(salario === "" || salario === null){
            swal("Campo vacio!", "El salario no puede estar vacio!", "error");
            validForm = false;
        }

        //validamos si el form si se valida
        if(validForm == true){

            if(generoEmp === 'mujer'){
                generoEmp = 'F';
            }else if(generoEmp === 'hombre'){
                generoEmp = 'M';
            }

            $.ajax({
                url:BASE_PATH + 'index.php/Empleado/saveOrUpdateEmploy',
                type:'POST',
                dataType:'json',
                data:{
                    name : nombre,
                    lastName : app,
                    mLastName : apm,
                    phone : tel,
                    email : corr,
                    gener : generoEmp,
                    salary : salario
                },
                success:function(json) {
                    //Validamos si el codigo de espuesta es 200
                    if(json.response_code === "200"){

                        swal("Exito!","Se guardo con exito la informacion", "success");
                        $("#nombre_p").val("");
                        $("#paterno_p").val("");
                        $("#materno_p").val("");
                        $("#num_tel").val("");
                        $("#correo").val("");
                        $("#sueldo").val("");

                    }else{
                        swal("Error : "+json.response_code,json.response_msg, "error");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!","Disculpe, existió un problema", "error");
                }
            });
        }
    });
});
