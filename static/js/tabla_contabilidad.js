$(function(){

	var contadorNewAbono = 0;
	let newTotal = 0.0;
	let abonos = 0.0;

	/**
	* metodo para hacer editables las celdas
	*/
	$("#btnEditDeuda").click(function(){
		$("#tablaEstadoCuenta td.celdaEditable").attr('contenteditable',true);
		$("#btnReCalculate").removeAttr('disabled');
		swal('Exito!', `Ya puede editar los montos.`, 'success');
	});
	
	/**
	* metodo para capturar la accion del click
	* sobre el boton de agregar un abono
	*/
	$("#btnAddAbono").click(function(){
		contadorNewAbono++;
		let f = new Date();
		let newRow = `<tr id='tr-newab-${contadorNewAbono}'>
						<td>${f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate()}</td>
						<td></td>
						<td class='allow_only_numbers celdaEditable montoAbono' contenteditable='true'></td>
						<td>
							<button data-id='tr-newab-${contadorNewAbono}' type='button' class='btn btn-danger waves-effect remove_mount' data-toggle='tooltip' data-placement='top' title='Eliminar campo'>
                                x
                            </button>
						</td>
					</tr>`;
        $(newRow).appendTo("#tablaEstadoCuenta tbody");
        $("#btnReCalculate").removeAttr('disabled');
	});

	/**
	* evento para el recalculo de la deuda
	*/
	$("#btnReCalculate").click(function(){
		console.log('recalculando....');
		
		let cargosListos = false;
		let cargos = 0.0;
		// recorremos todos los cargos
		$(".montoDeuda").each(function(){
			cargos += parseFloat($(this).text().replace(',',''));
			cargosListos = true;
		});

		let abonosListos = false;
		abonos = 0.0;
		// recorremos todos los abonos
		$(".montoAbono").each(function(){
			// validamos que el campo no tenga cero o nulo
			let valorCampo = $(this).text();
			if(valorCampo === "0" || valorCampo === "0.00" || valorCampo === null || valorCampo === ""){
				swal("Advertencia!", "no se permiten lo siguientes valore: 0, 0.00  o vacio", "info");
				abonosListos = false;
				return false;
			}else{
				abonos += parseFloat($(this).text().replace(',',''));
				let tempAbono = $(this).text().replace(',','');
				$(this).text(Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(tempAbono).replace('$',''));
				abonosListos = true;
			}
		});
		// validamos que todos los abonos yc argos esten validados
		if(cargosListos && abonosListos){
			if(cargos > abonos || cargos === abonos){
				newTotal = 0.0;
				newTotal = calculaDeuda1(cargos,abonos);
				$("#txtTotalDeuda").val(`${newTotal}`);
				$("#btnSaveCont").removeAttr('disabled');
			}else{
				swal("Advertencia!", "Los abonos superan la deuda favor de ingresar un monto más exacto", "info");
			}
		}
	});

	/**
	* evento para el guardar los cambios en la cuenta
	*/
	$("#btnSaveCont").click(function(){

		// evento que pregunta si esta seguro de hacer los cambios
		swal({
            title: "¿Esta seguro de estos cambios?",
            text: "Los montos se guardaran definitivamente",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!",
            closeOnConfirm: false
        }, function () {
        	ejecutaSave(newTotal,abonos);
        });

	});

	/** 
	* evento para desabilitar los botones de recalcular y guardars
	*/
	$("#close_modal_cuentas").click(function(){
		$("#btnReCalculate").attr('disabled');
		$("#btnSaveCont").attr('disabled');
	});

	/**
	* evebti para eliminar la celda que se agrego en el abono
	*/
	$("#tablaEstadoCuenta").on('click','.remove_mount',function(){
		let idNewCelda = $(this).attr('data-id');
		console.log(`valor id: ${idNewCelda}`);
		 $(`#${idNewCelda}`).remove();
	});
});

/**
* funcion que hace el save de los valores capturados
* en la cuenta, arma un json con todos los valores 
* y los envia
*/
function ejecutaSave(totalRestante,totalAbonos){
		let montoAbono = new Array();
		let idCuenta = '';
		let idCelda = $("#cuntaId").val();

		$("#tablaEstadoCuenta tbody tr").each(function(index) {
			let stringId = $(this).attr('id');
			let tipoMov = stringId.substring(0,9);
			let movId =  stringId.substring(9,stringId.length);
			let valorMonto = '';
			
			switch (tipoMov) {
				case 'tr-cargo-':
					idCuenta = movId;
					valorMonto = $(this).find('td:nth-child(2)').text()
					montoAbono[index] = {idMov:movId,tipoMov:'C',valMonto:valorMonto};
					break;
				case 'tr-abono-':
					valorMonto = $(this).find('td:nth-child(3)').text()
					montoAbono[index] = {idMov:movId,tipoMov:'A',valMonto:valorMonto};
					break;
				case 'tr-newab-':
					valorMonto = $(this).find('td:nth-child(3)').text()
					montoAbono[index] = {idMov:movId,tipoMov:'NA',valMonto:valorMonto};
					break;
			}
		});

		if(montoAbono.length === 0){
			swal("Advertencia!", "No se pudo procesar la solicitud, intentelo más tarde", "info");
		}else{
			$.ajax({
                url:BASE_PATH + 'index.php/Cuentas/updateMountsOnAcount',
                type:'POST',
                dataType:'json',
                data:{
                    newMountOnAcount: montoAbono,
                    numCuenta: idCuenta
                },
                success:function(json) {
                    if(json.response_code === "200"){
                    	updateMountView(idCelda,totalRestante,totalAbonos);
                        swal('Exito!', `Abono correcto.`, 'success');
                        $("#detail_modal").modal('hide');
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
		}
}

/**
* funcion para mostrar los nuevos valores de 
* total de abonos y total restante de la deuda una vez que se guardo en la base
*/
function updateMountView(cuenta,restante,abonos){
	$(`#tr-${cuenta} > td:nth-child(4)`).text(abonos);
    $(`#tr-${cuenta} > td:nth-child(5)`).text(restante);
}