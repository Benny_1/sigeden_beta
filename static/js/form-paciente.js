/**
 * @author Noe Ramos
 * @version 0.1
 * @copyright Todos los derechos reservados 2018
*/

//variables
var $demoMaskedInput = $('.demo-masked-input');
var familiarId = '0';
var enfermedad_patologica_familiar = []; 
var enfermedad_patologica_personal = [];
var padecimeinto_dental = [];
var adicciones = [];
var comentarioEnf = [];
var contadorEnf = -1;

$(function () {

    //Masked Input
    $demoMaskedInput.find('.date').inputmask('dd-mm-yyyy', { placeholder: '__-__-____' });
    $demoMaskedInput.find('.mobile-phone-number').inputmask('(999) 999-99-99', { placeholder: '(___) ___-__-__' });
    $demoMaskedInput.find('.email').inputmask({ alias: "email" });

    /**
    * Funcion para obtener las enferemadeades patologicas familiares
    * del multiselect, agregandolas a un array
    */
    $('#optgroup_familiar').multiSelect({
        afterSelect: function(values){

            enfermedad_patologica_familiar.push(values[0]);
        },
        afterDeselect: function(values){
                
            var ultimoValor = enfermedad_patologica_familiar.length;
                ultimoValor = ultimoValor-1
                
            for(var i = 0; i <= ultimoValor; i++){

                var valorTem = enfermedad_patologica_familiar[i];
                var valorEliminar = values;
                if(parseInt(valorTem) === parseInt(valorEliminar)){
                    enfermedad_patologica_familiar.splice(i,1);
                }
            }
        },

    });
    
    /**
    * Funcion para obtener las enferemadeades personales
    * del multi select , agregandolas a un array
    */
    $('#optgroup_personal').multiSelect({
        afterSelect: function(values){

            enfermedad_patologica_personal.push(values[0]);
            comentarioEnf.push("");
            $("#obs_enf").val("");
            contadorEnf++;

        },
        afterDeselect: function(values){
            var ultimoValor = enfermedad_patologica_personal.length;

            ultimoValor = ultimoValor-1;

            for(var i = 0; i <= ultimoValor; i++){

                var valorTem = enfermedad_patologica_personal[i];
                var valorEliminar = values;

                if(parseInt(valorTem) === parseInt(valorEliminar)){
                    enfermedad_patologica_personal.splice(i,1);
                    comentarioEnf.splice(i,1);
                    contadorEnf--;
                }
            }
        }
    });

    /**
    * Funcion para el optionGroup de adicciones
    * agregandolas a un array
    */
    $('#optgroup_adiccion').multiSelect({
        afterSelect: function(values){
            adicciones.push(values[0]);
        },
        afterDeselect: function(values){
            var ultimoValor = adicciones.length;
            ultimoValor = ultimoValor-1;
            for(var i = 0; i <= ultimoValor; i++){

                var valorTem = adicciones[i];
                var valorEliminar = values;
                    
                if(parseInt(valorTem) === parseInt(valorEliminar)){
                    adicciones.splice(i,1);
                }
            }
        }
    });
    
});


$(function(){
    $("#obs_enf").keypress(function(){
        console.log("valor contador: "+contadorEnf);

        comentarioEnf[contadorEnf] = $(this).val();
    });
});


/**
* Funciones KeyUp para validar capos nulos del 
* formulario del paciente
*/
$(function(){
    // valida nombre del paciente
    $('#nombre_p').on('keyup',function(){
        var valueInputName = $(this).val().length;
        if(valueInputName > 0){
            $('#nombre_p').parent().attr("class","form-line");
        }else{
            $('#nombre_p').parent().attr("class","form-line focused error");
        }
    });

    // valida apellido paterno del paciente
    $('#paterno_p').on('keyup',function(){
        var valueInputApp = $(this).val().length;
        if(valueInputApp > 0){
            $('#paterno_p').parent().attr("class","form-line");
        }else{
            $('#paterno_p').parent().attr("class","form-line focused error");
        }
    });

    // valida apellido materno del paciente
    $('#materno_p').on('keyup',function(){
        var valueInputApm = $(this).val().length;
        if(valueInputApm > 0){
            $('#materno_p').parent().attr("class","form-line");
        }else{
            $('#materno_p').parent().attr("class","form-line focused error");
        }
    });

    // valida la fecha de nacimiento del paciente
    $('#fecha_nac').on('keyup',function(){
        var valueInputFec = $(this).val().length;
        if(valueInputFec > 0){
            $('#fecha_nac').parent().attr("class","form-line");
        }else{
            $('#fecha_nac').parent().attr("class","form-line focused error");
        }
    });

});

/**
 * Funcion para la validacion y guardado de la informacion
 * del paciente, si los capos requeridos ya no se encuentran
 * vacios se envia la informacion 
 */
$(function(){
    //Formulario general 
    $("#next_ant_fam").click(function(){
  
        var form_dp_validado = true;
        //validamos que las variables no sean nulas en caso de que sean nulas se muestra un mensaje 
        //y se pone el foco en el compo nulos
        if($('#nombre_p').val() === ""){
            $('#nombre_p').parent().attr("class","form-line focused error");
            swal("Campo vacio!", "EL nombre no puede estar vacio!", "error");
            form_dp_validado = false;
        }else if($('#paterno_p').val() === ""){
            $('#paterno_p').parent().attr("class","form-line focused error");
            swal("Campo vacio!", "EL Apellido Paterno no puede estar vacio!", "error");
            form_dp_validado = false;
        }else if($('#materno_p').val() === ""){
            $('#materno_p').parent().attr("class","form-line focused error");
            swal("Campo vacio!", "EL Apellido Materno no puede estar vacio!", "error");
            form_dp_validado = false;
        }else if($('#fecha_nac').val() === ""){
            $('#fecha_nac').parent().attr("class","form-line focused error");
            swal("Campo vacio!", "La fecha de nacimiento no puede estar vacia!", "error");
            form_dp_validado = false;
        }else if(typeof $('input:radio[name=grupo_genero]:checked').attr('id') === "undefined"){
            swal("Campo vacio!", "Debe seleccionar un Genero para el Paciente!", "error");
            form_dp_validado = false;
        }else if($("#lugar_nac").val() === '0'){
            swal("Campo vacio!", "El lugar de nacimiento no puede estar vacia!", "error");
            form_dp_validado = false;
        }else if($("#ocupacion").val() === '0'){
            swal("Campo vacio!", "La ocupacion no puede estar vacia!", "error");
            form_dp_validado = false;
        }
        
        if(form_dp_validado){

            generoPersona = $('input:radio[name=grupo_genero]:checked').attr('id');

            if(generoPersona === 'mujer'){
                $("#isWoman").css('display','block');
                generoPersona = 'F';
            }else{
                $("#isWoman").css('display','none');
                generoPersona = 'M';
            }
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Paciente/saveOrUpdateInfoPatient',
                type:'POST',
                dataType:'json',
                data:{
                    namePatient: $('#nombre_p').val(),
                    appPatient: $('#paterno_p').val(),
                    apmPatient: $('#materno_p').val(),
                    fecNac: $('#fecha_nac').val(),
                    estado: $('#lugar_nac').val(),
                    direccion: $('#direccion').val(),
                    genero: generoPersona,
                    profesion: $('#ocupacion').val(),
                    estadoCivil: $('#est_civil').val(),
                    numTel: $('#num_tel').val(),
                    correo: $('#correo').val(),
                    observacion: $('#obs_pac').val(),
                },
                success:function(json) {
                    //Validamos si el codigo de espuesta es 200
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        $('#nombre_p').val("");
                        $('#paterno_p').val("");
                        $('#materno_p').val("");
                        $('#lugar_nac').val("0");
                        $('#direccion').val("");
                        $('#ocupacion').val("0");
                        $('#est_civil').val("0");
                        $('#correo').val("");
                        $('#fecha_nac').val("");
                        $('#num_tel').val("");
                        $('input:radio[name=grupo_genero]').attr('checked',false);
                        
                        $("#idPer").val(json.id_persona);

                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");

                        $('#op_2').parent().removeClass('disabled');
                        $('#op_2').click();
                        $('#op_1').parent().addClass('disabled');
                    // en caso de que no sea 200 se verifica si es 400 y se muestra un mensaje de error
                    }else if(json.response_code === "400"){
                        $('.page-loader-wrapper').fadeOut();
                        swal("Error!", json.json_msg, "error");
                        $("#idPer").val(json.id_persona);
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Error!","Disculpe, existió un problema", "error");
                }
            });
        }
    });

    //boton para cancelar y limpiar el registro del paciente
    $('#cancelarRegistro').click(function(){

        $('#nombre_p').val("");
        $('#paterno_p').val("");
        $('#materno_p').val("");
        $('#lugar_nac').val("0");
        $('#direccion').val("");
        $('#ocupacion').val("0");
        $('#est_civil').val("0");
        $('#correo').val("");
        $('#fecha_nac').val("");
        $('#num_tel').val("");
        $('input:radio[name=grupo_genero]').attr('checked',false);
        $("#idPer").val("");
    });
});

/**
* Funcion on change para detectar el cambio del 
* select del familiar y obtener su id y poder
* hacer la relacion de enfermedad patologica del
* familiar
*/
$(function(){
    $('#select_familiar').on('change',function(){
        familiarId = $(this).val();
    });
});


/**
* Funcion para guardar el los antecedentes
* patologicos de cada familiar
*/
$(function(){
    $('#btn_saveAnt_familiar').click(function(){
        
        var idPer = $("#idPer").val();

        if(familiarId === '0'){
            swal("Error!","Debe de seleccionar un familiar para la relacion","error");
        }else if(enfermedad_patologica_familiar.length === '0'){
            swal("Error!","Debe de seleccionar una enferemedad para la relacion","error");
        }else{
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Paciente/saveEnfermedadFamiliar',
                type:'POST',
                dataType:'json',
                data:{
                    paciente : idPer,
                    familiar : familiarId,
                    enferFamiliar : enfermedad_patologica_familiar
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");
                        $('#select_familiar option[value='+familiarId+']').remove();
                        $('#optgroup_familiar').multiSelect('deselect_all');
                        $('#select_familiar').val('0');
                        $('#btn_noAnt_familiares').prop('disabled',true);
                    }else{
                        $('.page-loader-wrapper').fadeOut();
                        swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                }
            });
        }

    });
});

/**
* Funcion para pasar al siguiente firmulario 
*/
$(function(){
    $('#btn_finish_ant_fam').click(function(){
        $('#op_3').parent().removeClass('disabled');
        $("#op_3").click();
        $('#op_2').parent().addClass('disabled');
    });
});


/**
 * Funcion del boton de no antecedentes 
 * de enferemedades patologicas familiares.
 */
$(function(){
    $('#btn_noAnt_familiares').click(function(){
        $('#op_2').parent().addClass('disabled');
        $("#op_3").click();
    });
});

/**
 * Funcion del boton de no antecedentes personales
 * de enfermedades patologicas.
 */
$(function(){
    $('#btn_noAnt_personales').click(function(){
        $('#op_4').parent().removeClass('disabled');
        $("#op_4").click();
        $('#op_3').parent().addClass('disabled');
    });
});

/**
* Funcion para guardar los antecedentes patologicos personales
* en caso de tenerlos
*/
$(function(){
    $("#btn_guardarAnte_personales").click(function(){
        
        var idPer = $("#idPer").val();
        console.log(enfermedad_patologica_personal.length);
        if(enfermedad_patologica_personal.length !== 0){

            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Paciente/saveEnfermedadPatologica',
                type:'POST',
                dataType:'json',
                data:{
                    idPersona : idPer,
                    enfermedades : enfermedad_patologica_personal,
                    observaciones : comentarioEnf
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");
                        $('#op_4').parent().removeClass('disabled');
                        $("#op_4").click();
                        $('#op_3').parent().addClass('disabled');
                    }else{
                        $('.page-loader-wrapper').fadeOut();
                        swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                }
            });
        }else{
            swal("Campo vacio!", "No puedes guardar informacion vacia", "error");
        }
    });
});

/**
* Funcion para el que al momento de seleciconar
* que el paciente tiene alguna adiccion muestre
* una modal para selecionar que tipo de adicciones tiene.
*/
$(function(){

    $( '#checkbox_adiccion' ).on( 'click', function() {
        if($(this).is(':checked')){
            $("#optionToxi").fadeIn();
            //$("#optionToxi").css('display','block');
        }else{
            $("#optionToxi").fadeOut();
            //$("#optionToxi").css('display','none');
        }
    });
});

/**
* Funcion para limpiar los campos 
* del formulario de habitos
*/
$(function(){
    $("#btn_limpia_hab_form").click(function(){
        $('input:radio[name=grupo_comidas]:checked').attr('checked',false);
        $('input:radio[name=grupo_higiene]:checked').attr('checked',false);
        $('#checkbox_hilo').removeAttr('checked');
        $('#checkbox_enjuage').removeAttr('checked');
        $('#checkbox_adiccion').removeAttr('checked');
        $('input:radio[name=grupo_cepillada]:checked').attr('checked',false);
        $('#cepilloDiente').val("");
    });
});

/**
* Funcion para el boton de 
* guardar los habitos del paciente
*/
$(function(){
    $('#btn_save_habitos').click(function(){
        //variable para validar el formulario
        var form_habit_validado = true;
        var adiccion = $('#checkbox_adiccion').prop('checked');

        if(adiccion === true){
            adiccion = '1';
            if(adicciones.length < 1 ){
                swal("Campo vacio!", "Se seleciono que el paciente tiene una adiccion favor de escojer almenos 1!", "error");
                form_habit_validado = false;
            }
        }else{
            adiccion = '0';
        }
        //validamos que se seleccion alguna opcion
        if(typeof $('input:radio[name=grupo_comidas]:checked').attr('id') === "undefined"){
            swal("Campo vacio!", "Debe seleccionar el numero de comidas al dia del paciente!", "error");
            form_habit_validado = false;
        }else if(typeof $('input:radio[name=grupo_higiene]:checked').attr('id') === "undefined"){
            swal("Campo vacio!", "Debe seleccionar que tan buena es la higiene del paciente!", "error");
            form_habit_validado = false;
        }else if(typeof $('input:radio[name=grupo_cepillada]:checked').attr('id') === "undefined"){
            swal("Campo vacio!", "Debe seleccionar cuantas veces al dia se cepilla el paciente!", "error");
            form_habit_validado = false;
        }

        if(form_habit_validado){
            //inicializacion de variables de habitos
            var idPer = $("#idPer").val();
            var comidas =  $('input:radio[name=grupo_comidas]:checked').attr('id');
            var higiene = $('input:radio[name=grupo_higiene]:checked').attr('id');
            var hilo =  $('#checkbox_hilo').prop('checked');
            var enjuague = $('#checkbox_enjuage').prop('checked');
            var cepillada = $('input:radio[name=grupo_cepillada]:checked').attr('id');
            //se agregan los siguientes campos
            var duros =  $('#checkbox_ali_duros').prop('checked');
            var inmunizacion =  $('#checkbox_inmunizacion').prop('checked');
            var anticonceptivo =  $('#checkbox_metod_anti').prop('checked');
            var embarazo =  $('input:radio[name=grupo_comidas]:checked').attr('id');
            var temperatura =  $('input:radio[name=grupo_temp_ele]:checked').attr('id');

            //reasignacion de valor
            if(hilo === true){
                hilo = '1';
            }else{
                hilo = '0';
            }

            if(enjuague === true){
                enjuague = '1';
            }else{
                enjuague = '0';
            }
            //se agregan validaciones
            if(duros === true){
                duros = '1';
            }else{
                duros = '0';
            }

            if(inmunizacion === true){
                inmunizacion = '1';
            }else{
                inmunizacion = '0';
            }

            if(anticonceptivo === true){
                anticonceptivo = '1';
            }else{
                anticonceptivo = '0';
            }

            $('.page-loader-wrapper').fadeIn();
            //funcion ajax para guardar los pacientes
            $.ajax({
                url:BASE_PATH + 'index.php/Paciente/saveOrUpdateHabitosPaciente',
                type:'POST',
                dataType:'json',
                data:{
                    idPac: idPer,
                    numComidas : comidas,
                    higieneDent : higiene,
                    hiloDent : hilo,
                    enjuagenDent : enjuague,
                    adic : adiccion,
                    cepilladoDent : cepillada,
                    arrayAdic : adicciones,
                    ingesDuros : duros,
                    inmuniza : inmunizacion,
                    antiConcep : anticonceptivo,
                    emabaTri : embarazo,
                    tempElevada : temperatura

                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        //LIMIAMOS CAMPOS
                        $('input:radio[name=grupo_comidas]').attr('checked',false);
                        $('input:radio[name=grupo_higiene]').attr('checked',false);
                        $('#checkbox_hilo').prop('checked', "");
                        $('#checkbox_enjuage').prop('checked', "");
                        $('#checkbox_adiccion').prop('checked', "");
                        $('input:radio[name=grupo_cepillada]').attr('checked',false);

                        //MENSAJE DE EXITO
                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");

                        //pasamos al siguiente formulario
                        $('#op_5').parent().removeClass('disabled');
                        $("#op_5").click(); 
                        $('#op_4').parent().addClass('disabled');

                    }else{
                        $('.page-loader-wrapper').fadeOut();
                        swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                }
            });
        }
    });
});

/**
* Funcion para guardar los padecimientos
* por diente.
*/
$(function(){
    var dienteNum  = '0';

    $("#numDiente").on('change',function(){
        dienteNum = $(this).val();
    });

    $("#btn_estat_diente").click(function(){
        
        var idPer = $("#idPer").val();
        padecimeinto_dental = $('.selectpicker').val();

        if(dienteNum === null || dienteNum === ""){
            swal("Campo vacio!", "Debe de seleccionar un diente para guardar un padecimiento!", "error");
        }else if(padecimeinto_dental.length < 0){
            swal("Campo vacio!", "Debe de selecionar almenos un padecimiento para guardarlo!", "error");
        }else{

            
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Paciente/savePadecimientoDental',
                type:'POST',
                dataType:'json',
                data:{
                    idPaciente : idPer,
                    idDiente : dienteNum,
                    padecimientos : padecimeinto_dental 
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        //$('#optgroup_padecimiento').multiSelect('deselect_all');
                        $('#numDiente').val('0');
                        $('#optgroup_padecimiento').selectpicker('deselectAll');
                        $('#numDiente').selectpicker('deselectAll');
                        //MENSAJE DE EXITO
                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");
                    }else{
                        $('.page-loader-wrapper').fadeOut();
                        $('#optgroup_padecimiento').selectpicker('deselectAll');
                        $('#numDiente').selectpicker('deselectAll');
                        swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                }
            });
            
        }
    });
});

/**
* Funcion para terminar la captura del
* historial clinico del paciente y salir al menu principal
*/
$(function(){
    $('#btn_finish_his').click(function(){
        location.href = BASE_PATH + 'index.php/Home';
    });
});

/**
* Funcion para continuar he ir a atender al paciente
*/

$(function(){
    $('#btn_next_evaluacion').click(function(){
        var idPaciente = $("#idPer").val();
        var form = $("<form action='"+BASE_PATH + 'index.php/Seguimiento/viewEvolucion'+"'' method='post'><input type='text' name='idPac' value='"+idPaciente+"'><input type='text' name='continue' value='1'></form>");
        $(document.body).append(form);
        form.submit();
    });
});

/**
* Funcion para el boton de generar PDF 
* del paciente
*/
$(function(){
    $('#btn_gen_pdf').click(function(){
        var idPaciente = $("#idPer").val();

        if(idPaciente === null || idPaciente === ""){
            swal("Campo vacio!", "El id del paciente es necesario para generar el PDF", "error");
        }else{
            location.href = BASE_PATH + 'index.php/Paciente/getHistoriaPdf/?idPac='+idPaciente;
        }
    });
});
