
var subCadena = [];
var idPaciente = '';
var idCita = '';
var eventoTem = '';
var fechaActual = new Date();

//colores de las citas
var citaCanceladaColor = '#FF5722';
var noAsistioCitaColor = '#F44336';
var supendioCitaColor = '#4CAF50';
var reAgendaCitaColor = '#FF9800';
var agendaDoc = $('#calendar');

var $demoMaskedInput = $('.demo-masked-input');

function getDateToday(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; 
    var yyyy = today.getFullYear();
    
    if(dd<10) {
        dd='0'+dd;
    }

    if(mm<10) {
        mm='0'+mm;
    }
    today = mm+'/'+dd+'/'+yyyy
    return today;
}

/**
* Funcion para el calendario
*/
$(document).ready(function() {
     
    agendaDoc.fullCalendar({
        header: {
            left: 'custom1,custom2, today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: fechaActual,
        minTime: '10:00:00',
        maxTime: '22:00:00',
        navLinks: true, // can click day/week names to navigate views
        businessHours: true, // display business hours
        editable: false,
        eventLimit: true,
        events: CITAS_AGENDADAS,
        eventClick: function(calEvent) {
            subCadena = calEvent.id.split('_');
            idPaciente = subCadena[0];
            idCita = subCadena[1];
            eventoTem = moment(calEvent.start);
            //validamos que sea una cita de la fecha actual
            if(eventoTem.format('MM/DD/YYYY') >= getDateToday()){
                //consultamos estatus de la cita 
                if(consultaEstatusCita(idPaciente)){
                    bloqueaBtnCitaDia(eventoTem.format('MM/DD/YYYY'),getDateToday());
                    $("#modalAtiendeCita").modal('show');
                }else{
                    // si no es un estatus valido lo regresa para captura la informacion del paciente
                    swal("Advertencia!", "La informacion del paciente esta incompleta tendra que llenar el formulario, sera redireccionado ...", "info");
                    setTimeout(function(){
                         let form = $("<form action='"+BASE_PATH + 'index.php/Busqueda'+"'' method='post'><input type='text' name='idCompletForm' value='"+idPaciente+"'></form>");
                        $(document.body).append(form);
                        form.submit();
                    },3000);
                }
            }else{
                swal("warninging!", "la cita no es del dia, por lo tanto no se puede atender!", "info");
            }
        },
        customButtons:{
            custom1: {
                text: 'Anterior',
                click: function() {
                    agendaDoc.fullCalendar('prev');
                    let moment = $('#calendar').fullCalendar('getDate');
                    let antMes = moment.format('MMMM');
                    let antYear = moment.format('YYYY');
                    $('.page-loader-wrapper').fadeIn();
                    // removemos todos los aventos
                    agendaDoc.fullCalendar('removeEvents');
                    getListCitas(antMes,antYear);
                }
            },
            custom2:{
              text: 'Siguiente',
                click: function() {
                    agendaDoc.fullCalendar('next');
                    let moment = $('#calendar').fullCalendar('getDate');
                    let sigMes = moment.format('MMMM');
                    let sigYear = moment.format('YYYY');
                    $('.page-loader-wrapper').fadeIn();
                    getListCitas(sigMes,sigYear);
                }  
            }
        }
    });

    $demoMaskedInput.find('.mobile-phone-number').inputmask('(999) 999-99-99', { placeholder: '(___) ___-__-__' });
});

/**
* Funcion para redireccionar al 
* formulario de nota de evolucion
* se envia el id del paciente y 
* el id de la cita
*/
$(function(){
    $("#btn_aten_cita").click(function(){
        var form = $("<form action='"+BASE_PATH + 'index.php/Seguimiento/viewEvolucion'+"'' method='post'><input type='text' name='idPac' value='"+idPaciente+"'><input type='text' name='idCita' value='"+idPaciente+"'><input type='text' name='idCitaPac' value='"+idCita+"'></form>");
        $(document.body).append(form);
        form.submit();
    });
});

$(function(){
    $("#btn_cancel_cita").click(function(){
        swal({
            title: "¿Esta seguro?",
            text: "Se cancelara la cita!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!",
            closeOnConfirm: false
        }, function () {
            updateCita(idCita, '4', null, null, supendioCitaColor);
        });
    });
});


$(function(){
    $("#btn_no_asis_cita").click(function(){
        updateCita(idCita, '5', null, null, noAsistioCitaColor);
    });
});

$(function(){
    $("#btn_sus_cita").click(function(){
        updateCita(idCita, '3', null, null,noAsistioCitaColor);
    });
});

$(function(){
    $("#btn_reAg_cita").click(function(){        
        $("#modalReAgendaCita").modal('show');
    });
});

$(function(){
    $("#btn_re_agenda_cita").click(function(){

        var fechaCita = $("#fechCita").val();
        var horaCita = $("#horaCita").val();

        updateCita(idCita, '2', fechaCita, horaCita,reAgendaCitaColor);
    });
});

/**
* Funcion para mostrar el datePiker y dateTime
*/
$(function(){

    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        minDate : new Date(),
        time: false
    });

    $('.timepicker').bootstrapMaterialDatePicker({
        format: 'HH:mm',
        clearButton: true,
        date: false,
        shortTime: true
    });
});

function updateCita(cita, status, fechaCi, horaCi,colorCita){

    $.ajax({
        url:BASE_PATH + 'index.php/Seguimiento/updateCita',
        type:'POST',
        dataType:'json',
        data:{
            idCitaPac : cita,
            statCita : status,
            fecha : fechaCi,
            hora : horaCi,
            idPac :idPaciente
        },
        success:function(json) {
            if(json.response_code === "200"){
                //ocultamos la modal
                $("#modalAtiendeCita").modal('hide');
                //actualizamos el color
                eventoTem.color = colorCita;
                //actuaizamos el evento
                if(status === '2'){
                    //Actualizar para que genere un nuevo evento
                    // y actualiza el color del evento actual para que
                    // cuando se refresque la pagina se vea el cambio que se hizo.s
                    $("#modalReAgendaCita").modal('hide');
                    eventoTem.start = fechaCi+'T'+horaCi;
                }
                $('#calendar').fullCalendar('updateEvent', eventoTem);
                //mostramos mensaje de exito
                swal("Exitos!", "Se actualizo la cita con exito!", "success");
            }
        },
        error : function(xhr, status) {
            swal("Error!", "Ocurrio un error durante la ejecución", "error");
        }
    });
}

function bloqueaBtnCitaDia(fechaCita, fechaHoy){

  if(fechaCita === fechaHoy){
    $("#btn_aten_cita").removeClass('disabled');
  }else{
    $("#btn_aten_cita").addClass('disabled');
  }

}

function getListCitas(month, year){
    $.ajax({
        url:BASE_PATH + 'index.php/Seguimiento/getCalendarQuotes',
        type:'POST',
        dataType:'json',
        data:{
            mesCon: month,
            yearCon: year
        },
        success:function(json) {
            
            if(json.response_code === "200"){
                var citas_pasadas = new Array();
                $.each(json.citasMes, function(i,cita){
                    citas_pasadas.push(
                            {
                                id: cita.ID_PAC_FK+'_'+cita.ID_CITA_PK,
                                title: cita.title,
                                start: cita.start+'T'+cita.HORA_CITA,
                                color: '#3F51B5'
                            }
                        );
                });
                agendaDoc.fullCalendar('addEventSource',citas_pasadas);
                $('.page-loader-wrapper').fadeOut();
            }else if(json.response_code === "201") {
                $('.page-loader-wrapper').fadeOut();
                agendaDoc.fullCalendar('addEventSource',{});
            }
        },
        error : function(xhr, status) {
            // quitamos el logo de espera
            $('.page-loader-wrapper').fadeOut();
            swal("Error!", "Ocurrio un error durante la ejecución", "error");
        }
    });
}

$(function(){
    $("#btnAddCita").click(function(){
        $("#modalAddCita").modal('show');
    });
});

/**
* si es nuevo paciente desplegamos el modal para que agrege campos minimos
* nombre, apeliido paterno, apellido materno y numero para comunicarse con el 
*/
$(function(){

    $("#newPaciente").click(function(){
        $("#modalNewPacForm").modal('show');
    });
});

/**
* Si el paciente ya existe se despliega una busqueda de pacientes
* para solo agendar el numero y la hora
*/
$(function(){

    $("#searchPaciente").click(function(){
        $("#modalSearchPacienteForm").modal('show');
    });
});

/**
* Funcion que guarda la informacion de un paciente nuevo
* regresa el id del paciente y el id de la cita
*/
$(function(){
    $("#saveAddCitaNewPac").click(function(){
        console.log("ahi voy a guardar la info .....");

        let nombrePac = $("#nameNewPac").val();
        let paternoPac = $("#appNewPac").val();
        let maternoPac = $("#apmNewPac").val();
        let telPac = $("#telNewPac").val();
        let fechaPac = $("#fechNewPac").val();
        let horaPac = $("#horaNewPac").val();

        if(nombrePac === null || nombrePac === ""){
            swal("Error!", "El nombre es obligatorio", "error");
        }else if(paternoPac === null || paternoPac === ""){
            swal("Error!", "El apellido paterno es obligatorio", "error");
        }else if(maternoPac === null || maternoPac === ""){
            swal("Error!", "El apellido materno es obligatorio", "error");
        }else if(telPac === null || telPac === ""){
            swal("Error!", "El telefono es obligatorio", "error");
        }else if(fechaPac === null || fechaPac === ""){
            swal("Error!", "La fecha de la cita es obligatorio", "error");
        }else if(horaPac === null || fechaPac === ""){
            swal("Error!", "La hora de la cita es obligatorio", "error");
        }else{

            // todos los campos estan bien vamos a enviarlos

            $.ajax({
                url:BASE_PATH + 'index.php/Seguimiento/addNewCitaPac',
                type:'POST',
                dataType:'json',
                data:{
                    pacName: nombrePac,
                    pacApp: paternoPac,
                    pacApm: maternoPac,
                    pacTel: telPac,
                    pacFech: fechaPac,
                    pacHora: horaPac
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        
                        $("#nameNewPac").val("");
                        $("#appNewPac").val("");
                        $("#apmNewPac").val("");
                        $("#telNewPac").val("");
                        $("#fechNewPac").val("");
                        $("#horaNewPac").val("");

                        newCita = {
                                id: json.id_pac+'_'+json.id_cita,
                                title: nombrePac,
                                start: fechaPac+'T'+horaPac,
                                color: '#009688'
                        };

                        agendaDoc.fullCalendar('renderEvent',newCita);
                        // mostramos la cita en la  agenda y cecrramos las modales
                        $("#modalAddCita").modal('hide');
                        $("#modalNewPacForm").modal('hide');
                        //mostramos mensaje de exito
                        swal('Exitos!', 'Se agendo con exito!', 'success');
                    }else if(json.response_code === '201'){
                        swal('Error!', `Error ${json.response_code} : ${json.response_msg}`, 'error');
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }

    });
});

/**
* Metodo para obtener la busqueda de pacientes por nombre
*/
$(function(){

    $("#searchPacName").click(function(){
        let namePac = $("#nameSearchPac").val();

        if(namePac === null || namePac === ""){
            swal("Error!", "El nombre a buscar no puede estar vacio", "error");
        }else{

            $.ajax({
                url:BASE_PATH + 'index.php/Seguimiento/searchNamePac',
                type:'POST',
                dataType:'json',
                data:{
                    pacNameSearch: namePac
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $("#pac_search_select").empty().append("<option value='0'> - Seleciona un paciente - </option>");
                        if(json.result_name !== null){
                            $("#pac_search_select").removeAttr('disabled');
                            $.each(json.result_name, function(i,name){
                                $("#pac_search_select").append(`<option value=${name.ID_PAC_PK}>${name.PACIENTE}</option>`);
                            });

                        }else{
                            swal("Error!", `No se encontraron coincidencias`, "error");
                        }
                    }else{
                        swal("Error!", `Error:${json.response_code} Mesnaje: ${json.response_msg}`, "error");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
    });


    $("#searchAgendaCita").click(function(){
        
        let pacIdSearch = $("#pac_search_select").val();
        let fechSearch = $("#fechPacExist").val();
        let horaSearch = $("#horaPacExis").val();
        let nameSelect = $("#pac_search_select option:selected").text();
        
        if(pacIdSearch === '0'){
            swal("Error!", "Dese seleccionar un paciente para agendar una cita", "error");
        }else if(fechSearch === ""){
            swal("Error!", "La fecha de la cita es obligatoria", "error");
        }else if(horaSearch === ""){
            swal("Error!", "La hora de la cita es obligatoria", "error");
        }else{

            $.ajax({
                url:BASE_PATH + 'index.php/Seguimiento/saveSearchPacCita',
                type:'POST',
                dataType:'json',
                data:{
                    idPacSearch : pacIdSearch,
                    fechaPacSearch : fechSearch,
                    horaPacSearch : horaSearch,
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        //vaceamos los campos
                        $("#nameSearchPac").val("");
                        $("#pac_search_select").empty().append("<option value='0'> - Seleciona un paciente - </option>");
                        $("#fechPacExist").val("");
                        $("#horaPacExis").val("");
                        // creamos la nueva cita
                        newCitaSe = {
                                id: pacIdSearch+'_'+json.id_cita,
                                title: nameSelect,
                                start: fechSearch+'T'+horaSearch,
                                color: '#009688'
                        };
                        // la agregamos
                        agendaDoc.fullCalendar('renderEvent',newCitaSe);
                        // cerramos las modales
                        $("#modalSearchPacienteForm").modal('hide');
                        $("#modalAddCita").modal('hide');
                        swal("Exitos!", "Se agendo con exito!", "success");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
    });
});

function consultaEstatusCita(paciente){
    let pacienteValidoCita = false;
    $.ajax({
        async: false,
        url:BASE_PATH + 'index.php/Seguimiento/getEstatPac',
        type:'POST',
        dataType:'json',
        data:{
            pacSearch : paciente
        },
        success:function(json) {
            if(json.response_code === "200"){
                pacienteValidoCita = true;
            }
        },
        error : function(xhr, status) {
            swal("Error!", "Ocurrio un error durante la ejecución", "error");
        }
    });
    console.log(pacienteValidoCita);
    return pacienteValidoCita;
}
