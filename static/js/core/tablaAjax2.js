/**
* Variables para la libreria
* antes de esta libreria se deben de cargar 
* 2 variables
* @var PAGINA_ACTUAL la cual contendra la pagina actual
* @var PAGINAS_TOTALES la contendra el numero de paginas
*/
var form = $("<form></form>");
//numero de registros por default
var numeRegistros = 5;

var RANGO_INI = 0;
var RANGO_FIN = 0;
var NUM_BTN_SHOW = 5;
var TEMPORAL_ANT = 0;
/**
* Funcion para agregar el select, el nav con el numero de
* paginas 
*/
$(function(){
    //Agregamos el select que tendra el numero de 
    //registros que se quiran mostrar por pagina
    $(".tabla-paginada").before("<div class=' row col-md-2'><select id='numRegistros' class='form-control'><option value='5' >5</option><option value='10' >10</option><option value='15' >15</option><option value='20' >20</option><option value='50' >50</option><option value='100' >100</option></select></div>");

    //Se Agrega el objeto nav con id : navPaginado y tambien se le agregna los botones siguiente y anterior
    //"<li id='pag-1' class='active'><a href='javascript:void(0);'>1</a></li>"+
    $(".tabla-paginada").after("<nav id='navPaginado' >"+
                                "<ul id='listaPaginas' class='pagination'>"+
                                    "<li id='btn_back_pag'class='disabled'>"+
                                        "<a href='javascript:void(0);' class='waves-effect'>"+
                                            "<i class='material-icons'>chevron_left</i>"+
                                        "</a>"+
                                    "</li>"+
                                    "<li id='btn_next_pag'>"+
                                        "<a class='waves-effect'>"+
                                            "<i class='material-icons'>chevron_right</i>"+
                                        "</a>"+
                                    "</li>"+
                                "</ul>"+
                            "</nav>");

    //Agregarmos los botones al nav
    listaBotones(PAGINAS_TOTALES,'S');
});

/**
* Funcion para recargar la tabla con el numero
* de registros que se seleccione
*/
$(function(){
   $("#numRegistros").on('change',function(){
        numeRegistros = $(this).val();
        // si cambia el numero de registros se vuelve a cargar la tabla sin filtros
        if(numeRegistros > 0){
            preparaAjax(null,URL_PAGINACION,'R');
        }
   });
});

/**
* Funcio  que crea los botones para la paginacion
* primero remueve todos los lementos botones que 
* tangan la clase .btn_pag, despues toma el total 
* de paginas y agrega los botones al nav con id : navPaginado.
* @param $TOTAL_PAGINAS : el numero de paginas a pintar
*/
function listaBotones(TOTAL_PAGINAS,RANGO){

    $(".btn_pag").remove();
    
    if(TOTAL_PAGINAS === '1'){
        $("#btn_next_pag").addClass('disabled');
    }


    // siguiente
    if(RANGO === 'S'){
        RANGO_FIN = RANGO_INI + NUM_BTN_SHOW;
        RANGO_INI = RANGO_INI + 1;
        if(TOTAL_PAGINAS < RANGO_FIN){
            RANGO_FIN = TOTAL_PAGINAS;
        }   
        pintaListaBtn(RANGO_INI,RANGO_FIN);
        RANGO_INI = RANGO_INI - 1;
        TEMPORAL_ANT = RANGO_INI-1;
    }

    //anterior
    if(RANGO === 'A'){

        // verificamos si el numero finbal es multiplo del numero de botones a mostrar
        var residuo = RANGO_FIN % NUM_BTN_SHOW;
        // si el residuo es diferente de cero entonces no es multiplo del numero de botonos a mostrar
        if(residuo !== 0){
            RANGO_FIN = RANGO_FIN - residuo;
        }else{
            RANGO_FIN = RANGO_FIN - NUM_BTN_SHOW;
        }
        //a rango final e incial le quitamos el numero de botones de paginas a mostrar
        RANGO_INI = RANGO_INI - NUM_BTN_SHOW;
        RANGO_INI = RANGO_INI + 1;

        if(RANGO_FIN < NUM_BTN_SHOW){
            RANGO_FIN = NUM_BTN_SHOW;
        }
        //si el rango inicial es 0 se pone en 1 
        if(RANGO_INI ===  0){
            RANGO_INI = 1;
        }

        pintaListaBtn(RANGO_INI,RANGO_FIN);
        RANGO_INI = RANGO_INI - 1;
        TEMPORAL_ANT = RANGO_INI - 1;
    }


    console.log(RANGO_FIN);
    console.log(RANGO_INI);
    console.log(NUM_BTN_SHOW);

    //pintado con datos
    if(RANGO === 'CD'){
        RANGO_INI = RANGO_INI + 1;
        pintaListaBtn(RANGO_INI,parseInt(TOTAL_PAGINAS));

    }
}

/**
* Funcion para hacer el pintado de los botones, recibe
* el inicio del pintado, el final del pintado 
* y empieza a recorrerse en un siclo for
* @param INICIO_COUNT
* @param FINAL_COUNT
*/
function pintaListaBtn(INICIO_COUNT, FINAL_COUNT){

    for (var i=INICIO_COUNT; i<=FINAL_COUNT; i++) {
            if(i === 1){
                $("#btn_next_pag").before("<li id='pag-"+i+"' class='active btn_pag' ><a href='javascript:void(0);' class='waves-effect'>"+i+"</a></li>");
            }else{
                $("#btn_next_pag").before("<li id='pag-"+i+"' class='btn_pag' ><a href='javascript:void(0);' class='waves-effect'>"+i+"</a></li>");
            }
    }

    if( FINAL_COUNT >= NUM_BTN_SHOW){
        $("#btn_next_pag").before("<li class='btn_pag' ><a href='javascript:void(0);' class='waves-effect'>...</a></li>");
        $("#btn_next_pag").before("<li class='btn_pag' ><a href='javascript:void(0);' class='waves-effect'>"+PAGINAS_TOTALES+"</a></li>");
    }
}


/**
* Funcion que prepara el Ajax para hacer la peticion
* recibe lo siguiente FORMULARIO_FILTRO, URL, TIPO
* @param $FORMULARIO_FILTRO : foemulario que se enviara para filtros
* @param $URL : url donde se hara la peticion
* @param $TIPO: que tipo de peticion es 'S', 'A', 'R'
*/
function preparaAjax(FORMULARIO_FILTRO,URL,TIPO){

    if(typeof FORMULARIO_FILTRO !== 'undefined' && FORMULARIO_FILTRO !== null){
        conDatos(FORMULARIO_FILTRO,URL,TIPO);
    }else{
        sinDatos(URL,TIPO);
    }
}

/**
* Funcion que hace la peticion ajax sin datos, valida si
* se pide la pagina siguiente o la pagina anterior, 
* para la pagina siguiente se usa el tipo 'S' cuando la 
* operacion es siguientes a la pagina actual se le suma 1,
* para la pagina anterior se usa el tipo 'N' cuando la 
* operacion es anterior a la pagina actual se le resta 1.
* @param $URL : la url de destino de la peticion
* @param $TIPO : 'S' si es siguiente pagina
*                'A' si es pagina anterior
*                'R' si un reload desde pagina cero
*/
function sinDatos(URL,TIPO){
    // se convierte la pagina actual a int
    var PAGINA_SD = parseInt(PAGINA_ACTUAL);

    if(TIPO === 'S'){
        
        if(PAGINA_SD === 0){
            PAGINA_SD = 1;
        }else{
            PAGINA_SD++;
        }

    }else if(TIPO === 'A'){

        if(PAGINA_SD !== 0){
            PAGINA_SD--;
        }
    }else{
        PAGINA_SD = 0;
    }
    
    $.ajax({
        url: URL,
        type:'POST',
        dataType:'json',
        data:{
            paginaConsulta : PAGINA_SD,
            numeroRegistros : numeRegistros
        },
        success:function(json) {
            if(json.response_code === "200"){

                PAGINA_ACTUAL =  parseInt(json.resultado.paginActual);
                ULTIMA_PAGINA =  parseInt(json.resultado.paginasTotales);

                if(PAGINA_ACTUAL > 0){
                    $("#btn_back_pag").removeClass('disabled');
                }else if(PAGINA_ACTUAL === 0){
                    $("#btn_back_pag").addClass('disabled');
                }

                if(ULTIMA_PAGINA === 0 || (ULTIMA_PAGINA-1) === PAGINA_ACTUAL){
                    $("#btn_next_pag").addClass('disabled');
                }else{
                    $("#btn_next_pag").removeClass('disabled');
                }

                if(ULTIMA_PAGINA < PAGINAS_TOTALES || ULTIMA_PAGINA > PAGINAS_TOTALES){
                    PAGINAS_TOTALES = ULTIMA_PAGINA; 
                    listaBotones(ULTIMA_PAGINA,'S');
                }

                // si la pagina actual es igual alrango se actualiozan los botones y el rango inicial
                //una variable temporal por si se desea regresar
                if(PAGINA_ACTUAL === RANGO_FIN){
                    RANGO_INI = RANGO_FIN;
                    listaBotones(ULTIMA_PAGINA,'S');
                }

                // si el tipo de consulta es anterior y la temporal es igual a la actual
                if(TIPO === 'A' && TEMPORAL_ANT === PAGINA_ACTUAL){
                    if(TEMPORAL_ANT !== 0){
                        listaBotones(ULTIMA_PAGINA,'A');
                    }
                }

                //quitamos la clase active al elemnto que no tenga
                $("#listaPaginas > li.active").removeClass('active');
                
                //agregamos el elemnto active a la pagina actual
                var paginaSelect = PAGINA_SD + 1;
                $("#pag-"+paginaSelect).addClass('active');

                rellenaTabla(json);
            }else{
                rellenaTabla(json);
            }
        },
        error : function(xhr, status) {
             swal("Error!", "Ocurrio un error durante la ejecución", "error");
        }
    });
}


/**
* Funcion para hacer la peticion ajax con filtros
* @param $FILTROS : filtros que se aplique para la busqueda
* @param $URL : la url a donde se hara la petición
* @param $TIPO : 'S' si es pagina siguiente 
*                'A' si es pagina anterior 
*                'R' si es recargar desde cero
*/
function conDatos(FILTROS,URL,TIPO){

    var PAGINA_CD = parseInt(PAGINA_ACTUAL);

    if(TIPO === 'S'){
        
        if(PAGINA_CD === 0){
            PAGINA_CD = 1;
        }else{
            PAGINA_CD++;
        }

    }else if(TIPO === 'A'){

        if(PAGINA_CD !== 0){
            PAGINA_CD--;
        }
    }else if(TIPO === 'R'){
        PAGINA_CD = 0;
    }

    form.append("<input type='text' id='paginaConsulta' name= 'paginaConsulta' class='input_pag' value='"+PAGINA_CD+"'>");
    form.append("<input type='text' id='numeroRegistros' name='numeroRegistros' class='input_pag' value='"+numeRegistros+"'>");
    
    //llenamos el formulario
    for (var i = 0; i < FILTROS.length; i++) {
        form.append("<input type='text' id='filtro_"+i+"' name='filtro_"+i+"' class='input_pag' value='"+FILTROS[i]+"'>");
    }

    $.ajax({
        url: URL,
        type:'POST',
        dataType:'json',
        data:form.serialize(),
        success:function(json) {
            if(json.response_code === "200"){
                //vaciamos el formulario
                form.find('.input_pag').remove();
                
                PAGINA_ACTUAL =  parseInt(json.resultado.paginActual);
                ULTIMA_PAGINA =  parseInt(json.resultado.paginasTotales);
                
                if(PAGINA_ACTUAL > 0){
                    $("#btn_back_pag").removeClass('disabled');
                }else if(PAGINA_ACTUAL === 0){
                    $("#btn_back_pag").addClass('disabled');
                }

                if(ULTIMA_PAGINA === 0 || (ULTIMA_PAGINA-1) === PAGINA_ACTUAL){
                    $("#btn_next_pag").addClass('disabled');
                }else{
                    $("#btn_next_pag").removeClass('disabled');
                }

                // si el numero de paginas sufre un cambio se vuelve a pintar el paginado
                if(ULTIMA_PAGINA < PAGINAS_TOTALES || ULTIMA_PAGINA > PAGINAS_TOTALES){
                    console.log("si entra al repaginado con datos");
                    PAGINAS_TOTALES = ULTIMA_PAGINA; 
                    listaBotones(ULTIMA_PAGINA,'CD');
                }

                // si la pagina actual es igual alrango se actualiozan los botones y el rango inicial
                //una variable temporal por si se desea regresar
                if(PAGINA_ACTUAL === RANGO_FIN){
                    RANGO_INI = RANGO_FIN;
                    listaBotones(ULTIMA_PAGINA,'S');
                }

                // si el tipo de consulta es anterior y la temporal es igual a la actual
                if(TIPO === 'A' && TEMPORAL_ANT === PAGINA_ACTUAL){
                    if(TEMPORAL_ANT !== 0){
                        listaBotones(ULTIMA_PAGINA,'A');
                    }
                }

                //quitamos la clase active al elemnto que no tenga
                $("#listaPaginas > li.active").removeClass('active');
                
                //agregamos el elemento active a la pagina actual
                var paginaSelect = PAGINA_CD + 1;
                $("#pag-"+paginaSelect).addClass('active');

                rellenaTabla(json);
            }else{
                rellenaTabla(json);
            }
        },
        error : function(xhr, status) {
            //en caso de que falle se limpiar el formulario para que cuando este activo
            // el servicio de nuevo no envie doble la peticion
            form.find('.input_pag').remove();
            swal("Error!", "Ocurrio un error durante la ejecución", "error");
        }
    });

}


