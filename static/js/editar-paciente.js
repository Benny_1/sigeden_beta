/**
 * @author Noe Ramos
 * @version 0.1
 * @copyright Noe ramos lopez, Todos los derechos reservados 2017
*/

var familiarId = '0';
var enfermedad_patologica_familiar = []; 
var enfermedad_patologica_personal = [];
var padecimeinto_dental = [];
var adicciones = [];
var idPariente = '0';

//URL para la paginacion 
var URL_PAGINACION = BASE_PATH + 'index.php/Busqueda/getPaginacion';

$(function(){

    /**
    * Funcion para obtener las enfermadeades patologicas familiares
    */
    $('#optgroup_familiar').multiSelect({
        afterSelect: function(values){
            enfermedad_patologica_familiar.push(values[0]);
        },
        afterDeselect: function(values){
                
            var ultimoValor = enfermedad_patologica_familiar.length;
                ultimoValor = ultimoValor-1
                
            for(var i = 0; i <= ultimoValor; i++){

                var valorTem = enfermedad_patologica_familiar[i];
                var valorEliminar = values;
                if(parseInt(valorTem) === parseInt(valorEliminar)){
                    enfermedad_patologica_familiar.splice(i,1);
                }
            }
        }
    });

    /**
    * Funcion para obtener las enferemadeades personales
    */
    $('#optgroup_personal').multiSelect({
        afterSelect: function(values){
            enfermedad_patologica_personal.push(values[0]);
        },
        afterDeselect: function(values){
            var ultimoValor = enfermedad_patologica_personal.length;
            ultimoValor = ultimoValor-1

            for(var i = 0; i <= ultimoValor; i++){

                var valorTem = enfermedad_patologica_personal[i];
                var valorEliminar = values;

                if(parseInt(valorTem) === parseInt(valorEliminar)){
                    enfermedad_patologica_personal.splice(i,1);
                }
            }
        }
    });

    /**
    * Funcion para el optionGroup de adicciones
    * agregandolas a un array
    */
    $('#optgroup_adiccion').multiSelect({
        afterSelect: function(values){
            adicciones.push(values[0]);
        },
        afterDeselect: function(values){
            var ultimoValor = adicciones.length;
            ultimoValor = ultimoValor-1;
            for(var i = 0; i <= ultimoValor; i++){

                var valorTem = adicciones[i];
                var valorEliminar = values;
                    
                if(parseInt(valorTem) === parseInt(valorEliminar)){
                    adicciones.splice(i,1);
                }
            }
        }
    });
});

/**
* Funcion para el que al momento de seleciconar
* que el paciente tiene alguna adiccion muestre
* una modal para selecionar que tipo de adicciones tiene.
*/
$(function(){

    $( '#checkbox_adiccion' ).on( 'click', function() {
        if($(this).is(':checked')){
            $("#optionToxi").fadeIn();
        }else{
            $("#optionToxi").fadeOut();
        }
    });
});

/**
* Funcion para obtener la informacion 
* del paciente en base al id que se selecciona,
* llena el formulario y muestra la modal
* @param idPac : id del paciente.
*/
$(function(){
    
    $("#table_paciente").on('click','.edit-paciente',function(){
        idPac = $(this).attr('data-id');
            $.ajax({
                url:BASE_PATH + 'index.php/Busqueda/getInfoPatient',
                type:'POST',
                dataType:'json',
                data:{
                   idPaciente : idPac
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        //SE CARGA LA INFORMACION BASICA DEL PACIENTE
                        $('#nombre_p').val(json.infoPer.NOMBRE_PAC);
                        $('#paterno_p').val(json.infoPer.APP_PAC);
                        $('#materno_p').val(json.infoPer.APM_PAC);
                        $('#direccion').val(json.infoPer.DIRECCION_PAC);
                        $('#correo').val(json.infoPer.CORREO_PAC);
                        $('#fecha_nac').val(json.infoPer.FECNAC_PAC);
                        $('#num_tel').val(json.infoPer.TEL_PAC);
                        $('#editPacienteId').val(json.infoPer.ID_PAC_PK);
                        $('#obs_pac').val(json.infoPer.OBSER_PAC);
                        
                        $('#lugar_nac').selectpicker('val',json.infoPer.ID_EST_FK);
                        $('#ocupacion').selectpicker('val',json.infoPer.ID_PROF_FK);
                        $('#est_civil').selectpicker('val',json.infoPer.EST_CIVIL_PAC);

                        if(json.infoPer.GENERO_PAC === 'F'){
                            generoPer = 'mujer';
                            $("input:radio[name=grupo_genero][id='"+generoPer+"']").attr('checked',true);
                            $("#isWoman").css('display','block');
                        }else{
                            generoPer = 'hombre';
                            $("input:radio[name=grupo_genero][id='"+generoPer+"']").attr('checked',true);
                            $("#isWoman").css('display','none');
                        }

                        //SE CARGAN LAS ENFERMEDADES FAMILIARES
                        $.each(json.enfermedadPersonal, function(i,ep){
                            $('#optgroup_personal').multiSelect('select',ep.ID_ENFER_FK);
                        });
                        
                        //VALIDAMOS QUE NO ESTEN NULOS LOS HABITOS
                        if(json.habitos !== null){
                            console.log("los habitos estan vacios....");
                            //SE CARGAN LOS HABITOS
                            if(json.habitos.ENJ_DENTAL === '1'){
                                $('#checkbox_enjuage').prop('checked','checked');
                            }

                            if(json.habitos.HIL_DENTAL === '1'){
                                $('#checkbox_hilo').prop('checked','checked');
                            }

                            if(json.habitos.INGES_ALIME_DURO === '1'){
                                $('#checkbox_ali_duros').prop('checked','checked');
                            }

                            if(json.habitos.INMU === '1'){
                                $('#checkbox_inmunizacion').prop('checked','checked');
                            }

                            if(json.habitos.METOD_ANTI_CONCEP === '1'){
                                $('#checkbox_metod_anti').prop('checked','checked');
                            }

                            $('input:radio[name=grupo_comidas][id='+json.habitos.NUM_COMI+'_c]').attr('checked',true);
                            $('input:radio[name=grupo_higiene][id='+json.habitos.HIG_DENTAL+'_h]').attr('checked',true);
                            $('input:radio[name=grupo_cepillada][id='+json.habitos.NUM_SEP+'_cpd]').attr('checked',true);
                            $('input:radio[name=grupo_temp_ele][id='+json.habitos.INGES_TEMP_ELEVAD+'_elevada]').attr('checked',true);
                            $('input:radio[name=grupo_embarazo][id='+json.habitos.TRIME_EMBAR+'_t]').attr('checked',true);
                        }

                        if(json.ADICCION === 'true'){
                            $('#checkbox_adiccion').prop('checked','checked');
                        }

                        $("#table_padecimiento tbody").html("");
                        if(json.dientes !== null){
                            $.each(json.dientes, function(i,d){

                                var newRow = "<tr id='tr-"+d.ID_DIENTE_FK+"'>"
                                                +"<td>"+d.NUM_DIENTE+"</td>"
                                                +"<td>"+d.DESC_PAD+"</td>"
                                                +"<td>"                      
                                                +"</td>"
                                            +"</tr>";
                                $(newRow).appendTo("#table_padecimiento tbody");
                            });
                        }else{
                            var newRow = `<tr> <td colspan='3' class='align-center'>No hay Datos</td> </tr>`;
                            $(newRow).appendTo("#table_padecimiento tbody");
                        }

                        if(json.quotes !== null){
                            $(".quotes-delete").remove();
                            $.each(json.quotes, function(i,q){
                                let newRenglon = llenarFilaQuote(q,i);
                                $("#acordionNotas").append(newRenglon);
                            });
                        }else{
                            $(".quotes-delete").remove();
                            $("#acordionNotas").append(`<h3 class="text-center quotes-delete">Sin información por el momento</h3>`);
                        }
                        //MOSTRAMOS LA MODAL
                        $("#modalPaciente").click();
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecucion", "error");
                }
            });
    });
});

/**
* Funcion que detecta el click del 
* boton elminar para dar de baja logica
* a un paciente, toma el id del paciente
* y lo manda en caso de exito 
* responde un response_code 200
*/
$(function(){
    
    $("#table_paciente").on('click','.delete-paciente',function(){
        var idPaciente = $(this).attr('data-id');
        console.log("el id del paciente a eliminar"+idPaciente);

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url:BASE_PATH + 'index.php/Busqueda/bajaPaciente',
                type:'POST',
                dataType:'json',
                data:{
                    idPac : idPaciente
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $("#tr-"+idPaciente).remove();
                        swal("Borrado!", "El campo fue borrado de la base de datos.", "success");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        });
        
    });
});

/**
* Funcion para editar la informacion personal
* del paciente.
*/
$(function(){
        //Formulario general 
    $("#editPatientInfoPer").click(function(){

        console.log("Boton para editar ");
        var form_dp_validado = true;

        if($('#nombre_p').val() === ""){
            $('#nombre_p').parent().attr("class","form-line focused error");
            swal("Campo vacio!", "EL nombre no puede estar vacio!", "error");
            form_dp_validado = false;
        }else if($('#paterno_p').val() === ""){
            $('#paterno_p').parent().attr("class","form-line focused error");
            swal("Campo vacio!", "EL Apellido Paterno no puede estar vacio!", "error");
            form_dp_validado = false;
        }else if($('#materno_p').val() === ""){
            $('#materno_p').parent().attr("class","form-line focused error");
            swal("Campo vacio!", "EL Apellido Materno no puede estar vacio!", "error");
            form_dp_validado = false;
        }else if($('#fecha_nac').val() === ""){
            $('#fecha_nac').parent().attr("class","form-line focused error");
            swal("Campo vacio!", "La fecha de nacimiento no puede estar vacia!", "error");
            form_dp_validado = false;
        }else if(typeof $('input:radio[name=grupo_genero]:checked').attr('id') == "undefined"){
            swal("Campo vacio!", "Debe seleccionar un Genero para el Paciente!", "error");
            form_dp_validado = false;
        }else if($("#lugar_nac").val() === '0'){
            swal("Campo vacio!", "El lugar de nacimiento no puede estar vacia!", "error");
            form_dp_validado = false;
        }
        
        if(form_dp_validado){

            generoPersona = $('input:radio[name=grupo_genero]:checked').attr('id');

            if(generoPersona === 'mujer'){
                generoPersona = 'F';
            }else{
                generoPersona = 'M';
            }
            
            $.ajax({
                url:BASE_PATH + 'index.php/Paciente/saveOrUpdateInfoPatient',
                type:'POST',
                dataType:'json',
                data:{
                    idPatient: $('#editPacienteId').val(),
                    namePatient: $('#nombre_p').val(),
                    appPatient: $('#paterno_p').val(),
                    apmPatient: $('#materno_p').val(),
                    fecNac: $('#fecha_nac').val(),
                    estado: $('#lugar_nac').val(),
                    direccion: $('#direccion').val(),
                    genero: generoPersona,
                    profesion: $('#ocupacion').val(),
                    religion: $('#religion').val(),
                    estadoCivil: $('#est_civil').val(),
                    numTel: $('#num_tel').val(),
                    correo: $('#correo').val(),
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecucion", "error");
                }
            });
            
        }
    });
});

/**
* Funion para cargar las 
* enfermedades de cada familiar
* recibe el id del paciente y el id del familiar
* @param idFam : id Familiar
* @param idPac : id Paciente 
*/
$(function(){
    $('#select_familiar').on('change',function(){
        idPariente = $(this).val(); 
        var idFam = $(this).val();
        var idPac = $("#editPacienteId").val();

        if(idFam !== '0' || idFam !== ""){

            $.ajax({
                url:BASE_PATH + 'index.php/Busqueda/getInfoFamiliar',
                type:'POST',
                dataType:'json',
                data:{
                    idFamiliar : idFam,
                    idPaciente: idPac
                },
                success:function(json) {
                    if(json.response_code === "200"){

                        while(enfermedad_patologica_familiar.length > 0){
                                enfermedad_patologica_familiar.pop();
                        }
                        $('#optgroup_familiar').multiSelect('deselect_all');
                        
                        $.each(json.enferFamiliar, function(i,ef){
                            $('#optgroup_familiar').multiSelect('select',ef.ID_ENFER_FK);
                        });

                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecucion", "error");
                }
            });

        }

    });
});

/**
* Funcion para actualizar la informacion de los
* antecedentes de enfermedades familiares
*/
$(function(){

    $("#editPatientFamiliar").click(function(){
         
        var idPac = $("#editPacienteId").val();

        if(idPariente !== "0" || idPariente !== null){

            $.ajax({
                    url:BASE_PATH + 'index.php/Busqueda/updateEnfermedadFamiliar',
                    type:'POST',
                    dataType:'json',
                    data:{
                        idPaciente : idPac,
                        idFamiliar : idPariente,
                        enfermedades : enfermedad_patologica_familiar
                    },
                    success:function(json) {
                        if(json.response_code === "200"){

                            while(enfermedad_patologica_familiar.length > 0){
                                enfermedad_patologica_familiar.pop();
                            }
                            swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");
                        }else{
                            swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                        }
                    },
                    error : function(xhr, status) {
                        swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                    }
                });
        }
    });
});

/**
* Funcion para editar las enfermedades 
* patologicacs del paciente.
* @param idPaciente : identificador del paciente
* @param enfermedad_patologica_personal : arreglo de enfermedades
*/
$(function(){
    $('#editPatientEnfermedad').click(function(){
        //id del paciente
        var idPaciente = $("#editPacienteId").val();
        console.log(enfermedad_patologica_personal.length);

            $.ajax({
                url:BASE_PATH + 'index.php/Busqueda/updateEnfermedadPatologica',
                type:'POST',
                dataType:'json',
                data:{
                    idPac : idPaciente,
                    enfermedades : enfermedad_patologica_personal
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");
                    }else{
                        swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                    }
                },
                error : function(xhr, status) {
                    swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                }
            });

    });
});

/**
* Funcion para editar los habitos del paciente
* 
*/
$(function(){

    $('#editPatientHabitos').click(function(){
        //variable para validar el formulario
        var form_habit_validado = true;
        var adiccion = $('#checkbox_adiccion').prop('checked');

        if(adiccion === true){
            adiccion = '1';
            console.log(adicciones.length);
            if(adicciones.length < 1 ){
                swal("Campo vacio!", "Se seleciono que el paciente tiene una adiccion favor de escojer almenos 1!", "error");
                form_habit_validado = false;
            }
        }else{
            adiccion = '0';
        }
        //validamos que se seleccion alguna opcion
        if(typeof $('input:radio[name=grupo_comidas]:checked').attr('id') === "undefined"){
            swal("Campo vacio!", "Debe seleccionar el numero de comidas al dia del paciente!", "error");
            form_habit_validado = false;
        }else if(typeof $('input:radio[name=grupo_higiene]:checked').attr('id') === "undefined"){
            swal("Campo vacio!", "Debe seleccionar que tan buena es la higiene del paciente!", "error");
            form_habit_validado = false;
        }else if(typeof $('input:radio[name=grupo_cepillada]:checked').attr('id') === "undefined"){
            swal("Campo vacio!", "Debe seleccionar cuantas veces al dia se cepilla el paciente!", "error");
            form_habit_validado = false;
        }

        if(form_habit_validado){
            //inicializacion de variables de habitos
            var idPer = $("#idPer").val();
            var comidas =  $('input:radio[name=grupo_comidas]:checked').attr('id');
            var higiene = $('input:radio[name=grupo_higiene]:checked').attr('id');
            var hilo =  $('#checkbox_hilo').prop('checked');
            var enjuague = $('#checkbox_enjuage').prop('checked');
            var cepillada = $('input:radio[name=grupo_cepillada]:checked').attr('id');
            //se agregan campos
            var duros =  $('#checkbox_ali_duros').prop('checked');
            var inmunizacion =  $('#checkbox_inmunizacion').prop('checked');
            var anticonceptivo =  $('#checkbox_metod_anti').prop('checked');
            var embarazo =  $('input:radio[name=grupo_comidas]:checked').attr('id');
            var temperatura =  $('input:radio[name=grupo_temp_ele]:checked').attr('id');

            //reasignacion de valor
            if(hilo === true){
                hilo = '1';
            }else{
                hilo = '0';
            }

            if(enjuague === true){
                enjuague = '1';
            }else{
                enjuague = '0';
            }
            
            //se agregan validaciones
            if(duros === true){
                duros = '1';
            }else{
                duros = '0';
            }

            if(inmunizacion === true){
                inmunizacion = '1';
            }else{
                inmunizacion = '0';
            }

            if(anticonceptivo === true){
                anticonceptivo = '1';
            }else{
                anticonceptivo = '0';
            }

            $('.page-loader-wrapper').fadeIn();
            //funcion ajax para guardar los pacientes
            $.ajax({
                url:BASE_PATH + 'index.php/Paciente/saveOrUpdateHabitosPaciente',
                type:'POST',
                dataType:'json',
                data:{
                    idPac: $('#editPacienteId').val(),
                    numComidas : comidas,
                    higieneDent : higiene,
                    hiloDent : hilo,
                    enjuagenDent : enjuague,
                    adic : adiccion,
                    cepilladoDent : cepillada,
                    arrayAdic : adicciones,
                    ingesDuros : duros,
                    inmuniza : inmunizacion,
                    antiConcep : anticonceptivo,
                    emabaTri : embarazo,
                    tempElevada : temperatura
                },
                success:function(json) {
                    if(json.response_code === "200"){

                        //QUITAMOS EL PAGE LOADER
                        $('.page-loader-wrapper').fadeOut();

                        //MENSAJE DE EXITO
                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");

                    }else{
                        $('.page-loader-wrapper').fadeOut();
                        swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                }
            });
        }
    });
});


$(function(){

    $("#numDentEdit").on("change", function(){
        var diente = $(this).val();
        var idPac = $("#editPacienteId").val();
        let tiene_pad = false;
        if(diente !== 0 && diente !== null){

            if(diente.length === 1){
                
                $.ajax({
                    url:BASE_PATH + 'index.php/Busqueda/getPadDent',
                    type:'POST',
                    dataType:'json',
                    data:{
                        idPaciente : idPac,
                        idDiente : diente[0]
                    },success:function(json) {
                        if(json.response_code === "200"){
                            if(json.padecimientos !== null){
                                
                                $('.selectpicker').selectpicker('deselectAll');
                                var selectPad = [];
                                $.each(json.padecimientos, function(i,p){
                                    selectPad.push(p.ID_PAD_FK);
                                });
                                $('.selectpicker').selectpicker('val',selectPad);
                                tiene_pad = true;
                            }else{
                                $('.selectpicker').selectpicker('deselectAll');
                                tiene_pad = false;
                            }
                        }else{
                            swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                        }
                    },error : function(xhr, status) {
                        swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                    }
                });
            }else{

                if(tiene_pad){
                    console.log("el diente tiene un padecimeinto solo se permite un diante por padecimiento");
                }
            }
        }
    });
});


$(function(){
    var dienteNum  = '0';
    var pad_dent_edit = [];
    $("#numDentEdit").on('change',function(){
        dienteNum = $(this).val();
    });

    $("#editPadDental").on('click', function(){
        var idPacEdit = $("#editPacienteId").val();
        pad_dent_edit = $('.selectpicker').val();

        if(dienteNum === null || dienteNum === ""){
            swal("Campo vacio!", "Debe de seleccionar un diente para guardar un padecimiento!", "error");
        }else if(pad_dent_edit.length < 0){
            swal("Campo vacio!", "Debe de selecionar almenos un padecimiento para guardarlo!", "error");
        }else{
            $('.page-loader-wrapper').fadeIn();

            $.ajax({
                url:BASE_PATH + 'index.php/Paciente/updatePadDent',
                type:'POST',
                dataType:'json',
                data:{
                    idPaciente : idPacEdit,
                    ID_PAD_FK : dienteNum,
                    padecimientos : pad_dent_edit,
                    formFull: FORMULARIO_INCOMPLETO_PAC
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();

                        $('#numDentEdit').val('0');
                        $('#optgroup_pad_edit').selectpicker('deselectAll');
                        $('#numDentEdit').selectpicker('deselectAll');

                        $("#table_padecimiento tbody").html("");
                            $.each(json.tabPadDent, function(i,d){

                                var newRow = "<tr id='tr-"+d.ID_DIENTE_FK+"'>"
                                                +"<td>"+d.NUM_DIENTE+"</td>"
                                                +"<td>"+d.DESC_PAD+"</td>"
                                                +"<td>"                      
                                                +"</td>"
                                            +"</tr>";
                                $(newRow).appendTo("#table_padecimiento tbody");
                            });
                        
                        swal("Registro Exitoso!", "Se guardo la informacion Correctamente!", "success");
                    }else{
                        $('.page-loader-wrapper').fadeOut();
                        swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                }
            });
        }

    });
});


/**
* Funcion para poder agendar una cita a un paciente,
* en caso de que ya cuente con una se mostrar un mnesaje de que
* el paciente ya esta agendado, en caso contrario mostrara 
* una modal para para agendar una cita.
*/
$(function(){

    $("#table_paciente").on('click','.cita-paciente',function(){
        var idPaciente = $(this).attr('data-id');

        $.ajax({
            url:BASE_PATH + 'index.php/Busqueda/getCitaPatient',
            type:'POST',
            dataType:'json',
            data:{
                idPac : idPaciente,
            },
            success:function(json) {
                if(json.response_code === "200"){
                    $("#modalAgendaCitaPac").modal('show');
                    $("#idPac").val(json.paciente);
                }else if(json.response_code === "201"){
                    swal("Existente!", "EL paciente ya tiene una cita agendada","info");
                }else{
                    swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                }
            },
            error : function(xhr, status) {
                swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
            }
        });

    });
});

/**
* Funcion que detecta el click al boton siguiente 
* de la paginacion valida si hay algun filtro ,
* en caso contrario se manda a pedir sin filtro 
* se manda el formulario en caso de existir, 
* la URL de destino y si es tipo siguiente 'S'
* @param $filtro : opcional
* @param $URL : url de destipo
* @param $TIPO : 'S' de siguiente
*/
$(function(){
    $(document).on('click','#btn_next_pag > a',function(){
        //variables
        var filtro = $("#name_filter").val();
        var filtro1 = $("#app_filter").val();
        var filtro2 = $("#apm_filter").val();
        var tieneFiltro = false;
        var parametros = [];

        //validaciones si tiene filtros
        if(filtro !== null && filtro !== ""){
            parametros.push(filtro);
            tieneFiltro = true;
        }else if(filtro1 !== null && filtro1 !== ""){
            parametros.push(filtro1);
            tieneFiltro = true;
        }else if(filtro2 !== null && filtro2 !== ""){
            parametros.push(filtro2);
            tieneFiltro = true;
        }else{
            tieneFiltro = false;
        }

        //validar si hacer la peticion con filtro
        if(tieneFiltro === true){
            preparaAjax(parametros,URL_PAGINACION,'S');
        }else{
            preparaAjax(null,URL_PAGINACION,'S');
        }

    });
});

/**
* Funcion que detecta el click del boton anterior
* de la paginacion , valida si ay algun filtro para
* para realizar la peticion en caso contrario se 
* se manda nulo, se envia tambien los parametros
* de URL y tipo 'A'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'A' de anterior
*/
$(function(){
    $(document).on('click','#btn_back_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'A');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'A');     
        }
    });
});

/**
* Funcion que detecta el click del boton de filtro
* recibe el filtro en caso de que sea nulo recarga
* la tabla desde cero, se envia URL y tipo 'R'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'R' de recargar o reload
*/
$(function(){
    $("#form_filtro").submit(function(){
        
        var filtro = $("#name_filter").val();
        var filtro1 = $("#app_filter").val();
        var filtro2 = $("#apm_filter").val();
        var tieneFiltro = false;
        var parametros = [];

        parametros.push(filtro);
        parametros.push(filtro1);
        parametros.push(filtro2);

        preparaAjax(parametros,URL_PAGINACION,'R');
    });
});

/**
* Boton que limpia todos los campos 
* cuando se cierra la modal de edicion 
* de informacion del paciente.
*/
$(function(){
    $('#cerrar_modal_info').click(function(){
        
        //limpiamos informacion general del paciente
        $('#nombre_p').val("");
        $('#paterno_p').val("");
        $('#materno_p').val("");
        $('#lugar_nac').selectpicker('val','');
        $('#direccion').val("");
        $('#ocupacion').selectpicker('val','');
        $('#est_civil').selectpicker('val','');
        $('#correo').val("");
        $('#fecha_nac').val("");
        $('#num_tel').val("");
        $('input:radio[name=grupo_genero]:checked').val('off');

        //limpiamos patologias personales
        $('#optgroup_personal').multiSelect('deselect_all');

        //limpiamos campos de habitos
        $('input:radio[name=grupo_comidas]:checked').val('off');
        $('input:radio[name=grupo_higiene]:checked').val('off');
        $('#checkbox_hilo').prop('checked', "");
        $('#checkbox_enjuage').prop('checked', "");
        $('#checkbox_adiccion').prop('checked', "");
        $('input:radio[name=grupo_cepillada]:checked').val('off');
        //campos nuevos
        $('#checkbox_ali_duros').prop('checked',"");
        $('#checkbox_inmunizacion').prop('checked',"");
        $('#checkbox_metod_anti').prop('checked',"");
        $('input:radio[name=grupo_comidas]:checked:checked').val('off');
        $('input:radio[name=grupo_temp_ele]:checked:checked').val('off');
        $(".quotes-delete").remove();
    });
});

/**
* Funcion para mostrar el datePiker y dateTime
*/
$(function(){

    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        minDate : new Date(),
        time: false
    });

    $('.timepicker').bootstrapMaterialDatePicker({
        format: 'HH:mm',
        clearButton: true,
        date: false,
        shortTime: true
    });
});


/**
* Funcion para agendar la cita del paciente
* recibe la hora y la fecha de la cita, si
* es exitoso regresa un response code 200
* @param fecha
* @param hora
*/
$(function(){
    $("#btn_agenda_cita").click(function(){

        var fecha = $("#fechCita").val(); 
        var hora = $("#horaCita").val();
        var paciente = $("#idPac").val();

        console.log("valor fecha : "+fecha);
        console.log("valor hora : "+hora);
        console.log("valor paciente : "+paciente);

        if(hora === ''){
            console.log(`si entro a la validacion${hora}`);
            swal("Error!", "Hora vacia", "error");
        }else{
            
            $.ajax({
                url:BASE_PATH + 'index.php/Busqueda/agendaCitaPac',
                type:'POST',
                dataType:'json',
                data:{
                    fechaCita : fecha,
                    horaCita : hora, 
                    pacCita : paciente
                },
                success:function(json) {
                    if(json.response_code === "200"){

                        swal("Exito!", "Se agendo la cita con exito","success");
                        $("#modalAgendaCitaPac").modal('hide');
                        $("#fechCita").val("");
                        $("#horaCita").val("");
                        $("#idPac").val("");

                    }else{
                        swal("Campo vacio!", "Error : "+json.response_code +" Mensaje : "+json.response_msg,"error");
                    }
                },
                error : function(xhr, status) {
                    swal("Campo vacio!", "Ocurrio un error durante la ejecución!", "error");
                }
            });
        }

    });
});

/**
* Funcion para el llenado de la tabla
* de profesiones y en caso de que 
* venga nulo se llena con un mensaje 
* de que no hay campos para mostrar
* @param $respuestaJson : json que 
* contiene toda la informacion que se 
* pondra en la tabla
*/
function rellenaTabla(respuestaJson){
    
    //$("#editToxicomania").modal('hide');

    if(respuestaJson.response_code !== '200'){
        swal("Error : "+respuestaJson.response_code, respuestaJson.response_msg, "error");
    }else{

        $("#table_paciente tbody").html("");
        if(respuestaJson.resultado.lista !== null){

            $.each(respuestaJson.resultado.lista, function(i,p){
            var alerta = "class=''";
            if(p.ALERTA === 'SI'){
                alerta = "class='warning'";
            }
            var newRow = "<tr id='tr-"+p.ID_PAC_PK+"' "+alerta+">"
                            +"<td>"+p.NOMBRE+"</td>"
                            +"<td>"+p.EDAD+"</td>"
                            +"<td>"+p.OBSER_PAC+"</td>"
                            +"<td>"
                                +"<button type='button' class='btn btn-danger waves-effect delete-paciente' data-id='"+p.ID_PAC_PK+"' data-toggle='tooltip' data-placement='top' title='Eliminar Paciente'>"
                                    +"<i class='material-icons'>delete_sweep</i>"
                                +"</button>"
                                +"<button type='button' class='btn btn-warning waves-effect edit-paciente' style='margin: 5px' data-id='"+p.ID_PAC_PK+"' data-toggle='tooltip' data-placement='top' title='Editar Paciente'>"
                                    +"<i class='material-icons'>create</i>"
                                +"</button>"
                                +"<button type='button' class='btn btn-info waves-effect cita-paciente' style='margin: 5px' data-id='"+p.ID_PAC_PK+"' data-toggle='tooltip' data-placement='top' title='Agendar Cita'>"
                                    +"<i class='material-icons'>event</i>"
                                +"</button>"
                            +"</td>"
                        +"</tr>";
                $(newRow).appendTo("#table_paciente tbody");

                //$('#nameEditToxicomania').val("");
                //$('#idEditToxicomania').val("");
            });
        }else{
            var newRow = "<tr>"
                            +"<td colspan='4' class='align-center'>No hay Datos</td>"
                        +"</tr>";
            $(newRow).appendTo("#table_paciente tbody");
        }
    }
}

$(function(){

    if(FORMULARIO_INCOMPLETO_PAC === 1){
        console.log("voy a dar click en la tabla ...");        
        $("#tr-"+ID_PAC_INCOM+" > td > button.edit-paciente").click();
        swal("Advertencia!", "Hasta que llene todo el formulario del paciente podra atender la cita", "warning");
    }
});


function llenarFilaQuote(quote,contador){

    let newQuote = `<div class="panel panel-col-black quotes-delete">
        <div class="panel-heading" role="tab" id="headingOne_${contador}">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_${contador}" aria-expanded="false" aria-controls="collapseOne_${contador}">
                    ${quote.FECH_CITA} - ${quote.DSC_PROC}
                </a>
            </h4>
        </div>
        <div id="collapseOne_${contador}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_${contador}">
            <div class="panel-body">
                <div class="col-md-12">
                    
                    <div class="col-md-4">
                        <b>TA Tencion Arterial :</b> <b class="color-rojo">*</b>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">show_chart</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" value="${quote.TA}" disabled>
                            </div>
                        </div>
                    </div>
                                        
                    <div class="col-md-4">
                        <b>FC Frecuencia Cardiaca : </b> <b class="color-rojo">*</b>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">favorite</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" value="${quote.FC}" disabled>
                            </div>
                        </div>
                    </div>
                                        
                    <div class="col-md-4">
                        <b>FR Frecuencia Respiratoria : </b> <b class="color-rojo">*</b>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">broken_image</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" value="${quote.FR}" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">

                    <div class="col-md-10">
                        <h5 class="text-center">Observaciones <b class="color-rojo">*</b></h5> 
                        <div class="form-group">
                            <div class="form-line">
                                <textarea rows="4" class="form-control no-resize" disabled> ${quote.OBS_PROC}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>`;
    return newQuote;
}