
//URL para la paginacion 
var URL_PAGINACION = BASE_PATH + 'index.php/Enfermedad/getPaginacion';

/**
* Funcion para detectar el clik delete el cual 
* hara el borrado logico del registos, envia la peticion
* en caso de ser exitoso regresa un response_code = '200'
* y borra el resgistro de la tabla
* @param idEnf : id de la enfermedad
*/
$(function(){

    $("#table_enfer").on('click','.delete-enf',function(){
        idEnf = $(this).attr('data-id');
        console.log("Id Profesion es "+ idEnf);
        swal({
            title: "¿Esta seguro?",
            text: "El registro se borrar definitivamente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!",
            closeOnConfirm: false
        }, function () {
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Enfermedad/deleteEnfermedad',
                type:'POST',
                dataType:'json',
                data:{
                    idEnfermedad : idEnf
                },
                success:function(json) {
                    $('.page-loader-wrapper').fadeOut();
                    if(json.response_code === "200"){
                        $("#tr-"+idEnf).remove();
                        swal("Borrado!", "El campo fue borrado de la base de datos.", "success");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });      
        });
    });
});

/**
* Funcion para detectar el click del boton
* para ir a buscar la informacion de la 
* enfermedad cuando es exitoso regresa
* un codigo de respuesta = 200
* y muestra una modal con la informacion
* que se puede editar
* @param idEnf : id de la enfermedad a buscar para editar 
*/
$(function(){
     $("#table_enfer").on('click','.edit-enf',function(){

        idEnf = $(this).attr('data-id');

        if(idEnf === null || idEnf === ""){
            swal("Error!", "El id de la profesion no puede estar vacio", "error");
        }else{
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Enfermedad/getInfoEnfermedad',
                type:'POST',
                dataType:'json',
                data:{
                    idEnfermedad : idEnf
                },
                success:function(json) {
                    $('.page-loader-wrapper').fadeOut();
                    if(json.response_code === "200"){
                        
                        $('#idEditEnfermedad').val(json.enfermedad.ID_ENFER_PK);
                        $('#nameEditEnfermedad').val(json.enfermedad.NOMBRE_ENFER);
                        $("#editEnfermedad").modal('show');
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
     });
});

/**
* Funcion para agregar una enfermedad a la tabla
* de enfermedades patologicas, recibe el nombre 
* de la enfermedad para agregarla a la lista
* en caso de exito se regresa un codigo 
* de respuesta = '200'.
* @param nameEnf : nombre de la enfermedad a guardar
*/
$(function(){

    $('#btn_save_enfermedad').click(function(){

        var nameEnf = $('#nombreEnfermedad').val();

        if(nameEnf === null || nameEnf === ""){
            swal("Error!", "El campo de nombre de profesion no puede estar vacio", "error");
        }else{
            var URL_SAVE_ENF = BASE_PATH +'index.php/Enfermedad/saveOrUpdateEnfermedad';
            var parametros = [nameEnf];
            preparaAjax(parametros,URL_SAVE_ENF,'R');
        }
    });
});

/**
* Funcion para actualziar alguna enfermedad
* cuando se es exitoso se regresa un codigo respuesta 200
* @param nameEnf : nombre de la enfermedad 
* @param idEnf : id de la enfermedad
*/
$(function(){
    $('#btn_edit_enfermedad').click(function(){
        var nameEnf = $('#nameEditEnfermedad').val();
        var idEnf = $('#idEditEnfermedad').val();

        if(nameEnf === null || nameEnf === ""){
            swal("Error!", "El campo de nombre de profesion no puede estar vacio", "error");
        }else if(idEnf === null || idEnf === ""){
            swal("Error!", "No hay id de profesion", "error");
        }else{

            var URL_EDIT_ENF = BASE_PATH +'index.php/Enfermedad/saveOrUpdateEnfermedad';
            var parametros = [nameEnf,idEnf];
            var filtro = $("#name_filter").val();

            if(filtro !== null && filtro !== ""){
                parametros = [nameEnf,idEnf,filtro];
            }
            preparaAjax(parametros,URL_EDIT_ENF,'E');
        }
    });
});

/**
* Funcion para guardar limipar el campo de la 
* modal para guardar una nueva enfermedad
*/
$(function(){
    $('#dont_save_enfermedad').click(function(){
        $('#nombreEnfermedad').val("");
    });
});


/**
* Funcion que detecta el click al boton siguiente 
* de la paginacion valida si hay algun filtro ,
* en caso contrario se manda a pedir sin filtro 
* se manda el formulario en caso de existir, 
* la URL de destino y si es tipo siguiente 'S'
* @param $filtro : opcional
* @param $URL : url de destipo
* @param $TIPO : 'S' de siguiente
*/
$(function(){
    $(document).on('click','#btn_next_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'S');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'S');
        }
    });
});

/**
* Funcion que detecta el click del boton anterior
* de la paginacion , valida si ay algun filtro para
* para realizar la peticion en caso contrario se 
* se manda nulo, se envia tambien los parametros
* de URL y tipo 'A'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'A' de anterior
*/
$(function(){
    $(document).on('click','#btn_back_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'A');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'A');     
        }
    });
});

$(function(){
    $("#table_enfer").on('click','.ck_alert',function(){
        var idEnf = $(this).attr('data-id');
        var valorCk = $('#checkbox_alert_'+idEnf).prop('checked');
        //console.log("valor del check "+valorCk);
        $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Enfermedad/alertEnfermedad',
                type:'POST',
                dataType:'json',
                data:{
                    idEnfermedad : idEnf,
                    valorCheck : valorCk
                },
                success:function(json) {
                    $('.page-loader-wrapper').fadeOut();
                    if(json.response_code === "200"){
                        swal("Alerta!", "Se modifico con exito la alerta", "success");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });      


    });
});

/**
* Funcion que detecta el click del boton de filtro
* recibe el filtro en caso de que sea nulo recarga
* la tabla desde cero, se envia URL y tipo 'R'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'R' de recargar o reload
*/
$(function(){
    $("#btn_filto").click(function(){
        var filtro = $("#name_filter").val();
        console.log(filtro);
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'R');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'R');     
        }
    });
});


/**
* Funcion para el llenado de la tabla
* de enfermedades y en caso de que 
* venga nulo se llena con un mensaje 
* de que no hay campos para mostrar
* @param $respuestaJson json que 
* contiene toda la informacion que se 
* pondra en la tabla
*/
function rellenaTabla(respuestaJson){
    
    $("#editEnfermedad").modal('hide');
    $("#formEnfermedad").modal('hide');

    $("#table_enfer tbody").html("");
    if(respuestaJson.resultado.lista !== null){

        $.each(respuestaJson.resultado.lista, function(i,p){

        var cheboxAlert = "<input type='checkbox' id='checkbox_alert_"+p.ID_ENFER_PK+"' data-id='"+p.ID_ENFER_PK+"' class='chk-col-blue ck_alert'/>"
        if(p.ALERT_ENFER === '1'){
            cheboxAlert = "<input type='checkbox' id='checkbox_alert_"+p.ID_ENFER_PK+"' data-id='"+p.ID_ENFER_PK+"' class='chk-col-blue ck_alert' checked />"
        }

        var newRow = "<tr id='tr-"+p.ID_ENFER_PK+"'>"
                        +"<td>"+p.ID_ENFER_PK+"</td>"
                        +"<td>"+p.NOMBRE_ENFER+"</td>"
                        +"<td>"+cheboxAlert+"<label for='checkbox_alert_"+p.ID_ENFER_PK+"'></label>"
                        +"</td>"
                        +"<td>"
                            +"<button type='button' class='btn btn-danger waves-effect delete-enf' data-id='"+p.ID_ENFER_PK+"' data-toggle='tooltip' data-placement='top' title='Eliminar Enfermedad'>"
                                +"<i class='material-icons'>delete_sweep</i>"
                            +"</button>"
                            +"<button type='button' class='btn btn-warning waves-effect edit-enf' style='margin: 5px' data-id='"+p.ID_ENFER_PK+"' data-toggle='tooltip' data-placement='top' title='Editar Enfermedad'>"
                                +"<i class='material-icons'>create</i>"
                            +"</button>"
                        +"</td>"
                    +"</tr>";
            $(newRow).appendTo("#table_enfer tbody");

            $('#nombreEnfermedad').val("");
        });
    }else{
        var newRow = "<tr>"
                        +"<td colspan='3' class='align-center'>No hay Datos</td>"
                    +"</tr>";
        $(newRow).appendTo("#table_enfer tbody");
    }
}