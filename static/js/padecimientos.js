
//URL para la paginacion 
var URL_PAGINACION = BASE_PATH + 'index.php/Padecimiento_dental/getPaginacion';

/**
* Funcion para hacer el borrado logico de un padecimiento
* Se envia el id que se desea eliminar
* si la operacion es exitosa recibe un JSON
* con codigo 200 y se eleimina el campo de la tabla
* @param $idPadec id del padecimiento
*/
$(function(){

    $("#tabla_padecimiento").on('click','.delete-padec',function(){
        
        idPadec = $(this).attr('data-id');

        swal({
            title: "¿Esta seguro?",
            text: "El registro se borrar definitivamente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url:BASE_PATH + 'index.php/Padecimiento_dental/deletePadecimiento',
                type:'POST',
                dataType:'json',
                data:{
                    idPadecimiento : idPadec
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $("#tr-"+idPadec).remove();
                        swal("Borrado!", "El campo fue borrado de la base de datos.", "success");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });      
        });
    });
});

/**
* Funcion para el boton de edicion de padecimiento
* recibe el id del padecimiento por el cual buscara
* se hace la peticion y si regresa un response_code = 200
* mostrar una modal con el nombre del padecimiento para editarlo
* @param $idPadec
*/
$(function(){
     $("#tabla_padecimiento").on('click','.edit-padec',function(){

        idPadec = $(this).attr('data-id');
        var namePadec = $('#nombrePadecimiento').val();

        if(idPadec === null || idPadec === ""){
            swal("Error!", "El id del padecimiento no puede estar vacio", "error");
        }else{
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Padecimiento_dental/getInfoPadecimiento',
                type:'POST',
                dataType:'json',
                data:{
                    idPadecimiento : idPadec
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        $('#idEditPadecimiento').val(json.padecimiento.ID_PAD_PK);
                        $('#nameEditPadecimiento').val(json.padecimiento.DESC_PAD);
                        $("#editPadecimiento").modal('show');

                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
     });
});

/**
* Funcion para el boton de guardar un nuevo padecimiento
* obtiene el nombre del padecimiento y lo envia al webServide
* si es exitosa la operacion regresa un response_code = 200
* @param $namePadec
*/
$(function(){

    $('#btn_save_padecimiento').click(function(){

        var namePadec = $('#nombrePadecimiento').val();

        if(namePadec === null || namePadec === ""){
            swal("Error!", "El campo de nombre de profesion no puede estar vacio", "error");
        }else{

            var URL_SAVE = BASE_PATH +'index.php/Padecimiento_dental/saveOrUpdatePadecimiento';
            var parametros = [namePadec];
            preparaAjax(parametros,URL_SAVE,'R');
        }
    });
});

/**
* Funcion para hacer la edicion de algun padecimiento dental
* Recibe el nombre del padecimiento y el id al cual 
* se le va a hacer la edicion
* @param $namePadec nombre del padecimiento
* @param $idPadec identificador del padecimiento
* @param $filtro filtro del padecimiento
*/
$(function(){
    $('#btn_edit_padecimiento').click(function(){
        var namePadec = $('#nameEditPadecimiento').val();
        var idPadec = $('#idEditPadecimiento').val();

        if(namePadec === null || namePadec === ""){
            swal("Error!", "El campo de nombre de profesion no puede estar vacio", "error");
        }else if(idPadec === null || idPadec === ""){
            swal("Error!", "No hay id de profesion", "error");
        }else{
            var URL_EDIT = BASE_PATH +'index.php/Padecimiento_dental/saveOrUpdatePadecimiento';
            var parametros = [namePadec,idPadec];
            var filtro = $("#name_filter").val();
            console.log(filtro);
            if(filtro !== null && filtro !== ""){
                parametros = [namePadec,idPadec,filtro];
            }
            preparaAjax(parametros,URL_EDIT,'E');
        }
    });
});

/**
* Funcion para limpiar el campo 
* del nombre del padecimiento
*/
$(function(){
    $('#dont_save_padecimiento').click(function(){
        $('#nombrePadecimiento').val("");
    });
});

/**
* Funcion que detecta el click al boton siguiente 
* de la paginacion valida si hay algun filtro ,
* en caso contrario se manda a pedir sin filtro 
* se manda el formulario en caso de existir, 
* la URL de destino y si es tipo siguiente 'S'
* @param $filtro opcional
* @param $URL url de destipo
* @param $TIPO 'S' de siguiente
*/
$(function(){
    $(document).on('click','#btn_next_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'S');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'S');
        }
    });
});

/**
* Funcion que detecta el click del boton anterior
* de la paginacion , valida si ay algun filtro para
* para realizar la peticion en caso contrario se 
* se manda nulo, se envia tambien los parametros
* de URL y tipo 'A'
* @param $filtro opcional
* @param $URL url de destino
* @param $TIPO 'A' de anterior
*/
$(function(){
    $(document).on('click','#btn_back_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'A');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'A');     
        }
    });
});

/**
* Funcion que detecta el click del boton de filtro
* recibe el filtro en caso de que sea nulo recarga
* la tabla desde cero, se envia URL y tipo 'R'
* @param $filtro opcional
* @param $URL url de destino
* @param $TIPO 'R' de recargar o reload
*/
$(function(){
    $("#btn_filto").click(function(){
        var filtro = $("#name_filter").val();
        console.log(filtro);
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'R');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'R');     
        }
    });
});

/**
* Funcion para el llenado de la tabla
* de padecimientos y en caso de que 
* venga nulo se llena con un mensaje 
* de que no hay campos para mostrar
* @param $respuestaJson json que 
* contiene toda la informacion que se 
* pondra en la tabla
*/
function rellenaTabla(respuestaJson){
    
    $("#editPadecimiento").modal('hide');
    $("#formPadecimiento").modal('hide');

    $("#tabla_padecimiento tbody").html("");
    if(respuestaJson.resultado.lista !== null){

        $.each(respuestaJson.resultado.lista, function(i,p){
        var newRow = "<tr id='tr-"+p.ID_PAD_PK+"'>"
                        +"<td>"+p.ID_PAD_PK+"</td>"
                        +"<td>"+p.DESC_PAD+"</td>"
                        +"<td>"
                            +"<button type='button' class='btn btn-danger waves-effect delete-padec' data-id='"+p.ID_PAD_PK+"' data-toggle='tooltip' data-placement='top' title='Eliminar Padecimiento'>"
                                +"<i class='material-icons'>delete_sweep</i>"
                            +"</button>"
                            +"<button type='button' class='btn btn-warning waves-effect edit-padec' style='margin: 5px' data-id='"+p.ID_PAD_PK+"' data-toggle='tooltip' data-placement='top' title='Editar Padecimiento'>"
                                +"<i class='material-icons'>create</i>"
                            +"</button>"
                        +"</td>"
                    +"</tr>";
            $(newRow).appendTo("#tabla_padecimiento tbody");

            $('#nombrePadecimiento').val("");
        });
    }else{
        $('.page-loader-wrapper').fadeOut();
        var newRow = "<tr>"
                        +"<td colspan='3' class='align-center'>No hay Datos</td>"
                    +"</tr>";
        $(newRow).appendTo("#tabla_padecimiento tbody");
    }
}