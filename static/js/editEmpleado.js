//URL para la paginacion
var URL_PAGINACION = BASE_PATH + 'index.php/Empleado/getPaginacion';

//Funcion para el mask de los inputs
$(function(){
    var $demoMaskedInput = $('.demo-masked-input');
    $demoMaskedInput.find('.mobile-phone-number').inputmask('(999) 99-99-99', { placeholder: '(___) __-__-__' });
    $demoMaskedInput.find('.email').inputmask({ alias: "email" });
    $demoMaskedInput.find('.money-dollar').inputmask('decimal', {'rightAlign': false,'alias': 'numeric','groupSeparator': ',','autoGroup': true,'digits': 2,'radixPoint': ".",'digitsOptional': false,'allowMinus': false,'placeholder': '0.00'});

});
/**
* Funcion para borrar un registro de la tabla
* de empleados, primero muestra un mensaje de 
* para verificar que el usuario esta seguro de eliminar 
* el campo , si responde que si se manda la peticion
* que recibe el id del empleado
* @param idEmploy
*/
$(function(){

    $("#table_employ").on('click','.delete-empleado',function(){
        var idEmploy = $(this).attr('data-id');

        swal({
            title: "¿Esta seguro?",
            text: "El registro se borrara definitivamente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url:BASE_PATH + 'index.php/Empleado/saveOrUpdateEmploy',
                type:'POST',
                dataType:'json',
                data:{
                    idEmp : idEmploy
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $("#tr-"+idEmploy).remove();
                        swal("Borrado!", "El campo fue borrado de la base de datos.", "success");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });      
        });

    });
});




/**
* Funcion para editar un registro de la tabla
* de empleados
* @param idEmploy
*/
$(function(){

    $("#table_employ").on('click','.edit-empleado',function(){
        var idEmploy = $(this).attr('data-id');

        $.ajax({
            url:BASE_PATH + 'index.php/Empleado/getEmployById',
            type:'POST',
            dataType:'json',
            data:{
                idEmp : idEmploy
            },
            success:function(json) {
                if(json.response_code === "200"){

                    $("#idEditEmpl").val(json.employInfo.ID_EMP_PK);
                    $("#editNombre_p").val(json.employInfo.NOM_EMP);
                    $("#editPaterno_p").val(json.employInfo.APP_EMP);
                    $("#editMaterno_p").val(json.employInfo.APM_EMP);
                    $("#editNum_tel").val(json.employInfo.TEL_EMP);
                    $("#editCorreo").val(json.employInfo.EMAIL_EMP);
                    $("#editSueldo").val(json.employInfo.SUELDO_EMP);

                    if(json.employInfo .GENERO_EMP === 'F'){
                        generoPer = 'mujer';
                        $("input:radio[name=grupo_genero][id='"+generoPer+"']").attr('checked',true);
                    }else{
                        generoPer = 'hombre';
                        $("input:radio[name=grupo_genero][id='"+generoPer+"']").attr('checked',true);
                    }

                    $("#modalEditEmpleado").modal('show');
                }
            },
            error : function(xhr, status) {
                swal("Error!", "Ocurrio un error durante la ejecución", "error");
            }
        });
    });
});

/**
* Funcion que detecta el click al boton siguiente 
* de la paginacion valida si hay algun filtro ,
* en caso contrario se manda a pedir sin filtro 
* se manda el formulario en caso de existir, 
* la URL de destino y si es tipo siguiente 'S'
* @param $filtro : opcional
* @param $URL : url de destipo
* @param $TIPO : 'S' de siguiente
*/
$(function(){
    $(document).on('click','#btn_next_pag > a',function(){
        //variables
        var filtro = $("#name_filter").val();
        var filtro1 = $("#app_filter").val();
        var filtro2 = $("#apm_filter").val();
        var parametros = [];

        parametros.push(filtro);
        parametros.push(filtro1);
        parametros.push(filtro2);

        preparaAjax(parametros,URL_PAGINACION,'S');

    });
});

/**
* Funcion que detecta el click del boton anterior
* de la paginacion , valida si ay algun filtro para
* para realizar la peticion en caso contrario se 
* se manda nulo, se envia tambien los parametros
* de URL y tipo 'A'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'A' de anterior
*/
$(function(){
    $(document).on('click','#btn_back_pag > a',function(){

        var filtro = $("#name_filter").val();
        var filtro1 = $("#app_filter").val();
        var filtro2 = $("#apm_filter").val();
        var parametros = [];

        parametros.push(filtro);
        parametros.push(filtro1);
        parametros.push(filtro2);

        preparaAjax(parametros,URL_PAGINACION,'A');
    });
});

/**
* Funcion que detecta el click del boton de filtro
* recibe el filtro en caso de que sea nulo recarga
* la tabla desde cero, se envia URL y tipo 'R'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'R' de recargar o reload
*/
$(function(){
    $("#btn_filto").click(function(){
        //variables
        var filtro = $("#name_filter").val();
        var filtro1 = $("#app_filter").val();
        var filtro2 = $("#apm_filter").val();
        var parametros = [];

        parametros.push(filtro);
        parametros.push(filtro1);
        parametros.push(filtro2);

        preparaAjax(parametros,URL_PAGINACION,'R');
    });
});

/**
* Funcion para guardar la informacion del
* empleado
*/
$(function(){
    $("#btn_edit_employ").click(function(){
        var nombre = $("#editNombre_p").val();
        var paterno = $("#editPaterno_p").val();
        var materno = $("#editMaterno_p").val();
        var telefono = $("#editNum_tel").val();
        var correo = $("#editCorreo").val();
        var sueldo = $("#editSueldo").val();
    });
});

/**
* Funcion para el llenado de la tabla
* de empleados y en caso de que 
* venga nulo se llena con un mensaje 
* de que no hay campos para mostrar
* @param $respuestaJson : json que 
* contiene toda la informacion que se 
* pondra en la tabla
*/
function rellenaTabla(respuestaJson){
    
    //$("#editToxicomania").modal('hide');

    if(respuestaJson.response_code !== '200'){
        swal("Error : "+respuestaJson.response_code, respuestaJson.response_msg, "error");
    }else{

        $("#table_employ tbody").html("");
        if(respuestaJson.resultado.lista !== null){

            $.each(respuestaJson.resultado.lista, function(i,p){
            var newRow = "<tr id='tr-"+p.ID_EMP_PK+"'>"
                            +"<td>"+p.NOM_EMP+"</td>"
                            +"<td>"+p.APP_EMP+"</td>"
                            +"<td>"+p.APM_EMP+"</td>"
                            +"<td>"+p.GENERO_EMP+"</td>"
                            +"<td>"
                                +"<button type='button' class='btn btn-danger waves-effect delete-empleado' data-id='"+p.ID_EMP_PK+"' data-toggle='tooltip' data-placement='top' title='Eliminar Empleado'>"
                                    +"<i class='material-icons'>delete_sweep</i>"
                                +"</button>"
                                +"<button type='button' class='btn btn-warning waves-effect edit-empleado' style='margin: 5px' data-id='"+p.ID_EMP_PK+"' data-toggle='tooltip' data-placement='top' title='Editar Empleado'>"
                                    +"<i class='material-icons'>create</i>"
                                +"</button>"
                            +"</td>"
                        +"</tr>";
                $(newRow).appendTo("#table_employ tbody");

                //$('#nameEditToxicomania').val("");
                //$('#idEditToxicomania').val("");
            });
        }else{
            var newRow = "<tr>"
                            +"<td colspan='3' class='align-center'>No hay Datos</td>"
                        +"</tr>";
            $(newRow).appendTo("#table_employ tbody");
        }
    }
}
