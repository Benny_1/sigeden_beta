//URL para la paginacion
var URL_PAGINACION = BASE_PATH + 'index.php/Profesion/getPaginacion';

/**
* Funcion oar ahacer el borrado de logico de la profesion
* se envia el identificador de la profesion que se 
* desea eliminar si la operacion es exitosa recibe un
* JSON con un response_code = '200' y se eliminar el
* campo de la tabla
* @param $idProf : identificador de la profesion
*/
$(function(){

    $("#tabla_profesion").on('click','.delete-prof',function(){
        idProf = $(this).attr('data-id');
        swal({
            title: "¿Esta seguro?",
            text: "El registro se borrar definitivamente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!",
            closeOnConfirm: false
        }, function () {
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Profesion/deleteProfesion',
                type:'POST',
                dataType:'json',
                data:{
                    idProfesion : idProf
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        $("#tr-"+idProf).remove();
                        swal("Borrado!", "El campo fue borrado de la base de datos.", "success");
                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });      
        });
    });
});

/**
* Funcion que detecta el click al boton de editar registro
* recibe el id de la profesion para ir a buscar la informacion
* que corresponda a ese registros y lo muestra en una modal
* @param $idProf : identificador de la profesion
*/
$(function(){
     $("#tabla_profesion").on('click','.edit-prof',function(){

        idProf = $(this).attr('data-id');
        var nameProf = $('#nombreProfesion').val();

        if(idProf === null || idProf === ""){
            swal("Error!", "El id de la profesion no puede estar vacio", "error");
        }else{
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Profesion/getInfoProfesion',
                type:'POST',
                dataType:'json',
                data:{
                    idProfesion : idProf
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        $('#idEditProfesion').val(json.profesion.ID_PROF_PK);
                        $('#nameEditProf').val(json.profesion.NOM_PROF);
                        $("#editProfesion").modal('show');
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
     });
});


/**
* Funcion que detecta el click del boton
* para agregar un nueva profesion
* @param $nameProf : nombre de la profesion a guardar
*/
$(function(){

    $('#btn_save_profesion').click(function(){

        var nameProf = $('#nombreProfesion').val();

        if(nameProf === null || nameProf === ""){
            swal("Error!", "El campo de nombre de profesion no puede estar vacio", "error");
        }else{
            var URL_SAVE_PROF = BASE_PATH +'index.php/Profesion/saveOrUpdateProfesion';
            var parametros = [nameProf];
            preparaAjax(parametros,URL_SAVE_PROF,'R');
        }
    });
});

/**
* Funcion para que detecta el click para actualziar
* la informacion de la profesion
* @param nameProf : nombre de la profesion
* @param idProf : id de la profesion 
*/
$(function(){
    $('#btn_edit_profesion').click(function(){
        var nameProf = $('#nameEditProf').val();
        var idProf = $('#idEditProfesion').val();

        if(nameProf === null || nameProf === ""){
            swal("Error!", "El campo de nombre de profesion no puede estar vacio", "error");
        }else if(idProf === null || idProf === ""){
            swal("Error!", "No hay id de profesion", "error");
        }else{

            var URL_EDIT_PROF = BASE_PATH +'index.php/Profesion/saveOrUpdateProfesion';
            var parametros = [nameProf,idProf];
            var filtro = $("#name_filter").val();

            if(filtro !== null && filtro !== ""){
                parametros = [nameProf,idProf,filtro];
            }
            preparaAjax(parametros,URL_EDIT_PROF,'E');
        }
    });
});

/**
* Funcion para limpiar el campo 
* del nombre del padecimiento
*/
$(function(){
    $('#dont_save_profesion').click(function(){
        $('#nombreProfesion').val("");
    });
});

/**
* Funcion que detecta el click al boton siguiente 
* de la paginacion valida si hay algun filtro ,
* en caso contrario se manda a pedir sin filtro 
* se manda el formulario en caso de existir, 
* la URL de destino y si es tipo siguiente 'S'
* @param $filtro : opcional
* @param $URL : url de destipo
* @param $TIPO : 'S' de siguiente
*/
$(function(){
    $(document).on('click','#btn_next_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'S');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'S');
        }
    });
});

/**
* Funcion que detecta el click del boton anterior
* de la paginacion , valida si ay algun filtro para
* para realizar la peticion en caso contrario se 
* se manda nulo, se envia tambien los parametros
* de URL y tipo 'A'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'A' de anterior
*/
$(function(){
    $(document).on('click','#btn_back_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'A');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'A');     
        }
    });
});

/**
* Funcion que detecta el click del boton de filtro
* recibe el filtro en caso de que sea nulo recarga
* la tabla desde cero, se envia URL y tipo 'R'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'R' de recargar o reload
*/
$(function(){
    $("#btn_filto").click(function(){
        var filtro = $("#name_filter").val();
        console.log(filtro);
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'R');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'R');     
        }
    });
});

/**
* Funcion para el llenado de la tabla
* de profesiones y en caso de que 
* venga nulo se llena con un mensaje 
* de que no hay campos para mostrar
* @param $respuestaJson : json que 
* contiene toda la informacion que se 
* pondra en la tabla
*/
function rellenaTabla(respuestaJson){
    
    $("#editProfesion").modal('hide');
    $("#formProfesion").modal('hide');

    if(respuestaJson.response_code !== '200'){
        swal("Error : "+respuestaJson.response_code, respuestaJson.response_msg, "error");
    }else{

        $("#tabla_profesion tbody").html("");
        if(respuestaJson.resultado.lista !== null){

            $.each(respuestaJson.resultado.lista, function(i,p){
            var newRow = "<tr id='tr-"+p.ID_PROF_PK+"'>"
                            +"<td>"+p.ID_PROF_PK+"</td>"
                            +"<td>"+p.NOM_PROF+"</td>"
                            +"<td>"
                                +"<button type='button' class='btn btn-danger waves-effect delete-prof' data-id='"+p.ID_PROF_PK+"' data-toggle='tooltip' data-placement='top' title='Eliminar Profesion'>"
                                    +"<i class='material-icons'>delete_sweep</i>"
                                +"</button>"
                                +"<button type='button' class='btn btn-warning waves-effect edit-prof' style='margin: 5px' data-id='"+p.ID_PROF_PK+"' data-toggle='tooltip' data-placement='top' title='Editar Profesion'>"
                                    +"<i class='material-icons'>create</i>"
                                +"</button>"
                            +"</td>"
                        +"</tr>";
                $(newRow).appendTo("#tabla_profesion tbody");

                $('#nameEditProf').val("");
                $('#idEditProfesion').val("");
            });
        }else{
            var newRow = "<tr>"
                            +"<td colspan='3' class='align-center'>No hay Datos</td>"
                        +"</tr>";
            $(newRow).appendTo("#tabla_profesion tbody");
        }
    }
}

