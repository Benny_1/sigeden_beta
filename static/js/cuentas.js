
var URL_PAGINACION = BASE_PATH + 'index.php/Cuentas/getPaginacion';

$(function(){
    var $demoMaskedInput = $('.demo-masked-input');
    $demoMaskedInput.find('.money-dollar').inputmask('decimal', {'rightAlign': false,'alias': 'numeric','groupSeparator': ',','autoGroup': true,'digits': 2,'radixPoint': ".",'digitsOptional': false,'allowMinus': false,'placeholder': '0.00'});
});

/**
* Funcion que detecta el click al boton siguiente 
* de la paginacion valida si hay algun filtro ,
* en caso contrario se manda a pedir sin filtro 
* se manda el formulario en caso de existir, 
* la URL de destino y si es tipo siguiente 'S'
* @param $filtro : opcional
* @param $URL : url de destipo
* @param $TIPO : 'S' de siguiente
*/
$(function(){
    $(document).on('click','#btn_next_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'S');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'S');
        }
    });
});

/**
* Funcion que detecta el click del boton anterior
* de la paginacion , valida si ay algun filtro
* para realizar la peticion en caso contrario se 
* se manda nulo, se envia tambien los parametros
* de URL y tipo 'A'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'A' de anterior
*/
$(function(){
    $(document).on('click','#btn_back_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'A');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'A');     
        }
    });
});

/**
* Funcion que detecta el click del boton de filtro
* recibe el filtro en caso de que sea nulo recarga
* la tabla desde cero, se envia URL y tipo 'R'
* @param $filtro : opcional
* @param $URL : url de destino
* @param $TIPO : 'R' de recargar o reload
*/
$(function(){
    $("#btn_filto").click(function(){
        var filtro = $("#name_filter").val();
        console.log(filtro);
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'R');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'R');     
        }
    });
});

/**
* Funcion para el llenado de la tabla
* de cuentas y en caso de que 
* venga nulo se llena con un mensaje 
* de que no hay campos para mostrar
* @param $respuestaJson : json que 
* contiene toda la informacion que se 
* pondra en la tabla
*/
function rellenaTabla(respuestaJson){

    if(respuestaJson.response_code !== '200'){
        swal("Error : "+respuestaJson.response_code, respuestaJson.response_msg, "error");
    }else{

        $("#tabla_cuentas tbody").html("");
        if(respuestaJson.resultado.lista !== null){

            $.each(respuestaJson.resultado.lista, function(i,cuenta){
                let newRow = `<tr id='tr-${cuenta.CUENTA}'>
                                <td>${cuenta.CUENTA}</td>
                                <td>${cuenta.PAC_NAME}</td>
                                <td>${cuenta.TOTAL_CUENTA}</td>
                                <td>${cuenta.ABONADO}</td>
                                <td>${cuenta.RESTANTE}</td>
                                <td>
                                    <button type='button' class='btn bg-indigo waves-effect view_detail' data-id-acount='${cuenta.CUENTA}' data-toggle='tooltip' data-placement='top' title='Ver estado de cuenta'>
                                        <i class='material-icons'>calculate</i>
                                    </button>
                                    <button type='button' class='btn bg-teal waves-effect add_mount' style='margin: 5px' data-id-acount='${cuenta.CUENTA}' data-toggle='tooltip' data-placement='top' title='Hacer un abono'>
                                        <i class='material-icons'>request_quote</i>
                                    </button>
                                </td>
                            </tr>`;
                $(newRow).appendTo("#tabla_cuentas tbody");
            });
        }else{
            let newRow = `<tr><td colspan='3' class='align-center'>No hay Datos</td></tr>`;
            $(newRow).appendTo("#tabla_cuentas tbody");
        }
    }
}

$(function (){

    /**
    * evento para hacer la busqueda de un paciente por nombre
    */
	$("#btn_search_pac").click(function(){
        let namePac = $("#searchPacName").val();

        if(namePac === null || namePac === ""){
            swal('Error!', 'El nombre a buscar no puede estar vacio', 'error');
        }else{

            $.ajax({
                url:BASE_PATH + 'index.php/Seguimiento/searchNamePac',
                type:'POST',
                dataType:'json',
                data:{
                    pacNameSearch: namePac
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $("#pac_search_select").empty().append("<option value='0'> - Seleciona un paciente - </option>");
                        if(json.result_name !== null){
                            $("#pac_search_select").removeAttr('disabled');
                            $.each(json.result_name, function(i,name){
                                $("#pac_search_select").append(`<option value=${name.ID_PAC_PK}>${name.PACIENTE}</option>`);
                            });

                        }else{
                            swal('Error!', `No se encontraron coincidencias`, 'error');
                        }
                    }else{
                        swal('Error!', `Error:${json.response_code} Mesnaje: ${json.response_msg}`, 'error');
                    }
                },
                error : function(xhr, status) {
                    swal('Error!', 'Ocurrio un error durante la ejecución', 'error');
                }
            });
        }
    });

    /**
    * evento para crear una nueva cuenta
    */
    $("#btn_save_new_cuenta").click(function(){
        let paciente = $("#pac_search_select").val();
        let montoCuenta = parseFloat($("#montoDeuda").val().replace(',', ''));

        if(paciente === null || paciente === ""){
            swal('Error!', 'el id no puede estar vacio', 'error');
        }else if(montoCuenta === null || montoCuenta === "" || montoCuenta === 0.00){
            swal('Error!', 'El monto no puede estar vacio o ser cero', 'error');
        }else{

            $.ajax({
                url:BASE_PATH + 'index.php/Cuentas/saveNewAcount',
                type:'POST',
                dataType:'json',
                data:{
                    idPacNewAcount: paciente,
                    mountNewAcount: montoCuenta
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $("#pac_search_select").val('0');
                        $("#montoDeuda").val("");
                        $("#searchPacName").val("");
                        $("#formCuenta").modal('hide');
                        swal('Exito!', `Operacion exitosa.`, 'success');
                    }else{
                        swal('Error!', `Error:${json.response_code} Mesnaje: ${json.response_msg}`, 'error');    
                    }
                },
                error : function(xhr, status) {
                    swal('Error!', "Ocurrio un error durante la ejecución", 'error');
                }
            });
        }
    });
});

$(function(){

    /**
    * evento para solo permitir que se ingresen numeros y punto en las celdas
    * de la tabla de estado cuenta
    */
    $("#tablaEstadoCuenta").on('keydown','.allow_only_numbers',function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) || 
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
 
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    /**
    * evento para mostrar la modal del detalle de la cuenta
    */
    $("#tabla_cuentas").on('click','.view_detail',function(){
        
        let idAcount = $(this).attr('data-id-acount');
        
        if(idAcount === null || idAcount === ""){
            swal('Error!', 'Campo vacio', 'error');
        }else{

            $.ajax({
                url:BASE_PATH + 'index.php/Cuentas/getAcountDetail',
                type:'POST',
                dataType:'json',
                data:{
                    idAcountSearch : idAcount
                },
                success:function(json) {
                    
                    $("#tablaEstadoCuenta tbody").html("");

                    if(json.response_code === "200"){

                        // pintamos los cargos
                        let dauda = json.cargos.TOTAL_CUENTA;
                        let fechaCargo = json.cargos.FECHA_CARGO;
                        let deudaFormat = parseFloat(dauda.replace(',', ''));

                        if(fechaCargo === null){
                            fechaCargo = 'no disponible'
                        }

                        let newRow = `<tr id='tr-cargo-${json.cargos.ID_CUENTA_PK}'> <td>${fechaCargo}</td> <td class='allow_only_numbers celdaEditable montoDeuda'>${dauda}</td> <td></td> <td></td> </tr>`;
                        $(newRow).appendTo("#tablaEstadoCuenta tbody");
                        
                        // pintamos los abonos
                        let sumAbono = 0;
                        $.each(json.abonos, function(i,ab){
                            let newRow = `<tr id='tr-abono-${ab.ID_ABONO_PK}'> <td>${ab.FECHA_ABONO}</td> <td></td> <td <td class='allow_only_numbers celdaEditable montoAbono'>${ab.MONTO_ABONO}</td> <td></td> </tr>`;
                            sumAbono += parseFloat(ab.MONTO_ABONO.replace(',',''));
                            $(newRow).appendTo("#tablaEstadoCuenta tbody");
                        });
                        
                        // calculamos el totol de la deuda
                        let totalDeuda = calculaDeuda1(deudaFormat, sumAbono);
                        $("#txtTotalDeuda").val(`${totalDeuda}`);
                        $("#cuntaId").val(idAcount);
                        // con estos parametros el modal es statico y solo se cierra con el boton de cerrar
                        $("#detail_modal").modal({backdrop: 'static', keyboard: false});

                    }else{
                        let newRow = `<tr><td colspan='3' class='align-center'>No hay Datos</td></tr>`;
                        $(newRow).appendTo("#tablaEstadoCuenta tbody");
                        $("#detail_modal").modal('show');
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
     });

    /**
    * evento para mostrar la modal de abono
    */
    $("#tabla_cuentas").on('click','.add_mount',function(){
        let idAcount = $(this).attr('data-id-acount');
        $("#idAcountAbono").val(idAcount);
        $("#abono_acount").modal('show');
    });

    /**
    * evento del click para agregar un abono
    */
    $("#btn_add_abono").click(function(){
        
        let acountAbono = $("#idAcountAbono").val();
        let mountAbono = parseFloat($("#mountAbono").val().replace(',', ''));


        if(acountAbono === "" || acountAbono === null){
            swal('Error!', 'la cuenta es incorrecta', 'error');
        }else if(mountAbono === "" || mountAbono === null || mountAbono === 0.00){
            swal('Error!', 'el monto a abonar no puede ser cero', 'error');
        }else if(validaMontos(acountAbono,mountAbono)){
            swal('Error!', 'El abono es mayor al restante de la deuda', 'error');
        }else{
            
            $.ajax({
                url:BASE_PATH + 'index.php/Cuentas/saveNewMountAcount',
                type:'POST',
                dataType:'json',
                data:{
                    idAcountAdd: acountAbono,
                    mountAdd: mountAbono
                },
                success:function(json) {
                    if(json.response_code === "200"){

                        $("#idAcountAbono").val("");
                        $("#mountAbono").val("");
                        cambioCeldaAbono(acountAbono,mountAbono);
                        $("#abono_acount").modal('hide');
                        swal('Exito!', `Abono correcto.`, 'success');
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
    });
});

/**
* funcion para validar que los monstos abonados
* no superen la deuda
*/
function validaMontos(cuenta,monto){

    let superaDeuda = false;
    let deuda = parseInt($(`#tr-${cuenta} > td:nth-child(3)`).text().replace(',', ''));
    let abonado = parseInt($(`#tr-${cuenta} > td:nth-child(4)`).text().replace(',', ''));
    let total = abonado + monto;

    if(total > deuda){
        superaDeuda = true;
    }

    return superaDeuda;
}

/**
* funcion para hacer el recalculo de la deuda
* una ves que se guardo en la ase para no tener
* que traer toda la informacion
*/
function cambioCeldaAbono(cuenta,monto){
    let deudaAnt = parseInt($(`#tr-${cuenta} > td:nth-child(3)`).text().replace(',', ''));
    let abonoAnt = parseInt($(`#tr-${cuenta} > td:nth-child(4)`).text().replace(',', ''));
    let totalAnt = parseInt($(`#tr-${cuenta} > td:nth-child(5)`).text().replace(',', ''));
    let totalAbono = abonoAnt + monto;
    let totalRestante = deudaAnt - totalAbono;
    let formateoAbono = new Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(totalAbono);
    let formateoRestante = new Intl.NumberFormat("es-MX", {style: "currency", currency: "MXN"}).format(totalRestante);
        
    $(`#tr-${cuenta} > td:nth-child(4)`).text(formateoAbono.replace('$',''));
    $(`#tr-${cuenta} > td:nth-child(5)`).text(formateoRestante.replace('$',''));
}

/**
* funcion para hacer el calculo de la deuda
* y formatear el resultado
*/
function calculaDeuda1(cargo, abono){
    let total = 0.0;
    total = cargo - abono;
    let totalFormat = new Intl.NumberFormat('es-MX', {style: 'currency', currency: 'MXN'}).format(total);
    return totalFormat.replace('$','');
}