
//URL para la paginacion 
var URL_PAGINACION = BASE_PATH + 'index.php/Procedimiento_dental/getPaginacion';

/**
* Funcion para hacer el borrado logico de un procedimiento dental
* Se envia el id que se desea eliminar
* si la operacion es exitosa recibe un JSON
* con codigo 200 y se eleimina el campo de la tabla
* @param $idPadec id del procedimiento
*/
$(function(){

    $("#tabla_procedimiento").on('click','.delete-proce',function(){
        
        idPadec = $(this).attr('data-id');

        swal({
            title: "¿Esta seguro?",
            text: "El registro se borrar definitivamente!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy seguro!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url:BASE_PATH + 'index.php/Procedimiento_dental/deletePadecimiento',
                type:'POST',
                dataType:'json',
                data:{
                    idPadecimiento : idPadec
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $("#tr-"+idPadec).remove();
                        swal("Borrado!", "El campo fue borrado de la base de datos.", "success");
                    }
                },
                error : function(xhr, status) {
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });      
        });
    });
});

/**
* Funcion para el boton de edicion de procedimiento
* recibe el id del procedimiento por el cual buscara
* se hace la peticion y si regresa un response_code = 200
* mostrar una modal con el nombre del procedimiento para editarlo
* @param $idPadec
*/
$(function(){
     $("#tabla_procedimiento").on('click','.edit-proce',function(){

        idProce = $(this).attr('data-id');
        //var nameProce = $('#nombreprocedimiento').val();

        if(idProce === null || idProce === ""){
            swal("Error!", "El id del procedimiento no puede estar vacio", "error");
        }else{
            $('.page-loader-wrapper').fadeIn();
            $.ajax({
                url:BASE_PATH + 'index.php/Procedimiento_dental/getInfoProcedimiento',
                type:'POST',
                dataType:'json',
                data:{
                    idProcedimiento : idProce
                },
                success:function(json) {
                    if(json.response_code === "200"){
                        $('.page-loader-wrapper').fadeOut();
                        $('#idEditProce').val(json.procedimiento.ID_PROC_PK);
                        $('#nameEditProce').val(json.procedimiento.DSC_PROC);
                        $("#editProcedimiento").modal('show');

                    }
                },
                error : function(xhr, status) {
                    $('.page-loader-wrapper').fadeOut();
                    swal("Error!", "Ocurrio un error durante la ejecución", "error");
                }
            });
        }
     });
});

/**
* Funcion para el boton de guardar un nuevo procedimiento
* obtiene el nombre del procedimiento y lo envia al webServide
* si es exitosa la operacion regresa un response_code = 200
* @param $nameProce
*/
$(function(){

    $('#btn_save_proce').click(function(){

        var nameProce = $('#nombreProcedimiento').val();

        if(nameProce === null || nameProce === ""){
            swal("Error!", "El campo de nombre de procedimiento no puede estar vacio", "error");
        }else{

            var URL_SAVE = BASE_PATH +'index.php/Procedimiento_dental/saveOrUpdateProcedimiento';
            var parametros = [nameProce];
            preparaAjax(parametros,URL_SAVE,'R');
        }
    });
});

/**
* Funcion para hacer la edicion de algun Procedimiento dental
* Recibe el nombre del Procedimiento y el id al cual 
* se le va a hacer la edicion
* @param $nameProce nombre del Procedimiento
* @param $idProce identificador del Procedimiento
* @param $filtro filtro del Procedimiento
*/
$(function(){
    $('#btn_edit_proce').click(function(){
        let nameProce = $('#nameEditProce').val();
        let idProce = $('#idEditProce').val();

        if(nameProce === null || nameProce === ""){
            swal("Error!", "El campo de nombre del procedimiento no puede estar vacio", "error");
        }else if(idProce === null || idProce === ""){
            swal("Error!", "No hay id de procedimiento", "error");
        }else{
            var URL_EDIT = BASE_PATH +'index.php/Procedimiento_dental/saveOrUpdateProcedimiento';
            var parametros = [nameProce,idProce];
            var filtro = $("#name_filter").val();
            console.log(filtro);
            if(filtro !== null && filtro !== ""){
                parametros = [nameProce,idProce,filtro];
            }
            preparaAjax(parametros,URL_EDIT,'E');
        }
    });
});

/**
* Funcion para limpiar el campo 
* del nombre del padecimiento
*/
$(function(){
    $('#dont_save_proce').click(function(){
        $('#nombreProcedimiento').val("");
    });
});

/**
* Funcion que detecta el click al boton siguiente 
* de la paginacion valida si hay algun filtro ,
* en caso contrario se manda a pedir sin filtro 
* se manda el formulario en caso de existir, 
* la URL de destino y si es tipo siguiente 'S'
* @param $filtro opcional
* @param $URL url de destipo
* @param $TIPO 'S' de siguiente
*/
$(function(){
    $(document).on('click','#btn_next_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'S');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'S');
        }
    });
});

/**
* Funcion que detecta el click del boton anterior
* de la paginacion , valida si ay algun filtro para
* para realizar la peticion en caso contrario se 
* se manda nulo, se envia tambien los parametros
* de URL y tipo 'A'
* @param $filtro opcional
* @param $URL url de destino
* @param $TIPO 'A' de anterior
*/
$(function(){
    $(document).on('click','#btn_back_pag > a',function(){
        var filtro = $("#name_filter").val();
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'A');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'A');     
        }
    });
});

/**
* Funcion que detecta el click del boton de filtro
* recibe el filtro en caso de que sea nulo recarga
* la tabla desde cero, se envia URL y tipo 'R'
* @param $filtro opcional
* @param $URL url de destino
* @param $TIPO 'R' de recargar o reload
*/
$(function(){
    $("#btn_filto").click(function(){
        var filtro = $("#name_filter").val();
        console.log(filtro);
        if(filtro === null || filtro === ""){
            preparaAjax(null,URL_PAGINACION,'R');
        }else{
            var parametros = [filtro];
            preparaAjax(parametros,URL_PAGINACION,'R');     
        }
    });
});

/**
* Funcion para el llenado de la tabla
* de padecimientos y en caso de que 
* venga nulo se llena con un mensaje 
* de que no hay campos para mostrar
* @param $respuestaJson json que 
* contiene toda la informacion que se 
* pondra en la tabla
*/
function rellenaTabla(respuestaJson){
    
    $("#editProcedimiento").modal('hide');
    $("#formProcedimiento").modal('hide');

    $("#tabla_procedimiento tbody").html("");
    if(respuestaJson.resultado.lista !== null){

        $.each(respuestaJson.resultado.lista, function(i,p){
        var newRow = "<tr id='tr-"+p.ID_PROC_PK+"'>"
                        +"<td>"+p.ID_PROC_PK+"</td>"
                        +"<td>"+p.DSC_PROC+"</td>"
                        +"<td>"
                            +"<button type='button' class='btn btn-danger waves-effect delete-proce' data-id='"+p.ID_PROC_PK+"' data-toggle='tooltip' data-placement='top' title='Eliminar Procedimiento'>"
                                +"<i class='material-icons'>delete_sweep</i>"
                            +"</button>"
                            +"<button type='button' class='btn btn-warning waves-effect edit-proce' style='margin: 5px' data-id='"+p.ID_PROC_PK+"' data-toggle='tooltip' data-placement='top' title='Editar Procedimiento'>"
                                +"<i class='material-icons'>create</i>"
                            +"</button>"
                        +"</td>"
                    +"</tr>";
            $(newRow).appendTo("#tabla_procedimiento tbody");

            $('#nombreProcedimiento').val("");
        });
    }else{
        $('.page-loader-wrapper').fadeOut();
        var newRow = "<tr>"
                        +"<td colspan='3' class='align-center'>No hay Datos</td>"
                    +"</tr>";
        $(newRow).appendTo("#tabla_procedimiento tbody");
    }
}