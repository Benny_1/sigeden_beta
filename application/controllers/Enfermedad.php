<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para los padecimientos dentales
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Enfermedad extends CI_Controller {
    
    /**
    * Funcion constrcutor de la clase Home_A
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }else{
            $numeroPermiso = '10';
            $permisos = explode(",", $this->session->userdata('permisos'));
            $numPer = count($permisos);
            $tienePermiso = FALSE;

            for ($i=0; $i < $numPer; $i++) {
                if($numeroPermiso == $permisos[$i]){
                    $tienePermiso = TRUE;
                    break;
                }
            }

            if($tienePermiso == FALSE){
                $this->session->set_flashdata("error","ACCESO DENEGADO (O_O;)");
                redirect("Home");
            }
        }
        
        $this->load->model("Enfermedad_model");
    }

    /**
    * Funcion para mostrar la pagin aprincipal
    * donde se puede agregar editar o eliminar
    * una enfermedad patologica.
    */
    public function index(){
        
        $numRegistros = 5;
        $pagianActual = 0;

        $datos = array();
        $datos['resultado'] = $this->Enfermedad_model->getEnfermedadPaginado($numRegistros,$pagianActual);

        $fragment = array();
        $fragment['VISTA'] = $this->load->view('enfermedad_view',$datos,TRUE);
        $fragment['ccsLibs'] = [""];
        $fragment['jsLibs'] = ['enfermedad.js','core/tablaAjax2.js'];
        
        $this->load->view('dashboard_view',$fragment);
    }


    /**
    * Funcion para obtener la info de la enfermedad
    * por el id, regresa un codigo y mensaje de respuesta
    * junto con la informacion solicitada.
    * @param $idEnfermedad identificador de la enfermeda
    * @return $JSON : ['response_code'] codigo de respuesta
    *               : ['response_msg'] mensaje de la respuesta
    *               : ['enfermedad'] informacion de la enfermedad consultada
    */
    public function getInfoEnfermedad(){
        
        $ident = $this->input->post('idEnfermedad');
        $jsonInfoEnfer = array();

        if($ident == NULL || $ident == ""){
            $jsonInfoEnfer['response_code'] = '400';
            $jsonInfoEnfer['response_msg'] = 'el id no puede estar vacio para la busqueda';
        }else{

            $infoEnfermedad = $this->Enfermedad_model->getInfoById($ident);

            if(!is_null($infoEnfermedad)){
                $jsonInfoEnfer['response_code'] = '200';
                $jsonInfoEnfer['response_msg'] = 'Operacion exitosa';
                $jsonInfoEnfer['enfermedad'] = $infoEnfermedad;
            }else{
                $jsonInfoEnfer['response_code'] = '400';
                $jsonInfoEnfer['response_msg'] = 'la consulta no tiene resultado';
            }
        }

        echo json_encode($jsonInfoEnfer);
    }

    /**
    * Funcion para agregar una nueva enfermedad 
    * al catologo o actualizar la Enfermedad
    * recibe la pagina actual , el numero de registros
    * por pagina , el identificador, el nombre de la enfermedad
    * el filtro por el que se hara la busqueda
    * @param $paginaConsulta 
    * @param $numeroRegistros
    * @param $filtro_1
    * @param $filtro_0
    * @param $filtro_2
    * @return JSON [response_code]
    *              [response_msg]
    *              [resultado]
    */
    public function saveOrUpdateEnfermedad(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $idEnfer = $this->input->post('filtro_1');
        $nameEnfer = $this->input->post('filtro_0');
        $filtro = $this->input->post('filtro_2');

        $jsonInfoEnfer = array();
        $infoEnfer = array();
        
        if($nameEnfer == NULL || $nameEnfer == ""){

            $jsonInfoAdic['response_code'] = '400';
            $jsonInfoAdic['response_msg'] = 'el nombre de la adiccion no puede estar vacio';

        }else if($idEnfer == NULL || $idEnfer == ""){
            //se hace una insercion
            $infoEnfer['NOMBRE_ENFER'] = strtoupper($nameEnfer);
            $infoEnfer['ESTAT_ENFER'] = '1';

            if($this->Enfermedad_model->saveEnfermedad($infoEnfer)){
                $jsonInfoEnfer['response_code'] = '200';
                $jsonInfoEnfer['response_msg'] = 'Informacion guardada';
                $jsonInfoEnfer['resultado'] = $this->Enfermedad_model->getEnfermedadPaginado(NULL,NULL);
            }else{
                $jsonInfoEnfer['response_code'] = '400';
                $jsonInfoEnfer['response_msg'] = 'Fallo durante la ejecucion';
            }
        }else{
            //se hace una actualizacion
            $infoEnfer['NOMBRE_ENFER'] = strtoupper($nameEnfer);
            $infoEnfer['ESTAT_ENFER'] = '1';

            if($this->Enfermedad_model->updateEnfermedad($infoEnfer,$idEnfer)){
                $jsonInfoEnfer['response_code'] = '200';
                $jsonInfoEnfer['response_msg'] = 'Informacion actualizada';
                
                if($filtro == NULL || $filtro == ""){
                    $jsonInfoEnfer['resultado'] = $this->Enfermedad_model->getEnfermedadPaginado($numero,$pagina);
                }else{
                    $jsonInfoEnfer['resultado'] = $this->Enfermedad_model->getEnfermedadPaginadoFiltro($filtro,$numero,$pagina);
                }
                
            }else{
                $jsonInfoEnfer['response_code'] = '400';
                $jsonInfoEnfer['response_msg'] = 'Fallo durante la ejecucion';   
            }
        }
        echo json_encode($jsonInfoEnfer);
    }

    /**
    * Funcion para hacer el borrado logico
    * de alguna enfermedad, recibe el id de la enfermedad
    * y actualiza el estatus a 0
    * @param idEnfermedad : identificador de la enfermedad
    */
    public function deleteEnfermedad(){
        
        $enfermedad = $this->input->post('idEnfermedad');
        $jsonEnfermedad = array();
        
        if($enfermedad == NULL || $enfermedad == ""){
            
            $jsonEnfermedad['response_code'] = '400';
            $jsonEnfermedad['response_msg'] = 'el ID no puede estar vacio o estar nulo';

        }else{

            $datosEnfermedad = array();
            $datosEnfermedad['ESTAT_ENFER'] = '0';

            if($this->Enfermedad_model->updateEnfermedad($datosEnfermedad,$enfermedad)){

                $jsonEnfermedad['response_code'] = '200';
                $jsonEnfermedad['response_msg'] = 'Operacion Exitosa!';  
            }else{

                $jsonEnfermedad['response_code'] = '400';
                $jsonEnfermedad['response_msg'] = 'Algo salio mal intentelo mas tarde';
            }
        }
        echo json_encode($jsonEnfermedad);
    }

    /**
    * Funcion para obtener la siguiente o anterior pagina 
    * de las enfermedades.
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros por pagina
    * @param $filtro_0 filtro para el paginado
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [resultado] resultado de la paginacion
    */
    public function getPaginacion(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $filtro = $this->input->post('filtro_0');

        $jsonPaginado = array();

        if($pagina == NULL || $pagina == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'La pagina debe de estar informada';

        }else if($numero == NULL || $numero == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'El numero de registros debe de estar informado';

        }else if($filtro == NULL || $filtro == ""){
            // se hace la busqueda sin fitro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Enfermedad_model->getEnfermedadPaginado($numero,$pagina);

        }else{
            //Se hace la busqueda con filtro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Enfermedad_model->getEnfermedadPaginadoFiltro($filtro,$numero,$pagina);

        }
        echo json_encode($jsonPaginado);
    }

    public function alertEnfermedad(){
        $enfermedad  = $this->input->post('idEnfermedad');
        $valorAlerta = $this->input->post('valorCheck');
        $alerta = '';
        $jsonAlertEnf = array();
        
        if($valorAlerta == 'true'){
            $alerta = '1';
        }else{
            $alerta = '0';
        }

        if($enfermedad == NULL || $enfermedad == ""){
            $jsonAlertEnf['response_code'] = '400';
            $jsonAlertEnf['response_msg'] = 'No hay enfermedad para hacer la actualizacion';
        }else if($this->Enfermedad_model->updateAlert($enfermedad,$alerta)){
            $jsonAlertEnf['response_code'] = '200';
            $jsonAlertEnf['response_msg'] = 'Operacion Exitosa';
        }else{
            $jsonAlertEnf['response_code'] = '400';
            $jsonAlertEnf['response_msg'] = 'No se pudo actualizar';
        }

        echo json_encode($jsonAlertEnf);
    }
}
