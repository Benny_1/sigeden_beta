<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para las profesiones
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Adiccion extends CI_Controller {
    
    /**
    * Funcion constrcutor de la clase Adiccion
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }else{
            
            $numeroPermiso = '11';
            $permisos = explode(",", $this->session->userdata('permisos'));
            $numPer = count($permisos);
            $tienePermiso = FALSE;

            for ($i=0; $i < $numPer; $i++) {
                if($numeroPermiso == $permisos[$i]){
                    $tienePermiso = TRUE;
                    break;
                }
            }

            if($tienePermiso == FALSE){
                $this->session->set_flashdata("error","ACCESO DENEGADO (O_O;)");
                redirect("Home");
            }
        }
        
        $this->load->model("Adiccion_model");
    }

    /**
    * Funcion principal para cargar la vista
    */
    public function index(){

        $numRegistros = 5;
        $pagianActual = 0;

        $datos = array();
        $datos['resultado'] = $this->Adiccion_model->getAdiccionPaginado($numRegistros,$pagianActual);

        $fragment = array();
        $fragment['VISTA'] = $this->load->view('adiccion_view',$datos,TRUE);
        $fragment['ccsLibs'] = [''];
        $fragment['jsLibs'] = ['core/tablaAjax2.js','adiccion.js'];

        $this->load->view('dashboard_view',$fragment);

    }

    /**
    * Funcion para obtenr la info de la adiccion por
    * por el id 
    */
    public function getInfoAdiccion(){
        
        $ident = $this->input->post('idAdiccion');
        $jsonInfoAdic = array();

        if($ident == NULL || $ident == ""){
            $jsonInfoAdic['response_code'] = '400';
            $jsonInfoAdic['response_msg'] = 'el id no puede estar vacio para la busqueda';
        }else{

            $infoAdiccion = $this->Adiccion_model->getInfoById($ident);

            if(!is_null($infoAdiccion)){
                $jsonInfoAdic['response_code'] = '200';
                $jsonInfoAdic['response_msg'] = 'Operacion exitosa';
                $jsonInfoAdic['adiccion'] = $infoAdiccion;
            }else{
                $jsonInfoAdic['response_code'] = '400';
                $jsonInfoAdic['response_msg'] = 'la consulta no tiene resultado';
            }
        }

        echo json_encode($jsonInfoAdic);
    }

    /**
    * Funcion para agregar una nueva adiccion 
    * al catologo o actualizar la informacion
    */
    public function saveOrUpdateAdiccion(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $idAdic = $this->input->post('filtro_1');
        $nameAdic = $this->input->post('filtro_0');
        $filtro = $this->input->post('filtro_2');

        $jsonInfoAdic = array();
        $infoAdic = array();
        
        if($nameAdic == NULL || $nameAdic == ""){

            $jsonInfoAdic['response_code'] = '400';
            $jsonInfoAdic['response_msg'] = 'el nombre de la adiccion no puede estar vacio';

        }else if($idAdic == NULL || $idAdic == ""){
            //se hace una insercion
            $infoAdic['NOM_ADIC'] = strtoupper($nameAdic);
            $infoAdic['ESTAT_ADIC'] = '1';

            if($this->Adiccion_model->saveAdiccion($infoAdic)){
                $jsonInfoAdic['response_code'] = '200';
                $jsonInfoAdic['response_msg'] = 'Informacion guardada';
                $jsonInfoAdic['resultado'] = $this->Adiccion_model->getAdiccionPaginado(NULL,NULL);
            }else{
                $jsonInfoAdic['response_code'] = '400';
                $jsonInfoAdic['response_msg'] = 'Fallo durante la ejecucion';
            }
        }else{
            //se hace una actualizacion
            $infoAdic['NOM_ADIC'] = strtoupper($nameAdic);
            $infoAdic['ESTAT_ADIC'] = '1';

            if($this->Adiccion_model->updateAdiccion($infoAdic,$idAdic)){
                $jsonInfoAdic['response_code'] = '200';
                $jsonInfoAdic['response_msg'] = 'Informacion actualizada';
                if($filtro == NULL || $filtro == ""){
                    $jsonInfoAdic['resultado'] = $this->Adiccion_model->getAdiccionPaginado($numero,$pagina);
                }else{
                    $jsonInfoAdic['resultado'] = $this->Adiccion_model->getAdiccionPaginadoFiltro($filtro,$numero,$pagina);
                }
            }else{
                $jsonInfoAdic['response_code'] = '400';
                $jsonInfoAdic['response_msg'] = 'Fallo durante la ejecucion';   
            }
        }
        echo json_encode($jsonInfoAdic);
    }

    /**
    * Funcion para hacer el borrado logico
    * de alguna adiccion, recibe el id de la adiccion
    * y actualiza el estatus a 0
    * @param $idAdiccion : identificador
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function deleteAdiccion(){
        
        $adiccion = $this->input->post('idAdiccion');
        $jsonAdiccion = array();
        
        if($adiccion == NULL || $adiccion == ""){
            
            $jsonAdiccion['response_code'] = '400';
            $jsonAdiccion['response_msg'] = 'el ID no puede estar vacio o estar nulo';

        }else{

            $datosAdiccion = array();
            $datosAdiccion['ESTAT_ADIC'] = '0';

            if($this->Adiccion_model->updateAdiccion($datosAdiccion,$adiccion)){

                $jsonAdiccion['response_code'] = '200';
                $jsonAdiccion['response_msg'] = 'Operacion Exitosa!';  
            }else{

                $jsonAdiccion['response_code'] = '400';
                $jsonAdiccion['response_msg'] = 'Algo salio mal intentelo mas tarde';
            }
        }
        echo json_encode($jsonAdiccion);
    }

    /**
    * Funcion para obtener la siguiente o anterior pagina 
    * de las toxicomanias.
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros por pagina
    * @param $filtro_0 filtro para el paginado
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [resultado] resultado de la paginacion
    */
    public function getPaginacion(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $filtro = $this->input->post('filtro_0');

        $jsonPaginado = array();

        if($pagina == NULL || $pagina == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'La pagina debe de estar informada';

        }else if($numero == NULL || $numero == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'El numero de registros debe de estar informado';

        }else if($filtro == NULL || $filtro == ""){
            // se hace la busqueda sin fitro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Adiccion_model->getAdiccionPaginado($numero,$pagina);

        }else{
            //Se hace la busqueda con filtro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Adiccion_model->getAdiccionPaginadoFiltro($filtro,$numero,$pagina);

        }
        echo json_encode($jsonPaginado);
    }
}