<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para las profesiones
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Profesion extends CI_Controller {
    
    /**
    * Metodo Constrcutor de la clase Profesio_A
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }else{

            $numeroPermiso = '12';
            $permisos = explode(",", $this->session->userdata('permisos'));
            $numPer = count($permisos);
            $tienePermiso = FALSE;

            for ($i=0; $i < $numPer; $i++) {
                if($numeroPermiso == $permisos[$i]){
                    $tienePermiso = TRUE;
                    break;
                }
            }

            if($tienePermiso == FALSE){
                $this->session->set_flashdata("error","ACCESO DENEGADO (O_O;)");
                redirect("Home");
            }
        }

        $this->load->model("Profesion_model");
    }

    /**
    * Funcion para carga la vista principal de las profesiones
    * donde se pueden agregar, editar , borrar o consultar las 
    * profesiones
    */
    public function index(){
        
        $numRegistros = 5;
        $pagianActual = 0;

        $datos = array();
        $datos['resultado'] = $this->Profesion_model->getProfesionPaginado($numRegistros,$pagianActual);
        
        $fragment = array();
        $fragment['VISTA'] = $this->load->view('profesion_view',$datos,TRUE);
        $fragment['ccsLibs'] = [""];
        $fragment['jsLibs'] = ['core/tablaAjax2.js','profesion.js'];

        $this->load->view('dashboard_view',$fragment);
    }

    /**
    * 
    */
    public function getInfoProfesion(){
        
        $ident = $this->input->post('idProfesion');
        $jsonInfoProf = array();

        if($ident == NULL || $ident == ""){
            $jsonInfoProf['response_code'] = '400';
            $jsonInfoProf['response_msg'] = 'el id no puede estar vacio para la busqueda';
        }else{

            $infoProfecion = $this->Profesion_model->getInfoById($ident);

            if(!is_null($infoProfecion)){
                $jsonInfoProf['response_code'] = '200';
                $jsonInfoProf['response_msg'] = 'Operacion exitosa';
                $jsonInfoProf['profesion'] = $infoProfecion;
            }else{
                $jsonInfoProf['response_code'] = '400';
                $jsonInfoProf['response_msg'] = 'la consulta no tiene resultado';
            }
        }

        echo json_encode($jsonInfoProf);
    }

    /**
    * Funcion para agregar una nueva profesion 
    * al catologo o actualizar la informacion
    */
    public function saveOrUpdateProfesion(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $idProf = $this->input->post('filtro_1');
        $nameProf = $this->input->post('filtro_0');
        $filtro = $this->input->post('filtro_2');

        $jsonInfoPrf = array();
        $infoProf = array();
        
        if($nameProf == NULL || $nameProf == ""){

            $jsonInfoPrf['response_code'] = '400';
            $jsonInfoPrf['response_msg'] = 'el nombre de la profesion no puede estar vacio';

        }else if($idProf == NULL || $idProf == ""){
            //se hace una insercion
            $infoProf['NOM_PROF'] = strtoupper($nameProf);
            $infoProf['ESTAT_PROF'] = '1';

            if($this->Profesion_model->saveProfesion($infoProf)){
                $jsonInfoPrf['response_code'] = '200';
                $jsonInfoPrf['response_msg'] = 'Informacion guardada';
                $jsonInfoPrf['resultado'] = $this->Profesion_model->getProfesionPaginado(NULL,NULL);
            }else{
                $jsonInfoPrf['response_code'] = '400';
                $jsonInfoPrf['response_msg'] = 'Fallo durante la ejecucion';
            }
        }else{
            //se hace una actualizacion
            $infoProf['NOM_PROF'] = strtoupper($nameProf);
            $infoProf['ESTAT_PROF'] = '1';

            if($this->Profesion_model->updateProfesion($infoProf,$idProf)){
                $jsonInfoPrf['response_code'] = '200';
                $jsonInfoPrf['response_msg'] = 'Informacion actualizada';
                if($filtro == NULL || $filtro == ""){
                    $jsonInfoPrf['resultado'] = $this->Profesion_model->getProfesionPaginado($numero,$pagina);
                }else{
                    $jsonInfoPrf['resultado'] = $this->Profesion_model->getProfesionPaginadoFiltro($filtro,$numero,$pagina);
                }
            }else{
                $jsonInfoPrf['response_code'] = '400';
                $jsonInfoPrf['response_msg'] = 'Fallo durante la ejecucion';   
            }
        }
        echo json_encode($jsonInfoPrf);
    }

    /**
    * Funcion para hacer el borrado logico
    * de alguna profesion, recibe el id de la profesion
    * y actualiza el estatus a 0
    */
    public function deleteProfesion(){
        
        $profesion = $this->input->post('idProfesion');
        $jsonProfesion = array();
        
        if($profesion == NULL || $profesion == ""){
            
            $jsonProfesion['response_code'] = '400';
            $jsonProfesion['response_msg'] = 'el ID no puede estar vacio o estar nulo';

        }else{

            $datosProfesion = array();
            $datosProfesion['ESTAT_PROF'] = '0';

            if($this->Profesion_model->updateProfesion($datosProfesion,$profesion)){

                $jsonProfesion['response_code'] = '200';
                $jsonProfesion['response_msg'] = 'Operacion Exitosa!';  
            }else{

                $jsonProfesion['response_code'] = '400';
                $jsonProfesion['response_msg'] = 'Algo salio mal intentelo mas tarde';
            }
        }
        echo json_encode($jsonProfesion);
    }


    /**
    * Funcion para obtener la siguiente o anterior pagina 
    * de los padecimientos dentales.
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros por pagina
    * @param $filtro_0 filtro para el paginado
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [resultado] resultado de la paginacion
    */
    public function getPaginacion(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $filtro = $this->input->post('filtro_0');

        $jsonPaginado = array();

        if($pagina == NULL || $pagina == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'La pagina debe de estar informada';

        }else if($numero == NULL || $numero == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'El numero de registros debe de estar informado';

        }else if($filtro == NULL || $filtro == ""){
            // se hace la busqueda sin fitro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Profesion_model->getProfesionPaginado($numero,$pagina);

        }else{
            //Se hace la busqueda con filtro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Profesion_model->getProfesionPaginadoFiltro($filtro,$numero,$pagina);

        }
        echo json_encode($jsonPaginado);
    }
}