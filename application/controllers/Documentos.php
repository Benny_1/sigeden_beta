<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, No� Ramos L�pez
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para las profesiones
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Documentos extends My_Controller {
    
    /**
    * Funcion constrcutor de la clase Home_A
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }
        
        $this->load->model("Documentos_model");
        $this->load->model("Doctor_model");
    }

    /**
    * Funcion para llamar la vista de la clase Home_A
    */
    public function index(){
        $infoDocs = array();
        $infoDocs['texto'] = $this->Documentos_model->getDocumento();

        $fragment = array();
        $fragment['VISTA'] = $this->load->view('documentos_view',$infoDocs,TRUE);
        $fragment['ccsLibs'] = [''];
        $fragment['jsLibs'] = ['documentos.js'];
        
        $this->load->view('dashboard_view',$fragment);
    }

    public function previewDoc(){

        $this->load->library('mydompdf');
        $infoPatient = array();
        $idPaciente = '3';
        $numeroCabeceras = 6;
        
        $infoPatient['infoPac'] = $this->Doctor_model->getInfoRepById($idPaciente);

        //tabla de antecedentes familiares
        $enfermedades = $this->Documentos_model->getAllEnfer($idPaciente);
        $relacionPariente = $this->Documentos_model->getEnferFamById($idPaciente);
        $numeroTablas = ceil(count($enfermedades) / $numeroCabeceras);
        $arrayTableFam = $this->creaTablas($numeroTablas,$enfermedades,$relacionPariente,$numeroCabeceras,'1');
        
        //tabla de antecedentes personales
        $enferPer = $this->Documentos_model->getAllEnferPac($idPaciente);
        $relPac = $this->Documentos_model->getEnferPerById($idPaciente);
        $numTabPer = ceil(count($enferPer) / $numeroCabeceras);
        $arrayTablePer = $this->creaTablas($numTabPer,$enferPer,$relPac,$numeroCabeceras,'2');

        $infoPatient['tfamily'] = $arrayTableFam;
        $infoPatient['tpersonal'] = $arrayTablePer;

        $infoPatient['infoHabit'] = $this->Doctor_model->getHabitosById($idPaciente);        
        
        $html = $this->load->view('historia_pdf_view',$infoPatient,TRUE);
        //$this->load->view('historia_pdf_view',$infoPatient);
        $filename = 'Historia_1';
        $this->mydompdf->generate($html, $filename, TRUE, 'A4', 'portrait');
    }

    /**
    * Funcion para guardar o actualizar los terminos 
    * y condiciones del servicio
    * @param textDoc : texto del documento
    */
    public function saveOrUpdateDoc(){
        $textoDocumento = $this->input->post('textDoc');
        $idDoc = $this->input->post('idDocumento');
        $jsonDoc = array();

        if($textoDocumento == NULL || $textoDocumento == ""){
            
            $jsonDoc['response_code'] = '400';
            $jsonDoc['response_msg'] = 'no se pueden guardar textos vacios';

        }else{

            $infoSaveDoc = array();

            if(isset($idDoc) || $idDoc == ""){
                $infoSaveDoc['ID_PER_FK'] = $this->session->userdata('id');
                $infoSaveDoc['TXT_DOC'] = $textoDocumento;
                if($this->Documentos_model->saveDoc($infoSaveDoc)){
                    $jsonDoc['response_code'] = '200';
                    $jsonDoc['response_msg'] = 'Operacion Exitosa';
                }else{
                    $jsonDoc['response_code'] = '400';
                    $jsonDoc['response_msg'] = 'Error al ejecutar';
                }
            }else{
                $infoSaveDoc['ID_PER_FK'] = $this->session->userdata('id');
                $infoSaveDoc['TXT_DOC'] = $textoDocumento;
                if($this->Documentos_model->updateDoc($infoSaveDoc,$idDoc)){
                    $jsonDoc['response_code'] = '200';
                    $jsonDoc['response_msg'] = 'Operacion Exitosa';
                }else{
                    $jsonDoc['response_code'] = '200';
                    $jsonDoc['response_msg'] = 'Error al ejecutar';
                }
            }
            
        }

        echo json_encode($jsonDoc);
    }
}