<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para las profesiones
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Home extends CI_Controller {
	
	/**
	* Funcion constructor de la clase Home_D
	*/
	public function __construct(){
		parent::__construct();

		if($this->session->userdata('name') == FALSE){
			$this->session->set_flashdata("error","ACCESO DENEGADO");
			redirect("Login");
		}else{
            //die(var_dump($this->session->userdata()));
            $numeroPermiso = '1';
            $permisos = explode(",", $this->session->userdata('permisos'));
            $numPer = count($permisos);
            $tienePermiso = FALSE;

            for ($i=0; $i < $numPer; $i++) {
                if($numeroPermiso == $permisos[$i]){
                    $tienePermiso = TRUE;
                    break;
                }
            }

            if($tienePermiso == FALSE){
                $this->session->set_flashdata("error","No Tiene Permisos (O_O;)");
                redirect("Login");
            }
		}
        $this->load->model("Admin_model");
		$this->load->model("Doctor_model");
	}

	/**
	*Funcion para llamar la vista de la clase Home
	*/
	public function index(){
		//variables para la vista

        $frament = array();
        $infoAdmin = array();
        $fechaActual = date("Y-m-d");
        $idDoctor = $this->session->userdata('id');
        // actualizamos la agenda de los pacientes que no se hayan atendidos
        $this->actualizaCitas($fechaActual,$idDoctor);
        
        if($this->session->userdata('tipo') == '1'){

            $infoAdmin['pacientes'] = $this->Admin_model->totalPacientes($idDoctor);
            $infoAdmin['citas'] = $this->Admin_model->totalCitas($idDoctor,$fechaActual);
            
            $frament['VISTA']= $this->load->view('admin_home_view',$infoAdmin,TRUE);
        }else{

            $infoAdmin['citas'] = $this->Admin_model->totalCitas($idDoctor,$fechaActual);
            
            $frament['VISTA']= $this->load->view('doc_home_view',$infoAdmin,TRUE);
        }
		
		
		$frament['ccsLibs'] = [''];
        $frament['jsLibs'] = ['core/jquery.countTo.js','home.js','core/notifications.js'];

		$this->load->view('dashboard_view',$frament);

	}


    public function actualizaCitas($fechaActual,$idDoctor){

        // obtenemos el numero de citas que no se atendieron
        $citasNoAten = $this->Admin_model->cuentaCitasNoAten($fechaActual,$idDoctor);

        // si hace la conversion de string a int
        $numCitas = intval($citasNoAten->NUM_CITAS);
        // si el numero de citas es mayor a cero se procesa a actualizar
        if($numCitas > 0){
            //die("si es mayor a 0");

            //lista de citas no atendidas
            $listaCitas = $this->Admin_model->getCitasNoAtendidas($fechaActual,$idDoctor);
            $numCit = sizeof($listaCitas);
            
            //recorremos la lista y hacemos el insert del detalle
            for ($i=0; $i < $numCit; $i++) {

                $detallesCitas = array(); 
                $detallesCitas['ID_PAC_FK'] = $listaCitas[$i]->ID_PAC_FK;
                $detallesCitas['ID_CITA_FK'] = $listaCitas[$i]->ID_CITA_PK;
                $detallesCitas['TA'] = '000/000';
                $detallesCitas['FC'] = '00';
                $detallesCitas['FR'] = '000';
                $detallesCitas['ID_PROC_FK'] = 99;
                $detallesCitas['OBS_PROC'] = 'Cita no atendida';

                $this->Admin_model->guardaDetalleCitaNoAtendida($detallesCitas);
            }
            // se les actualiza el estatus
            $this->Admin_model->actualizaCitasNoatendida($fechaActual,$idDoctor);
        }
    }

    public function getInfoUser(){
        $user_name = $this->input->post('name_user');
        $jsonInfo = array();
        if($user_name == NULL || $user_name == ''){
            $jsonInfo['response_code'] = '201';
            $jsonInfo['response_msg'] = 'el nombre de usuario esta vacio';
        }else{
            
            $jsonInfo['response_code'] = '200';
            $jsonInfo['response_msg'] = 'operacion exitosa';
            $jsonInfo['infoUsuario'] = $this->Admin_model->getDetalleUser($user_name);
        }

        echo json_encode($jsonInfo);
    }

    /*
    public function updateInfoUser(){
        
        $correo = $this->input->post('usr');
        $name = $this->input->post('name_user');
        $paterno = $this->input->post('ap_p');
        $materno = $this->input->post('ap_m');
        $password = $this->input->post('pass_n');
        $updateJson = array();
        $infoUpdate = array();

        if($correo != ""){
            $infoUpdate['NOMBRE_USR'] = $correo;
        }

        if($password != ""){
            $infoUpdate['PASSWORD_USR'] = $password;
        }


    }
    */
}
