<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ing, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar el seguimiento de los pacientes
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 07-12-2018
*/
class Seguimiento extends My_Controller {

    /**
    * Funcion constrcutor de la clase Seguimiento
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }

        $this->load->model('Seguimiento_model');
    }

    /**
    * Funcion para generar la vista de seguimiento
    */
    public function index(){
        date_default_timezone_set("America/Mexico_City");

        $my_date = new DateTime();
        $mesConsulta = date("F");
        $yearConsulta = date("Y");
        // fecha inicio del mes
        $my_date->modify('first day of '.$mesConsulta.$yearConsulta);
        $fechaIni = $my_date->format('Y-m-d');
        
        /* se quita opcion para obtener los ultimos 3 meses
        $fechaActual = date("Y-m-d");
        $nuevafecha = strtotime ('-90 day',strtotime ($fechaActual));
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        */

        $idDoctor = $this->session->userdata('id');
        $fragment = array();

        $fragment['VISTA'] = $this->load->view('seguimiento_view','',TRUE);
        $fragment['ccsLibs'] = ['js/core/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css','css/calendar/fullcalendar.min.css'];
        $fragment['citasAg'] = $this->Seguimiento_model->getCitas($fechaIni,$idDoctor);
        $fragment['jsLibs'] = ['core/moment.js','core/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js','core/calendar/fullcalendar.js','seguimiento.js'];

        $this->load->view('dashboard_view',$fragment);
    }

    /**
    * Funcion para cargar la vista de evolucion del
    * paciente , recibe el id del paciente, en caso de que no
    * se informe redirecciona a la pagina de home
    * @param idPac : id del paciente
    */
    public function viewEvolucion(){
        $idPaciente = $this->input->post('idPac');
        $historia = $this->input->post('continue');
        $citaPac = $this->input->post('idCitaPac');
        //validamos que no este nulo el id del paciente
        if($idPaciente != NULL || $idPaciente != ""){
            date_default_timezone_set("America/Mexico_City");
            $fechaActual = date("Y-m-d");
            $fragment = array();
            $datosView = array();
            
            //si es continue se crea un cita del dia
            if($historia == '1'){

                // se agrega la hora actual para hacer el registro de la cita
                $horaServidor  = time();
                $horaCita = date("H:i:s", $horaServidor);
                
                $infoCita = array();
                $infoCita['FECH_CITA'] = $fechaActual;
                $infoCita['HORA_CITA'] = $horaCita;
                $infoCita['ID_PAC_FK'] = $idPaciente;
                $infoCita['ID_EMP_FK'] = $this->session->userdata('id');
                $infoCita['ESTAT_CITA'] = '1';
                
                $this->Seguimiento_model->saveInfoCita($infoCita);
            }

            $datosView['infoPaciente'] = $this->Seguimiento_model->getPacInfo($idPaciente,$fechaActual);
            $datosView['procedimientos'] = $this->getCatalogo('PROCEDIMIENTOS');
            $datosView['padecDental'] = $this->Seguimiento_model->getPadecDentalById($idPaciente);

            $fragment['ccsLibs'] = ['js/core/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css','js/core/bootstrap-select/css/bootstrap-select.css'];
            $fragment['jsLibs'] = ['core/moment.js','core/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js','core/bootstrap-select/js/bootstrap-select.js','evolucion.js'];
            $fragment['VISTA'] = $this->load->view('evolucion_view',$datosView,TRUE);
            $fragment['infoPaciente'] = $datosView['infoPaciente'];
            
            $this->load->view('dashboard_view',$fragment);
        }else{
            $this->session->set_flashdata("error","No se puede Mostrar esta Seccion =(");
            redirect("Home");
        }
    }

    /**
    * 
    */
    public function updateCita(){
        $cita = $this->input->post('idCitaPac');
        $status = $this->input->post('statCita');
        //en caso de reagendar cita
        $fechaCita = $this->input->post('fecha');
        $horaCita = $this->input->post('hora');
        $jsonUpdate = array();
        $validaCita = TRUE;

        if($cita == NULL || $cita == ""){
            $validaCita = FALSE;
            $jsonUpdate['response_code'] = '400';
            $jsonUpdate['response_msg'] = 'La cita no esta informada';
        }else if($status == NULL || $status == ""){
            $validaCita = FALSE;
            $jsonUpdate['response_code'] = '400';
            $jsonUpdate['response_msg'] = 'La cita no esta informada';
        }

        if($validaCita == TRUE){

            $updateCita = array();
            $updateCita['ESTAT_CITA'] = $status;

            if($status == '2'){

                $updateCita['FECH_CITA'] = $fechaCita;
                $updateCita['HORA_CITA'] = $horaCita;
            }
            
            if($this->Seguimiento_model->updateCita($cita, $updateCita)){
                $jsonUpdate['response_code'] = '200';
                $jsonUpdate['response_msg'] = 'Actualzacion Exitosa';
            }else{
                $jsonUpdate['response_code'] = '400';
                $jsonUpdate['response_msg'] = 'Ocurrio un error =(';
            }
            
        }

        echo json_encode($jsonUpdate);
    }

    /**
    * 
    */
    public function saveDetalleCita(){
        
        $idPac = $this->input->post('idPaciente');
        $idC = $this->input->post('idCita');
        $tencion = $this->input->post('tencionArt');
        $frecuencia = $this->input->post('frecuenciaCar');
        $respiracion = $this->input->post('frecuenciaRes');
        $procedimiento = $this->input->post('proce');
        $observacion = $this->input->post('obs');
        $dienteEnfermo = $this->input->post('dienteEnf');
        $jsonDetalle = array();
        $validForm = TRUE;

        if($idPac == NULL || $idPac == ""){
            $validForm = TRUE;
            $jsonDetalle['response_code'] = '400';
            $jsonDetalle['response_msg'] = 'Id de paciente no informado';
        }

        if($idC == NULL || $idC == ""){
            $validForm = TRUE;
            $jsonDetalle['response_code'] = '400';
            $jsonDetalle['response_msg'] = 'Id de la cita no informado';
        }

        if($tencion == NULL || $tencion == ""){
            $validForm = TRUE;
            $jsonDetalle['response_code'] = '400';
            $jsonDetalle['response_msg'] = 'La tencion arterial no informada';
        }

        if($frecuencia == NULL || $frecuencia == ""){
            $validForm = TRUE;
            $jsonDetalle['response_code'] = '400';
            $jsonDetalle['response_msg'] = 'La fecuencia cardiaca no informada';
        }

        if($respiracion == NULL || $respiracion == ""){
            $validForm = TRUE;
            $jsonDetalle['response_code'] = '400';
            $jsonDetalle['response_msg'] = 'La Frecuencia Respiratoria no informada';
        }

        if($procedimiento == NULL || $procedimiento == ""){
            $validForm = TRUE;
            $jsonDetalle['response_code'] = '400';
            $jsonDetalle['response_msg'] = 'Procedimiento no informado';
        }

        if($validForm == TRUE){

            $infoDetalle = array();
            $infoDetalle['ID_PAC_FK'] = $idPac;
            $infoDetalle['ID_CITA_FK'] = $idC;
            $infoDetalle['TA'] = $tencion;
            $infoDetalle['FC'] = $frecuencia;
            $infoDetalle['FR'] = $respiracion;
            $infoDetalle['ID_PROC_FK'] = $procedimiento;
            $infoDetalle['OBS_PROC'] = $observacion;
            $citaUp = array();
            $citaUp['ESTAT_CITA'] = '0';

            if($this->Seguimiento_model->saveDetalle($infoDetalle)){
                
                $this->Seguimiento_model->updateCita($idC, $citaUp);
                
                $jsonDetalle['response_code'] = '200';
                $jsonDetalle['response_msg'] = 'Operecion Exitosa';
            }else{
                $jsonDetalle['response_code'] = '400';
                $jsonDetalle['response_msg'] = 'No se pudo Guardar la informacion';
            }
        }
        echo json_encode($jsonDetalle);
    }

    /**
    * Metodo para obtener las citas de un mes anterior o siguiente
    */
    public function getCalendarQuotes(){
        $mesConsulta = $this->input->post("mesCon");
        $yearConsulta = $this->input->post("yearCon");
        $jsonConCitas = array();
        $my_date = new DateTime();
        // fecha inicio del mes
        $my_date->modify('first day of '.$mesConsulta.$yearConsulta);
        $fechaIni = $my_date->format('Y-m-d');
        // fecha inicio del año
        $my_date->modify('last day of '.$mesConsulta.$yearConsulta);
        $fechaFin = $my_date->format('Y-m-d');
        // id del doctor que consulta
        $idDoctor = $this->session->userdata('id');
        $citas = $this->Seguimiento_model->getDiaryQuotes($fechaIni,$fechaFin,$idDoctor);

        if(is_null($citas)){
            $jsonConCitas ['response_code'] = '201';
            $jsonConCitas ['response_msg'] = 'Fecha sin citas';
        }else{
            $jsonConCitas ['response_code'] = '200';
            $jsonConCitas ['response_msg'] = 'Operacion Exitosa';
            $jsonConCitas ['citasMes'] = $citas;
        }

        echo json_encode($jsonConCitas);
    }

    /**
    * Funcion para agrgar a un paciente temporalmente
    * y poder agendar una cita desde la agenda
    * @param pacName
    * @param pacApp
    * @param pacApm
    * @param pacTel
    * @param pacFech
    * @param pacHora
    */
    public function addNewCitaPac(){

        $nombre = $this->input->post('pacName');
        $paterno = $this->input->post('pacApp');
        $materno = $this->input->post('pacApm');
        $cel = $this->input->post('pacTel');
        $fecha = $this->input->post('pacFech');
        $hora = $this->input->post('pacHora');
        $jsonAddNewPac = array();
        $validNewPac = TRUE;

        if($nombre == NULL || $nombre == ""){
            $jsonAddNewPac['response_code'] = '201';
            $jsonAddNewPac['response_msg'] = 'El nombre es nulo';
            $validNewPac = FALSE;
        }

        if($paterno == NULL || $paterno == ""){
            $jsonAddNewPac['response_code'] = '201';
            $jsonAddNewPac['response_msg'] = 'El appelido paterno es nulo';
            $validNewPac = FALSE;
        }

        if($materno == NULL || $materno == ""){
            $jsonAddNewPac['response_code'] = '201';
            $jsonAddNewPac['response_msg'] = 'El appelido materno es nulo';
            $validNewPac = FALSE;  
        }

        if($cel == NULL || $cel == ""){
            $jsonAddNewPac['response_code'] = '201';
            $jsonAddNewPac['response_msg'] = 'El telefono es nulo';
            $validNewPac = FALSE;
        }

        if($fecha == NULL || $fecha == ""){
            $jsonAddNewPac['response_code'] = '201';
            $jsonAddNewPac['response_msg'] = 'La fecha esta vacia';
            $validNewPac = FALSE;
        }

        if($hora == NULL || $hora == ""){
            $jsonAddNewPac['response_code'] = '201';
            $jsonAddNewPac['response_msg'] = 'La hora de la cita es nula';
            $validNewPac = FALSE;
        }

        if($validNewPac){
            

            $existePac = $this->Seguimiento_model->checkPacDb($nombre,$paterno,$materno);
            

            if(!is_null($existePac)){
                $jsonAddNewPac['response_code'] = '201';
                $jsonAddNewPac['response_msg'] = "El paciente {$nombre} ya existe, fue registrado el {$existePac->FCH_REG_PAC}";
            }else{
                /*
                $infoPac = array();
                // se le da un estatus de 3 para saber que esta incompleta la informacion del paciente
                $infoPac['NOMBRE_PAC'] = $nombre;
                $infoPac['APP_PAC'] = $paterno;
                $infoPac['APM_PAC'] = $materno;
                $infoPac['TEL_PAC'] = $cel;
                $infoPac['ESTAT_PAC'] = 3;
                $infoPac['FCH_REG_PAC'] = date("Y-m-d");

                $newIdPac = $this->Seguimiento_model->saveNewPac($infoPac);

                if($newIdPac){
                    
                    $infoNewCita = array();
                    $infoNewCita['FECH_CITA'] = $fecha;
                    $infoNewCita['HORA_CITA'] = $hora;
                    $infoNewCita['ID_PAC_FK'] = $newIdPac;
                    $infoNewCita['ID_EMP_FK'] = $this->session->userdata('id');
                    $infoNewCita['ESTAT_CITA'] = '1';

                    $citaID = $this->Seguimiento_model->saveNewCita($infoNewCita);

                    if($citaID){
                        $jsonAddNewPac['response_code'] = '200';
                        $jsonAddNewPac['response_msg'] = 'Operacion exitosa';
                        $jsonAddNewPac['id_pac'] = $newIdPac;
                        $jsonAddNewPac['id_cita'] = $citaID;
                    }
                    
                }else{
                    $jsonAddNewPac['response_code'] = '201';
                    $jsonAddNewPac['response_msg'] = 'No se pudo guardar la informacion del paciente';
                }*/
            }
            
        }
        echo json_encode($jsonAddNewPac);
    }

    /**
    * Metodo para hacer la busqueda de los nombres
    * de los pacuentes
    * @param searchName
    */
    public function searchNamePac(){

        $searchName = $this->input->post('pacNameSearch');
        $jsonSearchName = array();

        if($searchName == NULL || $searchName == ""){
            $jsonSearchName['response_code'] = '201';
            $jsonSearchName['response_msg'] = 'el nombre es requerido';
        }else{

            $jsonSearchName['response_code'] = '200';
            $jsonSearchName['response_msg'] = 'Operacion exitosa';
            $jsonSearchName['result_name'] = $this->Seguimiento_model->searchPacForName($searchName);
        }
        echo json_encode($jsonSearchName);
    }

    /**
    * Metodo para guardar las citas de los pacientes ya existentes
    */
    public function saveSearchPacCita(){

        $idPacSe = $this->input->post('idPacSearch');
        $fechPacSe = $this->input->post('fechaPacSearch');
        $horaPacSe = $this->input->post('horaPacSearch');
        $jsonSaveSearchPac = array();

        if($idPacSe == NULL || $idPacSe == ""){
            $jsonSaveSearchPac['response_code'] = '200';
            $jsonSaveSearchPac['response_msg'] = 'Debe selecionar algun paciente para agendar la cita';
        }else if($fechPacSe == NULL || $fechPacSe == ""){
            $jsonSaveSearchPac['response_code'] = '200';
            $jsonSaveSearchPac['response_msg'] = 'La fecha de la cita no esta indicada';
        }else if($horaPacSe == NULL || $horaPacSe == ""){
            $jsonSaveSearchPac['response_code'] = '200';
            $jsonSaveSearchPac['response_msg'] = 'La hora de la cita no esta indicada';
        }else{

            $newCitSearch = array();
            $newCitSearch['FECH_CITA'] = $fechPacSe;
            $newCitSearch['HORA_CITA'] = $horaPacSe;
            $newCitSearch['ID_PAC_FK'] = $idPacSe;
            $newCitSearch['ID_EMP_FK'] = $this->session->userdata('id');
            $newCitSearch['ESTAT_CITA'] = '1';

            $idCita = $this->Seguimiento_model->saveNewCita($newCitSearch);

            if($idCita){
                $jsonSaveSearchPac['response_code'] = '200';
                $jsonSaveSearchPac['response_msg'] = 'Operacion exitosa';
                $jsonSaveSearchPac['id_cita'] = $idCita;
            }
        }

        echo json_encode($jsonSaveSearchPac);
    }

    public function getEstatPac(){
        
        $paciente = $this->input->post('pacSearch');
        $jsonChekEstat = array();

        if($paciente == "" || $paciente == NULL){
            $jsonChekEstat['response_code'] = '400';
            $jsonChekEstat['response_msg'] = 'El id del paciente esta vacio';
        }else{
            $estatus = $this->Seguimiento_model->chekStatPac($paciente);
            
            if($estatus->ESTAT_PAC == '3'){
                $jsonChekEstat['response_code'] = '201';
                $jsonChekEstat['response_msg'] = 'Paciente sin información';
            }else{
                $jsonChekEstat['response_code'] = '200';
                $jsonChekEstat['response_msg'] = 'Paciente registrado';
            }
        }

        echo json_encode($jsonChekEstat);
    }
}

/*
SELECT CONTAC_WS(' ', NOMBRE_PAC, APP_PAC) as persona FROM personas WHERE concat_ws(' ', NOMBRE_PAC, APP_PAC) LIKE '%noe ramos%';

*/