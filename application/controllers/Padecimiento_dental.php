<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para los padecimientos dentales
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Padecimiento_dental extends CI_Controller {
    
    /**
    * Metodo Constrcutor de la clase Padecimiento_dental
    * valida si existe una sesion iniciada en caso de
    * que no, se regresa al Login con un mensaje de Error
    * en caso contrario lo deja entrar a la interfaz
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }else{
            $numeroPermiso = '13';
            $permisos = explode(",", $this->session->userdata('permisos'));
            $numPer = count($permisos);
            $tienePermiso = FALSE;

            for ($i=0; $i < $numPer; $i++) {
                if($numeroPermiso == $permisos[$i]){
                    $tienePermiso = TRUE;
                    break;
                }
            }

            if($tienePermiso == FALSE){
                $this->session->set_flashdata("error","ACCESO DENEGADO (O_O;)");
                redirect("Home");
            }
        }

        $this->load->model("PadecimientoDental_model");
    }

    /**
    * Metodo para mostrar la vista de los padecimientos
    * dentales con paginacion
    */
    public function index(){
        
        $numRegistros = 5;
        $pagianActual = 0;

        $datos = array();
        $datos['resultado'] = $this->PadecimientoDental_model->getPadecimientoPaginado($numRegistros,$pagianActual);
        
        $fragment = array();
        $fragment['VISTA'] = $this->load->view('padecimiento_view',$datos,TRUE);
        $fragment['ccsLibs'] = [""];
        $fragment['jsLibs'] = ['padecimientos.js','core/tablaAjax2.js'];

        $this->load->view('dashboard_view',$fragment);
    }

    /**
    * Funcion para obtener la información del
    * Padecimiento en base al id
    * @param $idPadecimiento
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [padecimiento] informacion del padecimiento
    */
    public function getInfoPadecimiento(){
        
        $ident = $this->input->post('idPadecimiento');
        $jsonInfoPadec = array();

        if($ident == NULL || $ident == ""){
            $jsonInfoPadec['response_code'] = '400';
            $jsonInfoPadec['response_msg'] = 'el id no puede estar vacio para la busqueda';
        }else{

            $infoPadecimiento = $this->PadecimientoDental_model->getInfoById($ident);

            if(!is_null($infoPadecimiento)){
                $jsonInfoPadec['response_code'] = '200';
                $jsonInfoPadec['response_msg'] = 'Operacion exitosa';
                $jsonInfoPadec['padecimiento'] = $infoPadecimiento;
            }else{
                $jsonInfoPadec['response_code'] = '400';
                $jsonInfoPadec['response_msg'] = 'la consulta no tiene resultado';
            }
        }

        echo json_encode($jsonInfoPadec);
    }

    /**
    * Funcion para agregar una nueva padecimiento 
    * al catologo o actualizar la informacion
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros a consultar
    * @param $filtro_2 nombre por el que se filtra
    * @param $filtro_1 id del padecimiento
    * @param $filtro_0 palabra para hacer el filtro
    * @param $namePadecimiento nombrel del padecimiento
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [padecimientos] listado de padecimientos
    */
    public function saveOrUpdatePadecimiento(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $idPadec = $this->input->post('filtro_1');
        $namePadec = $this->input->post('filtro_0');
        $filtro = $this->input->post('filtro_2');

        $jsonInfoPadec = array();
        $infoPadec = array();
        
        if($namePadec == NULL || $namePadec == ""){

            $jsonInfoPadec['response_code'] = '400';
            $jsonInfoPadec['response_msg'] = 'el nombre del padecimiento no puede estar vacio';

        }else if($idPadec == NULL || $idPadec == ""){
            //se hace una insercion
            $infoPadec['DESC_PAD'] = strtoupper($namePadec);
            $infoPadec['ESTAT_PAD'] = '1';

            if($this->PadecimientoDental_model->savePadecimiento($infoPadec)){
                $jsonInfoPadec['response_code'] = '200';
                $jsonInfoPadec['response_msg'] = 'Informacion guardada';
                $jsonInfoPadec['resultado'] = $this->PadecimientoDental_model->getPadecimientoPaginado(NULL,NULL);
            }else{
                $jsonInfoPadec['response_code'] = '400';
                $jsonInfoPadec['response_msg'] = 'Fallo durante la ejecucion';
            }
        }else{
            //se hace una actualizacion
            $infoPadec['DESC_PAD'] = strtoupper($namePadec);
            $infoPadec['ESTAT_PAD'] = '1';

            if($this->PadecimientoDental_model->updatePadecimiento($infoPadec,$idPadec)){
                
                $jsonInfoPadec['response_code'] = '200';
                $jsonInfoPadec['response_msg'] = 'Informacion actualizada';

                if($filtro == NULL || $filtro == ""){
                    $jsonInfoPadec['resultado'] = $this->PadecimientoDental_model->getPadecimientoPaginado($numero,$pagina);
                }else{
                    $jsonInfoPadec['resultado'] = $this->PadecimientoDental_model->getPadecimientoPaginadoFiltro($filtro,$numero,$pagina);
                }
                
            }else{
                $jsonInfoPadec['response_code'] = '400';
                $jsonInfoPadec['response_msg'] = 'Fallo durante la ejecucion';   
            }
        }
        echo json_encode($jsonInfoPadec);
    }

    /**
    * Funcion para hacer el borrado logico
    * de alguna padecimiento, recibe el id de la padecimiento
    * y actualiza el estatus a 0
    * @param $idPadecimiento 
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    */
    public function deletePadecimiento(){
        
        $padecimiento = $this->input->post('idPadecimiento');
        $jsonPadecimiento = array();
        
        if($padecimiento == NULL || $padecimiento == ""){
            
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'el ID no puede estar vacio o estar nulo';

        }else{

            $datosPadecimiento = array();
            $datosPadecimiento['ESTAT_PAD'] = '0';

            if($this->PadecimientoDental_model->updatePadecimiento($datosPadecimiento,$padecimiento)){

                $jsonPadecimiento['response_code'] = '200';
                $jsonPadecimiento['response_msg'] = 'Operacion Exitosa!';  
            }else{

                $jsonPadecimiento['response_code'] = '400';
                $jsonPadecimiento['response_msg'] = 'Algo salio mal intentelo mas tarde';
            }
        }
        echo json_encode($jsonPadecimiento);
    }

    /**
    * Funcion para obtener la siguiente o anterior pagina 
    * de los padecimientos dentales.
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros por pagina
    * @param $filtro_0 filtro para el paginado
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [resultado] resultado de la paginacion
    */
    public function getPaginacion(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $filtro = $this->input->post('filtro_0');

        $jsonPaginado = array();

        if($pagina == NULL || $pagina == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'La pagina debe de estar informada';

        }else if($numero == NULL || $numero == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'El numero de registros debe de estar informado';

        }else if($filtro == NULL || $filtro == ""){
            // se hace la busqueda sin fitro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->PadecimientoDental_model->getPadecimientoPaginado($numero,$pagina);

        }else{
            //Se hace la busqueda con filtro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->PadecimientoDental_model->getPadecimientoPaginadoFiltro($filtro,$numero,$pagina);

        }
        echo json_encode($jsonPaginado);
    }
}