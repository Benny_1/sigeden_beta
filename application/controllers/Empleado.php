<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para las profesiones
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Empleado extends CI_Controller {
    
    /**
    * Funcion constrcutor de la clase Home_A
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }else{
            $numeroPermiso = '2';
            $permisos = explode(",", $this->session->userdata('permisos'));
            $numPer = count($permisos);
            $tienePermiso = FALSE;

            for ($i=0; $i < $numPer; $i++) {
                if($numeroPermiso == $permisos[$i]){
                    $tienePermiso = TRUE;
                    break;
                }
            }

            if($tienePermiso == FALSE){
                $this->session->set_flashdata("error","ACCESO DENEGADO (O_O;)");
                redirect("Login");
            }
        }
        
        $this->load->model("Empleado_model");
    }

    /**
    * Funcion para cargar la vista de 
    * con el formulario de empleados
    * para guardar un nuevo empleado
    */
    public function index(){

        $fragment = array();
        $fragment['VISTA'] = $this->load->view('empleado_view','',TRUE);
        $fragment['ccsLibs'] = [''];
        $fragment['jsLibs'] = ['empleado.js'];

        $this->load->view('dashboard_view',$fragment);
    }

    /**
    * Funcion que carga la vista 
    * con la lista de empleados para editar
    * la informacion
    */
    public function viewEditEmploy(){

        $numRegistros = 5;
        $pagianActual = 0;

        $datosEdit = array();
        $datosEdit['resultado'] = $this->Empleado_model->getEmpleados($numRegistros,$pagianActual);
        
        $fragment = array();
        $fragment['VISTA'] = $this->load->view('empleado_edit_view',$datosEdit,TRUE);
        $fragment['ccsLibs'] = [''];
        $fragment['jsLibs'] = ['core/tablaAjax2.js','editEmpleado.js'];

        $this->load->view('dashboard_view',$fragment);
    }

    /**
    * Funcion para guardar o actalizar la informacion del paciente
    * si el id viene vacio se crea un nuevo registro de 
    * caso contrario se actualiza la informacion 
    * @param idEmp : id del empleado
    * @param name : nombre del empleado
    * @param lasName : apellido paterno
    * @param mLastName : apellido materno
    * @param phone : telefono
    * @param email : correo
    * @param gener : genero
    * @param salary : sueldo
    */
    public function saveOrUpdateEmploy(){

        $idEmpleado = $this->input->post('idEmp');
        $nombre = $this->input->post('name');
        $paterno = $this->input->post('lastName');
        $materno = $this->input->post('mLastName');
        $telefono = $this->input->post('phone');
        $correo = $this->input->post('email');
        $genero = $this->input->post('gener');
        $salario = $this->input->post('salary');
        $jsonEmploy = array();
        $validForm = TRUE;

        //se valian los campos a ingresar
        if(empty($nombre)){
            $validForm = FALSE;
        }else if(empty($paterno)){
            $validForm = FALSE;
        }else if(empty($materno)){
            $validForm = FALSE;
        }else if(empty($genero)){
            $validForm = FALSE;
        }else if(empty($salario) || $salario == '0.00'){
            $validForm = FALSE;
        }

        //si se valida con exito se guarda y se responde un codigo 200
        if($validForm == TRUE){

            $infoEmploy = array();

            $infoEmploy['NOM_EMP'] = trim($nombre);
            $infoEmploy['APP_EMP'] = trim($paterno);
            $infoEmploy['APM_EMP'] = trim($materno);
            $infoEmploy['TEL_EMP'] = trim($telefono);
            $infoEmploy['EMAIL_EMP'] = trim($correo);
            $infoEmploy['SUELDO_EMP'] = trim($salario);
            $infoEmploy['ESTAT_EMP'] = '1';
            $infoEmploy['FCH_REG_EMP'] = date("Y-m-d");

            if($this->Empleado_model->saveOrUpdateInfoEmploy($infoEmploy,$idEmpleado)){
                $jsonEmploy['response_code'] = '200';
                $jsonEmploy['response_msg'] = 'Operacion Exitosa!';
            }else{
                $jsonEmploy['response_code'] = '400';
                $jsonEmploy['response_msg'] = 'Ocurrio un error al guardar la informacion!';
            }
        }else{
            $jsonEmploy['response_code'] = '400';
            $jsonEmploy['response_msg'] = 'Alguno o varios campos Estan vacios';
        }
        echo json_encode($jsonEmploy);
    }

    /**
    * Funcion para que validar el id 
    * del empleado
    */
    public function getEmployById(){
        $idEmpleado = $this->input->post('idEmp');
        $infoEmploy = array();

        if($idEmpleado == NULL || $idEmpleado == ""){
            $infoEmploy['response_code'] = '400';
            $infoEmploy['response_msg'] = 'El id del empleado esta vacio';
        }else{
            $infoEmploy['response_code'] = '200';
            $infoEmploy['response_msg'] = 'Operacion exitosa!';
            $infoEmploy['employInfo'] =  $this->Empleado_model->getEmploy($idEmpleado);
        }
        echo json_encode($infoEmploy);
    }

    /**
    * Funcion para dar de baja a un empleado
    * valida si el id viene informado
    * en caso contrario regresa un codigo
    * de respuesta 400
    * @param idEmp : id del empleado
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function deleteEmploy(){

        $identificador = $this->input->post('idEmp');
        $jsonBajaEmp = array();

        if($identificador == NULL || $identificador == ""){
            $jsonBajaEmp['response_code'] = '400';
            $jsonBajaEmp['response_msg'] = 'el id del empleado a dar de baja esta vacio';
        }else{

            if($this->Empleado_model->bajaEmploy($identificador)){
                $jsonBajaEmp['response_code'] = '200';
                $jsonBajaEmp['response_msg'] = 'Operacion exitosa!';
            }else{
                $jsonBajaEmp['response_code'] = '400';
                $jsonBajaEmp['response_msg'] = 'Ocurrio un error al guardar la informacion';
            }
        }
        echo json_encode($jsonBajaEmp);
    }

    /**
    * Funcion para obtener la siguiente o anterior pagina 
    * de las toxicomanias.
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros por pagina
    * @param $filtro_0 filtro para el paginado
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [resultado] resultado de la paginacion
    */
    public function getPaginacion(){
        $filtro = array();
        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $filtro[0] = $this->input->post('filtro_0');
        $filtro[1] = $this->input->post('filtro_1');
        $filtro[2] = $this->input->post('filtro_2');

        $jsonPaginado = array();

        if($pagina == NULL || $pagina == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'La pagina debe de estar informada';

        }else if($numero == NULL || $numero == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'El numero de registros debe de estar informado';

        }else{
            //Se hace la busqueda con filtro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Empleado_model->getEmployPaginadoFiltro($filtro,$numero,$pagina);
        }
        echo json_encode($jsonPaginado);
    }
}