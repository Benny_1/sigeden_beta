<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author ING, Noe Ramos Lopez
 * @version 0.1
 * @copyright Todos los derechos reservados 2017
*/
class Login extends CI_Controller {
	/**
	* Funcion para le constructor de la clase Login
	*/
	public function __construct(){
		parent::__construct();
		$this->load->library('session'); 
		$this->load->model("Login_model");
		$this->load->model("Permisos_model");
	}

	/**
	*Funcion para mostrar la vista
	*/
	public function index(){
		$this->load->view('login_view');
	}

	/**
	*Funcion para validar a un usuario
	*/
	public function userdo(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$check_r = $this->input->post('rememberme');

		$encript = NULL;

		if($username == "" || $password == ""){
			$this->session->set_flashdata('error','LOS CAMPOS ESTAN VACIOS');
			redirect("Login");
		}else{

			$user = $this->Login_model->validateUser($username);

			if(!is_null($user)){

				$encript = hash('sha256',$password,FALSE);
				$idUser = $user->ID_USR_PK;

				if($user->PASSWORD_USR == $encript){
                    
                    $permisos = $this->Permisos_model->getPermissionByIdRol($user->ID_ROL_PK);
                    /*
					$datos_usuario = array(

						'id' => $user->ID_USR_PK,
						'name' => $user->NOMBRE_USR,
						'tipo' => $user->ID_TIPO_US_FK,
                        'permisos' => $user->DSC_PERMISO
					);*/

					$datos_usuario = array(

						'id' => $user->ID_USR_PK,
						'name' => $user->NOMBRE_USR,
						'tipo' => $user->ID_ROL_PK,
                        'permisos' => $permisos->PERMISOS
					);
					
                    $this->session->set_userdata($datos_usuario);
                    // -> se comenta la pista ya que no hay una relacion sertera 
                    // entre la tabla de empelados con usuarios 
                    $this->grabaPistaDb($idUser);
                    redirect("Home");

				}else{
					$this->session->set_flashdata('error','LA CONTRASEÑA NO ES VALIDA');
					redirect("Login/");
				}

			}else{

				$this->session->set_flashdata('error','UNO DE LOS CAMPOS ESTA ERRONEO VERIFIQUE POR FAVOR');
				redirect("Login/");
			}
		}
	}

	/**
	* Funcion para grabar una pista de entrada al sistema 
	* en la base de datos historica.
	*/
	public function grabaPistaDb($usuario){
		
		$horaServidor  = time();
		$hora = date("H:i:s", $horaServidor);
		
		$jsonPista = array();

		$jsonPista['ID_USR_FK'] = $usuario;
		$jsonPista['HORA_ING'] = $hora;
		$jsonPista['FECH_ING'] = date("Y-m-d");

		$this->Login_model->grabaPista($jsonPista);
	}

	/**
	*funcion para el cierre de sesion
	*/
	public function logaut(){

		$arr_sesiones = array();
		$arr_sesiones ['id'] = FALSE;
		$arr_sesiones ['name'] = FALSE;
		$arr_sesiones ['tipo'] = FALSE;

		$this->session->sess_destroy();

		redirect("Login");
	}
}
