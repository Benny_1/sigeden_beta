<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ing, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para la administracion de la agenda
 * Fecha de creacion : 23-06-2020
 * Fecha de actualzacion : N/A
*/
class Cuentas extends CI_Controller {
    
    /**
    * Funcion constrcutor de la clase Cuentas
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }

        $this->load->model("Cuentas_model");
    }

    /**
    * Carga la vista principal de las cuentas
    */
    public function index(){
        $NR = 5;
        $PAG = 0;
        $infoCuentas = array();
        $fragCuenta = array();

        $infoCuentas['resultado'] = $this->Cuentas_model->getAllAcountPag($NR,$PAG);
        $fragCuenta['VISTA'] = $this->load->view('cuentas_view',$infoCuentas,TRUE);
        $fragCuenta['ccsLibs'] = [''];
        $fragCuenta['jsLibs'] = ['cuentas.js','core/tablaAjax2.js','tabla_contabilidad.js'];
        
        $this->load->view('dashboard_view',$fragCuenta);
    }


    /**
    * Funcion para obtener la siguiente o anterior pagina 
    * de las toxicomanias.
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros por pagina
    * @param $filtro_0 filtro para el paginado
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [resultado] resultado de la paginacion
    */
    public function getPaginacion(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $filtro = $this->input->post('filtro_0');

        $jsonPaginado = array();

        if($pagina == NULL || $pagina == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'La pagina debe de estar informada';

        }else if($numero == NULL || $numero == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'El numero de registros debe de estar informado';

        }else if($filtro == NULL || $filtro == ""){
            // se hace la busqueda sin fitro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Cuentas_model->getAllAcountPag($numero,$pagina);

        }else{
            //Se hace la busqueda con filtro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Cuentas_model->getAllAcountPagFiltro($filtro,$numero,$pagina);

        }
        echo json_encode($jsonPaginado);
    }

    public function saveNewAcount(){

        $paciente = $this->input->post('idPacNewAcount');
        $montoCuenta = $this->input->post('mountNewAcount');
        $jsonNewAcount = array();

        if($paciente == "" || $paciente == NULL || $paciente == '0'){
            $jsonNewAcount['response_code'] = '201';
            $jsonNewAcount['response_msg'] = 'paciente no informado';
        }else if($montoCuenta == "" || $montoCuenta == NULL || $montoCuenta == 0.00){
            $jsonNewAcount['response_code'] = '201';
            $jsonNewAcount['response_msg'] = 'monto no puede estar vacio o en ceros';
        }else{

            $infoAcount = array();
            $infoAcount['ID_PAC_FK'] = $paciente;
            $infoAcount['FECHA_CARGO'] = date("Y-m-d");
            $infoAcount['TOTAL_CUENTA'] = $montoCuenta;

            if($this->Cuentas_model->saveAcount($infoAcount)){
                $jsonNewAcount['response_code'] = '200';
                $jsonNewAcount['response_msg'] = 'Operacion exitosa';
            }
        }
        echo json_encode($jsonNewAcount);
    }

    public function getAcountDetail(){

        $searchAcount = $this->input->post('idAcountSearch');
        $jsonSearcDetail = array();
        if($searchAcount == "" || $searchAcount == NULL){
            $jsonSearcDetail['response_code'] = '201';
            $jsonSearcDetail['response_msg'] = 'Cuenta vacia';
        }else{

            $deuda = $this->Cuentas_model->getMontoCuenta($searchAcount);
            $deatilAcount = $this->Cuentas_model->getDetailAcount($searchAcount);

            if(!is_null($deatilAcount) || !is_null($deuda)){
                $jsonSearcDetail['response_code'] = '200';
                $jsonSearcDetail['response_msg'] = 'Operacion exitosa';
                $jsonSearcDetail['abonos'] = $deatilAcount;
                $jsonSearcDetail['cargos'] = $deuda;
            }else{
                $jsonSearcDetail['response_code'] = '201';
                $jsonSearcDetail['response_msg'] = 'La cuenta no tiene detalle';
            }
        }
        echo json_encode($jsonSearcDetail);
    }

    public function saveNewMountAcount(){

        $cuenta = $this->input->post('idAcountAdd');
        $monto = str_replace(',','',$this->input->post('mountAdd'));
        $jsonNewAbono = array();

        if($cuenta == "" || $cuenta == NULL){
            $jsonNewAbono['response_code'] = '201';
            $jsonNewAbono['response_msg'] = 'Num de cuenta vacia';
        }else if($monto == "" || $monto == NULL){
            $jsonNewAbono['response_code'] = '201';
            $jsonNewAbono['response_msg'] = 'El monto no puede ser cero';
        }else{

            $result = $this->Cuentas_model->getAllAbono($cuenta);
            $totAbono =  str_replace(',','',$result->ABONADO);
            $totalAbonos = $totAbono + $monto;
            $deuda = str_replace(',','',$result->TOTAL_CUENTA);

            if($totalAbonos > $deuda){
                $jsonNewAbono['response_code'] = '201';
                $jsonNewAbono['response_msg'] = 'El monto a abonar es mayor a la deuda';
            }else{

                $datosAbono = array();
                $datosAbono['ID_CUENTA_FK'] = $cuenta;
                $datosAbono['FECHA_ABONO'] = date("Y-m-d");
                $datosAbono['MONTO_ABONO'] = $monto;

                if($this->Cuentas_model->saveNewMount($datosAbono)){

                    $jsonNewAbono['response_code'] = '200';
                    $jsonNewAbono['response_msg'] = 'Operacion exitosa';
                }else{

                    $jsonNewAbono['response_code'] = '201';
                    $jsonNewAbono['response_msg'] = 'Algo salio mal intentelo mas tarde';
                }
            }
        }
        echo json_encode($jsonNewAbono);
    }


    public function updateMountsOnAcount(){
        
        $valoresMontos = $this->input->post('newMountOnAcount');
        $numCuenta = $this->input->post('numCuenta');
        $jsonUpdateAcount = array();
        if($valoresMontos == NULL || $numCuenta == NULL){
            $jsonUpdateAcount['response_code'] = '201';
            $jsonUpdateAcount['response_msg'] = 'los valores estan vacios';
        }else{
            $contRegistros = 0;
            foreach ($valoresMontos as $montos ) {

                if($this->clasificaMov($numCuenta,$montos['tipoMov'],$montos['idMov'],$montos['valMonto'])){
                    $contRegistros++;
                }
            }

            if($contRegistros == count($valoresMontos)){
                $jsonUpdateAcount['response_code'] = '200';
                $jsonUpdateAcount['response_msg'] = 'Operacion exitosa';
            }
        }
        echo json_encode($jsonUpdateAcount);
    }

    private function clasificaMov($numCuenta, $tipoMov, $idMov, $montoMov){
        $datosCorrectos = FALSE;
        switch ($tipoMov) {
            case 'C':
                if($this->updateCargo($idMov,$montoMov)){
                    $datosCorrectos = TRUE;
                }
                break;
            case 'A':
                if($this->updateAbono($idMov,$montoMov)){
                    $datosCorrectos = TRUE;   
                }
                break;
            case 'NA':
                if($this->saveNewAbono($numCuenta,$montoMov)){
                    $datosCorrectos = TRUE;
                }
                break;
        }
        return $datosCorrectos;
    }

    private function updateCargo($idMov,$monto){
        $formateaMonto = str_replace(',','',$monto);
        if($this->Cuentas_model->updateAcountMount($idMov,$formateaMonto)){
            return TRUE;
        }
    }

    private function updateAbono($idMov,$monto){
        $formateaMonto = str_replace(',','',$monto);
        if($this->Cuentas_model->updateAcountAbono($idMov,$formateaMonto)){
            return TRUE;
        }
    }

    private function saveNewAbono($idCuenta,$monto){
        $datosNewAbono = array();
        $formateaMonto = str_replace(',','',$monto);
        $datosNewAbono['ID_CUENTA_FK'] = $idCuenta;
        $datosNewAbono['FECHA_ABONO'] = date("Y-m-d");
        $datosNewAbono['MONTO_ABONO'] = $formateaMonto;
        if($this->Cuentas_model->saveNewMount($datosNewAbono)){
            return TRUE;
        }
    }
}