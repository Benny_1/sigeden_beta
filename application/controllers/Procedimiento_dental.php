<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ing, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para los padecimientos dentales
 * Fecha de creacion : 18-06-2020
 * Fecha de actualzacion : N/A
*/
class Procedimiento_dental extends CI_Controller {
    
    /**
    * Metodo Constrcutor de la clase Procedimiento_dental
    * valida si existe una sesion iniciada en caso de
    * que no, se regresa al Login con un mensaje de Error
    * en caso contrario lo deja entrar a la interfaz
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }else{
            $numeroPermiso = '14';
            $permisos = explode(",", $this->session->userdata('permisos'));
            $numPer = count($permisos);
            $tienePermiso = FALSE;

            for ($i=0; $i < $numPer; $i++) {
                if($numeroPermiso == $permisos[$i]){
                    $tienePermiso = TRUE;
                    break;
                }
            }

            if($tienePermiso == FALSE){
                $this->session->set_flashdata("error","ACCESO DENEGADO (O_O;)");
                redirect("Home");
            }
        }

        $this->load->model("Procedimiento_model");
    }

    /**
    * Metodo para mostrar la vista de los padecimientos
    * dentales con paginacion
    */
    public function index(){
        
        $numRegistros = 5;
        $pagianActual = 0;

        $datos = array();
        $datos['resultado'] = $this->Procedimiento_model->getProcedimientoPaginado($numRegistros,$pagianActual);
        
        $fragment = array();
        $fragment['VISTA'] = $this->load->view('procedimiento_view',$datos,TRUE);
        $fragment['ccsLibs'] = [""];
        $fragment['jsLibs'] = ['procedimiento.js','core/tablaAjax2.js'];

        $this->load->view('dashboard_view',$fragment);
    }

    /**
    * Funcion para obtener la informacion de un 
    * procedimiento dental 
    * en base al id de que se le manda
    * @param idProcedimiento
    */
    public function getInfoProcedimiento(){
        
        $idProce = $this->input->post('idProcedimiento');
        $jsonInfoProce = array();

        if($idProce == NULL || $idProce == ""){
            $jsonInfoProce['response_code'] = '400';
            $jsonInfoProce['response_msg'] = 'el id no puede estar vacio para la busqueda';
        }else{

            $infoProcedimiento = $this->Procedimiento_model->getInfoProceById($idProce);

            if(!is_null($infoProcedimiento)){
                $jsonInfoProce['response_code'] = '200';
                $jsonInfoProce['response_msg'] = 'Operacion exitosa';
                $jsonInfoProce['procedimiento'] = $infoProcedimiento;
            }else{
                $jsonInfoProce['response_code'] = '400';
                $jsonInfoProce['response_msg'] = 'la consulta no tiene resultado';
            }
        }

        echo json_encode($jsonInfoProce);
    }

    /**
    * Metodo para guardar o actualizar un procedimiento
    * para dejar el filtro como estaba se manda los datos
    * paginado
    * @param paginaConsulta
    * @param numeroRegistros
    * @param filtro_1
    * @param filtro_0
    * @param filtro_2
    */
    public function saveOrUpdateProcedimiento(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $idProce = $this->input->post('filtro_1');
        $nameProce = $this->input->post('filtro_0');
        $filtro = $this->input->post('filtro_2');

        $jsonSaveOrUpdateProce = array();
        $infoProce = array();
        
        if($nameProce == NULL || $nameProce == ""){

            $jsonSaveOrUpdateProce['response_code'] = '400';
            $jsonSaveOrUpdateProce['response_msg'] = 'el nombre del procedimiento no puede estar vacio';

        }else if($idProce == NULL || $idProce == ""){
            //se hace una insercion
            $infoProce['DSC_PROC'] = strtoupper($nameProce);
            $infoProce['ESTAT_PROC'] = '1';

            if($this->Procedimiento_model->saveProcedimiento($infoProce)){
                $jsonSaveOrUpdateProce['response_code'] = '200';
                $jsonSaveOrUpdateProce['response_msg'] = 'Informacion guardada';
                $jsonSaveOrUpdateProce['resultado'] = $this->Procedimiento_model->getProcedimientoPaginado(NULL,NULL);
            }else{
                $jsonSaveOrUpdateProce['response_code'] = '400';
                $jsonSaveOrUpdateProce['response_msg'] = 'Fallo durante la ejecucion';
            }
        }else{
            //se hace una actualizacion
            $infoProce['DSC_PROC'] = strtoupper($nameProce);
            $infoProce['ESTAT_PROC'] = '1';

            if($this->Procedimiento_model->updateProcedimiento($infoProce,$idProce)){
                
                $jsonSaveOrUpdateProce['response_code'] = '200';
                $jsonSaveOrUpdateProce['response_msg'] = 'Informacion actualizada';

                if($filtro == NULL || $filtro == ""){
                    $jsonSaveOrUpdateProce['resultado'] = $this->Procedimiento_model->getProcedimientoPaginado($numero,$pagina);
                }else{
                    $jsonSaveOrUpdateProce['resultado'] = $this->Procedimiento_model->getProcedimientoPaginadoFiltro($filtro,$numero,$pagina);
                }
                
            }else{
                $jsonSaveOrUpdateProce['response_code'] = '400';
                $jsonSaveOrUpdateProce['response_msg'] = 'Fallo durante la ejecucion';   
            }
        }
        echo json_encode($jsonSaveOrUpdateProce);
    }

    /**
    * Metodo para dar de baja un procedimiento
    * @param idPadecimiento
    */
    public function deleteProcedimiento(){
        
        $procedimiento = $this->input->post('idPadecimiento');
        $jsonDeleteProce = array();
        
        if($procedimiento == NULL || $procedimiento == ""){
            
            $jsonDeleteProce['response_code'] = '400';
            $jsonDeleteProce['response_msg'] = 'el ID no puede estar vacio o estar nulo';

        }else{

            $datosProce = array();
            $datosProce['ESTAT_PROC'] = '0';

            if($this->Procedimiento_model->updateProcedimiento($datosProce,$procedimiento)){

                $jsonDeleteProce['response_code'] = '200';
                $jsonDeleteProce['response_msg'] = 'Operacion Exitosa!';  
            }else{

                $jsonDeleteProce['response_code'] = '400';
                $jsonDeleteProce['response_msg'] = 'Algo salio mal intentelo mas tarde';
            }
        }
        echo json_encode($jsonDeleteProce);
    }

    /**
    * Funcion para obtener la siguiente o anterior pagina 
    * de los padecimientos dentales.
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros por pagina
    * @param $filtro_0 filtro para el paginado
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [resultado] resultado de la paginacion
    */
    public function getPaginacion(){

        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $filtro = $this->input->post('filtro_0');

        $jsonPaginado = array();

        if($pagina == NULL || $pagina == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'La pagina debe de estar informada';

        }else if($numero == NULL || $numero == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'El numero de registros debe de estar informado';

        }else if($filtro == NULL || $filtro == ""){
            // se hace la busqueda sin fitro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Procedimiento_model->getProcedimientoPaginado($numero,$pagina);

        }else{
            //Se hace la busqueda con filtro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Procedimiento_model->getProcedimientoPaginadoFiltro($filtro,$numero,$pagina);

        }
        echo json_encode($jsonPaginado);
    }
}