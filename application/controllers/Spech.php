<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ing, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar el seguimiento de los pacientes
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 07-12-2018
*/
class Spech extends My_Controller {

    /**
    * Funcion constrcutor de la clase Seguimiento
    */
    public function __construct(){
        parent::__construct();

        if($this->session->userdata('name') == FALSE){
            $this->session->set_flashdata("error","ACCESO DENEGADO");
            redirect("Login");
        }
    }

    /**
    * Funcion para generar la vista de seguimiento
    */
    public function index(){

        $fragment['VISTA'] = $this->load->view('experimento_voz_view','',TRUE);
        $fragment['ccsLibs'] = [''];
        $fragment['jsLibs'] = ['voz.js'];

        $this->load->view('dashboard_view',$fragment);
    }
}