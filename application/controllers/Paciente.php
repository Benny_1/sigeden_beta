<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para las profesiones
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Paciente extends My_Controller {
	
	/**
	* Constructor de la clase Paciente
	*/	
	public function __construct(){
		parent::__construct();

		if($this->session->userdata('name') == FALSE){
			$this->session->set_flashdata("error","ACCESO DENEGADO");
			redirect("Login");
		}
        
		$this->load->model("Doctor_model");
        $this->load->model("Documentos_model");
	}

	/**
	* Funcion para llamar la vista
    * donde se muestra el formulario
    * para la captura de un paciente.
	*/
	public function index(){
		//variables para la vista
		$datos = array();
		$fragment = array();

        //ARREGLO QUE CONTIENE LA INFORMACION QUE SE MOSTRARA EN LA VISTA
		$datos['estados'] = $this->getCatalogo("ESTADOS");
		$datos['enfermedades'] = $this->getCatalogo("ENFERMEDADES");
		$datos['familiares'] = $this->getCatalogo("FAMILIARES");
		$datos['profesiones'] = $this->getCatalogo("PROFESIONES");
		$datos['dientes'] = $this->getCatalogo("DIENTES");
		$datos['padecimientos'] = $this->getCatalogo("PADECIMIENTOS");
        $datos['adicciones'] = $this->getCatalogo("TOXICOMANIAS");
        $datos['alertas'] = $this->Doctor_model->getAlertEnfer();

        //SE MANDA LLAMAR LA VISTA Y SE LE MANDA LA INFORMACION
		$fragment['VISTA'] = $this->load->view('paciente_view',$datos,TRUE);
        $fragment['ccsLibs'] = ['js/core/bootstrap-select/css/bootstrap-select.css'];
        $fragment['jsLibs'] = ['form-paciente.js','core/bootstrap-select/js/bootstrap-select.js'];

        //SE CARGA EL DASHBOARD CON EL FRAGMENTO DE LA VISTA		
        $this->load->view('dashboard_view',$fragment);
	}

    /**
    * Funcion para guardar la informacion del paciente
    * se obtiene la informacion principal del paciente
    * se valida que no exista esta informacion, en caso
    * de que exista se regresa un mensaje de que el paciente 
    *  ya esta registrado y se regresa el id del paciente
    * , en caso de que no exista se guarda la informacion 
    * y se le genera un id para poder identificarlo, se regresa
    * un mensaje de operacion exitosa y se regresa el id asignado
    * @param $idPatient : identificador
    * @param $namePatient : nombre
    * @param $appPatient : apellido paterno
    * @param $apmPatient : apellido materno
    * @param $fecNac : fecha de nacimiento
    * @param $estado : estado
    * @param $direccion : direccion
    * @param $genero : genero
    * @param $profesion : id profesion
    * @param $estadoCivil : id estado civil
    * @param $numTel : numero telefonico
    * @param $correo : correo
    * @param $obs_pac : observacion del paciente
    * @return JSON [response_code]
    *              [json_msg]
    *              [id_persona]
    */
	public function saveOrUpdateInfoPatient(){

		$json = array();
		//OBTENEMOS TODOS LOS CAMPOS QUE NOS ENVIAN
        $idPac = $this->input->post("idPatient");
		$name = $this->input->post("namePatient");
		$apPaterno = $this->input->post("appPatient");
		$apMaterno = $this->input->post("apmPatient");
		$fechaNac = $this->input->post("fecNac");
		$estado = $this->input->post("estado");
		$direccion = $this->input->post("direccion");
		$genero = $this->input->post("genero");
		$idProf = $this->input->post("profesion");
		$idEstadoCivil = $this->input->post("estadoCivil");
		$numTelefono = $this->input->post("numTel");
		$correo = $this->input->post("correo");
        $obs_pac = $this->input->post("observacion");

		$validForm = TRUE;

		//SE VALIDA QUE NO ESTE VACIO O NULO EL NOMBRE
		if($name == "" || $name == NULL || empty($name)){
			$validForm = FALSE;
			$json['response_code'] = '400';
			$json['json_msg'] = 'EL Nombre no puede estar vacio.';
		}

        //SE VALIDA QUE NO ESTE VACIO O NULO EL APELLIDO PATERNO
		if($apPaterno == "" || $apPaterno == NULL || empty($apPaterno)){
			$validForm = FALSE;
			$json['response_code'] = '400';
			$json['json_msg'] = 'EL Apellido Paterno no puede estar vacio.';
		}

        //SE VALIDA QUE NO ESTE VACIO O NULO EL APELLIDO MATERNO
		if($apMaterno == "" || $apMaterno == NULL || empty($apMaterno)){
			$validForm = FALSE;
			$json['response_code'] = '400';
			$json['json_msg'] = 'EL Apellido Paterno no puede estar vacio.';
		}

        //SE VALIDA QUE NO ESTE VACIO O NULO LA FECHA DE NACIMIENTO
		if($fechaNac == "" || $fechaNac == NULL || empty($fechaNac)){
			$validForm = FALSE;
			$json['response_code'] = '400';
			$json['json_msg'] = 'La fecha de nacimiento no puede estar vacio.';
		}

        //SE VALIDA QUE NO ESTE VACIO,NULO O QUE SEA CERO EL ID DEL ESTADO
		if($estado == "0" || $estado == NULL || $estado == ""){
			$validForm = FALSE;
			$json['response_code'] = '400';
			$json['json_msg'] = 'EL Estado no puede estar vacio.';
		}

        //SE VALIDA QUE NO ESTE VACIO O NULO EL GENERO DE LA PERSONA
		if($genero == "" || $genero == NULL){
			$validForm = FALSE;
			$json['response_code'] = '400';
			$json['json_msg'] = 'El genero no puede estar vacio.';
		}

        //SE VALIDA QUE NO ESTE VACIO, NULO O 0 EL ID DE LA PROFECION
		if($idProf == "0" || $idProf == "" || $idProf == NULL){
			$validForm = FALSE;
			$json['response_code'] = '400';
			$json['json_msg'] = 'La profesion no puede estar vacio.';
		}
        
        //SE VALIDA QUE NO ESTE VACIO, NULO O 0 EL ID DEL ESATDO CIVIL
		if($idEstadoCivil == "0" || $idEstadoCivil == "" || $idEstadoCivil == NULL){
			$validForm = FALSE;
			$json['response_code'] = '400';
			$json['json_msg'] = 'El estado civil no puede estar vacio.';
		}

        //SI LA DIRECCION ES NULA LA PONEMOS COMO SIN DIRECCION
        //EN CASO DE QUE NO SE LIMPIAN LOS ESPACIOS
		if(empty($direccion)){
			$direccion = "SIN DIRECCION";
		}else{
			$direccion = trim($direccion);
		}

        //SE VALIDA QUE EL NUMERO NO ESTE VACIO
        //SI ESTA VACIO SE CAMBIA POR NULL
		if(empty($numTelefono)){
			$numTelefono = NULL;
		}

        //SE VALIDA SI EL CORREO ESTA VACIOS
        // SI ESTA VACIO SE CAMBIA POR NULL
		if(empty($correo)){
			$correo = NULL;
		}

        //SI NO HUBO NINGUN PROBELMA CONTINUA PARA GUARDAR LA INFORMACION 
		if($validForm == TRUE){

            // SE LE DA FORMATO A LA FECHA QUE RECIBIMOS COMO STRING
			$time = strtotime($fechaNac);
			$newformat = date('Y-m-d',$time);

            // HORA EN QUE SE REGISTRA LA PERSONA
			$horaServidor  = time();
			$hora = date("H:i:s", $horaServidor);

            //SE CREA ARREGLO PARA QUE GUARDE TODA LA INFO DEL PACIENTE
			$dataPatient = array();
            
            if(!empty($idPac)){
                $dataPatient['ID_PAC_PK'] = $idPac;
            }

			$dataPatient['NOMBRE_PAC'] = trim($name);
			$dataPatient['APP_PAC'] = trim($apPaterno);
			$dataPatient['APM_PAC'] = trim($apMaterno);
			$dataPatient['FECNAC_PAC'] = $newformat;
			$dataPatient['ID_EST_FK'] = $estado;
			$dataPatient['DIRECCION_PAC'] = $direccion;
			$dataPatient['GENERO_PAC'] = $genero;
			$dataPatient['ID_PROF_FK'] = $idProf;
			$dataPatient['EST_CIVIL_PAC'] = $idEstadoCivil;
			$dataPatient['TEL_PAC'] = $numTelefono;
			$dataPatient['CORREO_PAC'] = $correo;
            $dataPatient['OBSER_PAC'] = $obs_pac;
			$dataPatient['ESTAT_PAC'] = 1;
            $dataPatient['FCH_REG_PAC'] = date("Y-m-d");
            $dataPatient['ID_USR_ALT'] = $this->session->userdata('id');

    		if($this->Doctor_model->saveOrUpdateInfoPat($dataPatient,$idPac)){
    			//OBTENERMOS EL ID DEL PACIENTE
    			$idPaciente = $this->Doctor_model->getIdPatient($name,$apPaterno,$apMaterno,$newformat,$estado);

    			$json['response_code'] = '200';
    			$json['json_msg'] = 'Operacion Exitosa!';
    			$json['id_persona'] = $idPaciente->ID_PAC_PK;

    		}else{
    			//MENSAJE EN CASO DE QUE NO SE PUEDA GAURDAR LA INFORMACION DEL PACIENTE
    			$json['response_code'] = '500';
    			$json['json_msg'] = 'No se pudo Guardar la informacion del Paciente, Intentelo mas tarde';
    		}
		}
		echo json_encode($json);
	}

    /**
    * Funcion para guardar las enfermedades
    * patologicas del paicente
    * @param $idPersona
    * @param $enfermedades
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function saveEnfermedadPatologica(){

        $identificador = $this->input->post('idPersona');
        $enfermedades = $this->input->post('enfermedades');
        $obse_enfer = $this->input->post('observaciones');
        $jsonEnfermedad = array();

        if($identificador == NULL || $identificador == ""){
            $jsonEnfermedad['response_code'] = '400';
            $jsonEnfermedad['response_msg'] = 'El id de la persona no puede estar nulo';
        }
        // obtenemos el numero de enfermedades que vienen
        $numEnfer = sizeof($enfermedades);

        if($numEnfer > 0 ){
            
            $alertas = $this->Doctor_model->getAlertEnfer();
            $numAlert = sizeof($alertas);
            for ($i=0; $i < $numEnfer; $i++) {

                $infoRelacion = array();
                $infoRelacion['ID_PAC_FK'] = $identificador;
                $infoRelacion['ID_ENFER_FK'] = $enfermedades[$i];
                $infoRelacion['ALERTA_REL'] = '0';
                $infoRelacion['OBS_REL'] = $obse_enfer[$i];

                //validamos si es una alerta la relacion
                for ($j=0; $j <$numAlert; $j++) {
                    if($enfermedades[$i] == $alertas[$j]->ID_ENFER_PK){
                        $infoRelacion['ALERTA_REL'] = '1';
                    }
                }

                $this->Doctor_model->saveRelacionEnfer($infoRelacion);
            }
            $jsonEnfermedad['response_code'] = '200';
            $jsonEnfermedad['response_msg'] = 'Operacion exitosa';   
        }else{
            $jsonEnfermedad['response_code'] = '400';
            $jsonEnfermedad['response_msg'] = 'No hay enfermedades que guardar';   
        }
        echo json_encode($jsonEnfermedad);
    }

    /**
    * Metodo para guardar la informacion de
    * las enfermedades patologicas del familiar,
    * recibe el id del paciente , el id del familiar y 
    * los id de las enfermedades patologicas asignadas
    * al familiar.
    * @param $paciente
    * @param $familiar
    * @param $enferFamiliar
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function saveEnfermedadFamiliar(){

        $idPaciente = $this->input->post('paciente');
        $idFamiliar = $this->input->post('familiar');
        $enferFamiliar = $this->input->post('enferFamiliar');
        $jsonEnferFamiliar = array();

        $numEnferFam = sizeof($enferFamiliar);

        if($idPaciente == NULL || $idPaciente == ""){

            $jsonEnferFamiliar['response_code'] = '400';
            $jsonEnferFamiliar['response_msg'] = 'No hay paciente al cual asignarle el historial familiar';

        }else if($idFamiliar == NULL || $idFamiliar == ""){

            $jsonEnferFamiliar['response_code'] = '400';
            $jsonEnferFamiliar['response_msg'] = 'No se selecciono ningun familiar';

        }else if($numEnferFam == 0){

            $jsonEnferFamiliar['response_code'] = '400';
            $jsonEnferFamiliar['response_msg'] = 'No se selecciono ninguna enfermedad';

        }else{

            for ($i=0; $i < $numEnferFam; $i++) {

                $infoRelacion = array();
                $infoRelacion['ID_PAC_FK'] = $idPaciente;
                $infoRelacion['ID_PAR_FK'] = $idFamiliar;
                $infoRelacion['ID_ENFER_FK'] = $enferFamiliar[$i];
                
                $this->Doctor_model->saveEnferFamiliar($infoRelacion);
                
                $jsonEnferFamiliar['response_code'] = '200';
                $jsonEnferFamiliar['response_msg'] = 'Operacion exitosa!';
            }
        }
        echo json_encode($jsonEnferFamiliar);
    }

    /**
    * Funcion para guardar la informacion 
    * de los habitos del paciente.
    * @param idPac
    * @param numComidas
    * @param higieneDent
    * @param hiloDent
    * @param enjuagenDent
    * @param adic
    * @param cepilladoDent
    * @param arrayAdic
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function saveOrUpdateHabitosPaciente(){

        //inicializacion de la variables
        $paciente = $this->input->post('idPac');
        $comidas = $this->input->post('numComidas');
        $higiene = $this->input->post('higieneDent');
        $hilo = $this->input->post('hiloDent');
        $enjuague = $this->input->post('enjuagenDent');
        $adiccion = $this->input->post('adic');
        $cepillado = $this->input->post('cepilladoDent');
        $adicciones = $this->input->post('arrayAdic');
        //se agregan campos a habitos
        $ingestaDuros = $this->input->post('ingesDuros');
        $inmunizacion = $this->input->post('inmuniza');
        $anticonceptivo = $this->input->post('antiConcep');
        $emabaTriemabaTri = $this->input->post('emabaTri');
        $temperaturaElevada = $this->input->post('tempElevada');

        $jsonHabitos = array();
        $numAdicciones = 0;

        if($adicciones != NULL){
            $numAdicciones = sizeof($adicciones);
        }

        //Se valida si el id del paciente es nulo o viene vacio
        if($paciente == NULL || $paciente == ""){
            
            $jsonHabitos['response_code'] = '400';
            $jsonHabitos['response_msg'] = 'No hay id para relacionar con los habitos!';
        
        //Se valida el numero de comidas al dia del paciente no sea nula 
        }else if($comidas == NULL || $comidas == ""){
            
            $jsonHabitos['response_code'] = '400';
            $jsonHabitos['response_msg'] = 'No se selecciono el numero de comidas!';

        //Se valida el tipo de higiene del paciente no sea nula
        }else if($higiene == NULL || $higiene == ""){
            
            $jsonHabitos['response_code'] = '400';
            $jsonHabitos['response_msg'] = 'No se selecciono la higiene dental que tiene el paciente!';

        //se valida que el hilo dental no venga nulo
        }else if($hilo == NULL || $hilo == ""){
            
            $jsonHabitos['response_code'] = '400';
            $jsonHabitos['response_msg'] = 'No se informo si el paciente usa o no hilo dental!';

        //Se valida que el enjuague bucal no venga nulo
        }else if($enjuague == NULL || $enjuague == ""){
            
            $jsonHabitos['response_code'] = '400';
            $jsonHabitos['response_msg'] = 'No se informo si el paciente usa o no enjuague dental!';

        //se valida que la adiccion no venga nulo
        }else if($adiccion == NULL || $adiccion == ""){
            
            $jsonHabitos['response_code'] = '400';
            $jsonHabitos['response_msg'] = 'No se informa si el paciente tiene una adiccion!';

        //se valida que el numero de cepillado no este nulo
        }else if($cepillado == NULL || $cepillado == ""){
            
            $jsonHabitos['response_code'] = '400';
            $jsonHabitos['response_msg'] = 'No se informo el cepillado del paciente!';

        //Se valida si se selecciono que el paciente tiene alguna adiccion
        }else{
            
            //Se arregla la informacion del paciente
            $datosHabitos = array();
            $datosHabitos['NUM_COMI'] = substr($comidas,0);
            $datosHabitos['HIG_DENTAL'] = substr($higiene,0);
            $datosHabitos['HIL_DENTAL'] = $hilo;
            $datosHabitos['ENJ_DENTAL'] = $enjuague;
            $datosHabitos['ADICCION'] = $adiccion;
            $datosHabitos['NUM_SEP'] = substr($cepillado,0);
            //se agregan campos
            $datosHabitos['INGES_ALIME_DURO'] = $ingestaDuros;
            $datosHabitos['INGES_TEMP_ELEVAD'] = substr($temperaturaElevada,0);
            $datosHabitos['INMU'] = $inmunizacion;
            $datosHabitos['METOD_ANTI_CONCEP'] = $anticonceptivo;
            $datosHabitos['TRIME_EMBAR'] = substr($emabaTriemabaTri,0);

            //validamos si el paciente ya tien habitos en caso de que si hace una actualizacion
            $existeRegistro = $this->Doctor_model->validHabitos($paciente);

            if(empty($existeRegistro)){

                //agregamos el id del paciente para guardarlo
                $datosHabitos['ID_PAC_FK'] = $paciente;
                
                //se valida si se pudo guardar la informacion de los habitos del paciente
                if($this->Doctor_model->saveHabitos($datosHabitos)){

                    if($adiccion == '1'){

                        for ($i=0; $i < $numAdicciones; $i++) {

                            $infoRelAdic = array();
                            $infoRelAdic['ID_ADIC_FK'] = $adicciones[$i];
                            $infoRelAdic['ID_PAC_FK'] = $paciente;
                    
                            $this->Doctor_model->saveAdicPac($infoRelAdic);
                    
                        }
                    }

                    $jsonHabitos['response_code'] = '200';
                    $jsonHabitos['response_msg'] = 'Operacion exitosa!';

                }else{
                    $jsonHabitos['response_code'] = '400';
                    $jsonHabitos['response_msg'] = 'No se pudo guardar la informacion de los habitos del paciente';
                }
            }else{
                //se actualizan los habitos del paciente
                if($this->Doctor_model->updateHabitos($datosHabitos,$paciente)){
                    $jsonHabitos['response_code'] = '200';
                    $jsonHabitos['response_msg'] = 'Actualzacion exitosa!';
                }else{
                    $jsonHabitos['response_code'] = '400';
                    $jsonHabitos['response_msg'] = 'No se pudieron actualizar los habitos del paciente =(';
                }
            }
        }
        echo json_encode($jsonHabitos);
    }

    /**
    * Funcion para guardar los padecimeintos dentales del paciente
    * @param $idPaciente
    * @param $idDiente
    * @param $padecimientos
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function savePadecimientoDental(){

        $idPac = $this->input->post('idPaciente');
        $idDienteArray = $this->input->post('idDiente');
        $padecimientoArray = $this->input->post('padecimientos');
        $numPadecimiento = 0;
        $numDientes = 0;
        $opcionInser = 0;
        $jsonPadecimiento = array();

        if(is_array($padecimientoArray)){
            $numPadecimiento = sizeof($padecimientoArray);
        }

        if(is_array($idDienteArray)){
            $numDientes = sizeof($idDienteArray);
        }

        if($idPac == "" || $idPac == NULL){
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'el id del paciente no puede estar vacio'; 
        }else if($numDientes < 0){
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'El número de dientes debe ser mayor a 0';
        }else if($numPadecimiento < 0){
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'El número de padecimineto debe ser mayor a 0';
        }else if($numDientes > 1 && $numPadecimiento > 1){
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'Solo se puede un padecimiento por muchos dientes o muchos padecimientos a un solo diente';
        }else{

            $contadorFor = 0;            
            
            // si dientes es mayor a 1 y padecimiento igual a 1 entonces se hace el for para recorrer los dientes
            if($numDientes > 1 && $numPadecimiento == 1){
                $opcionInser = 1;
                $contadorFor = $numDientes;
            // si los padecimientos es mayor a 1 y dientes igual a 1 entonces se hace el for para recorrer los pad
            }else if($numPadecimiento > 1 && $numDientes == 1){
                $opcionInser = 2;
                $contadorFor = $numPadecimiento;
            }else if($numPadecimiento == 1 && $numDientes == 1){
                $opcionInser = 3;
                $contadorFor = 1;
            }

            for ($i=0; $i < $contadorFor; $i++) {

                $infoPadecimiento = array();
                $infoPadecimiento['ID_PAC_FK'] = $idPac;
                
                if($opcionInser == 1){
                    $infoPadecimiento['ID_DIENTE_FK'] = $idDienteArray[$i];
                    $infoPadecimiento['ID_PAD_FK'] = $padecimientoArray[0];
                }else if($opcionInser == 2){
                    $infoPadecimiento['ID_DIENTE_FK'] = $idDienteArray[0];
                    $infoPadecimiento['ID_PAD_FK'] = $padecimientoArray[$i];
                }else if($opcionInser == 3){
                    $infoPadecimiento['ID_DIENTE_FK'] = $idDienteArray[0];
                    $infoPadecimiento['ID_PAD_FK'] = $padecimientoArray[0];
                }
                
                $this->Doctor_model->saveInfoPadecimiento($infoPadecimiento);
            }

            $jsonPadecimiento['response_code'] = '200';
            $jsonPadecimiento['response_msg'] = 'Operacion exitosa!';
        }
        echo json_encode($jsonPadecimiento);
    }


    public function updatePadDent(){

        $idPac = $this->input->post('idPaciente');
        $idDienteArray = $this->input->post('ID_PAD_FK');
        $padecimientoArray = $this->input->post('padecimientos');
        $formCompleto = $this->input->post('formFull');
        $numPadecimiento = 0;
        $opcionInser = 0;
        $jsonPadecimiento = array();

        if(is_array($padecimientoArray)){
            $numPadecimiento = sizeof($padecimientoArray);
        }

        if(is_array($idDienteArray)){
            $numDientes = sizeof($idDienteArray);
        }

        // si el formulario se completa se procede a cambiar el estatus del paciente
        if($formCompleto === 1){
            $this->Doctor_model->cambiaEstatPac($idPac);
        }

        if($idPac == "" || $idPac == NULL){
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'El id del paciente no puede estar vacio'; 
        }else if($numDientes < 0){
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'El número de dientes debe ser mayor a 0';
        }else if($numPadecimiento < 0){
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'El número de padecimineto debe ser mayor a 0';
        }else if($numDientes > 1 && $numPadecimiento > 1){
            $jsonPadecimiento['response_code'] = '400';
            $jsonPadecimiento['response_msg'] = 'Solo se puede un padecimiento por muchos dientes o muchos padecimientos a un solo diente';
        }else{

            // borramos la informacion del padecimiento 
            $this->Doctor_model->deletePadDent($idPac,$idDienteArray);
            
            $contadorFor = 0;
            // si dientes es mayor a 1 y padecimiento igual a 1 entonces se hace el for para recorrer los dientes
            if($numDientes > 1 && $numPadecimiento == 1){
                $opcionInser = 1;
                $contadorFor = $numDientes;
            // si los padecimientos es mayor a 1 y dientes igual a 1 entonces se hace el for para recorrer los pad
            }else if($numPadecimiento > 1 && $numDientes == 1){
                $opcionInser = 2;
                $contadorFor = $numPadecimiento;
            }else if($numPadecimiento == 1 && $numDientes == 1){
                $opcionInser = 3;
                $contadorFor = 1;
            }

            //despues hacemos los nuevos inserts
            for ($i=0; $i < $contadorFor; $i++) {

                $infoPadecimiento = array();
                $infoPadecimiento['ID_PAC_FK'] = $idPac;
                
                if($opcionInser == 1){
                    $infoPadecimiento['ID_DIENTE_FK'] = $idDienteArray[$i];
                    $infoPadecimiento['ID_PAD_FK'] = $padecimientoArray[0];
                }else if($opcionInser == 2){
                    $infoPadecimiento['ID_DIENTE_FK'] = $idDienteArray[0];
                    $infoPadecimiento['ID_PAD_FK'] = $padecimientoArray[$i];
                }else if($opcionInser == 3){
                    $infoPadecimiento['ID_DIENTE_FK'] = $idDienteArray[0];
                    $infoPadecimiento['ID_PAD_FK'] = $padecimientoArray[0];
                }
                
                $this->Doctor_model->saveInfoPadecimiento($infoPadecimiento);
            }
            
            $jsonPadecimiento['response_code'] = '200';
            $jsonPadecimiento['response_msg'] = 'Operacion Exitosa';
            $jsonPadecimiento['tabPadDent'] = $this->Doctor_model->getPadecimientoDiente($idPac);
        }
        echo json_encode($jsonPadecimiento);
    }

    /**
    * Funcion para cargar el pdf
    * @param $idPac
    */
    public function getHistoriaPdf(){
        
        $this->load->library('mydompdf');

        $idPaciente = $this->input->get('idPac');
        $infoPatient = array();
        $numeroCabeceras = 6;

        $infoPatient['infoPac'] = $this->Doctor_model->getPatientById($idPaciente);
        
        //tabla de antecedentes familiares
        $enfermedades = $this->Documentos_model->getAllEnferPac($idPaciente);
        $relacionPariente = $this->Documentos_model->getEnferFamById($idPaciente);
        $numeroTablas = ceil(count($enfermedades) / $numeroCabeceras);
        $arrayTable = $this->creaTablas($numeroTablas,$enfermedades,$relacionPariente,$numeroCabeceras,'1');
        
        //tabla de antecedentes personales
        $enferPer = $this->Documentos_model->getAllEnferPac($idPaciente);
        $relPac = $this->Documentos_model->getEnferPerById($idPaciente);
        $numTabPer = ceil(count($enferPer) / $numeroCabeceras);
        $arrayTablePer = $this->creaTablas($numTabPer,$enferPer,$relPac,$numeroCabeceras,'2');


        $infoPatient['tfamily'] = $arrayTable;
        $infoPatient['tpersonal'] = $arrayTablePer;
        $infoPatient['infoHabit'] = $this->Doctor_model->getHabitosById($idPaciente);

        $html = $this->load->view('historia_pdf_view',$infoPatient,TRUE);
        $filename = 'Historia_1';
        $this->mydompdf->generate($html, $filename, TRUE, 'A4', 'portrait');
    }
}