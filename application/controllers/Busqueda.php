<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author ING, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Controlodar para las profesiones
 * Fecha de creacion : N/A
 * Fecha de actualzacion : 13-11-2018
*/
class Busqueda extends My_Controller {
	/**
	* Funcion constructor para la clase Busqueda
	*/
	public function __construct(){
		parent::__construct();

		if($this->session->userdata('name') == FALSE){
			$this->session->set_flashdata("error","ACCESO DENEGADO");
			redirect("Login");
		}

		$this->load->model("Doctor_model");
	}

	/**
	* Funcion para llamar la vista
    * y hacer la busqueda de un paciente
	*/
	public function index(){

        $idPaciente = $this->input->post('idCompletForm');
		$numRegistros = 5;
        $pagianActual = 0;
		$datos = array();
		$fragment = array();
        $idUser =  $this->session->userdata('id');

		if($idPaciente == NULL){
            $datos['resultado'] = $this->Doctor_model->getPacientePaginado($numRegistros,$pagianActual,$idUser);
            $datos['formPacFull'] = 0;
        }else{
            $datos['resultado'] = $this->Doctor_model->getPacientePaginadoFiltro($idPaciente,$numRegistros,$pagianActual);
            $datos['formPacFull'] = 1;
        }

		
		$datos['estados'] = $this->getCatalogo("ESTADOS");
		$datos['profesiones'] = $this->getCatalogo("PROFESIONES");
        $datos['familiares'] = $this->getCatalogo("FAMILIARES");
        $datos['enfermedades'] = $this->getCatalogo("ENFERMEDADES");
        $datos['adicciones'] = $this->getCatalogo("TOXICOMANIAS");
        // se agrega combox para poder editar los padecimientos dentales
        $datos['padecimientos'] = $this->getCatalogo("PADECIMIENTOS");
        $datos['dientes'] = $this->getCatalogo("DIENTES");

		$fragment['VISTA'] = $this->load->view('paciente_search_view',$datos,TRUE);
        $fragment['ccsLibs'] = ['js/core/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css','js/core/bootstrap-select/css/bootstrap-select.css'];
        $fragment['jsLibs'] = ['core/moment.js','core/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js','core/bootstrap-select/js/bootstrap-select.js','core/tablaAjax2.js','editar-paciente.js'];

        $this->load->view('dashboard_view',$fragment);
	}

    /**
    * Funcion para obtenr la informacion del paciente
    * por id.
    * @param $idPaciente : identificador del paciente
    * @return JSON [response_code] : codigo de respuesta
    *              [response_msg] :  mensaje de respuesta
    *              [infoPer] : informacion basica del paciente
    *              [habitos] : habitos del paciente
    *              [ADICCION] : Opcional, adiccion
    *              [adicciones] : Opcional, lista de adicciones
    *              [enfermedadPersonal] : lista de enfermedades patologicas
    */
	public function getInfoPatient(){

		$jsonPatient = array();
		$idSearch = $this->input->post("idPaciente");

		if($idSearch == "" || $idSearch == NULL){
			$jsonPatient['response_code'] = '500';
			$jsonPatient['json_msg'] = 'EL id del paciente es nulo'; 
		}else{

			$persona = $this->Doctor_model->getPatientById($idSearch);
			$habitos = $this->Doctor_model->getHabitosById($idSearch);
			$enfermedades = $this->Doctor_model->getEnferById($idSearch);
            $dientes = $this->Doctor_model->getPadecimientoDiente($idSearch);
            $hitoriaCitas = $this->Doctor_model->getHistoryQuotes($idSearch);

            if(!is_null($habitos) && $habitos->ADICCION == '1'){
                $jsonPatient['ADICCION'] = 'true';
                $jsonPatient['adicciones'] = $this->Doctor_model->getToxicomania($idSearch);
            }else{
                $jsonPatient['ADICION'] = 'false';
            }

			$jsonPatient['response_code'] = '200';
            $jsonPatient['response_msg'] = 'Operacion exitosa!';
            $jsonPatient['infoPer'] = $persona;
            $jsonPatient['habitos'] = $habitos;
            $jsonPatient['enfermedadPersonal'] = $enfermedades;
            $jsonPatient['dientes'] = $dientes;
            $jsonPatient['quotes'] = $hitoriaCitas;

		}

		echo json_encode($jsonPatient);
	}

    /**
    * Metodo apra actualizar las enfermedades patologicas
    * del paciente
    * @param idPac : identificador del paciente
    * @param enfermedades : array con todas las enfermedades
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function updateEnfermedadPatologica(){
        $idPaciente = $this->input->post('idPac');
        $enfermedadesArray = $this->input->post('enfermedades');

        $jsonUpdateEnfer = array();

        if($idPaciente == NULL || $idPaciente == "" ){
            $jsonUpdateEnfer['response_code'] = '400';
            $jsonUpdateEnfer['response_msg'] = 'No se informo el id del paciente';
        }else{

            if(is_array($enfermedadesArray)){
                $numEnfer = sizeof($enfermedadesArray);                
            }else{
                $numEnfer = 0;
                $this->Doctor_model->deleteEnfermedades($idPaciente);
            }

            if($numEnfer > 0){
                
                $this->Doctor_model->deleteEnfermedades($idPaciente);
                
                $alertas = $this->Doctor_model->getAlertEnfer();
                $numAlert = sizeof($alertas);

                for ($i=0; $i < $numEnfer; $i++) {

                    $infoRelacion = array();
                    $infoRelacion['ID_PAC_FK'] = $idPaciente;
                    $infoRelacion['ID_ENFER_FK'] = $enfermedadesArray[$i];
                    $infoRelacion['ALERTA_REL'] = '0';
                    //validamos si es una alerta la relacion
                    for ($j=0; $j <$numAlert; $j++) {
                        if($enfermedadesArray[$i] == $alertas[$j]->ID_ENFER_PK){
                            $infoRelacion['ALERTA_REL'] = '1';
                        }
                    }
                    $this->Doctor_model->saveRelacionEnfer($infoRelacion,$idPaciente);
                }

                $jsonUpdateEnfer['response_code'] = '200';
                $jsonUpdateEnfer['response_msg'] = 'Operacion exitosa';
            }else{
                $jsonUpdateEnfer['response_code'] = '200';
                $jsonUpdateEnfer['response_msg'] = 'No hay enfermedades que guardar';
            }

        }
        echo json_encode($jsonUpdateEnfer);
    }

    /**
    * Funcion para obtener las enfermedades familiares
    * @param idFamiliar : identificador Familiar
    * @param idPaciente :  identificador Paciente
    * @return JSON [response_code] : codigo de respuesta
    *              [response_msg] : mensaje de respuesta
    *              [enferFamiliar] : lista de enfermedades del pariente
    */
    public function getInfoFamiliar(){

        $idenFam = $this->input->post('idFamiliar');
        $idenPac = $this->input->post('idPaciente');
        $jsonFamiliar = array();

        if($idenFam == "" || $idenFam == NULL){
            $jsonFamiliar['response_code'] = '400';
            $jsonFamiliar['response_msg'] = 'El id del familiardebe de estar informado para hacer la busqueda';
        }else if($idenPac == "" || $idenPac == NULL){
            $jsonFamiliar['response_code'] = '400';
            $jsonFamiliar['response_msg'] = 'El id del paciente debe de estar informado para hacer la busqueda';
        }else{
            $jsonFamiliar['response_code'] = '200';
            $jsonFamiliar['response_msg'] = 'Operacion Exitosa!';
            $jsonFamiliar['enferFamiliar'] = $this->Doctor_model->getInfoFamiliarById($idenFam,$idenPac);
        }
        echo json_encode($jsonFamiliar);
    }

    /**
    * Funcion para actualizar las enfermedades
    * familiares del paciente
    * @param idPaciente : id paciente
    * @param idFamiliar : id familiar
    * @param enfermedades : array con las enfermedades
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function updateEnfermedadFamiliar(){

        $idPaciente = $this->input->post('idPaciente');
        $idFamiliar = $this->input->post('idFamiliar');
        $enfermedadesFam = $this->input->post('enfermedades');
        $jsonEnferFamiliar = array();
        
        if($idPaciente == "" || $idPaciente == NULL){
            $jsonEnferFamiliar['response_code'] = '400';
            $jsonEnferFamiliar['response_msg'] = 'El id del paciente no puede estar vacio';
        }else if($idFamiliar == "" || $idFamiliar == NULL){
            $jsonEnferFamiliar['response_code'] = '400';
            $jsonEnferFamiliar['response_msg'] = 'El id del familiar no puede estar vacio';
        }else{
 
            $numEnferFam = 0;

            if(is_array($enfermedadesFam)){
                $numEnferFam = sizeof($enfermedadesFam);
            }

            $this->Doctor_model->deleteRelacionFamiliar($idFamiliar,$idPaciente);
            
            for ($i=0; $i < $numEnferFam; $i++) {

                $infoRelacion = array();
                $infoRelacion['ID_PAC_FK'] = $idPaciente;
                $infoRelacion['ID_PAR_FK'] = $idFamiliar;
                $infoRelacion['ID_ENFER_FK'] = $enfermedadesFam[$i];
                
                $this->Doctor_model->saveEnferFamiliar($infoRelacion);
            }

            $jsonEnferFamiliar['response_code'] = '200';
            $jsonEnferFamiliar['response_msg'] = 'Operacion exitosa!';
        }
        echo json_encode($jsonEnferFamiliar);
    }

    /**
    * Funcion para obtener la siguiente o anterior pagina 
    * de las toxicomanias.
    * @param $paginaConsulta pagina a consultar
    * @param $numeroRegistros numero de registros por pagina
    * @param $filtro_0 filtro para el paginado
    * @return JSON [response_code] codigo de respuesta
    *              [response_msg] mensaje de respuesta
    *              [resultado] resultado de la paginacion
    */
    public function getPaginacion(){
        $filtro = array();
        $pagina = $this->input->post('paginaConsulta');
        $numero = $this->input->post('numeroRegistros');
        $filtro[0] = $this->input->post('filtro_0');
        $filtro[1] = $this->input->post('filtro_1');
        $filtro[2] = $this->input->post('filtro_2');

        $jsonPaginado = array();

        if($pagina == NULL || $pagina == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'La pagina debe de estar informada';

        }else if($numero == NULL || $numero == ""){
            
            $jsonPaginado['response_code'] = '400';
            $jsonPaginado['response_msg'] = 'El numero de registros debe de estar informado';

        }else{
            //Se hace la busqueda con filtro
            $jsonPaginado['response_code'] = '200';
            $jsonPaginado['resultado'] = $this->Doctor_model->getPacientePaginadoFiltro($filtro,$numero,$pagina);

        }
        echo json_encode($jsonPaginado);
    }

    /**
    * Funcion para dar de baja a un paciente
    * recibe el id del paciente
    * @param $idPac : id del paciente
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function bajaPaciente(){
        $paciente = $this->input->post('idPac');
        $jsonDeletePac = array();
        
        if($paciente == NULL || $paciente == ""){
            
            $jsonDeletePac['response_code'] = '400';
            $jsonDeletePac['response_msg'] = 'el ID no puede estar vacio o estar nulo';

        }else{

            $datosPac = array();
            $datosPac['ESTAT_PAC'] = '0';

            if($this->Doctor_model->updatePaciente($datosPac,$paciente)){

                $jsonDeletePac['response_code'] = '200';
                $jsonDeletePac['response_msg'] = 'Operacion Exitosa!';  
            }else{

                $jsonDeletePac['response_code'] = '400';
                $jsonDeletePac['response_msg'] = 'Algo salio mal intentelo mas tarde';
            }
        }
        echo json_encode($jsonDeletePac);
    }

    /**
    * Funcion para validar si algun paciente 
    * ya tiene agendada una cita
    * recibe le id del paciente
    * @param idPac : id del paciente
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function getCitaPatient(){
        
        $idPaciente = $this->input->post('idPac');
        $jsonCita = array();

        if(empty($idPaciente)){
            $jsonCita['response_code'] = '400';
            $jsonCita['response_msg'] = 'id paciente vacio';
        }else{
            
            $info = $this->Doctor_model->getCitaPac($idPaciente);

            if(is_null($info)){
                $jsonCita['response_code'] = '200';
                $jsonCita['response_msg'] = 'Paciente sin cita';
                $jsonCita['paciente'] = $idPaciente;
            }else{
                $jsonCita['response_code'] = '201';
                $jsonCita['response_msg'] = 'Paciente con cita';
                $jsonCita['paciente'] = $info->ID_CITA_PK;
            }
        }
        echo json_encode($jsonCita);
    }

    /**
    * Funion para agendar las citas 
    * de los pacientes
    * @param $fechaCita
    * @param horacita
    * @param pacCita
    * @return JSON [response_code]
    *              [response_msg]
    */
    public function agendaCitaPac(){

        $fecha = $this->input->post('fechaCita');
        $hora = $this->input->post('horaCita');
        $paciente = $this->input->post('pacCita');
        $validaCita = TRUE;
        $jsonAgenCita = array();
        $doctor = '';

        if($this->session->userdata('tipo') == '3'){
            $doctor = $this->input->post('docCita');
        }else{
            $doctor = $this->session->userdata('id');
        }

        if($fecha == NULL || $fecha == ""){
            $validaCita = FALSE;
        }

        if($hora == NULL || $hora == ""){
            $validaCita = FALSE;
        }

        if($paciente == NULL || $paciente == ""){
            $validaCita = FALSE;
        }

        if($validaCita == TRUE){

            $infoCita = array();
            $infoCita['FECH_CITA'] = $fecha;
            $infoCita['HORA_CITA'] = $hora;
            $infoCita['ID_PAC_FK'] = $paciente;
            $infoCita['ID_EMP_FK'] = $doctor;
            $infoCita['ESTAT_CITA'] = '1';

            if($this->Doctor_model->saveInfoCita($infoCita)){
                $jsonAgenCita['response_code'] = '200';
                $jsonAgenCita['response_msg'] = 'Operacion Exitosa!';
            }else{
                $jsonAgenCita['response_code'] = '400';
                $jsonAgenCita['response_msg'] = 'Ocurrio un error al intentar guardar la informacion';    
            }

        }else{
            $jsonAgenCita['response_code'] = '400';
            $jsonAgenCita['response_msg'] = 'No se cumple con los campos para agendar una cita';
        }
        echo json_encode($jsonAgenCita);
    }

    /**
    * Funcion para obtener los padecimiento 
    * por paciente y diente
    * @param idPaciente
    * @param idDiente
    */
    public function getPadDent(){
        $jsonPadDent = array();
        $paciente = $this->input->post("idPaciente");
        $diente = $this->input->post("idDiente");
        $validaDatos = TRUE;

        if($paciente == "" || $paciente == NULL){
            $validaDatos = FALSE;
            $jsonPadDent['response_code'] = "200";
            $jsonPadDent['response_msg'] = "Es requerido el paciente";
        }

        if($diente == "" || $diente == NULL){
            $validaDatos = FALSE;
            $jsonPadDent['response_code'] = "200";
            $jsonPadDent['response_msg'] = "Es requerido el numero de diente";
        }

        if($validaDatos == TRUE){
            $jsonPadDent['response_code'] = "200";
            $jsonPadDent['padecimientos'] = $this->Doctor_model->getPadDentById($paciente,$diente);
        }

        echo json_encode($jsonPadDent);
    }


    public function getCitaById(){
        $idCitaConsulta = $this->input->post('idCitaPaciente');
        $jsonResCita = array();

        $jsonResCita['response_code'] = '200';
        $jsonResCita['response_msg'] = 'Operacion exitosa';
        $jsonResCita['detalle'] = $this->Doctor_model->getDetalleCitaById($idCitaConsulta);

        echo json_encode($jsonResCita);
    }

}
