<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ing. Noé Ramos Lopez
 * @version 1.0
 * @copyright Todos los derechos reservados 2020
 * Modelo que tiene las funciones para hacer
 * Altabas, Bajas logicas, Consultas y Modificaciones 
 * de un procedimiento dental
 * Fecha de creacion : 18-06-2020
 * Fecha de actualzacion : N/A
*/

class Procedimiento_model extends My_Model{
    /**
    * Constructor de la clase Padecimiento model
    */
    public function __construct(){
         parent::__construct();
        $this->load->database();
    }

    /**
    * Metodo que obtiene el listado de procedimeintos en la base de datos
    * recibe el numero de registros a mostrar y la pagina a consultar
    * @param $registros
    * @param $pagna
    */
    public function getProcedimientoPaginado($registros,$pagina){
        
        $this->db->select('ID_PROC_PK, DSC_PROC');
        $this->db->where('ESTAT_PROC','1');
        $consulta = $this->db->from('PROCEDIMIENTO')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

    /**
    * metodo para obtener la informacion de un procedimeinto
    * en base al id del procedimiento
    * @param $id
    */
    public function getInfoProceById($id){
        $this->db->select('ID_PROC_PK, DSC_PROC');
        $this->db->where('ID_PROC_PK',$id);
        $query = $this->db->get('PROCEDIMIENTO');
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Metodo para guardar los procedimientos
    * @param $datos
    */
    public function saveProcedimiento($datos){
        $this->db->insert('PROCEDIMIENTO',$datos);
        return TRUE;
    }

    /**
    * Metodo para hacer la actualizacion de los procedimientos
    * @param $datos
    * @param $id
    */
    public function updateProcedimiento($datos,$id){
        $this->db->where('ID_PROC_PK',$id);
        $this->db->update('PROCEDIMIENTO',$datos);
        return TRUE;
    }

    /**
    * Metodo para obtener el listado de procedimientos pero con un filtro
    * recibe el campo a filtrarm el numero de registros a mostrar y la pagina
    * a consultar
    * @param $filtro
    * @param $registros
    * @param $pagina
    */
    public function getProcedimientoPaginadoFiltro($filtro,$registros,$pagina){
        $this->db->select('ID_PROC_PK, DSC_PROC');
        $this->db->where('ESTAT_PROC','1');
        $this->db->like('DSC_PROC', $filtro);
        $consulta = $this->db->from('PROCEDIMIENTO')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }
}