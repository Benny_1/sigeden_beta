<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author ING, Noe Ramos Lopez
 * @version 0.1
 * @copyright Todos los derechos reservados 2017
*/

class Admin_model extends CI_Model{
    /**
    * Contrsutor para la clase 
    * Adiccion Model
    */
    public function __construct(){
        $this->load->database();
    }

    /**
    * Funcion para obtener el total
    * de pacientes por doctor
    * @param $idDoc : identificador del doctor
    * @return numero de pacientes
    */
    public function totalPacientes($idDoc){
        $this->db->select('COUNT(ID_PAC_PK) AS NUMERO');
        $this->db->from('PACIENTE');
        $this->db->where('ID_USR_ALT',$idDoc);
        $this->db->where('ESTAT_PAC','1');
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion para obtener el totoal de citas por 
    * fecha y doctor al que se le agenda la cita
    */
    public function totalCitas($idDoc,$fecha){
        $this->db->select('COUNT(ID_CITA_PK) AS NUMERO');
        $this->db->from('AGENDA_CITA');
        $this->db->where('FECH_CITA',$fecha);
        $this->db->where('ID_EMP_FK',$idDoc);
        $this->db->where('ESTAT_CITA','1');
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    public function actualizaCitasNoatendida($fchActual, $idDoc){
         //update agenda_cita set estat_cita = 5 where fech_cita < '2019-06-12' and estat_cita = 1;
        $this->db->where('FECH_CITA < ',$fchActual);
        $this->db->where('ID_EMP_FK',$idDoc);
        $this->db->where('ESTAT_CITA',1);
        $this->db->set('ESTAT_CITA',5);
        $this->db->update('AGENDA_CITA');
        return TRUE;
    }

    public function cuentaCitasNoAten($fchActual, $idDoc){
        $this->db->select('COUNT(ID_CITA_PK) AS NUM_CITAS');
        $this->db->from('AGENDA_CITA');
        $this->db->where('FECH_CITA < ',$fchActual);
        $this->db->where('ID_EMP_FK',$idDoc);
        $this->db->where('ESTAT_CITA',1);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    public function getCitasNoAtendidas($fchActual, $idDoc){
        $this->db->select('ID_CITA_PK,ID_PAC_FK');
        $this->db->from('AGENDA_CITA');
        $this->db->where('FECH_CITA < ',$fchActual);
        $this->db->where('ID_EMP_FK',$idDoc);
        $this->db->where('ESTAT_CITA',1);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }
    
    public function guardaDetalleCitaNoAtendida($datos){
        $this->db->insert('DETALLE_CITA',$datos);
        return TRUE;
    }

    public function getDetalleUser($correo){
        $this->db->select('NOM_EMP,APP_EMP,APM_EMP');
        $this->db->from('EMPLEADO');
        $this->db->where('EMAIL_EMP',$correo);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }
}


/**


SELECT 
    G.ID_CITA_PK,NOMBRE_PAC,APP_PAC,APM_PAC,TA,FC,FR,OBS_PROC,ID_PROC_FK,DSC_PROC
FROM 
    AGENDA_CITA G INNER JOIN DETALLE_CITA D ON(G.ID_CITA_PK = D.ID_CITA_FK)
    INNER JOIN PACIENTE P ON(G.ID_PAC_FK = P.ID_PAC_PK)
    INNER JOIN PROCEDIMIENTO C ON(D.ID_PROC_FK = C.ID_PROC_PK)
WHERE G.ID_CITA_PK = 18;



-- padecimientos dentales del paciente
-------------------------------
1.-PADECIMIENTO_DENTAL
2.-DIENTE_NUM
3.-REL_PAD_DIENTE
-------------------------------
4.-PACIENTE
5.-ESTADO
6.-PROFESIONES
-------------------------------
7.-REL_PAC_ENFER
8.-ENFERMEDAD_PATOLOGICA
9.-PARIENTE
10.-REL_PAC_PAR
-------------------------------
11.-ADICCION
12.-REL_ADIC_PER
-------------------------------
13.-HABITOS
-------------------------------
14.-AGENDA_CITA
-------------------------------
16.-EMPLEADO
17.-DETALLE_CITA
18.-PROCEDIMIENTO
19.-PERMISOS
20.-MENU
21.-DOCUMENTO_DOC
-------------------------------






*/
