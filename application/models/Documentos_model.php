<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos
 * @version 0.1
 * @copyright Noe ramos lopex, Todos los derechos reservados 2017
*/

class Documentos_model extends CI_Model{
    /**
    * Contrsutor para la clase 
    * Adiccion Model
    */
    public function __construct(){
        $this->load->database();
    }

    public function getDocumento(){
        $this->db->select('ID_DOC_PK,TXT_DOC');
        $this->db->from('DOCUMENTO_DOC');
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    public function saveDoc($info){
        $this->db->insert('DOCUMENTO_DOC',$info);
        return TRUE;
    }

    public function updateDoc($info, $id){
        $this->db->where('ID_DOC_PK',$id);
        $this->db->update('DOCUMENTO_DOC',$info);
        return TRUE;
    }

    /**
    * Funcion para obtener el listado de las enfermedades
    * con estatus activo
    * @return $array() : enfermedades
    */
    public function getAllEnfer($idPac){
        
        $this->db->select('ID_ENFER_PK,NOMBRE_ENFER');
        $this->db->from('REL_PAC_PAR R');
        $this->db->join('ENFERMEDAD_PATOLOGICA E','R.ID_ENFER_FK = E.ID_ENFER_PK');
        $this->db->where('ESTAT_ENFER','1');
        $this->db->where('ID_PAC_FK',$idPac);
        $this->db->group_by('ID_ENFER_PK');

        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    //SELECT ID_PAR,ID_ENFER FROM REL_PER_PAR  WHERE ID_PER = 11;
    public function getEnferFamById($idPac){

        //var_dump($idPac);
        $this->db->select('NOMBRE_PAR , GROUP_CONCAT(ID_ENFER_FK ORDER BY ID_ENFER_FK) AS ENFERMEDAD');
        $this->db->from('REL_PAC_PAR R');
        $this->db->join('PARIENTE_PACIENTE P','R.ID_PAR_FK = P.ID_PAR_PK');
        $this->db->where('ID_PAC_FK',$idPac);
        $this->db->group_by('ID_PAR_FK');

        /**

        SELECT 
            NOMBRE_PAR , GROUP_CONCAT(ID_ENFER_FK ORDER BY ID_ENFER_FK) AS ENFERMEDAD
        FROM REL_PAC_PAR R
        INNER JOIN PARIENTE_PACIENTE P ON(R.ID_PAR_FK = P.ID_PAR_PK)
        WHERE ID_PAC_FK = 3
        GROUP BY ID_PAR_FK;

        */
        $query = $this->db->get();
        //$query = $this->db->get_compiled_select();
        //die(var_dump($query));
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Funcion para obtener el nombre de los parientes
    * del paciente para la relacion de enfermedades patologicas
    * @return $array() :  Lista de nombres de parientes
    */
    public function getNomPariente(){
        $this->db->select('ID_PAR_PK,NOMBRE_PAR');
        $this->db->from('PARIENTE_PACIENTE');
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }
    
    /**
    * Para obtener el nombre y id de las 
    * enfermedades relacionasdas al paciente
    * @param $id
    * array() : Nombre de la enfermedad y el id
    */
    public function getAllEnferPac($id){
        $this->db->select('ID_ENFER_PK,NOMBRE_ENFER');
        $this->db->from('REL_PAC_ENFER R');
        $this->db->join('ENFERMEDAD_PATOLOGICA E','R.ID_ENFER_FK = E.ID_ENFER_PK');
        $this->db->where('ID_PAC_FK',$id);
        $this->db->order_by('ID_ENFER_PK');
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Funcion para obtener la relacion
    * de enfermedades patologicas personales
    * del paciente recube el id del paciente
    * @param $idPac : identificador del paciente
    * @return arreglo con las enfermedades
    */
    public function getEnferPerById($idPac){

        $this->db->select('NOMBRE_PAC AS NOMBRE_PAR, GROUP_CONCAT(ID_ENFER_FK ORDER BY ID_ENFER_FK) AS ENFERMEDAD');
        $this->db->from('REL_PAC_ENFER R');
        $this->db->join('PACIENTE P','R.ID_PAC_FK = P.ID_PAC_PK');
        $this->db->where('ID_PAC_FK',$idPac);
        $query = $this->db->get();
        
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }
}

/**

SELECT
    NOMBRE_PAR,
    GROUP_CONCAT(NOMBRE_ENFER) AS ENFERMEDAD
FROM REL_PAC_PAR R
    INNER JOIN PARIENTE_PACIENTE P ON(R.ID_PAR_FK = P.ID_PAR_PK)
    INNER JOIN ENFERMEDAD_PATOLOGICA E ON(R.ID_ENFER_FK = E.ID_ENFER_PK)
WHERE ID_PAC_FK = 19
GROUP BY ID_PAR_FK;



SELECT 
    `ID_PAR_FK`, 
    GROUP_CONCAT(if(ID_ENFER_FK = 1, 'SI', 'NO')) AS DIABETES, 
    GROUP_CONCAT(if(ID_ENFER_FK = 2, 'SI', 'NO')) AS ASMA, 
    GROUP_CONCAT(if(ID_ENFER_FK = 3, 'SI', 'NO')) AS CANCER, 
    GROUP_CONCAT(if(ID_ENFER_FK = 4, 'SI', 'NO')) AS ENFERMEDEDADES_RENALES, 
    GROUP_CONCAT(if(ID_ENFER_FK = 5, 'SI', 'NO')) AS CONVULSIONES, 
    GROUP_CONCAT(if(ID_ENFER_FK = 6, 'SI', 'NO')) AS HIPERTENECION, 
    GROUP_CONCAT(if(ID_ENFER_FK = 7, 'SI', 'NO')) AS CARDIO_PATIA, 
    GROUP_CONCAT(if(ID_ENFER_FK = 8, 'SI', 'NO')) AS ALERGIAS, 
    GROUP_CONCAT(if(ID_ENFER_FK = 9, 'SI', 'NO')) AS FINADO, 
    GROUP_CONCAT(if(ID_ENFER_FK = 10, 'SI', 'NO')) AS TUBERCULOSIS, 
    GROUP_CONCAT(if(ID_ENFER_FK = 11, 'SI', 'NO')) AS TRAUMATISMOS, 
    GROUP_CONCAT(if(ID_ENFER_FK = 12, 'SI', 'NO')) AS INTERVENCIONES_QUIRURGICAS,
    GROUP_CONCAT(if(ID_ENFER_FK = 13, 'SI', 'NO')) AS HEPATOPATIAS, 
    GROUP_CONCAT(if(ID_ENFER_FK = 14, 'SI', 'NO')) AS ENFERMEDADES_DE_TRANSMISION_SEXUAL, 
    GROUP_CONCAT(if(ID_ENFER_FK = 15, 'SI', 'NO')) AS FIEBRE_REUMATICA, 
    GROUP_CONCAT(if(ID_ENFER_FK = 16, 'SI', 'NO')) AS OTROS, 
    GROUP_CONCAT(if(ID_ENFER_FK = 17, 'SI', 'NO')) AS NINGUNA 
FROM `REL_PAC_PAR` `R` 
    JOIN `PARIENTE_PACIENTE` `P` ON `R`.`ID_PAR_FK` = `P`.`ID_PAR_PK` 
    JOIN `ENFERMEDAD_PATOLOGICA` `E` ON `R`.`ID_ENFER_FK` = `E`.`ID_ENFER_PK` 
WHERE `ID_PAC_FK` = '7' 
GROUP BY `ID_PAR_FK`
*/