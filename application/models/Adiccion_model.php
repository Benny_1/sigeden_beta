<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos
 * @version 0.1
 * @copyright Noe ramos lopex, Todos los derechos reservados 2017
*/

class Adiccion_model extends My_Model{
    /**
    * Contrsutor para la clase 
    * Adiccion Model
    */
    public function __construct(){
        $this->load->database();
    }

    /**
    * Funcion para obtener la paginacion de las adicciones
    * @param $registros : numero de registros por pagina
    * @param $pagina : numero de pagina a consultar
    * @return $array() ['lista'] : Listado de las adiciones
    *                  ['paginasTotales'] : numero de paginas totales de la consulta
    *                  ['paginActual'] : pagina que se consulto
    */
    public function getAdiccionPaginado($registros,$pagina){
        $this->db->select('ID_ADIC_PK, NOM_ADIC');
        $this->db->where('ESTAT_ADIC','1');
        $consulta = $this->db->from('ADICCION')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }
    
    /**
    * Funcion para obtener las adicciones
    * con un parametro como filtro
    * @param $filtro : nombre de la profesion a filtrar
    * @param $registros : numero de registros que se mostraran por pagina
    * @param $pagina : pagina a consultar
    */
    public function getAdiccionPaginadoFiltro($filtro,$registros,$pagina){
        $this->db->select('ID_ADIC_PK, NOM_ADIC');
        $this->db->where('ESTAT_ADIC','1');
        $this->db->like('NOM_ADIC', $filtro);
        $consulta = $this->db->from('ADICCION')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

    /**
    * Funcion para obtener una adiccion
    * por id, regresa la informacion consultada
    * @param $id : identificador
    * @return $array() : datos de la adiccion
    */
    public function getInfoById($id){
        $this->db->select('ID_ADIC_PK, NOM_ADIC');
        $this->db->where('ID_ADIC_PK',$id);
        $query = $this->db->get('ADICCION');
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion para guardar la informacion de una
    * adicion.
    * @param $datos [NOM_ADIC,ESTAT_ADIC]
    * @return TRUE si es exitoso FALSE si falla
    */
    public function saveAdiccion($datos){
        $this->db->insert('ADICCION',$datos);
        return TRUE;
    }

    /**
    * Funcion para actualizar la informacion de una
    * adiccion
    * @param $datos [NOM_ADIC,ESTAT_ADIC] : informacion
    * @param $id : identificador donde se hara la actualizacion
    */
    public function updateAdiccion($datos,$id){
        $this->db->where('ID_ADIC_PK',$id);
        $this->db->update('ADICCION',$datos);
        return TRUE;
    }

}