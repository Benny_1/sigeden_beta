<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos
 * @version 0.1
 * @copyright Noe ramos lopex, Todos los derechos reservados 2017
*/
//CLASE DE Login_model
class Login_model extends CI_Model{
	/**
	* Funcion para el constructor de Login_model
	*/
	public function __construct(){
		$this->load->database();
	}

	/**
	* Funcion para verificar si exite el nombre de usuario en la base
	* @param name
	* @return arreglo de la informacion de un usario
	*/
	public function validateUser($name){

		$this->db->select('U.ID_USR_PK, U.NOMBRE_USR, U.PASSWORD_USR, R.ID_ROL_PK, R.NOM_ROL ');
		$this->db->from('USUARIO U');
		$this->db->join('ROL R ','U.ID_ROL_FK = R.ID_ROL_PK');
		$this->db->where('NOMBRE_USR = BINARY',$name);
		
		$query = $this->db->get();
		return ($query->num_rows() <= 0) ? NULL : $query->row(); 
	}

	public function grabaPista($datos){
		$this->db->insert("HIST_INGRESO",$datos);
		return TRUE;
	}

}

/*


SELECT U.NOMBRE_USR, U.PASSWORD_USR, R.ID_ROL_PK, R.NOM_ROL 
from USUARIO U 
	inner join ROL R on(U.ID_ROL_FK = R.ID_ROL_PK)
WHERE NOMBRE_USR = BINARY 'kuruladi90@gmail.com';

*/