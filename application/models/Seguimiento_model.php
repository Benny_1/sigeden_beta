<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ing, Noé Ramos López
 * @version 0.1
 * @copyright Todos los derechos reservados 2018
*/

class Seguimiento_model extends My_Model{
    /**
    * Contrsutor para la clase 
    * Seguimiento Model
    */
    public function __construct(){
        $this->load->database();
    }

    /**
    * Funcion para obtener las citas en base 
    * a la fecha y al id del empleado
    * @param $fecha : fecha de busqueda
    * @param $doc : id del doctor
    */
    public function getCitas($fecha,$doc){
        $this->db->select('ID_PAC_FK,ID_CITA_PK,NOMBRE_PAC AS title,FECH_CITA AS start,HORA_CITA,ESTAT_CITA');
        $this->db->from('AGENDA_CITA G');
        $this->db->join('PACIENTE P','G.ID_PAC_FK = P.ID_PAC_PK');
        $this->db->where('FECH_CITA >= ',$fecha);
        $this->db->where('ID_EMP_FK',$doc);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    public function getPacInfo($id,$fechaActual){

        $this->db->select('ID_PAC_PK,NOMBRE_PAC,APP_PAC,APM_PAC,ID_CITA_PK, IF(FECH_CITA ="'.$fechaActual.'", "YES", "NO") AS HOY');
        $this->db->from('PACIENTE P');
        $this->db->join('AGENDA_CITA C','P.ID_PAC_PK = C.ID_PAC_FK');
        $this->db->where('ID_PAC_PK',$id);
        //$this->db->where('FECH_CITA',$fecha);
        //$this->db->where('ESTAT_CITA','1');
        //$consulta = $this->db->get_compiled_select();
        //die(var_dump($consulta));
        $query = $this->db->get();
        //die(var_dump($query->result()));

        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    public function getPacInfoCont($id){
        $this->db->select('ID_PAC_PK,NOMBRE_PAC,APP_PAC,APM_PAC');
        $this->db->from('PACIENTE');
        $this->db->where('ID_PAC_PK',$id);
        $this->db->where('ESTAT_CITA','1');
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion para guardar el detalle de la cita
    * recibe los datos a guardar y regresa TRUE 
    * Si es exitoso
    * @param $datos [ID_PAC_FK,ID_CITA_FK,TA,FC,FR,ID_PROC_FK,OBS_PROC]
    * @return TRUE
    */
    public function saveDetalle($datos){
        return $this->db->insert('DETALLE_CITA',$datos);
    }

    /**
    * Funcion para hacer la actualizacion de los estatus
    * de las citas agendadas
    * @param $cita : id de la cita
    * @param $datos [ESTAT_CITA,FECH_CITA,HORA_CITA]
    * @return TRUE
    */
    public function updateCita($cita,$datos){
        $this->db->where('ID_CITA_PK',$cita);
        return $this->db->update('AGENDA_CITA',$datos);
    }

    public function saveInfoCita($cita){
        return $this->db->insert('AGENDA_CITA',$cita);;
    }

    public function getPadecDentalById($id){
        $this->db->select('ID_DIENTE_FK,NUM_DIENTE,DESC_PAD');
        $this->db->from('REL_PAD_DIENTE R');
        $this->db->join('DIENTE_NUM D','R.ID_DIENTE_FK = D.ID_DIENTE_PK');
        $this->db->join('PADECIMIENTO_DENTAL M','R.ID_PAD_FK = M.ID_PAD_PK');
        $this->db->where('ID_PAC_FK',$id);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    public function getListCitas($idPac){
        $this->db->select();
        $this->db->from();
        $this->db->where();
        $this->db->order_by();
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    public function getInfoCita(){
        $this->db->select();
        $this->db->from();
        $this->db->where();
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Metodo para obtener las citas por mes y año
    * de cada doctor
    * @param $fechIni
    * @param $fechFin
    */
    public function getDiaryQuotes($fechIni, $fechFin, $doc){
        $this->db->select('ID_PAC_FK,ID_CITA_PK,NOMBRE_PAC AS title,FECH_CITA AS start,HORA_CITA,ESTAT_CITA');
        $this->db->from('AGENDA_CITA G');
        $this->db->join('PACIENTE P','G.ID_PAC_FK = P.ID_PAC_PK');
        $this->db->where('FECH_CITA >= ',$fechIni);
        $this->db->where('FECH_CITA <= ',$fechFin);
        $this->db->where('ID_EMP_FK',$doc);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    public function saveNewPac($datosPac){
        $this->db->insert('PACIENTE',$datosPac);
        return $this->db->insert_id();
    }

    public function saveNewCita($cita){
        $this->db->insert('AGENDA_CITA',$cita);
        return $this->db->insert_id();
    }

    /**
    * Metodo para hacer la busqueda de pacientes
    * por nombre y regresar una lista con las coincidencias
    * @param $nameSearch
    */
    public function searchPacForName($nameSearch){
        $this->db->select('ID_PAC_PK, CONCAT_WS(" ", NOMBRE_PAC, APP_PAC, APM_PAC) AS PACIENTE');
        $this->db->from('PACIENTE');
        $this->db->like('CONCAT_WS(" ", NOMBRE_PAC, APP_PAC, APM_PAC)',$nameSearch);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    public function chekStatPac($pacId){
        $this->db->select('ESTAT_PAC');
        $this->db->from('PACIENTE');
        $this->db->where('ID_PAC_PK',$pacId);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    public function checkPacDb($name,$app,$apm){
        $this->db->select('ID_PAC_PK,FCH_REG_PAC');
        $this->db->from('PACIENTE');
        $this->db->where('NOMBRE_PAC',$name);
        $this->db->where('APP_PAC',$app);
        $this->db->where('APM_PAC',$apm);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }
}

/*
//QUERY NUEVO 
SELECT 
    ID_PAC_FK, 
    ID_CITA_PK,
    NOMBRE_PAC,
    FECH_CITA,
    HORA_CITA,
    ESTAT_CITA 
FROM 
    AGENDA_CITA G 
    JOIN PACIENTE P ON G.ID_PAC_FK = P.ID_PAC_PK
WHERE 
    FECH_CITA >= 'DATE_SUB(2019-02-05, INTERVAL 30 DAY)'
    AND ID_EMP_FK = 1;

//QUERY VIEJO
SELECT 
    `ID_PAC_FK`, 
    `ID_CITA_PK`, 
    `NOMBRE_PAC` AS `title`, 
    `FECH_CITA`, 
    `HORA_CITA`, 
    `ESTAT_CITA` 
FROM 
    `AGENDA_CITA` `G` 
    INNER JOIN `PACIENTE` `P` ON `G`.`ID_PAC_FK` = `P`.`ID_PAC_PK` 
WHERE 
    `FECH_CITA` >= 'DATE_SUB("2019-02-05" , interval 30 day)' 
    AND `ID_EMP_FK` = '1';
    
SELECT 
    ID_PAC_PK,
    NOMBRE_PAC,
    APP_PAC,
    APM_PAC,
    ID_CITA_PK, 
    IF(FECH_CITA = now(), "YES", "NO") AS HOY
FROM
    PACIENTE P
    INNER JOIN  AGENDA_CITA C ON(P.ID_PAC_PK = C.ID_PAC_FK)
WHERE ID_PAC_PK = 2;









SELECT 
    ID_DIENTE_FK,
    NUM_DIENTE,
    DESC_PAD
FROM 
    REL_PAD_DIENTE R INNER JOIN DIENTE_NUM D ON(R.ID_DIENTE_FK = D.ID_DIENTE_PK) 
    INNER JOIN PADECIMIENTO_DENTAL M ON(R.ID_PAD_FK = M.ID_PAD_PK)
WHERE
    ID_PAC_FK = 20;



    */