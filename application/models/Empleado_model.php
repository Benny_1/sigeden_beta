<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos
 * @version 0.1
 * @copyright Noe ramos lopex, Todos los derechos reservados 2017
*/

class Empleado_model extends My_Model{
    /**
    * Contrsutor para la clase 
    * Profesion Model
    */
    public function __construct(){
        $this->load->database();
    }

    /**
    * Funcion para obtener el listado de los 
    * empleados
    * @return lista de empleados
    */
    public function getEmpleados($registros,$pagina){

        $this->db->select('ID_EMP_PK,NOM_EMP,APP_EMP,APM_EMP,GENERO_EMP');
        $this->db->from('EMPLEADO');
        $this->db->where('ESTAT_EMP','1');
        $consulta = $this->db->get_compiled_select();
        
        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

    /**
    * Funcion para guarda o actualizar
    * la informacion del empleado
    * @param $id : identificador del empleado
    * @param $Array [NOM_EMP,APP_EMP,APM_EMP,TEL_EMP,EMAIL_EMP,SUELDO_EMP,FCH_REG_EMP]
    * @return TRUE si la operacion es exitosa o FALSE si falla.
    */
    public function saveOrUpdateInfoEmploy($info,$id){

        if(empty($id)){
            $this->db->insert('EMPLEADO',$info);
        }else{
            $this->db->where('ID_EMP_PK',$id);
            $this->db->update('EMPLEADO',$info);
        }

        return TRUE;
    }

    /**
    * Funcion para hacer la baja
    * logica del empleado
    * @param $idEmp : id del empleado
    * @return TRUE cuando es exitoso o FALSE si falla
    */
    public function bajaEmploy($idEmp){
        $this->db->set('ESTAT_EMP','0');
        $this->db->where('ID_EMP_PK',$idEmp);
        $this->db->update('EMPLEADO');
        return TRUE;
    }

    /**
    * Funcion para obtener la informacion
    * del empleado en base al id que se le pase
    * @param $idEmploy
    */
    public function getEmploy($idEmploy){
        
        $this->db->select('ID_EMP_PK,NOM_EMP,APP_EMP,APM_EMP,GENERO_EMP,TEL_EMP,EMAIL_EMP,SUELDO_EMP');
        $this->db->from('EMPLEADO');
        $this->db->where('ID_EMP_PK',$idEmploy);
        $query = $this->db->get();

        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion para obtener el listado de empleados
    * con el filtro de nombre o apellido paterno o materno
    * @param $filtro
    * @param $registro
    * @param $pagina
    */
    public function getEmployPaginadoFiltro($filtro,$registros,$pagina){
        $this->db->select('ID_EMP_PK,NOM_EMP,APP_EMP,APM_EMP,GENERO_EMP');
        $this->db->from('EMPLEADO');
        $this->db->where('ESTAT_EMP','1');

        if($filtro[0] != NULL && $filtro[0] != ""){
            $this->db->like('NOM_EMP', $filtro[0]);
        }

        if($filtro[1] != NULL && $filtro[1] != ""){
            $this->db->like('APP_EMP', $filtro[1]);
        }

        if($filtro[2] != NULL && $filtro[2] != ""){
            $this->db->like('APM_EMP', $filtro[2]);
        }

        $consulta = $this->db->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);   
    }
}