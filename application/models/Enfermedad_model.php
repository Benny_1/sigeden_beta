<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos
 * @version 0.1
 * @copyright Noe ramos lopex, Todos los derechos reservados 2017
*/

class Enfermedad_model extends My_Model{
    /**
    * Contrsutor para la clase 
    * Enfermedad Model
    */
    public function __construct(){
        $this->load->database();
    }

    /**
    * Funcion para obtener el listado 
    * de las enfermedades patologicas con paginacion
    * @param $registros numero e registros que se quieren por pagina
    * @param $pagina la pagina a consultar
    * @return array() respuesta que regresa el listado , el total de paginas y pagina actual
    */
    public function getEnfermedadPaginado($registros,$pagina){
        
        $this->db->select('ID_ENFER_PK, NOMBRE_ENFER,ALERT_ENFER');
        $this->db->where('ESTAT_ENFER','1');
        $consulta = $this->db->from('ENFERMEDAD_PATOLOGICA')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

    /**
    * Funcion para obtener la informacion 
    * de una enfermedad por id, recibe el 
    * identificador de la enfermedad y 
    * regresa la informacion
    * @param $id : identificador
    * @return $array() : arreglo con la informacion
    */
    public function getInfoById($id){
        $this->db->select('ID_ENFER_PK, NOMBRE_ENFER');
        $this->db->where('ID_ENFER_PK',$id);
        $query = $this->db->get('ENFERMEDAD_PATOLOGICA');
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion para agregar una nueva enfermedad
    * recibe los datos de la enfermedad para guardarla
    * @param $datos[NOMBRE_ENFER,ESTAT_ENFER]
    * @return $TRUE en caso de exito FALSE si falla
    */
    public function saveEnfermedad($datos){
        $this->db->insert('ENFERMEDAD_PATOLOGICA',$datos);
        return TRUE;
    }

    /**
    * Funcion para hacer la actualizacion de
    * una enfermedad patologica
    * @param $datos[NOMBRE_ENFER,ESTAT_ENFER] : datos a actulizar
    * @param $id : identificador de la enfermedad
    * @return TRUE en caso de exito FALSE en caso de fallo
    */
    public function updateEnfermedad($datos,$id){
        $this->db->where('ID_ENFER_PK',$id);
        $this->db->update('ENFERMEDAD_PATOLOGICA',$datos);
        return TRUE;
    }

    /**
    * Funcion para obtener las enfermedades patologicas
    * con un parametro como filtro
    * @param $filtro : nombre del padecimiento por el cual se hara el filtro
    * @param $registros : numero de registros que se mostraran por pagina
    * @param $pagina : pagina a consultar
    */
    public function getEnfermedadPaginadoFiltro($filtro,$registros,$pagina){
        $this->db->select('ID_ENFER_PK, NOMBRE_ENFER,ALERT_ENFER');
        $this->db->where('ESTAT_ENFER','1');
        $this->db->like('NOMBRE_ENFER', $filtro);
        $consulta = $this->db->from('ENFERMEDAD_PATOLOGICA')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

    public function updateAlert($id,$valorA){
        $this->db->set('ALERT_ENFER',$valorA);
        $this->db->where('ID_ENFER_PK',$id);
        $this->db->update('ENFERMEDAD_PATOLOGICA');
        return TRUE;
    }
}