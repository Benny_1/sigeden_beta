<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author ING, Noé Ramos Lopez
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Modelo que tiene las funciones para hacer
 * Altabas, Bajas logicas, Consultas y Modificaciones 
 * de un padecimiento dental
*/

class PadecimientoDental_model extends My_Model{
    /**
    * Constructor de la clase Padecimiento model
    */
    public function __construct(){
         parent::__construct();
        $this->load->database();
    }

    /**
    * Funcion para obtener el listado 
    * de los padecimientos dentales paginado
    * @param $registros numero e registros que se quieren por pagina
    * @param $pagina la pagina a consultar
    * @return array() respuesta que regresa el listado , el total de paginas y pagina actual
    */
    public function getPadecimientoPaginado($registros,$pagina){
        
        $this->db->select('ID_PAD_PK, DESC_PAD');
        $this->db->where('ESTAT_PAD','1');
        $consulta = $this->db->from('PADECIMIENTO_DENTAL')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

    /**
    * Funcion que hace la busqueda 
    * de un padecimiento dental por ID
    * @param $id identificador del padecimiento
    * @return la informacion del pacimiento
    */
    public function getInfoById($id){
        $this->db->select('ID_PAD_PK, DESC_PAD');
        $this->db->where('ID_PAD_PK',$id);
        $query = $this->db->get('PADECIMIENTO_DENTAL');
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion que guarda un padecimeinto nuevo
    * @param $datos[DESC_PAD,ESTAT_PAD]
    * @return TRUE si es exitoso FLASE si falla
    */
    public function savePadecimiento($datos){
        $this->db->insert('PADECIMIENTO_DENTAL',$datos);
        return TRUE;
    }

    /**
    * Funcion que hace la actualizacion del padecimiento
    * @param $datos[ESTAT_PAD]
    * @param $id identificador donde se hara la actualizacion
    * @return TRUE si es exitoso FALSE si falla
    */
    public function updatePadecimiento($datos,$id){
        $this->db->where('ID_PAD_PK',$id);
        $this->db->update('PADECIMIENTO_DENTAL',$datos);
        return TRUE;
    }

    /**
    * Funcion para obtener los padecimientos dentales
    * con un parametro como filtro
    * @param $filtro : nombre del padecimiento por el cual se hara el filtro
    * @param $registros : numero de registros que se mostraran por pagina
    * @param $pagina : pagina a consultar
    */
    public function getPadecimientoPaginadoFiltro($filtro,$registros,$pagina){
        $this->db->select('ID_PAD_PK, DESC_PAD');
        $this->db->where('ESTAT_PAD','1');
        $this->db->like('DESC_PAD', $filtro);
        $consulta = $this->db->from('PADECIMIENTO_DENTAL')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }
}