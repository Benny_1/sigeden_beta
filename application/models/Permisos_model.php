<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos
 * @version 0.1
 * @copyright Noe ramos lopex, Todos los derechos reservados 2017
*/

class Permisos_model extends CI_Model{
	/**
	* Funcion para el constructor de Login_model
	*/
	public function __construct(){
		$this->load->database();
	}

	public function getPermissionByIdRol($idRol){
		
		$this->db->select('GROUP_CONCAT(ID_MENU_FK) AS PERMISOS');
		$this->db->from('PERMISOS');
		$this->db->where('ID_ROL_FK',$idRol);

		$query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
	}
	/**

	select GROUP_CONCAT(ID_MENU_FK) AS PERMISOS from permisos p where ID_ROL_FK = 1;
	*/
}