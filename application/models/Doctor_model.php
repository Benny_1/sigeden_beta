<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos
 * @version 0.1
 * @copyright Noe ramos lopez, Todos los derechos reservados 2017
*/

class Doctor_model extends My_Model{
	/**
	* Funcion para el constructor de
	*/
	public function __construct(){
		$this->load->database();
	}

    /**
    * Metodo para verificar si paciente
    * ya estaba previamente registrado
    * @param $name
    * @param $paterno
    * @param $materno
    * @param $fecha
    * @param $estado
    * @param $profesion
    * @param $est_civil
    * @return $ID_PAC_PK
    */
    public function existePersona($name,$paterno,$materno,$fecha,$estado,$profesion,$est_civil){
        
        $this->db->select('ID_PAC_PK');
        $this->db->where('NOMBRE_PAC',$name);
        $this->db->where('APP_PAC',$paterno);
        $this->db->where('APM_PAC',$materno);
        $this->db->where('FECNAC_PAC',$fecha);
        $this->db->where('ID_EST_FK',$estado);
        $this->db->where('ID_PROF_FK',$profesion);
        $this->db->where('EST_CIVIL_PAC',$est_civil);
        $query = $this->db->get('PACIENTE');
        
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

	public function getIdPatient($name,$paterno,$materno,$fecha,$estado){
		$this->db->select('ID_PAC_PK');
		$this->db->where('NOMBRE_PAC',$name);
		$this->db->where('APP_PAC',$paterno);
		$this->db->where('APM_PAC',$materno);
		$this->db->where('FECNAC_PAC',$fecha);
		$this->db->where('ID_EST_FK',$estado);
		$query = $this->db->get('PACIENTE');
		return ($query->num_rows() <= 0) ? NULL : $query->row();	
	}

    /**
    * Funcion para obtener el listado paginado
    * de los pacientes, recibe el numero de registros
    * que se quieren por pagina y la pagina a consultar
    * @param $registros : numero de registros por pagina
    * @param $pagina : pagina a consultar
    * @return $array() ['lista'] : Listado de los pacientes
    *                  ['paginasTotales'] : numero de paginas totales de la consulta
    *                  ['paginActual'] : pagina que se consulto
    */
    public function getPacientePaginado($registros,$pagina,$idUsr){
    /*
    SELECT 
        ID_PAC_PK,
        CONCAT(NOMBRE_PAC,' ',APP_PAC) AS NOMBRE,
        TIMESTAMPDIFF(YEAR,FECNAC_PAC,CURDATE()) AS EDAD,
        IF(OBSER_PAC = NULL, 'SIN OBSERVACION', OBSER_PAC) AS OBSER,
        IF(ALERTA_REL = 1, 'SI', 'NO') AS ALERTA
    FROM PACIENTE P
        LEFT JOIN REL_PAC_ENFER R ON(P.ID_PAC_PK = R.ID_PAC_FK)
    WHERE ESTAT_PAC IN(1,3)
    AND ID_USR_ALT = 7 
    GROUP BY ID_PAC_PK;
    */
        $this->db->select("ID_PAC_PK,CONCAT(NOMBRE_PAC,' ',APP_PAC) AS NOMBRE,TIMESTAMPDIFF(YEAR,FECNAC_PAC,CURDATE()) AS EDAD,OBSER_PAC,IF(ALERTA_REL = 1, 'SI', 'NO') AS ALERTA");
        $this->db->from('PACIENTE P');
        //$this->db->join('ESTADO E ','P.ID_EST_FK = E.ID_EST_PK');
        $this->db->join('REL_PAC_ENFER R','P.ID_PAC_PK = R.ID_PAC_FK','LEFT');
        $estat = array('3','1');
        $this->db->where_in('ESTAT_PAC',$estat);
        $this->db->where('ID_USR_ALT',$idUsr);
        $this->db->group_by('ID_PAC_PK');
        $consulta = $this->db->get_compiled_select();
        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

    /**
    * Funcion para obtener las adicciones
    * con un parametro como filtro
    * @param $filtro : nombre de la profesion a filtrar
    * @param $registros : numero de registros que se mostraran por pagina
    * @param $pagina : pagina a consultar
    */
    public function getPacientePaginadoFiltro($filtro,$registros,$pagina){
        
        $this->db->select("ID_PAC_PK,CONCAT(NOMBRE_PAC,' ',APP_PAC) AS NOMBRE,TIMESTAMPDIFF(YEAR,FECNAC_PAC,CURDATE()) AS EDAD,OBSER_PAC,IF(ALERTA_REL = 1, 'SI', 'NO') AS ALERTA");
        $this->db->from('PACIENTE P');
        //$this->db->join('ESTADO E ','P.ID_EST_FK = E.ID_EST_PK');
        $this->db->join('REL_PAC_ENFER R','P.ID_PAC_PK = R.ID_PAC_FK','LEFT');

        // silos filtros es un arreglo se buscan
        if(is_array($filtro)){
            if($filtro[0] != NULL && $filtro[0] != ""){
                $this->db->like('NOMBRE_PAC', $filtro[0]);
            }

            if($filtro[1] != NULL && $filtro[1] != ""){
                $this->db->like('APP_PAC', $filtro[1]);
            }

            if($filtro[2] != NULL && $filtro[2] != ""){
                $this->db->like('APM_PAC', $filtro[2]);
            }
        }else{
            // silos filtros es el id del paciente se busca el id
            $this->db->where_in('P.ID_PAC_PK',$filtro);
        }



        $estat = array('3','1');
        $this->db->where_in('ESTAT_PAC',$estat);

        $this->db->group_by('ID_PAC_PK');
        
        $consulta = $this->db->get_compiled_select();
      
        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

	/**
	* Funcion para obtener la informacion general 
	* del paciente.
    * @param $idPatient :  identificador del paciente
    * @return $array() :  Informacion del paciente
	*/
	public function getPatientById($idPatient){
		$this->db->select("ID_PAC_PK,NOMBRE_PAC,APP_PAC,APM_PAC,DATE_FORMAT(FECNAC_PAC,'%m-%d-%Y') AS FECNAC_PAC,ID_EST_FK,DIRECCION_PAC,GENERO_PAC,ID_PROF_FK,EST_CIVIL_PAC,TEL_PAC,CORREO_PAC,OBSER_PAC");
		$this->db->from('PACIENTE');
		$this->db->where('ID_PAC_PK',$idPatient);
		$query = $this->db->get();
		return ($query->num_rows() <= 0) ? NULL : $query->row();
	}

    /**
    * Funcion para guardar o actualizar
    * la informacion del paciente
    * recibe los datos y el id, si el id
    * es nulo es una insercion en caso
    * contrario es una actualizacion
    * @param $datos[]
    * @param $id
    * @return 
    */
    public function saveOrUpdateInfoPat($datos,$id){
        
        if(is_null($id) || $id == ""){
            $this->db->insert('PACIENTE',$datos);
        }else{
            $this->db->where('ID_PAC_PK',$id);
            $this->db->update('PACIENTE',$datos);
        }
        return TRUE;
    }

    /**
    * Funcion para guardar la relacion de
    * enfermedad patologica del paciente
    */
    public function saveRelacionEnfer($relEnfer){
        $this->db->insert('REL_PAC_ENFER',$relEnfer);
        return TRUE;
    }

    /**
    * Funcion para guardar la relacion de 
    * enfermedad patologica familiar del 
    * paciente
    */
    public function saveEnferFamiliar($relFamiliar){
        $this->db->insert('REL_PAC_PAR',$relFamiliar);
        return TRUE;
    }

    /**
    * Funcion para guardar la relacion de
    * habitos que tiene el paciente
    */
    public function saveHabitos($datos){
        $this->db->insert('HABITOS',$datos);
        return TRUE;
    }

    /**
    * Funcion para guardar la relacion 
    * de adiccion del paciente
    */
    public function saveAdicPac($datos){
        $this->db->insert('REL_ADIC_PER',$datos);
        return TRUE;
    }

    /**
    * Funcion para traer los habitos del paciente
    * @param $identificador : identificador del paciente
    * @return $array() : resultado de la busqueda
    */
    public function getHabitosById($identificador){
        $this->db->select('NUM_COMI,HIG_DENTAL,HIL_DENTAL,ENJ_DENTAL,ADICCION,NUM_SEP,INGES_ALIME_DURO,INGES_TEMP_ELEVAD,INMU,METOD_ANTI_CONCEP,TRIME_EMBAR');
        $this->db->from('HABITOS');
        $this->db->where('ID_PAC_FK',$identificador);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion que regresa las 
    * enfermedades patologicas 
    * del paciente
    * @param $identificador : id del paciente
    * @return $array() : resultado de la busqueda
    */
    public function getEnferById($identificador){
        $this->db->select('ID_ENFER_FK');
        $this->db->from('REL_PAC_ENFER');
        $this->db->where('ID_PAC_FK',$identificador);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();   
    }

    /**
    * Funcion para obtener las 
    * enfermedades patologicas 
    * familiares del paciente
    * @param $identificador : id del paciente
    * @return $array() : resultado de la busqueda
    */
    public function getEnferFamById($identificador){
        $this->db->select('ID_PAR_FK,ID_ENFER_FK');
        $this->db->from('REL_PAC_PAR');
        $this->db->where('ID_PAC_FK',$identificador);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();   
    }

    /**
    * Funcion para obtener
    * las toxicomanias 
    * del paciente
    */
    public function getToxicomania($identificador){
        $this->db->select('ID_ADIC_FK');
        $this->db->from('REL_ADIC_PER');
        $this->db->where('ID_PAC_FK',$identificador);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Funcion para hacer un borrado de la relacion de enfermedades
    * del paciente y poder crear una nueva
    * @param $idPer :  identificador paciente
    * @return TRUE si es exitoso o FALSE si falla.
    */
    public function deleteEnfermedades($idPer){
        $this->db->where('ID_PAC_FK',$idPer);
        $this->db->delete('REL_PAC_ENFER');
        return TRUE;
    }

    /**
    * Funcion para obtener las enfermedades patologicas
    * de un familiar dependiendo del familiar que se 
    * selecciona.
    * @param $idFam : identificador del familiar
    * @param $idPac :  identificador del paciente
    * @return $array() : relacion padecimiento familiar 
    */
    public function getInfoFamiliarById($idFam,$idPac){
        $this->db->select('ID_ENFER_FK');
        $this->db->from('REL_PAC_PAR');
        $this->db->where('ID_PAC_FK',$idPac);
        $this->db->where('ID_PAR_FK',$idFam);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Funcion para borrar la relacion de antecedentes
    * familiares y crear una nueva
    * @param $idFam : identificador del familiar
    * @param $idPer : identificarode del paciente
    * @return TRUE si es exitoso o FLASE si falla.
    */
    public function deleteRelacionFamiliar($idFam,$idPer){
        $this->db->where('ID_PAC_FK',$idPer);
        $this->db->where('ID_PAR_FK',$idFam);
        $this->db->delete('REL_PAC_PAR');
        return TRUE;
    }

    /**
    * Funcion para guardar la informacion de los padecimientos
    * dentales del paciente.
    * @param $relacion[ID_PAC_FK,ID_DIENTE_FK,ID_PAD_FK]
    * @return TRUE si es exitoso o FALSE si falla
    */
    public function saveInfoPadecimiento($relacion){
        $this->db->insert('REL_PAD_DIENTE',$relacion);
        return TRUE;
    }

    /**
    * Funcion para
    */
    public function getInfoRepById($idPaciente){
        $this->db->select('NOMBRE_PAC,APP_PAC,APM_PAC,TEL_PAC,FECNAC_PAC,NOMBRE_EST,DIRECCION_PAC,GENERO_PAC,NOM_PROF,EST_CIVIL_PAC');
        $this->db->from('PACIENTE');
        $this->db->join('ESTADO','PACIENTE.ID_EST_FK = ESTADO.ID_EST_PK');
        $this->db->join('PROFESIONES','PACIENTE.ID_PROF_FK = PROFESIONES.ID_PROF_PK');
        $this->db->where('ID_PAC_PK',$idPaciente);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion para saber si el paciente
    * tiene una cita activa en el sistema
    * en caso de que si, regresa la cita
    * en caso contrario se regresa nulo
    * @param $paciente : id del paciente
    * @return $row : informacion del paciente
    */
    public function getCitaPac($paciente){
        
        $this->db->select('ID_CITA_PK');
        $this->db->from('AGENDA_CITA');
        $this->db->where('ID_PAC_FK',$paciente);
        $this->db->where('ESTAT_CITA','1');
        $query = $this->db->get();

        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    public function saveInfoCita($cita){
        $this->db->insert('AGENDA_CITA',$cita);
        return TRUE;
    }

    /**
    * Funcion para validar si el paciente tiene registro en habitos
    * en caso de que si tenga se regresa el id del paciente
    * si no se regresa NULL
    * @param $idPac : identificador del paciente
    * @return array() : id del paciente
    */
    public function validHabitos($idPac){
        $this->db->select('ID_PAC_FK');
        $this->db->from('HABITOS');
        $this->db->where('ID_PAC_FK',$idPac);
        $query = $this->db->get();

        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Metodo para  actualizar los habitos de
    * higiene de la persona
    * @param $datos[NUM_COMI,HIG_DENTAL,HIL_DENTAL,ENJ_DENTAL,ADICCION,NUM_SEP]
    * @param $id : identificador del paciente
    * @return TRUE en caso de exito FLASE si algo sale mal
    */
    public function updateHabitos($datos,$id){
        
        $this->db->where('ID_PAC_FK',$id);
        $this->db->update('HABITOS',$datos);

        return TRUE;
    }

    public function updatePaciente($datos,$id){
        $this->db->where('ID_PAC_PK',$id);
        $this->db->update('PACIENTE',$datos);
        return TRUE;
    }

    public function getAlertEnfer(){
        $this->db->select('ID_ENFER_PK');
        $this->db->from('ENFERMEDAD_PATOLOGICA');
        $this->db->where('ALERT_ENFER','1');
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    public function getPadecimientoDiente($id){
        $this->db->select('ID_DIENTE_FK,NUM_DIENTE,DESC_PAD');
        $this->db->from('REL_PAD_DIENTE R');
        $this->db->join('DIENTE_NUM D','R.ID_DIENTE_FK = D.ID_DIENTE_PK');
        $this->db->join('PADECIMIENTO_DENTAL M','R.ID_PAD_FK = M.ID_PAD_PK');
        $this->db->where('ID_PAC_FK',$id);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    //funcion pra obtener los padecimeintos por diente y paciente
    public function getPadDentById($idPac,$idDien){
        //select * from rel_pad_diente where id_pac_fk = 20 and id_diente_fk = 30
        $this->db->select('ID_PAD_FK');
        $this->db->from('REL_PAD_DIENTE');
        $this->db->where('ID_PAC_FK',$idPac);
        $this->db->where('ID_DIENTE_FK',$idDien);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    //ver que cargue el detalle de los dientes
    public function getDetalleCitas($id){
        $this->db->select('ID_DIENTE_FK,NUM_DIENTE,DESC_PAD');
        $this->db->from('REL_PAD_DIENTE');
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();   
    }

    //borrar la relacion que ya se tenia
    public function deletePadDent($idPac, $idDiente){
        

        $dienteArray = array();
        $dienteArray['ID_PAC_FK'] =  $idPac;
        foreach ($idDiente as $ide) {
            $dienteArray += [ "ID_DIENTE_FK" => $ide ];
        }

        $this->db->delete('REL_PAD_DIENTE',$dienteArray);
        

        return TRUE;
    }

    public function getDetalleCitaById($idDetalle){
        /*
            SELECT G.ID_CITA_PK, NOMBRE_PAC, APP_PAC, APM_PAC, TA, FC, FR, OBS_PROC, ID_PROC_FK, DSC_PROC 
            FROM  AGENDA_CITA G INNER JOIN DETALLE_CITA D ON(G.ID_CITA_PK = D.ID_CITA_FK) 
                INNER JOIN PACIENTE P ON(G.ID_PAC_FK = P.ID_PAC_PK) 
                INNER JOIN PROCEDIMIENTO C ON(D.ID_PROC_FK = C.ID_PROC_PK) 
            WHERE  G.ID_CITA_PK = 18;
        */

        $this->db->select(' G.ID_CITA_PK, NOMBRE_PAC, APP_PAC, APM_PAC, TA, FC, FR, OBS_PROC, ID_PROC_FK,DSC_PROC ');
        $this->db->from('AGENDA_CITA G ');
        $this->db->join('DETALLE_CITA D ','G.ID_CITA_PK = D.ID_CITA_FK');
        $this->db->join('PACIENTE P ','G.ID_PAC_FK = P.ID_PAC_PK');
        $this->db->join('PROCEDIMIENTO C ','D.ID_PROC_FK = C.ID_PROC_PK');
        $this->db->where('G.ID_CITA_PK',$idDetalle);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    public function cambiaEstatPac($paciente){
        $this->db->set('ESTAT_PAC','1');
        $this->db->where('ID_PAC_PK',$id);
        $this->db->update('PACIENTE');
        return TRUE;
    }

    public function getHistoryQuotes($idPaciente){
        /*
            SELECT FECH_CITA,TA,FC,FR,DSC_PROC,OBS_PROC 
            FROM DETALLE_CITA D 
                INNER JOIN AGENDA_CITA A ON(D.ID_PAC_FK = A.ID_PAC_FK)
                INNER JOIN PROCEDIMIENTO P ON(D.ID_PROC_FK = P.ID_PROC_PK)
            WHERE A.ID_PAC_FK = 118 GROUP BY ID_CITA_PK;
        */

        $this->db->select('FECH_CITA,TA,FC,FR,DSC_PROC,OBS_PROC');
        $this->db->from('DETALLE_CITA D');
        $this->db->join('AGENDA_CITA A','D.ID_CITA_FK = A.ID_CITA_PK');
        $this->db->join('PROCEDIMIENTO P','D.ID_PROC_FK = P.ID_PROC_PK');
        $this->db->where('A.ID_PAC_FK',$idPaciente);
        //$this->db->group_by('ID_CITA_PK');
        //$query = $this->db->get_compiled_select();
        //die(var_dump($query));
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }


    public function getAllDetailQuoteToDay($toDay){

        //SELECT  `FECH_CITA`,  `TA`,  `FC`,  `FR`,  `DSC_PROC`, `OBS_PROC` 
        //FROM DETALLE_CITA D INNER JOIN AGENDA_CITA A ON(D.ID_CITA_FK = A.ID_CITA_PK) 
        //INNER JOIN PROCEDIMIENTO P ON(D.ID_PROC_FK = P.ID_PROC_PK) 
        //WHERE FECH_CITA = '2020-06-22';

        $this->db->select('FECH_CITA,TA,FC,FR,DSC_PROC,OBS_PROC');
        $this->db->from('DETALLE_CITA D');
        $this->db->join('AGENDA_CITA A','D.ID_CITA_FK = A.ID_CITA_PK');
        $this->db->join('PROCEDIMIENTO P','D.ID_PROC_FK = P.ID_PROC_PK');
        $this->db->where('FECH_CITA',$toDay);
        $query = $this->db->get();
        return ($query->num_rows() <= 0) ? NULL : $query->result();   
    }
}

    
//se hace modificacion inciial para el gitflow

//SELECT TIMESTAMPDIFF(YEAR,FECNAC_PAC,CURDATE()) AS EDAD FROM PACIENTE;
/*

SELECT 
    ID_PAC_PK,
    CONCAT(NOMBRE_PAC,' ',APP_PAC) AS NOMBRE,
    TIMESTAMPDIFF(YEAR,FECNAC_PAC,CURDATE()) AS EDAD,
    OBSER_PAC,
    if(ALERTA_REL = 1, 'SI', 'NO') AS ALERTA
FROM PACIENTE P
    INNER JOIN ESTADO E ON(P.ID_EST_FK = E.ID_EST_PK)
    LEFT JOIN REL_PAC_ENFER R ON(P.ID_PAC_PK = R.ID_PAC_FK)
GROUP BY ID_PAC_PK;



select * from detalle_cita where id_pac_fk = 118;


SELECT ID_CITA_FK,TA,FC,FR,OBS_PROC FROM DETALLE_CITA WHERE ID_PAC_FK = 118;





SELECT 
    `FECH_CITA`, 
    `TA`, 
    `FC`, 
    `FR`, 
    `DSC_PROC`, 
    `OBS_PROC`
FROM 
    DETALLE_CITA D 
INNER JOIN 
    AGENDA_CITA A ON(D.ID_CITA_FK = A.ID_CITA_PK) 
INNER JOIN 
    PROCEDIMIENTO P ON(D.ID_PROC_FK = P.ID_PROC_PK) 
WHERE 
    FECH_CITA = '2020-06-22';
    D.ID_PAC_FK = 118 
GROUP BY 
    ID_CITA_PK;
************************************************************
SELECT 
    `FECH_CITA`, 
    `TA`, 
    `FC`, 
    `FR`, 
    `DSC_PROC`, 
    `OBS_PROC` 
FROM 
    `DETALLE_CITA` `D` 
JOIN `AGENDA_CITA` `A` ON `D`.`ID_PAC_FK` = `A`.`ID_PAC_FK` 
JOIN `PROCEDIMIENTO` `P` ON `D`.`ID_PROC_FK` = `P`.`ID_PROC_PK` 
WHERE `A`.`ID_PAC_FK` = '118' 
GROUP BY `ID_CITA_PK`



*/