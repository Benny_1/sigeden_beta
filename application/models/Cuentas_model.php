<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Ing, Noé Ramos López
 * @version 0.1
 * @copyright Noe ramos lopez, Todos los derechos reservados 2020
 * Fecha de creacion : 23-06-2020
 * Fecha de actualzacion : N/A
 * Modelo para la tabla cuentas_pacientes
*/
class Cuentas_model extends MY_Model{
	/**
	* Funcion para el constructor de Login_model
	*/
	public function __construct(){
		$this->load->database();
	}

	public function getAllAcountPag($numReg,$pag){
		$this->db->select('CUENTA, ID_PAC_FK, PAC_NAME, TOTAL_CUENTA, ABONADO, RESTANTE');
		$this->db->from(' ESTADO_CUENTA_PACIENTE');
        $consulta = $this->db->get_compiled_select();
        return $this->getTablaPaginada($consulta,$numReg,$pag);
	}

	/**
    * Funcion para obtener las cuentas
    * con un parametro como filtro
    * @param $filtro : nombre del paciente a filtrar
    * @param $registros : numero de registros que se mostraran por pagina
    * @param $pagina : pagina a consultar
    */
    public function getAllAcountPagFiltro($filtro,$registros,$pagina){
        $this->db->select('CUENTA, ID_PAC_FK, PAC_NAME, TOTAL_CUENTA, ABONADO, RESTANTE');
        $this->db->like('PAC_NAME', $filtro);
        $consulta = $this->db->from('ESTADO_CUENTA_PACIENTE')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

	public function saveAcount($infoAcount){
		$this->db->insert('CUENTA_PACIENTE',$infoAcount);
		return TRUE;
	}

	public function getDetailAcount($idSearchAcount){
		$this->db->select('ID_ABONO_PK,FECHA_ABONO,FORMAT(MONTO_ABONO,2) AS MONTO_ABONO');
		$this->db->from('CUENTA_PAC_ABONO');
		$this->db->where('ID_CUENTA_FK',$idSearchAcount);
		$query = $this->db->get();
		return ($query->num_rows() <= 0) ? NULL : $query->result();
	}

	public function getAllAbono($idCuenta){
		$this->db->select('ABONADO,TOTAL_CUENTA');
		$this->db->from('ESTADO_CUENTA_PACIENTE');
		$this->db->where('CUENTA',$idCuenta);
		$query = $this->db->get();
		return ($query->num_rows() <= 0) ? NULL : $query->row();	
	}
	public function saveNewMount($infoAbono){
		$this->db->insert('CUENTA_PAC_ABONO',$infoAbono);
		return TRUE;
	}

	public function getMontoCuenta($idCuenta){
		$this->db->select('FECHA_CARGO,ID_CUENTA_PK,FORMAT(TOTAL_CUENTA,2) AS TOTAL_CUENTA');
		$this->db->from('CUENTA_PACIENTE');
		$this->db->WHERE('ID_CUENTA_PK',$idCuenta);
		$query = $this->db->get();
		return ($query->num_rows() <= 0) ? NULL : $query->row();
	}

	public function updateAcountMount($idAcount, $monto){
		$this->db->where('ID_CUENTA_PK',$idAcount);
		$this->db->set('TOTAL_CUENTA',$monto);
        $this->db->update('CUENTA_PACIENTE');
        return TRUE;
	}

	public function updateAcountAbono($idAcount, $monto){
		$this->db->where('ID_ABONO_PK',$idAcount);
		$this->db->set('MONTO_ABONO',$monto);
        $this->db->update('CUENTA_PAC_ABONO');
        return TRUE;
	}
}

/**

CREATE TABLE CUENTA_PACIENTE(
	ID_CUENTA_PK int(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	ID_PAC_FK int(5),
	TOTAL_CUENTA decimal(10,2),
	FOREIGN KEY(ID_PAC_FK) REFERENCES PACIENTE(ID_PAC_PK)
);

CREATE TABLE CUENTA_PAC_ABONO(
	ID_ABONO_PK int(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	ID_CUENTA_FK int(5),
	FECHA_ABONO date,
	MONTO_ABONO decimal(10,2),
	FOREIGN KEY(ID_CUENTA_FK) REFERENCES CUENTA_PACIENTE(ID_CUENTA_PK)
);

CREATE OR REPLACE VIEW ESTADO_CUENTA_PACIENTE AS
	SELECT 
		LPAD(ID_CUENTA_PK, 5, '0') AS CUENTA,
		C.ID_PAC_FK,CONCAT(NOMBRE_PAC," ",APP_PAC," ", APM_PAC) AS PAC_NAME,
		FORMAT(TOTAL_CUENTA,2) AS TOTAL_CUENTA,
		FORMAT(COALESCE(SUM(MONTO_ABONO), 0),2) AS ABONADO,
		FORMAT((TOTAL_CUENTA-COALESCE(SUM(MONTO_ABONO),0)),2) AS RESTANTE 
	FROM 
		CUENTA_PACIENTE C 
	LEFT JOIN 
		CUENTA_PAC_ABONO B ON( C.ID_CUENTA_PK = B.ID_CUENTA_FK) 
	INNER JOIN 
		PACIENTE P ON(C.ID_PAC_FK = P.ID_PAC_PK)
	GROUP BY
		C.ID_CUENTA_PK;

ALTER TABLE tabla1 add colNueva varchar(2) NOT NULL AFTER col1;


ALTER TABLE CUENTA_PACIENTE ADD FECHA_CARGO DATE AFTER ID_PAC_FK;

*/
