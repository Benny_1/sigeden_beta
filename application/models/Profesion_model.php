<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author ING, Noé Ramos López
 * @version 1.0
 * @copyright Todos los derechos reservados 2018
 * Modelo que tiene las funciones para hacer
 * Altabas, Bajas logicas, Consultas y Modificaciones 
 * de una profesion
*/
class Profesion_model extends My_Model{
    /**
    * Contrsutor para la clase 
    * Profesion Model
    */
    public function __construct(){
        $this->load->database();
    }
    
    /**
    * Funcion para obtener las profesiones con paginacion
    * @param $registros : numero de registros por pagina
    * @param $pagina : pagina a consultar
    * @return $array() ['lista'] : Listado de las profesiones
    *                  ['paginasTotales'] : numero de paginas totales de la consulta
    *                  ['paginActual'] : pagina que se consulto
    */
    public function getProfesionPaginado($registros,$pagina){
        $this->db->select('ID_PROF_PK, NOM_PROF');
        $this->db->where('ESTAT_PROF','1');
        $consulta = $this->db->from('PROFESIONES')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }

    /**
    * Funcion para obtener la informacion de una profesion
    * por su identificador
    * @param $id identificador de l profesion
    * @return $array() los datos de la profesion
    */
    public function getInfoById($id){
        $this->db->select('ID_PROF_PK, NOM_PROF');
        $this->db->where('ID_PROF_PK',$id);
        $query = $this->db->get('PROFESIONES');
        return ($query->num_rows() <= 0) ? NULL : $query->row();
    }

    /**
    * Funcion para guardar la informacion de una profesion nueva
    * @param $datos['NOM_PROF', 'ESTAT_PROF'] datos a guardar
    * @return TRUE si es exitoso FALSE si falla
    */
    public function saveProfesion($datos){
        $this->db->insert('PROFESIONES',$datos);
        return TRUE;
    }

    /**
    * Funcion para actualizar el nombre de la profesion
    * @param $datos['NOM_PROF','ESTAT_PROF'] datos a actualizar
    * @param $id identificador de la profesion
    * @return TRUE si es exitoso o FALSE si lla
    */
    public function updateProfesion($datos,$id){
        $this->db->where('ID_PROF_PK',$id);
        $this->db->update('PROFESIONES',$datos);
        return TRUE;
    }

    /**
    * Funcion para obtener las profesiones
    * con un parametro como filtro
    * @param $filtro : nombre de la profesion a filtrar
    * @param $registros : numero de registros que se mostraran por pagina
    * @param $pagina : pagina a consultar
    */
    public function getProfesionPaginadoFiltro($filtro,$registros,$pagina){
        $this->db->select('ID_PROF_PK, NOM_PROF');
        $this->db->where('ESTAT_PROF','1');
        $this->db->like('NOM_PROF', $filtro);
        $consulta = $this->db->from('PROFESIONES')->get_compiled_select();

        return $this->getTablaPaginada($consulta,$registros,$pagina);
    }
}