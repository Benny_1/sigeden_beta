<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author TSU, Noé Ramos
 * @version 0.1
 * @copyright Noe ramos lopez, Todos los derechos reservados 2017
*/

class Catalogo_model extends CI_Model{
    /**
    * Contrsutor para la clase
    * Catalogo Model
    */
    public function __construct(){
        $this->load->database();
    }

    /**
    * Metodo para obtener
    * Todos los estados de la base
    * @return lista de estados
    */
    public function getEstado(){
        $this->db->select('ID_EST_PK,NOMBRE_EST');
        $query = $this->db->get('ESTADO');
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Metodo para obtener 
    * Todas las enfermedades.
    * @return lista de las enfermedades
    */
    public function getEnfermedad(){
        $this->db->select('ID_ENFER_PK,NOMBRE_ENFER');
        $this->db->where('ESTAT_ENFER','1');
        $query = $this->db->get('ENFERMEDAD_PATOLOGICA');
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Metodo para obtener 
    * Todos los familiares
    * @return lista de familiares
    */
    public function getFamily(){
        $this->db->select('ID_PAR_PK,NOMBRE_PAR');
        $query = $this->db->get('PARIENTE_PACIENTE');
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Metodo para obtener 
    * Todas las profesiones
    * @return lista de profesiones
    */
    public function getProfesiones(){
        $this->db->select('ID_PROF_PK,NOM_PROF');
        $this->db->where('ESTAT_PROF','1');
        $query = $this->db->get('PROFESIONES');
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }
    
    /**
    * Metodo para obtener
    * Todos los dientes
    * @return lista de dientes
    */
    public function getDientes(){
        $this->db->select('ID_DIENTE_PK,NUM_DIENTE');
        $query = $this->db->get('DIENTE_NUM');
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Metodo para obtener
    * los padecimientos dentales
    * @return lista de padecimientos dentales
    */
    public function getPadecimientos(){
        $this->db->select('ID_PAD_PK,DESC_PAD');
        $query = $this->db->get('PADECIMIENTO_DENTAL');
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

    /**
    * Metodo par aobtener
    * las adicciones
    * @return lista de adicciones
    */
    public function getAdicciones(){
        $this->db->select('ID_ADIC_PK,NOM_ADIC');
        $this->db->where('ESTAT_ADIC','1');
        $query = $this->db->get('ADICCION');
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }


    public function getProce(){
        $this->db->select('ID_PROC_PK,DSC_PROC');
        $this->db->where('ESTAT_PROC','1');
        $query = $this->db->get('PROCEDIMIENTO');
        return ($query->num_rows() <= 0) ? NULL : $query->result();
    }

}