<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Historia clinica</title>
    <!-- Bootstrap Core Css -->
</head>
<body>
    <!-- ./Seccion de informacion personal -->
    <div class="row">
        <div class="col-md-6">
            <a href="<?=base_url()?>index.php/Documentos"> Regresar A editar</a>
            <br>
            <h2 class="text-center">
                Historia Clinica
            </h2>
            <div class="col-xs-3 text-left ">
                <p class="font-10"><strong>Nombre</strong>: <?php echo " ".$infoPac->NOMBRE_PAC?></p>
            </div>
            <div class="col-xs-5 text-left font-10">
                <p><strong>Apellido Paterno </strong> : <?php echo " ejemplo".$infoPac->APP_PAC?></p>
            </div>
            <div class="col-xs-4 text-left font-10">
                <p><strong>Apellido Materno</strong> : <?php echo " ".$infoPac->APM_PAC?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="col-xs-4 text-left font-10">
                <p><strong>Fecha de nacimiento </strong>:<?php echo " ".$infoPac->FECNAC_PAC?></p>
            </div>
            <div class="col-xs-4 text-left font-10">
                <p><strong>Estado </strong> : <?php echo " ".$infoPac->NOMBRE_EST?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="col-xs-7 text-left font-10">
                <p><strong>Direccion</strong>: <?php echo " ".$infoPac->DIRECCION_PAC?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="col-xs-3 text-left font-10">
                <p><strong>Genero </strong> :
                <?php 
                    if($infoPac->GENERO_PAC == 'F'){
                            echo 'Femenino';
                        }else{
                            echo 'Masculino';
                    }
                ?>
                </p>
            </div>
            <div class="col-xs-4 text-left font-10">
                <p><strong>Profesion</strong> : <?php echo " ".$infoPac->NOM_PROF?></p>
            </div>
            <div class="col-xs-5 text-left font-10">
                <p>
                    <strong>Estado Civil</strong> : 
                <?php

                    switch ($infoPac->EST_CIVIL_PAC) {
                        case '1':
                            echo "Soltero";
                            break;
                        case '2':
                            echo "Viudo";
                            break;
                        case '3':
                            echo "Casado";
                            break;
                        case '4':
                            echo "Divorsiado";
                            break;
                    }
                    
                ?>
                </p>
            </div>
        </div>
    </div>
    <!-- ./Seccion de informacion personal -->
    <hr>
    <!-- ./Seccion de antecedentes familiares-->
    <div class="row">
        <div class="col-md-6">
            <h2 class="text-center">Antecedentes Heredofamiliares</h2>
            <br>
            <?php
            foreach ($tfamily as $tabla) {
                echo $tabla;   
            }
            ?>
        </div>
    </div>
    <!-- ./Seccion de antecedentes familiares-->
    <hr>
    <!-- ./Seccion de antecedentes Personales-->
    <div class="row">
        <div class="col-md-6">
            <h2  class="text-center">Antecedentes Personales</h2>
            <br>
            <?php
            foreach ($tpersonal as $per) {
                echo $per;   
            }
            ?>
        </div>    
    </div>
    <!-- ./Seccion de antecedentes Personales-->
    <hr>
    <!-- ./Seccion de habitos de higiene-->
    <div class="row">
        <div class="col-md-6">
            <h2 class="text-center"> Habitos de Higiene Bucal </h2>
            <br>
            <div class="col-xs-5 text-left font-10">
                <p>
                    <strong>¿Numero de comidas al dia?</strong> : 
                    <?php echo " ".$infoHabit->NUM_COMI?>
                </p>
            </div>
            <div class="col-xs-5 text-left font-10">
                <p>
                    <strong>¿Higiene dental?</strong> : 
                    <?php
                        if($infoHabit->HIG_DENTAL == '1'){
                            echo "Buena";
                        }else if($infoHabit->HIG_DENTAL == '2'){
                            echo "Regular";
                        }else if($infoHabit->HIG_DENTAL == '3'){
                            echo "Mala";
                        }
                    ?>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="col-xs-5 text-left font-10">
                <p>
                    <strong>¿Usa hilo dental?</strong> : 
                    <?php 
                        if($infoHabit->HIL_DENTAL == '1'){
                            echo "SI";
                        }else{
                            echo "NO";
                        }
                    ?>
                </p>
            </div>
            <div class="col-xs-5 text-left font-10">
                <p>
                    <strong>¿Usa enjuague dental?</strong> : 
                    <?php 
                        if($infoHabit->ENJ_DENTAL == '1'){
                            echo "SI";
                        }else{
                            echo "NO";
                        }
                    ?>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="col-xs-5 text-left font-10">
                <p>
                    <strong>¿Toxicomanias?</strong> : 
                    <?php 
                        if($infoHabit->ADICCION == '1'){
                            echo "SI";
                        }else{
                            echo "NO";
                        }
                    ?>
                </p>
            </div>
            <div class="col-xs-5 text-left font-10">
                <p>
                    <strong>¿Numero de cepilladas al dia?</strong> : 
                    <?php echo " ".$infoHabit->NUM_SEP?>
                </p>
            </div>
        </div>
    </div>
    <!-- ./Seccion de habitos de higiene-->

    <!-- Jquery Core Js -->
    <script src="<?=base_url()?>static/js/core/jquery.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="<?=base_url()?>static/bootstrap/js/bootstrap.js"></script>
</body>
</html>