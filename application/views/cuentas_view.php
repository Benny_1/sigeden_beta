<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <section class="content">
        
        <div class="container-fluid">
            
            <div class="block-header">
                <h2>Cuentas</h2>
            </div>
            
            <div class="row clearfix">
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="card">
                        <div class="header">
                            <h2>
                                Cuentas de los pacientes
                            </h2>
                        </div>
                        
                        <div class="body">

                            <div class="row">
                                <div class="col-md-3">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#formCuenta">
                                        <i class="material-icons">add_box</i><span>Agregar Cuenta</span>
                                    </button>
                                </div> 
                                <div class="col-xs-12 col-md-12">
                                    <div class="row">
                                        <div class="col-xs-8 col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">filter_list</i>
                                                </span>
                                                <div class="form-line">
                                                    <input id="name_filter" type="text" class="form-control" placeholder="Nombre pacientes">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 col-lg-2">
                                            <button type="button" id="btn_filto" class="btn btn-info btn-circle-lg waves-effect waves-circle waves-float">
                                                <i class="material-icons">search</i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table id="tabla_cuentas" class="table tabla-paginada">
                                            <thead>
                                                <tr>
                                                    <th>CUENTA</th>
                                                    <th>PACIENTE</th>
                                                    <th>DEUDA</th>
                                                    <th>ABONADO</th>
                                                    <th>TOTAL</th>
                                                    <th>ACCIONES</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    if(!is_null($resultado['lista'])) :
                                                        foreach ($resultado['lista'] as $cu) { ?>
                                                            <tr id="tr-<?php echo  $cu->CUENTA?>">
                                                                <td><?php echo $cu->CUENTA;?></td>
                                                                <td><?php echo $cu->PAC_NAME;?></td>
                                                                <td><?php echo $cu->TOTAL_CUENTA;?></td>
                                                                <td><?php echo $cu->ABONADO;?></td>
                                                                <td><?php echo $cu->RESTANTE;?></td>
                                                                <td>
                                                                    <button type="button" class="btn bg-indigo waves-effect view_detail" data-id-acount="<?php echo $cu->CUENTA?>" data-toggle="tooltip" data-placement="top" title="Ver estado de cuenta">
                                                                        <i class="material-icons">calculate</i> 
                                                                    </button>
                                                                    <button type="button" class="btn bg-teal waves-effect add_mount" data-id-acount="<?php echo $cu->CUENTA?>" data-toggle="tooltip" data-placement="top" title="Hacer un abono"> 
                                                                        <i class="material-icons">request_quote</i> 
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                    <?php
                                                        }
                                                    endif;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>


        </div>
    </section>


            <div class="modal fade" id="formCuenta" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Agregar Cuenta</h4>
                        </div>
                        <div class="modal-body">
                            <h4> Campos con <b class="color-rojo">*</b> son obligatorios</h4>
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-xs-9 col-md-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">person</i><b class="color-rojo">*</b>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="searchPacName" name="searchPacName" class="form-control" placeholder="Nombre a buscar" focused>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button id="btn_search_pac" class="btn btn-info btn-circle-lg waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Buscar Paciente">
                                                <i class="material-icons">search</i>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-10 col-md-10">
                                            <br>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">person</i><b class="color-rojo">*</b>
                                                </span>
                                                <select id="pac_search_select" class="form-control"disabled>
                                                    <option value="0"> - Seleciona un paciente - </option>
                                                </select>                                    
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row demo-masked-input">
                                        <div class="col-xs-10 col-md-10">
                                            <b>Monto del adeudo</b>
                                            <b class="color-rojo">*</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">attach_money</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="montoDeuda" name="montoDeuda" class="form-control money-dollar">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_save_new_cuenta" type="button" class="btn btn-primary waves-effect">Guardar</button>
                            <button id="dont_save_cuenta" type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="detail_modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Detalle cuenta</h4>
                        </div>
                        <div class="modal-body">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-11">
                                    <input id="cuntaId" type="text" name="cuntaId" style="display: none;" />
                                    <div class="table-responsive">

                                        <table id="tablaEstadoCuenta" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Fecha</th>
                                                    <th>Deuda</th>
                                                    <th>Abono</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- aqui se agrega la info de las cuentas-->
                                            </tbody>
                                        </table>
                                        <label for="txtTotalDeuda">Total deuda</label>
                                        <input id="txtTotalDeuda" disabled />
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <button id="btnEditDeuda" type="button" class="btn btn-warning btn-circle-lg waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Editar deuda">
                                        <i class="material-icons">border_color</i>
                                    </button>
                                
                                    <button id="btnAddAbono" type="button" class="btn btn-success btn-circle-lg waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Agregar un abono">
                                        <i class="material-icons">library_add</i>
                                    </button>
                                
                                    <button id="btnReCalculate" type="button" class="btn bg-cyan btn-circle-lg waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Re-Calcular deuda" disabled>
                                        <i class="material-icons">account_balance</i>
                                    </button>
                                    <button id="btnSaveCont" type="button" class="btn btn-info btn-circle-lg waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Guardar cambios" disabled>
                                        <i class="material-icons">save</i>
                                    </button>
                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            
                            <button id="close_modal_cuentas" type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="abono_acount" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Hacer abono a cuenta</h4>
                        </div>
                        <div class="modal-body">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">

                                    <div class="row">
                                        <input type="text" id="idAcountAbono" style='display: none;'>
                                        <div class="col-xs-9 col-md-10">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">attach_money</i><b class="color-rojo">*</b>
                                                </span>
                                                <div class="form-line demo-masked-input">
                                                    <input type="text" id="mountAbono" name="mountAbono" class="form-control money-dollar" focused>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button id="btn_add_abono" class="btn btn-info btn-circle-lg waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="top" title="Agregar abono">
                                                <i class="material-icons">library_add_check</i>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            
                            <button id="close_modal_abono" type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>