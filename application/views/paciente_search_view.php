<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                LISTA DE PACIENTES
                            </h2>
                        </div>
                        <div class="body">
                            
                                <div class="row">
                                    <form id="form_filtro" action="javascript:void(0);">
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">filter_list</i>
                                            </span>
                                            <div class="form-line">
                                                <input id="name_filter" type="text" class="form-control" value="<?php if($formPacFull){ echo $resultado['lista'][0]->NOMBRE; } ?>" placeholder="Nombre">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">filter_list</i>
                                            </span>
                                            <div class="form-line">
                                                <input id="app_filter" type="text" class="form-control" placeholder="Apellido Paterno">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">filter_list</i>
                                            </span>
                                            <div class="form-line">
                                                <input id="apm_filter" type="text" class="form-control" placeholder="Apellido Materno">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">
                                            <i class="material-icons">search</i>
                                        </button>
                                    </div>
                                    </form>
                                </div> 
                            
                            <div class="table-responsive">


                                <table id="table_paciente" class="table table-condensed table-bordered tabla-paginada font-12">
                                    <thead>
                                        <tr>
                                            <th>NOMBRE</th>
                                            <th>EDAD</th>
                                            <th>OBSERVACION</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $impAlert = "";
                                            if(!is_null($resultado['lista'])) :
                                                foreach ($resultado['lista'] as $paciente) { ?>
                                                    <tr id="tr-<?php echo  $paciente->ID_PAC_PK;?>" <?php $impAlert = ($paciente->ALERTA == 'SI') ? "class='warning'":  "class=''"; echo $impAlert;?>>
                                                        <td><?php echo $paciente->NOMBRE;?></td>
                                                        <td><?php echo $paciente->EDAD;?></td>
                                                        <td><?php echo $paciente->OBSER_PAC;?></td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger waves-effect delete-paciente" data-id="<?php echo $paciente->ID_PAC_PK?>" data-toggle="tooltip" data-placement="top" title="Eliminar Paciente">
                                                                <i class="material-icons">delete_sweep</i> 
                                                            </button>
                                                            <button type="button" class="btn btn-warning waves-effect edit-paciente" data-id="<?php echo $paciente->ID_PAC_PK?>" data-toggle="tooltip" data-placement="top" title="Editar Paciente"> 
                                                                 <i class="material-icons">create</i> 
                                                            </button>
                                                            <button type="button" class="btn btn-info waves-effect cita-paciente" data-id="<?php echo $paciente->ID_PAC_PK?>" data-toggle="tooltip" data-placement="top" title="Agendar Cita"> 
                                                                 <i class="material-icons">event</i> 
                                                            </button>
                                                        </td>
                                                    </tr>
                                        <?php
                                                }
                                                
                                            endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            
           </div>
        
    </section>

    <!-- Default Size -->
    <button id="modalPaciente" type="button" class="btn btn-default waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal" style="display:none;">MODAL - DEFAULT SIZE</button>
    
    <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Informacion del Paciente</h4>
                </div>
                <div class="modal-body">
                    
                    <div class="row clearfix">
                        <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                            <div class="panel-group" id="accordion_17" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-col-black">
                                    <div class="panel-heading" role="tab" id="headingOne_17">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseOne_17" aria-expanded="true" aria-controls="collapseOne_17">
                                                <i class="material-icons">perm_contact_calendar</i>
                                                Información Personal.
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_17">
                                        <div class="panel-body">
                                            <div class="demo-masked-input">
                                                <div class="row clearfix">
                                                    <div class="col-md-12">
                                                        <input type="text" id="editPacienteId" name="editPacienteId" style="display: none;">
                                                        
                                                        <div class="col-md-3">
                                                            <b>Nombre</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">person</i>
                                                                </span>
                                                                <div class="form-line">
                                                                    <input type="text" id="nombre_p" name="nombre_p" class="form-control" placeholder="Username" focused>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    <div class="col-md-3">
                                                        <b>Apellido Paterno</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">person</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="paterno_p" name="paterno_p" class="form-control" placeholder="Apellido Paterno">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Apellido Materno</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">person</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="materno_p" name="materno_p" class="form-control" placeholder="Apellido Materno">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Fecha de Nancimiento</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="fecha_nac" name="fecha_nac" class="form-control date" placeholder="Ex: 30-07-1990">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    
                                                    <div class="col-md-3">
                                                        <b>Genero</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">wc</i>
                                                            </span>
                                                            <input name="grupo_genero" type="radio" id="hombre" class="with-gap radio-col-blue"/>
                                                            <label for="hombre">Hombre</label>
                                                            <input name="grupo_genero" type="radio" id="mujer" class="with-gap radio-col-pink" />
                                                            <label for="mujer">Mujer</label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Lugar de Nacimiento</b>
                                                        <select id="lugar_nac" class="form-control show-tick">
                                                                    <option value="0">-Estado-</option>
                                                                    <?php
                                                                        if(!is_null($estados)) :
                                                                            foreach ($estados as $est) { ?>
                                                                            <option value="<?php echo $est->ID_EST_PK?>"><?php echo $est->NOMBRE_EST?></option>
                                                                        <?php 
                                                                            }
                                                                        endif;
                                                                    ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Dirección </b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">domain</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="direccion" name="direccion" class="form-control" placeholder="Dirección">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Ocupación</b>
                                                        <select id="ocupacion" name="ocupacion" class="form-control show-tick">
                                                            <option value="0">-Profesión-</option>
                                                            <?php
                                                                if(!is_null($profesiones)) :
                                                                    foreach ($profesiones as $prof) { ?>
                                                                    <option value="<?php echo $prof->ID_PROF_PK?>"><?php echo $prof->NOM_PROF?></option>
                                                                <?php 
                                                                    }
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="col-md-3">
                                                        <b>Estado Civil</b>
                                                        <select id="est_civil" name="est_civil" class="form-control show-tick">
                                                            <option value="0">-Estado Civil-</option>
                                                            <option value="1">Soltero</option>
                                                            <option value="2">Viudo</option>
                                                            <option value="3">Casado</option>
                                                            <option value="4">Divorsiado</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Numero de Telefono</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">phone_iphone</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="num_tel" nombre="num_tel" class="form-control mobile-phone-number" placeholder="Ex: +00 (000) 000-00-00">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Correo Electronico</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">email</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="correo" name="correo" class="form-control email" placeholder="Ex: example@example.com">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-3">
                                                        <b>Observacion</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">location_searching</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="obs_pac" name="obs_pac" class="form-control" maxlength="100" placeholder="Ex: Hijo de doña Marta">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <button type="button" id="editPatientInfoPer" class="btn btn-success waves-effect">
                                                        <i class="material-icons">send</i>
                                                        <span>Guardar Cambios</span>
                                                    </button>
                                                </div>
                                        </div>
                                    </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-col-black">
                                    <div class="panel-heading" role="tab" id="headingTwo_17">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseTwo_17" aria-expanded="false" aria-controls="collapseTwo_17">
                                                <i class="material-icons">supervisor_account</i>Antecedentes Familiares
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_17">
                                        <div class="panel-body">
                                            <!-- -->
                                            <div class="col-md-12">
                                                <select id="select_familiar" class="form-control">
                                                    <option value="0">-Familiar-</option>
                                                        <?php
                                                            if(!is_null($familiares)) :
                                                            foreach ($familiares as $fm) { ?>
                                                                <option value="<?php echo $fm->ID_PAR_PK?>"><?php echo $fm->NOMBRE_PAR?></option>
                                                        <?php 
                                                            }
                                                        endif;
                                                    ?>
                                                </select>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="colmd-12">
                                                <select id="optgroup_familiar" class="ms" multiple="multiple">
                                                    <?php
                                                        if(!is_null($enfermedades)) :
                                                            foreach ($enfermedades as $en) { ?>
                                                                <option value="<?php echo $en->ID_ENFER_PK?>"><?php echo $en->NOMBRE_ENFER?></option>
                                                        <?php 
                                                            }
                                                        endif;
                                                    ?>
                                                </select>
                                            </div>
                                            <br>
                                            <div class="col-md-3">
                                                <button type="button" id="editPatientFamiliar" class="btn btn-success waves-effect">
                                                    <i class="material-icons">send</i>
                                                    <span>Guardar Cambios</span>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-col-black">
                                    <div class="panel-heading" role="tab" id="headingThree_17">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseThree_17" aria-expanded="false" aria-controls="collapseThree_17">
                                                <i class="material-icons">library_books</i> Antecedentes Patologicos
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree_17">
                                        <div class="panel-body">
                                        <!-- -->
                                            <h4>Antecedentes patologicos personales</h4>
                                            <div class="row clearfix">
                                                
                                                <div class="col-md-12">
                                                    <select id="optgroup_personal" class="ms" multiple="multiple">
                                                        <?php
                                                            if(!is_null($enfermedades)) :
                                                                    foreach ($enfermedades as $en) { ?>
                                                                        <option value="<?php echo $en->ID_ENFER_PK?>"><?php echo $en->NOMBRE_ENFER?></option>
                                                            <?php 
                                                                    }
                                                                endif;
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="col-md-3">
                                                <button type="button" id="editPatientEnfermedad" class="btn btn-success waves-effect">
                                                    <i class="material-icons">send</i>
                                                    <span>Guardar Cambios</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-col-black">
                                    <div class="panel-heading" role="tab" id="headingFour_17">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseFour_17" aria-expanded="false" aria-controls="collapseFour_17">
                                                <i class="material-icons">verified_user</i> Antecedentes no patologicos
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_17">
                                        <div class="panel-body">
                                            <div class="col-md-12">
                                                <h4>Habitos</h4>
                                                <div class="row clearfix">

                                                    <div class="col-md-12"> 
                                                        <div class="col-md-4">
                                                            <b>¿Numero de comidas al día?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">restaurant</i>
                                                                </span>
                                                                <input name="grupo_comidas" type="radio" id="1_c" class="with-gap radio-col-red"/>
                                                                <label for="1_c">2</label>
                                                                <input name="grupo_comidas" type="radio" id="2_c" class="with-gap radio-col-orange" />
                                                                <label for="2_c">3</label>
                                                                <input name="grupo_comidas" type="radio" id="3_c" class="with-gap radio-col-green" />
                                                                <label for="3_c">4</label>
                                                                <input name="grupo_comidas" type="radio" id="4_c" class="with-gap radio-col-blue" />
                                                                <label for="4_c">5</label>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-4">
                                                            <b>¿Higiene dental?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">brush</i>
                                                                </span>
                                                                <input name="grupo_higiene" type="radio" id="1_h" class="with-gap radio-col-blue"/>
                                                                <label for="1_h">Buena</label>
                                                                <input name="grupo_higiene" type="radio" id="2_h" class="with-gap radio-col-orange" />
                                                                <label for="2_h">Regular</label>
                                                                <input name="grupo_higiene" type="radio" id="3_h" class="with-gap radio-col-red" />
                                                                <label for="3_h">Mala</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <b>¿Hilo dental?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">shopping_basket</i>
                                                                </span>
                                                                <input type="checkbox" id="checkbox_hilo" class="chk-col-blue"/>
                                                                <label for="checkbox_hilo"></label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <b>¿Enjuague bocal?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">local_drink</i>
                                                                </span>
                                                                <input type="checkbox" id="checkbox_enjuage" class="chk-col-blue"/>
                                                                <label for="checkbox_enjuage"></label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <b>¿Ingesta de alimentos duros?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">fastfood</i>
                                                                </span>
                                                                <input type="checkbox" id="checkbox_ali_duros" class="chk-col-red"/>
                                                                <label for="checkbox_ali_duros"></label>
                                                            </div>
                                                        </div>
                                                                                                     
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="col-md-7">
                                                            <b>¿Ingesta de alimentos o bebidas a temperaturas elevadas?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">mood</i>
                                                                </span>
                                                                <input name="grupo_temp_ele" type="radio" id="1_elevada" class="with-gap radio-col-red"/>
                                                                <label for="1_elevada">Muy calientes</label>
                                                                <input name="grupo_temp_ele" type="radio" id="2_elevada" class="with-gap radio-col-blue" />
                                                                <label for="2_elevada">Muy frias</label>
                                                                <input name="grupo_temp_ele" type="radio" id="3_elevada" class="with-gap radio-col-orange" />
                                                                <label for="3_elevada">Ambas</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <b>¿Cuantas veces al dia se cepilla?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">mood</i>
                                                                </span>
                                                                <input name="grupo_cepillada" type="radio" id="1_cpd" class="with-gap radio-col-red"/>
                                                                <label for="1_cpd">1</label>
                                                                <input name="grupo_cepillada" type="radio" id="2_cpd" class="with-gap radio-col-orange" />
                                                                <label for="2_cpd">2</label>
                                                                <input name="grupo_cepillada" type="radio" id="3_cpd" class="with-gap radio-col-green" />
                                                                <label for="3_cpd">3</label>
                                                                <input name="grupo_cepillada" type="radio" id="4_cpd" class="with-gap radio-col-blue" />
                                                                <label for="4_cpd">4</label>
                                                            </div>
                                                        </div> 
                                                    </div>

                                                    <h4>Toxicomanias</h4>
                                                        <div class="col-md-12">    
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">smoking_rooms</i>
                                                                </span>
                                                                <input type="checkbox" id="checkbox_adiccion" class="chk-col-red"/>
                                                                <label for="checkbox_adiccion"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12" id="optionToxi" style="display: none;">
                                                            <select id="optgroup_adiccion" class="ms" multiple="multiple">
                                                            <?php
                                                                if(!is_null($adicciones)) :
                                                                    foreach ($adicciones as $adic) { ?>
                                                                        <option value="<?php echo $adic->ID_ADIC_PK?>"><?php echo $adic->NOM_ADIC?></option>
                                                            <?php 
                                                                    }
                                                                endif;
                                                            ?>
                                                            </select>
                                                        </div>
                                                    <h4>Otras observaciones</h4>
                                                    <div class="col-md-12">
                                                        <div class="col-md-3">
                                                            <b>¿Inmunizaciones?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">bubble_chart</i>
                                                                </span>
                                                                <input type="checkbox" id="checkbox_inmunizacion" class="chk-col-red"/>
                                                                <label for="checkbox_inmunizacion"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <b>¿Metodo Anticonceptivo?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">child_care</i>
                                                                </span>
                                                                <input type="checkbox" id="checkbox_metod_anti" class="chk-col-black"/>
                                                                <label for="checkbox_metod_anti"></label>
                                                            </div>
                                                        </div>
                                                        <!-- En caso de que sea mujer mostrar esta opcion-->
                                                        <div class="col-md-6" id="isWoman">
                                                            <b>¿Embarazo trimestral?</b>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">
                                                                    <i class="material-icons">pregnant_woman</i>
                                                                </span>
                                                                <input name="grupo_embarazo" type="radio" id="1_t" class="with-gap radio-col-blue"/>
                                                                <label for="1_t">Trimestre 1</label>
                                                                <input name="grupo_embarazo" type="radio" id="2_t" class="with-gap radio-col-orange" />
                                                                <label for="2_t">Trimestre 2</label>
                                                                <input name="grupo_embarazo" type="radio" id="3_t" class="with-gap radio-col-red" />
                                                                <label for="3_t">Trimestre 3</label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <button type="button" name="editPatientHabitos" id="editPatientHabitos" class="btn btn-success waves-effect">
                                                            <i class="material-icons">send</i>
                                                            <span>Guadar Cambios</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-col-black">
                                    <div class="panel-heading" role="tab" id="headingFive_17">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseFive_17" aria-expanded="false" aria-controls="collapseThree_17">
                                                <i class="material-icons">folder_shared</i>Padecimientos Dentales
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive_17">
                                        <div class="panel-body">
                                        <!-- -->
                                            <h4 class="text-center">Relacion Padecimiento Dental.</h4>
                                            <div class="row clearfix">
                                                
                                                <div class="col-md-12">
                                                    <select id="numDentEdit" class="form-control" multiple data-selected-text-format="count" data-actions-box="true" data-size="4" title="Seleccione un diente">
                                                        <option value="0">-Seleccione un diente-</option>
                                                        <?php
                                                            if(!is_null($dientes)) :
                                                                foreach ($dientes as $dent) { ?>
                                                                    <option value="<?php echo $dent->ID_DIENTE_PK?>"><?php echo $dent->NUM_DIENTE?></option>
                                                        <?php 
                                                                }
                                                            endif;
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="col-md-12">
                                                    <select id="optgroup_pad_edit" class="selectpicker" data-width="100%" multiple data-actions-box="true" data-size="4" data-selected-text-format="count"  title="Seleccione un padecimiento">
                                                        <?php
                                                            if(!is_null($padecimientos)) :
                                                                foreach ($padecimientos as $pad) { ?>
                                                                    <option value="<?php echo $pad->ID_PAD_PK?>"><?php echo $pad->DESC_PAD?></option>
                                                        <?php 
                                                                }
                                                            endif;
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="col-md-3">
                                                <button type="button" id="editPadDental" class="btn btn-success waves-effect">
                                                    <i class="material-icons">send</i>
                                                    <span>Guardar Cambios</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="panel panel-col-black">
                                    <div class="panel-heading" role="tab" id="headingSix_17">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseSix_17" aria-expanded="false" aria-controls="collapseThree_17">
                                                <i class="material-icons">trending_up</i> Estatus Dientes
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSix_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix_17">
                                        <div class="panel-body">

                                            <h4 class="text-center">Padecimientos dentales del paciente.</h4>
                                            <div class="row clearfix">
                                                
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table id="table_padecimiento" class="table table-condensed table-bordered font-12">
                                                            <thead>
                                                                <tr>
                                                                    <th>DIENTE</th>
                                                                    <th>PADECIMIENTO</th>
                                                                    <th>ATENDIDO</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                            <br>

                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-col-black">
                                    <div class="panel-heading" role="tab" id="headingSix_17">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_17" href="#collapseSeven_17" aria-expanded="false" aria-controls="collapseThree_17">
                                                <i class="material-icons">date_range</i> Notas de evolucion
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseSeven_17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix_17">
                                        <div class="panel-body">
<!--  ************************************************************************************************************************************************************************* -->
                                            <h4 class="text-center">Historico de citas.</h4>
                                            <div class="row clearfix">


                                            <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                                <div id="acordionNotas" class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                                                    
                                                </div>
                                            </div>

<!--  ************************************************************************************************************************************************************************* -->
                                            </div>
                                            <br>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="cerrar_modal_info" type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="material-icons">highlight_off</i>Cerrar Formulario</button>
                </div>
            </div>
        </div>
    </div>

    <!--Agendar CITA Paciente-->
    <div class="modal fade" id="modalAgendaCitaPac" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Agendar Cita</h4>
                        </div>
                        <div class="modal-body">
                            <input type="text" id="idPac" name="idPac" style="display: none;">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <b>Cita</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">event</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="fechCita" name="fechCita" class="datepicker form-control" placeholder="Fecha Cita" focused>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <b>Hora</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">event</i>
                                            </span>
                                            <div class="form-line">
                                                <input id="horaCita" name="horaCita" type="text" class="timepicker form-control" placeholder="Please choose a time...">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_agenda_cita" type="button" class="btn btn-primary waves-effect">Agendar Cita</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

    <!--Fin Agenda CITA Paciente-->
<script type="text/javascript">
    var FORMULARIO_INCOMPLETO_PAC = <?php print($formPacFull);?>;
    if(FORMULARIO_INCOMPLETO_PAC === 1){
        var ID_PAC_INCOM = <?php print($resultado['lista'][0]->ID_PAC_PK);?>;
    }
</script>
