<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Seguimiento</h2>
            </div>

            <div class="row clearfix">    
                <div class="col-md-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Horario
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-md-8">
                                    <span class="badge bg-orange">Se Re-agendo cita</span>
                                    <span class="badge bg-green">Se Suspendio cita</span>
                                    <span class="badge bg-deep-orange">Se Cancelo Cita</span>
                                    <span class="badge bg-red">No Asistió a Cita</span>
                                </div>
                                <div class="col-md-3">
                                    <button id="btnAddCita" class="btn btn-primary">+ Agendar cita</button>
                                </div>
                            </div>
                            <div class="row">
                                <div id='calendar'></div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </section>

        <!--admin citas Paciente-->
    <div class="modal fade" id="modalAtiendeCita" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Atiende Cita</h4>
                        </div>
                        <div class="modal-body">
                            <input type="text" id="idPac" name="idPac" style="display: none;">
                            <!-- Body modal-->
                            <div class="col-md-12 button-demo">
                                <!-- Atender Cita -->
                                <button id="btn_aten_cita" type="button" class="btn btn-info">Atender</button>
                                <!-- Re-agenda -->
                                <button id="btn_reAg_cita" type="button" class="btn btn-info waves-effect">Re agendar</button>
                                <!-- Suspender -->
                                <button id="btn_sus_cita" type="button" class="btn btn-info waves-effect">Suspender</button>
                                <!-- Cancelar -->
                                <button id="btn_cancel_cita" type="button" class="btn btn-info waves-effect">Cancelar</button>
                                <!-- No asistio -->
                                <button id="btn_no_asis_cita" type="button" class="btn btn-info waves-effect">No asistio</button>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <!-- Cerrar modal -->
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

    <!--Fin admin citas Paciente-->


    <!--ReAgendar CITA Paciente-->
    <div class="modal fade" id="modalReAgendaCita" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Agendar Cita</h4>
                        </div>
                        <div class="modal-body">
                            <input type="text" id="idPac" name="idPac" style="display: none;">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <b>Cita</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">event</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="fechCita" name="fechCita" class="datepicker form-control" placeholder="Fecha Cita" focused>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <b>Hora</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">event</i>
                                            </span>
                                            <div class="form-line">
                                                <input id="horaCita" name="horaCita" type="text" class="timepicker form-control" placeholder="Please choose a time...">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_re_agenda_cita" type="button" class="btn btn-primary waves-effect">Re agendar Cita</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>


    <!--Agendar CITA Paciente-->
    <div class="modal fade" id="modalAddCita" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Selecionar Persona</h4>
                        </div>
                        <div class="modal-body">
                            <!-- Body modal-->
                            <div class="col-md-12 button-demo">
                                <button id="newPaciente" class="btn btn-primary">Nuevo paciente</button>
                                <button id="searchPaciente" class="btn btn-primary">Paciente ya registrado</button>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <!-- Cerrar modal -->
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

    <!--Fin Agenda CITA Paciente-->


    <!--formulario basica nuevo paciente Paciente-->
    <div class="modal fade" id="modalNewPacForm" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Informacion Basica</h4>
                        </div>
                        <div class="modal-body">
                            <h5>Campos con <b class="color-rojo">*</b> son obligatorios</h5>
                            <!-- Body modal-->
                            <div class="col-md-12 button-demo">
                                
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="nameNewPac" name="nameNewPac" class="form-control" placeholder="Nombre" focused>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="appNewPac" name="appNewPac" class="form-control" placeholder="Apellido Paterno" >
                                    </div>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="apmNewPac" name="apmNewPac" class="form-control" placeholder="Apellido Materno" >
                                    </div>
                                </div>

                                <div class="input-group demo-masked-input">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone_iphone</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="telNewPac" name="telNewPac" class="mobile-phone-number form-control" placeholder="Contacto" >
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">event</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="fechNewPac" name="fechNewPac" class="datepicker form-control" placeholder="Fecha cita">
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">event</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="horaNewPac" name="horaNewPac" class="timepicker form-control" placeholder="Hora de la cita">
                                    </div>
                                </div>
                                <button id="saveAddCitaNewPac" class="btn btn-primary">Agendar cita</button>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <!-- Cerrar modal -->
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

    <!--Fin formulario basica nuevo paciente Paciente-->

    <!--formulario busqueda paciente Paciente-->
    <div class="modal fade" id="modalSearchPacienteForm" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Busqueda de paciente</h4>
                        </div>
                        <div class="modal-body">
                            <!-- Body modal-->
                            <div class="col-md-12 button-demo">

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="nameSearchPac" name="nameSearchPac" class="form-control" placeholder="Nombre a busacar" focused>
                                    </div>
                                </div>
                                <button id="searchPacName" class="btn btn-primary">Buscar paciente</button>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    
                                        <select id="pac_search_select" class="form-control"disabled>
                                            <option value="0"> - Seleciona un paciente - </option>
                                        </select>
                                    
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">event</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="fechPacExist" name="fechPacExist" class="datepicker form-control" placeholder="Fecha cita">
                                    </div>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">event</i>
                                        <b class="color-rojo">*</b>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" id="horaPacExis" name="horaPacExis" class="timepicker form-control" placeholder="Hora de la cita">
                                    </div>
                                </div>

                                <button id="searchAgendaCita" class="btn btn-primary">Agenda cita</button>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <!-- Cerrar modal -->
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

    <!--Fin formulario busqueda paciente Paciente-->