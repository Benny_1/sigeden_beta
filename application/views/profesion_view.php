<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <section class="content">
        
        <div class="container-fluid">
            
            <div class="block-header">
                <h2>PREFSIONES</h2>
            </div>
            
            <div class="row clearfix">
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="card">
                        <div class="header">
                            <h2>
                                Profesiones
                            </h2>
                        </div>
                        
                        <div class="body">
                            <div class="row">
                                <div class="col-md-3">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#formProfesion">
                                        <i class="material-icons">add_box</i> Agregar Profesion 
                                    </button>
                                </div> 
                                <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">filter_list</i>
                                                    </span>
                                                    <div class="form-line">
                                                        <input id="name_filter" type="text" class="form-control" placeholder="Nombre Profesion">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <button type="button" id="btn_filto" class="btn btn-primary btn-lg m-l-15 waves-effect">
                                                    <i class="material-icons">search</i>
                                                </button>
                                            </div>
                                        </div>                  
                                    <div class="table-responsive">
                                        <table id="tabla_profesion" class="table tabla-paginada">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>NOMBRE DE LA PROFESION</th>
                                                    <th>ACCION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                    if(!is_null($resultado['lista'])) :
                                                        foreach ($resultado['lista'] as $prof) { ?>
                                                            <tr id="tr-<?php echo  $prof->ID_PROF_PK?>">
                                                                <td><?php echo $prof->ID_PROF_PK;?></td>
                                                                <td><?php echo $prof->NOM_PROF;?></td>
                                                                <td>
                                                                    <button type="button" class="btn btn-danger waves-effect delete-prof" data-id="<?php echo $prof->ID_PROF_PK?>" data-toggle="tooltip" data-placement="top" title="Eliminar Profesion">
                                                                        <i class="material-icons">delete_sweep</i> 
                                                                    </button>
                                                                    <button type="button" class="btn btn-warning waves-effect edit-prof" data-id="<?php echo $prof->ID_PROF_PK?>" data-toggle="tooltip" data-placement="top" title="Editar Profesion"> 
                                                                         <i class="material-icons">create</i> 
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                <?php
                                                        }
                                                    endif;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="modal fade" id="formProfesion" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Agregar Profesion</h4>
                        </div>
                        <div class="modal-body">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">

                                    <div class="col-md-10">
                                        <b>Nombre Profesion</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">add_box</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="nombreProfesion" name="nombreProfesion" class="form-control" placeholder="Nombre Profesion" focused>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_save_profesion" type="button" class="btn btn-primary waves-effect">Guardar Cambios</button>
                            <button id="dont_save_profesion" type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>


            
            <div class="modal fade" id="editProfesion" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Actualizar Profesion</h4>
                        </div>
                        <div class="modal-body">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">

                                    <div class="col-md-10">
                                        <b>Nombre Profesion</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="idEditProfesion" name="idEditProfesion" style="display:none">
                                                <input type="text" id="nameEditProf" name="nameEditProf" class="form-control" placeholder="Nombre Profesion" focused>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_edit_profesion" type="button" class="btn btn-primary waves-effect">Guardar Cambios</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>