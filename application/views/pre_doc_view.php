<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Historia clinica</title>
    <!-- Bootstrap Core Css -->
    <link href="<?=base_url()?>static/bootstrap/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
    </style>
</head>
<body>
    <div class="row">
        <div class="col-md-6">
            <a href="<?=base_url()?>index.php/Documentos"> Regresar A editar</a>
            <br>
            <h1 class="text-center">
                Historia Clinica
            </h1>
            <div class="col-md-3">
                <label>Nombre : </label><?php echo " ".$infoPac->NOMBRE_PAC?>
            </div>
            <div class="col-md-5">
                <label>Apellido Paterno : </label><?php echo " ejemplo".$infoPac->APP_PAC?>
            </div>
            <div class="col-md-4">
                <label>Apellido Materno : </label><?php echo " ".$infoPac->APM_PAC?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-5">
                <label>Fecha de nacimiento : </label><?php echo " ".$infoPac->FECNAC_PAC?>
            </div>
            <div class="col-md-5">
                <label>Estado : </label><?php echo " ".$infoPac->NOMBRE_EST?>
            </div>
            <div class="col-md-7">
                <label>Direccion : </label><?php echo " ".$infoPac->DIRECCION_PAC?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-3">
                <label>Genero : </label>
                <?php 
                    if($infoPac->GENERO_PAC == 'F'){
                            echo 'Femenino';
                        }else{
                            echo 'Masculino';
                    }
                ?>
            </div>
            <div class="col-md-4">
                <label>Profesion : </label><?php echo " ".$infoPac->NOM_PROF?>
            </div>
            <div class="col-md-5">
                <label>Estado Civil : </label>
                <?php

                    switch ($infoPac->EST_CIVIL_PAC) {
                        case '1':
                            echo "Soltero";
                            break;
                        case '2':
                            echo "Viudo";
                            break;
                        case '3':
                            echo "Casado";
                            break;
                        case '4':
                            echo "Divorsiado";
                            break;
                    }
                    
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h1 class="text-center">Antecedentes Eredofamiliares</h1>
            <br>
            <?php
            foreach ($tfamily as $tabla) {
                echo $tabla;   
            }
            ?>
        </div>
    </div>
    <br>
    <br>
    <br>
    <label>Antecedentes Personales</label>
    <br>
    <br>
    <table>
        <thead>
            <tr>
                <th>Enfermedad 1</th>
                <th>Enfermedad 2</th>
                <th>Enfermedad 3</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <label>Habitos</label>
    <br>
    <br>
    <label>¿Numero de comidas al dia ?</label>
    <br>
    <label>¿Higiene dental?</label>
    <br>
    <label>¿Usa hilo dental?</label>
    <br>
    <label>¿Usa enjuague dental?</label>
    <br>
    <label>¿Toxicomanias?</label>
    <br>
    <label>¿Numero de cepilladas al dia?</label>

    <!-- Jquery Core Js -->
    <script src="<?=base_url()?>static/js/core/jquery.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="<?=base_url()?>static/bootstrap/js/bootstrap.js"></script>

</body>
</html>