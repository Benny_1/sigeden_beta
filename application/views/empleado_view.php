<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <section class="content">
        
        <div class="container-fluid">
            
            <div class="block-header">
                <h2>Registro de empleados</h2>
            </div>
            
            <div class="row clearfix"> 
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="card">
                        <div class="header">
                            <h2>
                                Empleados
                            </h2>
                        </div>
                        
                        <div class="body">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <b>Nombre</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="nombre_p" name="nombre_p" class="form-control" placeholder="Username" focused>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <b>Apellido Paterno</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="paterno_p" name="paterno_p" class="form-control" placeholder="Apellido Paterno">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <b>Apellido Materno</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="materno_p" name="materno_p" class="form-control" placeholder="Apellido Materno">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Genero</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">wc</i>
                                            </span>
                                            <input name="grupo_genero" type="radio" id="hombre" class="with-gap radio-col-blue"/>
                                            <label for="hombre">Hombre</label>
                                            <input name="grupo_genero" type="radio" id="mujer" class="with-gap radio-col-pink" />
                                            <label for="mujer">Mujer</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="demo-masked-input">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <b>Numero de Telefono</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">phone_iphone</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="num_tel" nombre="num_tel" class="form-control mobile-phone-number" placeholder="Ex: +00 (000) 000-00-00">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <b>Correo Electronico</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">email</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="correo" name="correo" class="form-control email" placeholder="Ex: example@example.com">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <b>Sueldo</b> <b class="color-rojo">*</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">attach_money</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="sueldo" name="sueldo" class="form-control money-dollar" placeholder="Ex: 999,999.99">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <button type="button" id="save_empleado" class="btn btn-success waves-effect" data-toggle="tooltip" data-placement="top" title="Guardar La informacion del Empleado">
                                        <i class="material-icons">send</i>
                                        <span>Guardar</span>
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" id="cancelarRegisEmpleado" class="btn btn-warning waves-effect" data-toggle="tooltip" data-placement="top" title="Limpia todos los campos del formulario">
                                        <i class="material-icons">delete</i>
                                        <span>Limipiar campos</span>
                                    </button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>