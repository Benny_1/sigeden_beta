<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>EMPLEADOS</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                LISTA DE EMPLEADOS
                            </h2>
                        </div>
                        <div class="body">

                            <div class="row">
                                <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_list</i>
                                        </span>
                                        <div class="form-line">
                                            <input id="name_filter" type="text" class="form-control" placeholder="Nombre">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_list</i>
                                        </span>
                                        <div class="form-line">
                                            <input id="app_filter" type="text" class="form-control" placeholder="Apeliido Paterno">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">filter_list</i>
                                        </span>
                                        <div class="form-line">
                                            <input id="apm_filter" type="text" class="form-control" placeholder="Apeliido Materno">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" id="btn_filto" class="btn btn-primary btn-lg m-l-15 waves-effect">
                                        <i class="material-icons">search</i>
                                    </button>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table id="table_employ" class="table tabla-paginada">
                                    <thead>
                                        <tr>
                                            <th>NOMBRE</th>
                                            <th>APELLIDO PATERNO</th>
                                            <th>APELLIDO MATERNO</th>
                                            <th>TIPO EMPLEADO</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(!is_null($info['lista'])) :
                                                foreach ($info['lista'] as $emp) { ?>
                                                    <tr id="tr-<?php echo  $emp->ID_EMP_PK?>">
                                                        <td><?php echo $emp->NOM_EMP;?></td>
                                                        <td><?php echo $emp->APP_EMP;?></td>
                                                        <td><?php echo $emp->APM_EMP;?></td>
                                                        <td><?php echo $emp->GENERO_EMP;?></td>
                                                        <td>
                                                            <button type="button" class="btn btn-danger waves-effect delete-empleado" data-id="<?php echo $emp->ID_EMP_PK?>" data-toggle="tooltip" data-placement="top" title="Eliminar Empleado">
                                                                <i class="material-icons">delete_sweep</i> 
                                                            </button>
                                                            <button type="button" class="btn btn-warning waves-effect edit-empleado" data-id="<?php echo $emp->ID_EMP_PK?>" data-toggle="tooltip" data-placement="top" title="Editar Empleado"> 
                                                                 <i class="material-icons">create</i> 
                                                            </button>
                                                        </td>
                                                    </tr>
                                        <?php
                                                }
                                            endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            
           </div>
        
    </section>

    <div class="modal fade" id="modalEditEmpleado" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Actualizar Empleado</h4>
                </div>
                <div class="modal-body">
                <!-- Formulario modal-->
                            <input type="text" id="idEditEmpl" name="idEditEmpl" style="display: none;">
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <b>Nombre</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="editNombre_p" name="nombre_p" class="form-control" placeholder="Username" focused>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <b>Apellido Paterno</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="editPaterno_p" name="paterno_p" class="form-control" placeholder="Apellido Paterno">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <b>Apellido Materno</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="editMaterno_p" name="materno_p" class="form-control" placeholder="Apellido Materno">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <b>Genero</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">wc</i>
                                            </span>
                                            <input name="grupo_genero" type="radio" id="hombre" class="with-gap radio-col-blue"/>
                                            <label for="hombre">Hombre</label>
                                            <input name="grupo_genero" type="radio" id="mujer" class="with-gap radio-col-pink" />
                                            <label for="mujer">Mujer</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="demo-masked-input">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <b>Numero de Telefono</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">phone_iphone</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="editNum_tel" nombre="num_tel" class="form-control mobile-phone-number" placeholder="Ex: (000) 000-00-00">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <b>Correo Electronico</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">email</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="editCorreo" name="correo" class="form-control email" placeholder="Ex: example@example.com">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <b>Sueldo</b> <b class="color-rojo">*</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">attach_money</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="editSueldo" name="sueldo" class="form-control money-dollar" placeholder="Ex: 999,999.99">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                <!-- Formulario modal-->
                </div>
                <div class="modal-footer">
                    <button id="btn_edit_employ" type="button" class="btn btn-primary waves-effect">Guardar Cambios</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>