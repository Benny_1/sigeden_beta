<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <section class="content">
        
        <div class="container-fluid">
            
            <div class="block-header">
                <h2>PADECIMIENTOS DENTALES</h2>
            </div>
            
            <div class="row clearfix">
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="card">
                        <div class="header">
                            <h2>
                                Padecimientos Dentales
                            </h2>
                        </div>
                        
                        <div class="body">
                            <div class="row">
                                <div class="col-md-3">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#formPadecimiento">
                                        <i class="material-icons">add_box</i> Agregar Padecimiento 
                                    </button>
                                </div>

                                <div class="col-md-12">
                                    
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">filter_list</i>
                                                    </span>
                                                    <div class="form-line">
                                                        <input id="name_filter" type="text" class="form-control" placeholder="Nombre Padecimiento">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <button type="button" id="btn_filto" class="btn btn-primary btn-lg m-l-15 waves-effect">
                                                    <i class="material-icons">search</i>
                                                </button>
                                            </div>
                                        </div>
                                    
                                    <div class="table-responsive">
                                        <table id="tabla_padecimiento" class="table tabla-paginada">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>NOMBRE DEL PADECIMIENTO</th>
                                                    <th>ACCION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                    if(!is_null($resultado['lista'])) :
                                                        foreach ($resultado['lista'] as $padec) { ?>
                                                            <tr id="tr-<?php echo  $padec->ID_PAD_PK?>">
                                                                <td><?php echo $padec->ID_PAD_PK;?></td>
                                                                <td><?php echo $padec->DESC_PAD;?></td>
                                                                <td>
                                                                    <button type="button" class="btn btn-danger waves-effect delete-padec" data-id="<?php echo $padec->ID_PAD_PK?>" data-toggle="tooltip" data-placement="top" title="Eliminar Padecimiento">
                                                                        <i class="material-icons">delete_sweep</i> 
                                                                    </button>
                                                                    <button type="button" class="btn btn-warning waves-effect edit-padec" data-id="<?php echo $padec->ID_PAD_PK?>" data-toggle="tooltip" data-placement="top" title="Editar Padecimiento"> 
                                                                         <i class="material-icons">create</i> 
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                <?php
                                                        }
                                                    endif;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="modal fade" id="formPadecimiento" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Agregar Padecimiento</h4>
                        </div>
                        <div class="modal-body">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">

                                    <div class="col-md-10">
                                        <b>Nombre Padecimiento</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">add_box</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="nombrePadecimiento" name="nombrePadecimiento" class="form-control" placeholder="Nombre Padecimiento" focused>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_save_padecimiento" type="button" class="btn btn-primary waves-effect">Guardar Cambios</button>
                            <button id="dont_save_padecimiento" type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>


            
            <div class="modal fade" id="editPadecimiento" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Actualizar Padecimiento</h4>
                        </div>
                        <div class="modal-body">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">

                                    <div class="col-md-10">
                                        <b>Nombre Padecimiento</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="idEditPadecimiento" name="idEditPadecimiento" style="display:none">
                                                <input type="text" id="nameEditPadecimiento" name="nameEditPadecimiento" class="form-control" placeholder="Nombre Padecimiento" focused>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_edit_padecimiento" type="button" class="btn btn-primary waves-effect">Guardar Cambios</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>