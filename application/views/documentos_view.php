<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>TERMINOS Y CONDICIONES</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Editar terminos y condiciones 
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <textarea id="tinymce">
                                    <?php
                                        if(!is_null($texto)):
                                            echo $texto->TXT_DOC;
                                        endif;
                                    ?>

                                </textarea>
                                <br>
                                <div class="col-md-3">
                                    <button id="btn_save_doc" class="btn btn-success waves-effect">
                                        <i class="material-icons">save</i>
                                        Guadar Formato
                                    </button>
                                </div>
                                <div class="col-md-3">
                                    <a id="btn_pre_view" class="btn btn-success waves-effect" href="<?=base_url()?>index.php/Documentos/previewDoc">
                                        <i class="material-icons">pageview</i>
                                        PreView Documento
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </section>