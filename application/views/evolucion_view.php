<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <section class="content">
        
        <div class="container-fluid">
            
            <div class="block-header">
                <h2>Evolucion del Paciente</h2>
            </div>
            
            <div class="row clearfix"> 
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="card">
                        <div class="header">
                            <h2 class="text-center">
                                Nota de evolución
                            </h2>
                        </div>
                        
                        <div class="body">
                            <div id="cita_paginacion" class="row">

                                    <?php
                                        $arregloCitas = "";
                                        foreach ($infoPaciente as $cita) {
                                            $arregloCitas = $arregloCitas.$cita->ID_CITA_PK.",";
                                            if($cita->HOY == "YES"){
                                        ?>
                                        <div class="col-md-12">
                                            <input type="text" id="id_pac" name="id_pac" style="display: none;" value="<?php echo $cita->ID_PAC_PK;?>">
                                            <input type="text" id="id_cita" name="id_cita" style="display: none;" value="<?php echo $cita->ID_CITA_PK;?>">
                                            <input type="text" id="num_citas" name="num_citas" style="display: none;" value="<?php echo $arregloCitas;?>">
                                            <div class="col-md-4">
                                                <b>Nombre</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">person</i>
                                                    </span>
                                                    <div class="form-line disabled">
                                                        <input type="text" id="nombre_pac" name="nombre_pac" class="form-control" value="<?php echo $cita->NOMBRE_PAC;?>" placeholder="Nombre" disabled>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <b>Apellido Paterno</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">person</i>
                                                    </span>
                                                    <div class="form-line disabled">
                                                        <input type="text" id="paterno_pac" name="paterno_pac" class="form-control" value="<?php echo $cita->APP_PAC;?>" placeholder="Apellido Paterno" disabled>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <b>Apellido Materno</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">person</i>
                                                    </span>
                                                    <div class="form-line disabled">
                                                        <input type="text" id="materno_pac" name="materno_pac" class="form-control" value="<?php echo $cita->APM_PAC;?>" placeholder="Apellido Materno" disabled>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    <?php 
                                        }
                                    }
                                    ?>

                                <div class="demo-masked-input">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <b>TA Tencion Arterial :</b> <b class="color-rojo">*</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">show_chart</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="ta_pac" name="ta_pac" class="form-control arteria" placeholder="Ex: 120/80 mmhg">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <b>FC Frecuencia Cardiaca : </b> <b class="color-rojo">*</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">favorite</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="fc_pac" name="fc_pac" class="form-control cardiaca" placeholder="Ex: 60 x min">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <b>FR Frecuencia Respiratoria : </b> <b class="color-rojo">*</b>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">broken_image</i>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" id="fr_pac" name="fr_pac" class="form-control respiracion" placeholder="Ex: 20 x min ">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <b>Procedimiento</b> <b class="color-rojo">*</b>
                                        <select id="opt_proce" class="form-control">
                                            <option value="0">-Seleccione un Procedimiento-</option>
                                            <?php
                                                if(!is_null($procedimientos)) :
                                                    foreach ($procedimientos as $prc) { ?>
                                                        <option value="<?php echo $prc->ID_PROC_PK?>"><?php echo $prc->DSC_PROC?></option>
                                            <?php 
                                                }
                                                endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <b>Padecimiento - Diente</b> <b class="color-rojo">*</b>
                                        <select id="opt_diente_aten" class="selectpicker" title="Seleccione un padecimiento" multiple>
                                            <option value="0">-Estado Diente-</option>
                                            <?php
                                                if(!is_null($padecDental)) {
                                                    foreach ($padecDental as $pd) { ?>
                                                        <option value="<?php echo $pd->ID_DIENTE_FK?>"><?php echo $pd->NUM_DIENTE." - ".$pd->DESC_PAD?></option>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-11">
                                        <h5 class="text-center">Observaciones <b class="color-rojo">*</b></h5> 
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea id="obs_cita" rows="4" class="form-control no-resize" placeholder="1000 Caracteres"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <button type="button" id="save_evolucion" class="btn btn-success waves-effect" data-toggle="tooltip" data-placement="top" title="Guardar">
                                        <i class="material-icons">portrait</i>
                                        <span>Guardar</span>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!--Modal Contabilidad-->
    <div class="modal fade" id="modalCobra" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Cobros</h4>
                        </div>
                        <div class="modal-body">
                            <input type="text" id="idPac" name="idPac" style="display: none;">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <b>Cita</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">event</i>
                                            </span>
                                            <div class="form-line">
                                                <!-- <input type="text" id="fechCita" name="fechCita" class="datepicker form-control" placeholder="Fecha Cita" focused>-->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_save_cobros" type="button" class="btn btn-primary waves-effect">Re agendar Cita</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>





    <!--Agendar CITA Paciente-->
    <div class="modal fade" id="modalReAgendaCita" tabindex="-1" role="dialog" >
        <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Agendar Siguiente Cita</h4>
                        </div>
                        <div class="modal-body">
                            <input type="text" id="idPac" name="idPac" style="display: none;">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <b>Cita</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">event</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="fechCita" name="fechCita" class="datepicker form-control" placeholder="Fecha Cita" focused>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <b>Hora</b> <b class="color-rojo">*</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">event</i>
                                            </span>
                                            <div class="form-line">
                                                <input id="horaCita" name="horaCita" type="text" class="timepicker form-control" placeholder="Please choose a time...">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_agenda_cita_evo" type="button" class="btn btn-primary waves-effect">Agendar Cita</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
