<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <section class="content">
        
        <div class="container-fluid">
            
            <div class="block-header">
                <h2>Enfermedades</h2>
            </div>
            
            <div class="row clearfix">
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="card">
                        <div class="header">
                            <h2>
                                Enfermedades
                            </h2>
                        </div>
                        
                        <div class="body">
                            <div class="row">
                                <div class="col-md-3">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#formEnfermedad">
                                        <i class="material-icons">add_box</i> Agregar Enfermedad 
                                    </button>
                                </div> 
                                <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">filter_list</i>
                                                    </span>
                                                    <div class="form-line">
                                                        <input id="name_filter" type="text" class="form-control" placeholder="Nombre Profesion">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <button type="button" id="btn_filto" class="btn btn-primary btn-lg m-l-15 waves-effect">
                                                    <i class="material-icons">search</i>
                                                </button>
                                            </div>
                                        </div>                 
                                    <div id="tabla-paginada"  class="table-responsive">
                                        <table id="table_enfer" class="table tabla-paginada">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>NOMBRE DE LA ENFERMEDAD</th>
                                                    <th>ALERTA</th>
                                                    <th>ACCION</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                    if(!is_null($resultado['lista'])) :
                                                        foreach ($resultado['lista'] as $enf) { ?>
                                                            <tr id="tr-<?php echo  $enf->ID_ENFER_PK?>">
                                                                <td><?php echo $enf->ID_ENFER_PK;?></td>
                                                                <td><?php echo $enf->NOMBRE_ENFER;?></td>
                                                                <!--<td><?php echo $enf->NOMBRE_ENFER;?></td>-->
                                                                <td>
                                                                    <?php 
                                                                        if($enf->ALERT_ENFER == '1'){
                                                                            echo "<input type='checkbox' id='checkbox_alert_".$enf->ID_ENFER_PK."' data-id='".$enf->ID_ENFER_PK."' class='chk-col-blue ck_alert' checked/>";
                                                                        }else{
                                                                            echo "<input type='checkbox' id='checkbox_alert_".$enf->ID_ENFER_PK."' data-id='".$enf->ID_ENFER_PK."' class='chk-col-blue ck_alert'/>";
                                                                        }
                                                                    ?>
                                                                    <label for="checkbox_alert_<?php echo $enf->ID_ENFER_PK?>"></label>
                                                                </td>
                                                                <td>
                                                                    <button type="button" class="btn btn-danger waves-effect delete-enf" data-id="<?php echo $enf->ID_ENFER_PK?>" data-toggle="tooltip" data-placement="top" title="Eliminar Enfermedad">
                                                                        <i class="material-icons">delete_sweep</i> 
                                                                    </button>
                                                                    <button type="button" class="btn btn-warning waves-effect edit-enf" data-id="<?php echo $enf->ID_ENFER_PK?>" data-toggle="tooltip" data-placement="top" title="Editar Enfermedad"> 
                                                                         <i class="material-icons">create</i> 
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                <?php
                                                        }
                                                    endif;
                                                ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="modal fade" id="formEnfermedad" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Agregar Enfermedad</h4>
                        </div>
                        <div class="modal-body">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">

                                    <div class="col-md-10">
                                        <b>Nombre Enfermedad</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">add_box</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="nombreEnfermedad" name="nombreEnfermedad" class="form-control" placeholder="Nombre Enfermedad" focused>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_save_enfermedad" type="button" class="btn btn-primary waves-effect">Guardar Cambios</button>
                            <button id="dont_save_enfermedad" type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>


            
            <div class="modal fade" id="editEnfermedad" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Actualizar Enfermedad</h4>
                        </div>
                        <div class="modal-body">
                        <!-- Formulario modal-->
                            <div class="row clearfix">
                                <div class="col-md-12">

                                    <div class="col-md-10">
                                        <b>Nombre Enfermedad</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">person</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="idEditEnfermedad" name="idEditEnfermedad" style="display:none">
                                                <input type="text" id="nameEditEnfermedad" name="nameEditEnfermedad" class="form-control" placeholder="Nombre Enfermedad" focused>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Formulario modal-->
                        </div>
                        <div class="modal-footer">
                            <button id="btn_edit_enfermedad" type="button" class="btn btn-primary waves-effect">Guardar Cambios</button>
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>