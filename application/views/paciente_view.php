<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            <!-- Advanced Form Example With Validation -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                HISTORIA CLINICA
                            </h2>
                            <ul class="header-dropdown m-r-0">
                                <li>
                                    <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Da click si tienes duda de como llenar el formulario">
                                        <i class="material-icons">help</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul id="list_menu" class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a id="op_1" href="#home_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">face</i>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a id="op_2" href="#profile_with_icon_title" data-toggle="tab" onClick="return false" >
                                        <i class="material-icons">people</i>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a id="op_3" href="#messages_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">person</i>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a id="op_4" href="#settings_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">content_paste</i>
                                    </a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a id="op_5" href="#explorar_with_icon_title" data-toggle="tab">
                                        <i class="material-icons">speaker_notes</i>
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                                    <h4>Datos personales</h4>
                                    <small>Los campos con <b class="color-rojo">*</b> son obligatorios</small>
                                    <br>
                                    <br>
                                    <input id="idPer" name="idPer" style="display: none;" />
                                    <!-- EMPIEZA EL TABPANEL 1-->
                                    <div class="demo-masked-input">
                                        <div class="row clearfix">
                                           
                                                <div class="col-md-12">
                                                    
                                                    <div class="col-md-3">
                                                        <b>Nombre</b> <b class="color-rojo">*</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">person</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="nombre_p" name="nombre_p" class="form-control" placeholder="Username" focused />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Apellido Paterno</b> <b class="color-rojo">*</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">person</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="paterno_p" name="paterno_p" class="form-control" placeholder="Apellido Paterno">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Apellido Materno</b> <b class="color-rojo">*</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">person</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="materno_p" name="materno_p" class="form-control" placeholder="Apellido Materno">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Fecha de Nancimiento</b> <b class="color-rojo">*</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">date_range</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="fecha_nac" name="fecha_nac" class="form-control date" placeholder="Ex: 30-07-1990">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    
                                                    <div class="col-md-3">
                                                        <b>Genero</b> <b class="color-rojo">*</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">wc</i>
                                                            </span>
                                                            <input name="grupo_genero" type="radio" id="hombre" class="with-gap radio-col-blue"/>
                                                            <label for="hombre">Hombre</label>
                                                            <input name="grupo_genero" type="radio" id="mujer" class="with-gap radio-col-pink" />
                                                            <label for="mujer">Mujer</label>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-3">
                                                        <b>Lugar de Nacimiento</b> <b class="color-rojo">*</b>
                                                        
                                                                <select id="lugar_nac" class="form-control show-tick">
                                                                    <option value="0">-Estado-</option>
                                                                    <?php
                                                                        if(!is_null($estados)) :
                                                                            foreach ($estados as $est) { ?>
                                                                            <option value="<?php echo $est->ID_EST_PK?>"><?php echo $est->NOMBRE_EST?></option>
                                                                        <?php 
                                                                            }
                                                                        endif;
                                                                    ?>
                                                                </select>
                                                            
                                                        
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Dirección </b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">domain</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="direccion" name="direccion" class="form-control" placeholder="Dirección">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Ocupación</b> <b class="color-rojo">*</b>
                                                        <select id="ocupacion" name="ocupacion" class="form-control show-tick">
                                                            <option value="0">-Profesión-</option>
                                                            <?php
                                                                if(!is_null($profesiones)) :
                                                                    foreach ($profesiones as $prof) { ?>
                                                                    <option value="<?php echo $prof->ID_PROF_PK?>"><?php echo $prof->NOM_PROF?></option>
                                                                <?php 
                                                                    }
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="col-md-3">
                                                        <b>Estado Civil</b> <b class="color-rojo">*</b>
                                                        <select id="est_civil" name="est_civil" class="form-control show-tick">
                                                            <option value="0">-Estado Civil-</option>
                                                            <option value="1">Soltero</option>
                                                            <option value="2">Viudo</option>
                                                            <option value="3">Casado</option>
                                                            <option value="4">Divorsiado</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Numero de Telefono</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">phone_iphone</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="num_tel" nombre="num_tel" class="form-control mobile-phone-number" placeholder="Ex: (000) 000-00-00">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <b>Correo Electronico</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">email</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="correo" name="correo" class="form-control email" placeholder="Ex: example@example.com">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <b>Observacion</b>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <i class="material-icons">location_searching</i>
                                                            </span>
                                                            <div class="form-line">
                                                                <input type="text" id="obs_pac" name="obs_pac" class="form-control" maxlength="100" placeholder="Ex: Hijo de doña Marta">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <button type="button" id="next_ant_fam" class="btn btn-success waves-effect" data-toggle="tooltip" data-placement="top" title="Guardar La informacion del paciente">
                                                        <i class="material-icons">send</i>
                                                        <span>Guardar y Continuar</span>
                                                    </button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" id="cancelarRegistro" class="btn btn-warning waves-effect" data-toggle="tooltip" data-placement="top" title="Limpia todos los campos del formulario">
                                                        <i class="material-icons">delete</i>
                                                        <span>Limipiar campos</span>
                                                    </button>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /TERMINA EL TABPANEL 1-->
                                <div role="tabpanel" class="tab-pane fade in" id="profile_with_icon_title">
                                    <div class = "row clearfix">
                                        
                                        <h4>Antecedentes patologicos familiares</h4>
                                        <div class="col-md-12">
                                            <select id="select_familiar" class="form-control">
                                                <option value="0">-Familiar-</option>
                                                    <?php
                                                        if(!is_null($familiares)) :
                                                            foreach ($familiares as $fm) { ?>
                                                                <option value="<?php echo $fm->ID_PAR_PK?>"><?php echo $fm->NOMBRE_PAR?></option>
                                                        <?php 
                                                            }
                                                        endif;
                                                    ?>
                                            </select>
                                        </div>
                                        <div class="colmd-12">
                                            <select id="optgroup_familiar" class="ms" multiple="multiple">
                                                    <?php
                                                        if(!is_null($enfermedades)) :
                                                            foreach ($enfermedades as $en) { ?>
                                                                <option value="<?php echo $en->ID_ENFER_PK?>"><?php echo $en->NOMBRE_ENFER?></option>
                                                        <?php 
                                                            }
                                                        endif;
                                                    ?>
                                            </select>
                                        </div>
                                        <br/>
                                        <div class="col-md-12">

                                            <button type="button" name="btn_saveAnt_familiar" id="btn_saveAnt_familiar"  class="btn btn-success waves-effect">
                                                <i class="material-icons">save</i>
                                                <span>Guardar Antecedente</span>
                                            </button>
                                            <button type="button" name="btn_noAnt_familiares" id="btn_noAnt_familiares" class="btn btn-warning waves-effect">
                                                <i class="material-icons">report</i>
                                                <span>Sin Antecedentes </span>
                                            </button>
                                            <button type="button" name="btn_finish_ant_fam" id="btn_finish_ant_fam" class="btn btn-info waves-effect">
                                                <i class="material-icons">arrow_forward</i>
                                                <span>Continuar</span>
                                            </button>
                                        </div>
                                    </div>
                                    <br/>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages_with_icon_title">
                                    <h4>Antecedentes patologicos personales</h4>
                                    <div class="row clearfix">
                                     <div class="col-md-12">
                                        <select id="optgroup_personal" class="ms" multiple="multiple">
                                                <?php
                                                    if(!is_null($enfermedades)) :
                                                        foreach ($enfermedades as $en) { ?>
                                                            <option value="<?php echo $en->ID_ENFER_PK?>"><?php echo $en->NOMBRE_ENFER?></option>
                                                    <?php 
                                                        }
                                                    endif;
                                                ?>
                                        </select>
                                    </div>
                                    <div id="inputObs" class="col-md-5">
                                        <b>Observaciones</b>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">textsms</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="obs_enf" name="obs_enf" class="form-control" maxlength="100" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                   </div>
                                    <div class="colmd-12">
                                        <button type="button" name="btn_guardarAnte_personales" id="btn_guardarAnte_personales"  class="btn btn-success waves-effect">
                                            <i class="material-icons">save</i>
                                            <span>Guardar Antecedente</span>
                                        </button>
                                            <button type="button" name="btn_noAnt_personales" id="btn_noAnt_personales" class="btn btn-warning waves-effect">
                                            <i class="material-icons">report</i>
                                            <span>Sin Antecedentes </span>
                                        </button>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="settings_with_icon_title">
                                    <h3 class="text-center">Antecedentes no patologicos</h3>
                                    <h4>Habitos</h4>
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            
                                            <div class="col-md-3">
                                                <b>¿Numero de comidas al día?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">restaurant</i>
                                                    </span>
                                                        <input name="grupo_comidas" type="radio" id="1_c" class="with-gap radio-col-red"/>
                                                        <label for="1_c">2</label>
                                                        <input name="grupo_comidas" type="radio" id="2_c" class="with-gap radio-col-orange" />
                                                        <label for="2_c">3</label>
                                                        <input name="grupo_comidas" type="radio" id="3_c" class="with-gap radio-col-green" />
                                                        <label for="3_c">4</label>
                                                        <input name="grupo_comidas" type="radio" id="4_c" class="with-gap radio-col-blue" />
                                                        <label for="4_c">5</label>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <b>¿Higiene dental?</b>

                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">brush</i>
                                                    </span>
                                                    
                                                        <input name="grupo_higiene" type="radio" id="1_h" class="with-gap radio-col-blue"/>
                                                        <label for="1_h">Buena</label>
                                                        <input name="grupo_higiene" type="radio" id="2_h" class="with-gap radio-col-orange" />
                                                        <label for="2_h">Regular</label>
                                                        <input name="grupo_higiene" type="radio" id="3_h" class="with-gap radio-col-red" />
                                                        <label for="3_h">Mala</label>
                                                </div>

                                            </div>

                                            <div class="col-md-2">
                                                <b>¿Hilo dental?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">shopping_basket</i>
                                                    </span>
                                                    <input type="checkbox" id="checkbox_hilo" class="chk-col-blue"/>
                                                    <label for="checkbox_hilo"></label>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <b>¿Enjuague bocal?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">local_drink</i>
                                                    </span>
                                                    <input type="checkbox" id="checkbox_enjuage" class="chk-col-blue"/>
                                                    <label for="checkbox_enjuage"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <b>¿Ingesta de alimentos duros?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">fastfood</i>
                                                    </span>
                                                    <input type="checkbox" id="checkbox_ali_duros" class="chk-col-red"/>
                                                    <label for="checkbox_ali_duros"></label>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <b>¿Cuantas veces al dia se cepilla?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">mood</i>
                                                    </span>
                                                    
                                                        <input name="grupo_cepillada" type="radio" id="1_cpd" class="with-gap radio-col-red"/>
                                                        <label for="1_cpd">1</label>
                                                        <input name="grupo_cepillada" type="radio" id="2_cpd" class="with-gap radio-col-orange" />
                                                        <label for="2_cpd">2</label>
                                                        <input name="grupo_cepillada" type="radio" id="3_cpd" class="with-gap radio-col-green" />
                                                        <label for="3_cpd">3</label>
                                                        <input name="grupo_cepillada" type="radio" id="4_cpd" class="with-gap radio-col-blue" />
                                                        <label for="4_cpd">4</label>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <b>¿Ingesta de alimentos o bebidas a temperaturas elevadas?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">mood</i>
                                                    </span>
                                                        <input name="grupo_temp_ele" type="radio" id="1_elevada" class="with-gap radio-col-red"/>
                                                        <label for="1_elevada">Muy calientes</label>
                                                        <input name="grupo_temp_ele" type="radio" id="2_elevada" class="with-gap radio-col-blue" />
                                                        <label for="2_elevada">Muy frias</label>
                                                        <input name="grupo_temp_ele" type="radio" id="3_elevada" class="with-gap radio-col-orange" />
                                                        <label for="3_elevada">Ambas</label>
                                                </div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    <h4>Toxicomanias</h4>
                                    <div class="row clearfix">
                                        <div class="col-md-12">    
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">smoking_rooms</i>
                                                </span>
                                                <input type="checkbox" id="checkbox_adiccion" class="chk-col-red"/>
                                                <label for="checkbox_adiccion"></label>
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="optionToxi" style="display: none;">
                                            <select id="optgroup_adiccion" class="ms" multiple="multiple">
                                            <?php
                                                if(!is_null($adicciones)) :
                                                    foreach ($adicciones as $adic) { ?>
                                                        <option value="<?php echo $adic->ID_ADIC_PK?>"><?php echo $adic->NOM_ADIC?></option>
                                                <?php 
                                                    }
                                                endif;
                                            ?>
                                            </select>
                                        </div>
                                    </div>
                                    <h4>Otras observaciones</h4>
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <b>¿Inmunizaciones?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">bubble_chart</i>
                                                    </span>
                                                    <input type="checkbox" id="checkbox_inmunizacion" class="chk-col-red"/>
                                                    <label for="checkbox_inmunizacion"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <b>¿Metodo Anticonceptivo?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">child_care</i>
                                                    </span>
                                                    <input type="checkbox" id="checkbox_metod_anti" class="chk-col-black"/>
                                                    <label for="checkbox_metod_anti"></label>
                                                </div>
                                            </div>
                                            <!-- En caso de que sea mujer mostrar esta opcion-->
                                            <div class="col-md-4" id="isWoman">
                                                <b>¿Embarazo trimestral?</b>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">pregnant_woman</i>
                                                    </span>
                                                    <input name="grupo_higiene" type="radio" id="1_t" class="with-gap radio-col-blue"/>
                                                    <label for="1_t">Trimestre 1</label>
                                                    <input name="grupo_higiene" type="radio" id="2_t" class="with-gap radio-col-orange" />
                                                    <label for="2_t">Trimestre 2</label>
                                                    <input name="grupo_higiene" type="radio" id="3_t" class="with-gap radio-col-red" />
                                                    <label for="3_t">Trimestre 3</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="button" name="btn_save_habitos" id="btn_save_habitos" class="btn btn-success waves-effect">
                                                <i class="material-icons">send</i>
                                                <span>Guardar</span>
                                            </button>
                                            <button type="button" name="btn_limpia_hab_form" id="btn_limpia_hab_form" class="btn btn-warning waves-effect">
                                                <i class="material-icons">delete</i>
                                                <span>Limpiar campos</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane fade" id="explorar_with_icon_title">
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <select id="numDiente" class="form-control" multiple data-selected-text-format="count" data-actions-box="true" title="Seleccione un diente">
                                                <option value="0">-Seleccione un diente-</option>
                                                <?php
                                                    if(!is_null($dientes)) :
                                                        foreach ($dientes as $dent) { ?>
                                                            <option value="<?php echo $dent->ID_DIENTE_PK?>"><?php echo $dent->NUM_DIENTE?></option>
                                                    <?php 
                                                        }
                                                    endif;
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <!-- data-live-search="true" -->
                                            <select id="optgroup_padecimiento" class="selectpicker" multiple data-actions-box="true" data-width="100%" data-size="4" data-selected-text-format="count" title="Seleccione un padecimiento">
                                                <?php
                                                    if(!is_null($padecimientos)) :
                                                        foreach ($padecimientos as $pad) { ?>
                                                            <option value="<?php echo $pad->ID_PAD_PK?>"><?php echo $pad->DESC_PAD?></option>
                                                    <?php 
                                                        }
                                                    endif;
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <button type="button" name="btnNoAnt" id="btn_estat_diente" class="btn btn-success waves-effect">
                                                <i class="material-icons">send</i>
                                                <span>Guardar</span>
                                            </button>
                                            <button type="button" name="btn_finish_his" id="btn_finish_his" class="btn btn-primary waves-effect">
                                                <i class="material-icons">how_to_reg</i>
                                                <span>Terminar Historial</span>
                                            </button>
                                            <button type="button" name="btn_next_evaluacion" id="btn_next_evaluacion" class="btn bg-indigo waves-effect">
                                                <i class="material-icons">last_page</i>
                                                <span>Continuar a atender</span>
                                            </button>
                                            <button type="button" name="btn_gen_pdf" id="btn_gen_pdf" class="btn btn-warning waves-effect">
                                                <i class="material-icons">picture_as_pdf</i>
                                                <span>Generar PDF</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Advanced Form Example With Validation -->
        </div>
    </section>